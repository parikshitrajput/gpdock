//
//  ZSeatSelector.swift
//  ZSeatSelector_Swift
//
//  Created by Ricardo Zertuche on 8/24/15.
//  Copyright © 2015 Ricardo Zertuche. All rights reserved.
//

import UIKit

protocol ZSeatSelectorDelegate {
    func seatSelected(_ seat: ZSeat)
    func getSelectedSeats(_ seats: NSMutableArray)
}

class ZSeatSelector: UIScrollView, UIScrollViewDelegate {

    var seatSelectorDelegate: ZSeatSelectorDelegate?
    var seat_width:     CGFloat = 40.0
    var seat_height:    CGFloat = 40.0
    var selected_seats          = NSMutableArray()
    var seat_price:     Float   = 10.0

    var left_available_image     = UIImage()
    var left_unavailable_image   = UIImage()
    var left_disabled_image      = UIImage()
    var left_selected_image      = UIImage()

    var right_available_image     = UIImage()
    var right_unavailable_image   = UIImage()
    var right_disabled_image      = UIImage()
    var right_selected_image      = UIImage()


    let zoomable_view       = UIView()
    let verticalPadding:CGFloat = 10
    let horizontalPadding:CGFloat = 1

    var selected_seat_limit:Int = 0
    var layout_type                = ""

    // MARK: - Init and Configuration

    func setSeatSize(_ size: CGSize){
        seat_width  = size.width
        seat_height = size.height
    }

    func floorPlan(map: FloorPlan) -> Void {
        var initial_seat_x: CGFloat = 15
        //var initial_seat_y: CGFloat = 10

        var initial_seat_y: CGFloat = 65

        var maxHeight: CGFloat = 0.0

        for parkingRow in map.rows {

            var separatorMaxY:CGFloat = seat_height + CGFloat(verticalPadding)
            let dockNameOriginX:CGFloat = initial_seat_x
            //var dockNameOriginY:CGFloat = 10

            var separatorOriginX:CGFloat = 15
            //var separatorOriginY:CGFloat = 10
           // var separatorOriginY:CGFloat = 40

            for boat in parkingRow.leftFacingBoats{
                drawBoatWithPosition(initial_seat_x, initialBoat_y: initial_seat_y, boat: boat)
                initial_seat_y = initial_seat_y + seat_height + verticalPadding
                if initial_seat_y > separatorMaxY{
                    separatorMaxY = initial_seat_y
                }
                if initial_seat_y > maxHeight{
                    maxHeight = initial_seat_y
                }

            }


           // initial_seat_y = 10
            initial_seat_y = 65

            separatorOriginX = initial_seat_x + seat_width + horizontalPadding
            //separatorOriginY = initial_seat_y

            initial_seat_x = initial_seat_x + seat_width + (2*horizontalPadding+5)

            for boat in parkingRow.rightFacingBoats{
                drawBoatWithPosition(initial_seat_x, initialBoat_y: initial_seat_y, boat: boat)
                initial_seat_y = initial_seat_y + seat_height + verticalPadding

                if initial_seat_y > separatorMaxY{
                    separatorMaxY = initial_seat_y
                }
                if initial_seat_y > maxHeight{
                    maxHeight = initial_seat_y
                }
            }
            //dockNameOriginY = separatorMaxY

            let separatorView = UIView(frame: CGRect(x: separatorOriginX, y: 40/*separatorOriginY*/, width: 5.0, height: separatorMaxY-40))
            separatorView.backgroundColor = UIColor.clear
            self.zoomable_view.addSubview(separatorView)



            let dockNameWidth  = (2*seat_width) + (2*horizontalPadding) + 5.0
            //let dockName = UILabel(frame: CGRect(x: dockNameOriginX, y: dockNameOriginY, width: dockNameWidth, height: 30))
            let dockName = UILabel(frame: CGRect(x: dockNameOriginX, y: 10, width: dockNameWidth, height: 45))
            dockName.font = fonts.Raleway.semiBold.font(.xLarge)
            dockName.text = parkingRow.title
            dockName.textColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            dockName.backgroundColor = UIColor.groupTableViewBackground//kApplicationGreenColor//UIColor(red: 34.0/255.0, green: 155.0/255.0, blue: 74.0/255.0, alpha: 1.0)
            dockName.textAlignment = .center
            CommonClass.makeViewCircularWithCornerRadius(dockName, borderColor: .clear, borderWidth: 0, cornerRadius: 3)
            //dockName.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 4)
            self.zoomable_view.addSubview(dockName)


            initial_seat_x = initial_seat_x + (2*seat_width)
            //initial_seat_y = 10

            initial_seat_y = 65
        }

        zoomable_view.frame = CGRect(x: 0, y: 0, width: initial_seat_x, height: maxHeight+45)
        self.contentSize = zoomable_view.frame.size
        let newContentOffsetX: CGFloat = (self.contentSize.width - self.frame.size.width) / 2
        self.contentOffset = CGPoint(x: newContentOffsetX, y: 0)
        selected_seats = NSMutableArray()
        self.setZoomScale()
        self.delegate = self
        self.addSubview(zoomable_view)
        self.showsVerticalScrollIndicator = true
        self.showsHorizontalScrollIndicator = true
        self.contentOffset = CGPoint(x: 0, y: 0)
        self.isScrollEnabled = true
    }


    func setZoomScale() {
        let minZoom = min(self.bounds.size.width / zoomable_view.bounds.size.width, self.bounds.size.height / zoomable_view.bounds.size.height);
        let currentZoom = max(self.bounds.size.width / zoomable_view.bounds.size.width, self.bounds.size.height / zoomable_view.bounds.size.height);
        self.minimumZoomScale = minZoom;
        self.zoomScale = currentZoom
    }






    func drawBoatWithPosition(_ initialBoat_x: CGFloat, initialBoat_y: CGFloat, boat: ParkingSpace) -> Void {
        let boatButton = ZSeat(frame: CGRect(
            x: initialBoat_x,
            y: initialBoat_y,
            width: CGFloat(seat_width),
            height: CGFloat(seat_height)))
        boatButton.boat = boat

        if boat.isBooked {
            self.setSeatAsDisabled(boatButton)
        }else {
            if boat.isAvailable{
                self.setSeatAsAvaiable(boatButton)
            }else {
                self.setSeatAsUnavaiable(boatButton)
            }
        }

        boatButton.available = boat.isAvailable
        boatButton.disabled = (boat.isBooked || !boat.isAvailable)
        boatButton.backgroundColor = UIColor.lightText
        boatButton.addTarget(self, action: #selector(ZSeatSelector.seatSelected(_:)), for: .touchDown)
        CommonClass.makeViewCircularWithCornerRadius(boatButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 3)
        zoomable_view.addSubview(boatButton)
    }

    // MARK: - Seat Selector Methods

    @objc func seatSelected(_ sender: ZSeat) {
        if !sender.selected_seat && sender.available {
            if selected_seat_limit != 0 {
                checkSeatLimitWithSeat(sender)
            }
            else {
                self.setSeatAsSelected(sender)
                selected_seats.add(sender)
            }
        }
        else {
            selected_seats.remove(sender)
            if !sender.available && sender.disabled {
                self.setSeatAsDisabled(sender)
            }else {
                if sender.available && !sender.disabled {
                    self.setSeatAsAvaiable(sender)
                }
            }
        }

        seatSelectorDelegate?.seatSelected(sender)
        seatSelectorDelegate?.getSelectedSeats(selected_seats)
    }
    func checkSeatLimitWithSeat(_ sender: ZSeat) {
        if selected_seats.count < selected_seat_limit {
            setSeatAsSelected(sender)
            selected_seats.add(sender)
        }
        else {
            let seat_to_make_avaiable: ZSeat = selected_seats[0] as! ZSeat
            if seat_to_make_avaiable.disabled {
                self.setSeatAsDisabled(seat_to_make_avaiable)
            }
            else {
                self.setSeatAsAvaiable(seat_to_make_avaiable)
            }
            selected_seats.removeObject(at: 0)
            self.setSeatAsSelected(sender)
            selected_seats.add(sender)
        }
    }

    // MARK: - Seat Images & Availability

    func setAvailableImage() {
        self.left_available_image = UIImage(named: "lA")!
        self.left_unavailable_image = UIImage(named: "lU")!
        self.left_disabled_image = UIImage(named: "lU")!
        self.left_selected_image = UIImage(named: "lS")!
        self.right_available_image = UIImage(named: "rA")!
        self.right_unavailable_image = UIImage(named: "rU")!
        self.right_disabled_image = UIImage(named: "rU")!
        self.right_selected_image = UIImage(named: "rS")!
    }



    func setSeatAsUnavaiable(_ sender: ZSeat) {
        sender.setImage((sender.boat.facing == .left) ? left_unavailable_image : right_unavailable_image, for: UIControlState())
        sender.selected_seat = false
    }

    func setSeatAsAvaiable(_ sender: ZSeat) {
         sender.setImage((sender.boat.facing == .left) ? left_available_image : right_available_image, for: UIControlState())
        sender.selected_seat = false
    }

    func setSeatAsDisabled(_ sender: ZSeat) {
        sender.setImage((sender.boat.facing == .left) ? left_disabled_image : right_disabled_image, for: UIControlState())
        sender.selected_seat = false
    }

    func setSeatAsSelected(_ sender: ZSeat) {
        sender.setImage((sender.boat.facing == .left) ? left_selected_image : right_selected_image, for: UIControlState())
        sender.selected_seat = true
    }

    // MARK: - UIScrollViewDelegate

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //print("zoom")
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.subviews[0]
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        //print(scale)
    }

}


class ZSeat: UIButton {
    var available:      Bool    = true;
    var disabled:       Bool    = true;
    var selected_seat:    Bool    = true;
    
    var boat : ParkingSpace = ParkingSpace(){
        didSet{
            available = boat.isAvailable
            disabled = (!boat.isAvailable || boat.isBooked)
        }
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
}
