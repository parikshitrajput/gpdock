//
//  CommonClass.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SystemConfiguration


let loaderSize = CGSize(width: 120, height: 120)
let imageViewSize = CGSize(width: 70, height: 70)

let screenWidth = UIScreen.main.bounds.width
let imgV = UIImageView(frame: CGRect(x:0, y:-22,width:screenWidth, height:68))

enum ImageAction: String {
    case Update = "update"
    case Remove = "remove"
    case None = ""

}

func generateBoundaryString() -> String {
    return "Boundary-\(UUID().uuidString)"

}

class CommonClass: NSObject {
    
    static let sharedInstance = CommonClass()
    override init() {
        super.init()
    }


    func prepareHeader(withAuth:Bool) -> Dictionary<String,String>{
        let accept = "application/json"
        var header = Dictionary<String,String>()
        if withAuth{
            let userJSON = User.loadUserInfo()
            let user = User(json: userJSON!)
            let userToken = user.userToken
            if !userToken.isEmpty{
                header.updateValue(userToken, forKey: "accessToken")
            }
        }
        header.updateValue(accept, forKey: "Accept")
        return header
    }
    
    func hadleAccessTokenError(_ showLoginModule:Bool){
        kUserDefaults.set(false, forKey: kIsLoggedIN)
        let user = User()
        user.saveUserInfo(user)
        kUserDefaults.synchronize()
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)

            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: false)
        }

        NotificationCenter.default.post(name: .SESSION_EXPIRED_NOTIFICATION, object: nil, userInfo: ["user":user])
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let presentingVC = appDelegate.window!.rootViewController!
        self.showAlertForSesseion(vc: presentingVC, showLoginModule: showLoginModule)
    }

    func showAlertForSesseion(vc:UIViewController,showLoginModule:Bool){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Your session has expired, Please login/register for further process.", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) {[vc] (action) in
            if showLoginModule{
                GPDock.openLoginModule(in: vc, shouldLogin: true)
            }
        }

        alert.addAction(okayAction)
        vc.present(alert, animated: true, completion: nil)
    }

    func reRegisterForFirebase(){
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.unregisterFirebaseToken { (done) in
            if done{
                appDelegate.registerFirebaseToken()
            }
        }
    }

    func setLeftIconForTextField(_ textField:UITextField,leftIcon: UIImage){
        let leftImageView = UIButton()
        leftImageView.setImage(leftIcon, for: .normal)
        leftImageView.isUserInteractionEnabled = false
        let imgFrame = textField.frame
        leftImageView.frame = CGRect(x: 5, y: 5, width: imgFrame.size.height-10, height:imgFrame.size.height-10)
        textField.leftView = leftImageView
        textField.leftViewMode = UITextFieldViewMode.always
    }

    class var gradientTextColors : Array<UIColor>{
        var colors = Array<UIColor>()
        for counter in gradientTextColor.startRed..<gradientTextColor.endRed{
            let color = UIColor(red: CGFloat(gradientTextColor.startRed+counter)/255.0, green: CGFloat(gradientTextColor.startGreen-(counter*2))/255.0, blue: CGFloat(gradientTextColor.startBlue-(counter*2))/255.0, alpha: 1.0)
            colors.append(color)
        }
        return colors
    }

    
    class var gradientViewColors : Array<UIColor>{
        var colors = Array<UIColor>()
        for counter in gradientViewColor.startRed..<gradientViewColor.endRed{
            let color = UIColor(red: CGFloat(gradientViewColor.startRed+counter)/255.0, green: CGFloat(gradientViewColor.startGreen-(counter*2))/255.0, blue: CGFloat(gradientViewColor.startBlue-(counter*2))/255.0, alpha: 1.0)
            colors.append(color)
        }
        return colors
    }


    class func validateMaxValue(_ textField: UITextField, maxValue: Int, range: NSRange, replacementString string: String) -> Bool{

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        //if delete all characteres from textfield
        if(newString.isEmpty) {
            return true
        }
        //check if the string is a valid number
        let numberValue = Int(newString)
        if(numberValue == nil) {
            return false
        }
        return numberValue! <= maxValue
    }

    func userAvatarImage(username:String) -> UIImage{
        let configuration = LetterAvatarBuilderConfiguration()

        configuration.username = (username.trimmingCharacters(in: .whitespaces).count == 0) ? "NA" : username
        configuration.lettersColor = UIColor.white
        configuration.backgroundColors = [kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor]
        return UIImage.makeLetterAvatar(withConfiguration: configuration) ?? UIImage(named:"user_image")!
    }

    func showSortingOptions(withDelegate delegate: SortingSelectionViewControllerDelegate){
        let vc = AppStoryboard.Home.viewController(SortingSelectionViewController.self)
        vc.delegate = delegate
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        nav.navigationBar.isHidden = true
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(nav, animated: true, completion: nil)
    }


    func showSortingOptions(in viewController: UIViewController,completionBlock:@escaping (_ sortingType : String, _ orderBy:String) -> Void){

        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        actionSheet.view.tintColor = kNavigationColor
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            completionBlock("", "")
        }
        actionSheet.addAction(cancelAction)
        let priceHighToLowAction: UIAlertAction = UIAlertAction(title: "Price - High to Low", style: .default)
        { action -> Void in
            completionBlock("Price","DESC")
        }
        actionSheet.addAction(priceHighToLowAction)
        let priceLowToHighAction: UIAlertAction = UIAlertAction(title: "Price - Low to High", style: .default)
        { action -> Void in
            completionBlock("Price","ASC")
        }
        actionSheet.addAction(priceLowToHighAction)

        let reviewsAction: UIAlertAction = UIAlertAction(title: "Reviews", style: .default)
        { action -> Void in
            completionBlock("Reviews","DESC")
        }
        actionSheet.addAction(reviewsAction)

        let distanceAction: UIAlertAction = UIAlertAction(title: "Nearest First", style: .default)
        { action -> Void in
            completionBlock("Distance","ASC")
        }
        actionSheet.addAction(distanceAction)

        viewController.present(actionSheet, animated: true, completion: nil)

    }

    func showGotoLocationSettingAlert(in viewController: UIViewController,completionBlock:@escaping(_ isEnabling:Bool)->Void){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Location access seems disabled\r\nGo to settings to enabled", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completionBlock(false)
            alert.dismiss(animated: true, completion: nil)
        }

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            completionBlock(true)
            let url = UIApplicationOpenSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }

        alert.addAction(okayAction)
        alert.addAction(settingsAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    let colors: [UIColor] = [
        UIColor.red,
        UIColor.green,
        UIColor.blue,
        UIColor.orange,
        UIColor.yellow
    ]


    class func presentControllerWithShadow(_ viewController:UIViewController, overViewController presentingViewController: UIViewController,completion: (() -> Void)?){
        imgV.backgroundColor = UIColor.black
        imgV.alpha = 0.3
        presentingViewController.modalPresentationStyle = .custom
        presentingViewController.navigationController?.navigationBar.addSubview(imgV)
        presentingViewController.present(viewController, animated: true, completion: completion)
    }
    class func dismissWithShadow(_ viewController:UIViewController,completion: (() -> Void)?){
        imgV.removeFromSuperview()
        viewController.dismiss(animated: true, completion: completion)
    }

    class func makeViewCircular(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.masksToBounds = true
    }
    class func makeViewCircularWithRespectToHeight(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
    }


    class func makeViewCircularWithCornerRadius(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat, cornerRadius: CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }


    class var isConnectedToNetwork: Bool {

    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)

    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
    $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
    SCNetworkReachabilityCreateWithAddress(nil, $0)
    }
    }) else {

    return false
    }

    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
    return false
    }

    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)

    return (isReachable && !needsConnection)
    }

    class var isLoggedIn: Bool{
        var result = false
        if let r = kUserDefaults.bool(forKey:kIsLoggedIN) as Bool?{
            result = r
        }
        return result//kUserDefaults.bool(forKey: kIsLoggedIN)
    }

    
   // class var normalUpdateShownDate
    class func setNormalUpdateKey(){
        kUserDefaults.set(Date().timeIntervalSinceReferenceDate, forKey: kNormalUpdateShownOnDate)
    }
    
    class var shouldShowNormalUpdate:Bool{
        if let date = kUserDefaults.double(forKey: kNormalUpdateShownOnDate) as Double?{
            return date+(7*24*60*60) < Date().timeIntervalSinceReferenceDate
        }
        return true
    }
    
     func setPlaceHolder(_ textField: UITextField, placeHolderString placeHolder: String, withColor color: UIColor){
        let p = NSAttributedString(string: placeHolder, attributes: [NSAttributedStringKey.foregroundColor : color])
        textField.attributedPlaceholder = p;
    }

    class var isRunningSimulator: Bool
        {
        get
        {
            return TARGET_OS_SIMULATOR != 0
        }
    }

    //MARK:- Loader display Methods
    class func showLoader()
    {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(kNavigationColor)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.setImageViewSize(imageViewSize)
        SVProgressHUD.show(#imageLiteral(resourceName: "loaderImage"), status: "")
    }


    class var isLoaderOnScreen: Bool
    {
        return SVProgressHUD.isVisible()
    }
    class func showError(withStatus status: String)
    {
        SVProgressHUD.showError(withStatus: status)
    }
    class func showSuccess(withStatus status: String)
    {
        SVProgressHUD.showSuccess(withStatus: status)
    }


    class func showLoader(withStatus status: String)
    {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundLayerColor(UIColor.white.withAlphaComponent(0.9))
        SVProgressHUD.setBackgroundColor(UIColor.white.withAlphaComponent(0.9))
        SVProgressHUD.setForegroundColor(kNavigationColor)
        SVProgressHUD.setImageViewSize(imageViewSize)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.show(#imageLiteral(resourceName: "loaderImage"), status: status)
    }
    class func updateLoader(withStatus status: String)
    {
        if SVProgressHUD.isVisible(){
            SVProgressHUD.setStatus(status)
            SVProgressHUD.setImageViewSize(imageViewSize)
            SVProgressHUD.setMinimumSize(loaderSize)
        }else{
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.setBackgroundLayerColor(UIColor.white.withAlphaComponent(0.9))
            SVProgressHUD.setBackgroundColor(UIColor.white.withAlphaComponent(0.9))
            SVProgressHUD.setForegroundColor(kNavigationColor)
            SVProgressHUD.setImageViewSize(imageViewSize)
            SVProgressHUD.setMinimumSize(loaderSize)
            SVProgressHUD.show(#imageLiteral(resourceName: "loaderImage"), status: status)
        }
    }

    class func showLoader(withStatus status: String, inView view: UIView)
    {
        //SVProgressHUD.show(withStatus: status, on: view)
    }
    
    class func hideLoader()
    {
        if SVProgressHUD.isVisible(){
            SVProgressHUD.dismiss()
        }
    }
    
    func clearAllPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel()
            }
        }
    }

    func clearLastPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in

                if let lastTask = tasks.last{
                    lastTask.cancel()
                }
        }
    }


    class func getSectedSevices(services:Array<Service>) -> String{
        var selectedServices = ""
        for service in services{
            if selectedServices == ""{
                selectedServices = service.ID
            }else{
                selectedServices = selectedServices+","+service.ID
            }
        }
        return selectedServices
    }

    class func getSectedAmenities(amenities:Array<Aminity>) -> String{
        var selectedAmenities = ""
        for amenity in amenities{
            if selectedAmenities == ""{
                selectedAmenities = amenity.ID
            }else{
                selectedAmenities = selectedAmenities+","+amenity.ID
            }
        }
        return selectedAmenities
    }


    class func getETAFromDateString(_ dateString: String) -> String
    {
        var eta = ""
        let df  = DateFormatter()
        df.locale = Locale.current
        df.locale = Locale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "EEEE"
        let etaDay = df.string(from: date);
        df.dateFormat = "MM-dd-YY"
        let etaDate = df.string(from: date);
        eta = "ETA \(etaDay), \(etaDate)"
        return eta
    }


    class func dateFromBookingDateString(_ dateString: String) -> String
    {
        let df  = DateFormatter()
        df.locale = Locale.current
        df.timeZone = TimeZone.current
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "MM/dd/YY"
        let etaDate = df.string(from: date);
        return etaDate
    }

    class func getDayAndDateFromDateString(_ dateString: String) -> String
    {
        var eta = ""
        let df  = DateFormatter()
        df.locale = Locale.current
        df.timeZone = TimeZone.current
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "EEEE"
        let etaDay = df.string(from: date);
        df.dateFormat = "MM-dd-YY"
        let etaDate = df.string(from: date);
        eta = "ETA \(etaDay), \(etaDate)"
        return eta
    }
    

    class func dateWithString(_ dateString: String) -> String {

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }


    class func formattedDateWithString(_ dateString: String,format :String) -> String {
        //"dd-MMM-YYYY, hh:mm a"
        //"EEEE dd-MMM-YYYY h:mm a"
        //"hh:mm a, dd-MMM-YYYY"
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd-MMM-YYYY"
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = format
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }
    
    class func formattedCompletedTaskDateWithString(_ dateStr: String,timeStr:String) -> String {
        //"dd-MMM-YYYY, hh:mm a"
        //"EEEE dd-MMM-YYYY h:mm a"
        //"hh:mm a, dd-MMM-YYYY"

        let dateString = "\(dateStr)T\(timeStr):00.000Z"

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd-MMM-YYYY"
                //let date = dayTimePeriodFormatter.string(from: d)
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd MMM YYYY"
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }


    class func shortSelectedDate(_ fromDate:Date, toDate: Date)->(fromDate: Date,toDate:Date){
        if fromDate.compare(toDate) == ComparisonResult.orderedDescending{
            return (toDate,fromDate)
        }else{
            return (fromDate,toDate)
        }
    }

    func formattedDateWith(_ date:Date,format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateStyle = .medium
            dateFormatter.dateFormat = format
            let da = dateFormatter.string(from: date)
            return da
    }



    class func formattedTimeFromDateWith(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let da = "\(dateFormatter.string(from: date)) UTC"
        return da
    }

    class func formattedDateForSubmittionFromDateWith(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "dd-MM-YYYY"
        let da = dateFormatter.string(from: date)
        return da
    }



    class func getModifiedDateFromDateString(_ dateString: String) -> String
    {

        let df  = DateFormatter()
        df.locale = Locale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "dd-MM-YY"
        return df.string(from: date);
    }

    //MARK:- Email Validation
    class func isValidEmailAddress(_ emailStr: String) -> Bool
    {
        if((emailStr.isEmpty) || emailStr.count == 0)
        {
            return false
        }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@",emailRegex)
        if(emailPredicate.evaluate(with: emailStr)){
            return true
        }
        return false
    }
    class func validateUserName(_ username: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_USERNAME = 1
        let MAXIMUM_LENGTH_LIMIT_USERNAME = 20

        let nameRegex = "[a-zA-Z _.@ ]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)

        if username.count == 0
        {
            return false
        }

        else if username.count < MINIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if username.count > MAXIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if !nameTest.evaluate(with: username)
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Password Validation
    class func validatePassword(_ password: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_PASSWORD = 6
        let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20

        if password.count == 0
        {
            return false
        }
        else if password[0] == " "
        {
            return false
        }
        else if password.count < MINIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else if password.count > MAXIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Email Validation
    class func validateEmail(_ email : String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if email.count == 0
        {
            return false
        }

        else if !emailTest.evaluate(with: email)
        {
            return false
        }

        else
        {
            return true
        }
    }


    class func matchConfirmPassword(_ password :String , confirmPassword : String)-> Bool{


        if password==confirmPassword {
            return true
        }
        else{
            return false
        }
    }

    class func validatePhoneNumber(_ phone : String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phone == filtered
    }

    class func classNameAsString(_ obj: Any) -> String {
        return String(describing: type(of: obj)).components(separatedBy:"__").last!
    }

    func openLocationSetting(){
        if #available(iOS 10.0, *) {
            if let url = URL(string: "App-Prefs:root=LOCATION_SERVICES") {
                UIApplication.shared.open(url, completionHandler: .none)
            }
        } else {
            if let url = URL(string: "prefs:root=LOCATION_SERVICES") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, completionHandler: .none)
                } else {
                    if UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }

    class func setupNoDataView(_ tableView:UITableView,message:String){
        let bgView = UIView()
        bgView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let noReviewLabel = UILabel()
        noReviewLabel.sizeToFit()
        bgView.addSubview(noReviewLabel)
        noReviewLabel.center = CGPoint(x:tableView.center.x, y:tableView.frame.size.height*1/2)
        noReviewLabel.textAlignment = NSTextAlignment.center
        noReviewLabel.text = message
        tableView.backgroundView = noReviewLabel
    }
    
}
