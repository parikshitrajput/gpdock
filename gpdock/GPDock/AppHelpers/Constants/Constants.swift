//
//  Constants.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//

import UIKit
import Firebase

let DEBUG = false
let DEBUG_Log = false

let kNavigationColor = UIColor(red:16.0/255.0, green:122.0/255.0, blue:196.0/255, alpha:1.0)
let kApplicationGreenColor = UIColor(red:26.0/255.0, green:188.0/255.0, blue:96.0/255, alpha:1.0)
let kApplicationRedColor = UIColor(red:179.0/255.0, green:0.0/255.0, blue:27.0/255, alpha:1.0)
let kApplicationBrownColor = UIColor(red:101.0/255.0, green:67.0/255.0, blue:33.0/255, alpha:1.0)

struct ApplicationColor{
    static let lightGray = UIColor(red: 35.0/255.0, green: 97.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 42.0/255.0, green: 65.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 55.0/255.0, green: 32.0/255.0, blue: 208.0/255.0, alpha: 1.0)
}


struct gradientTextColor{
    static let startRed = 35
    static let startGreen = 97
    static let startBlue = 248
    static let endRed = 55

    static let start = UIColor(red: 35.0/255.0, green: 97.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 42.0/255.0, green: 65.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 55.0/255.0, green: 32.0/255.0, blue: 208.0/255.0, alpha: 1.0)
}

struct gradientViewColor{
    static let startRed = 35
    static let startGreen = 97
    static let startBlue = 248
    static let endRed = 55 //44  124   249
    static let start = UIColor(red: 44.0/255.0, green: 124.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 43.0/255.0, green: 66.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 70.0/255.0, green: 58.0/255.0, blue: 216.0/255.0, alpha: 1.0) 
}

let warningMessageShowingDuration = 1.25

struct StripeLimit{
    //limit in USD
    static let lowerLimit = 5.0
    static let upperLimit = 99999.99
}

/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
    public static let GPDockSlideNavigationControllerDidClose = NSNotification.Name("SlideNavigationControllerDidClose")
    public static let USER_DID_LOGGED_IN_NOTIFICATION = NSNotification.Name("UserDidLoggedInNotification")
    public static let SESSION_EXPIRED_NOTIFICATION = NSNotification.Name("SessionExpiredNotification")

    public static let REFRESH_NOTIFICATION_LIST_NOTIFICATION = NSNotification.Name("RefreshNotificationListNotification")
    public static let USER_DID_UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UserDidUpdateProfileNotification")
    public static let USER_DID_ADD_NEW_CARD_NOTIFICATION = NSNotification.Name("UserDidAddNewCardNotification")
    public static let BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION = NSNotification.Name("BookingDidCancelledByUserNotification")
    public static let FIRInstanceIDTokenRefreshNotification = NSNotification.Name.MessagingRegistrationTokenRefreshed
    public static let USER_PAID_AT_MARINA_NOTIFICATION = NSNotification.Name("UserPaidAtMarinaNotification")
    public static let USER_FAVORATES_MARINA_NOTIFICATION = NSNotification.Name("UserFavoratesMarinaNotification")
    public static let USER_DID_ADD_BOAT_NOTIFICATION = NSNotification.Name("UserDidAddNotification")
    public static let SHOULD_USER_UPDATE_APPLICATION_VERSION = NSNotification.Name("shouldUserUpdateApplicationNotification")
    //public static let DISMISS_VIEW = NSNotification.Name("dismisView")
}



enum PushNotificationType:String{
    case notificationType = "notification_type"
    case bookingCreatedByCustomerNotification = "customer_create_booking"
    case bookingCreatedByBusinessNotification = "business_create_booking"
    case bookingCancelledByBusinessNotification = "booking_cancel_by_business"
    case bookingCancelledByCustomerNotification = "booking_cancel_by_customer"
    case bookingCompletedByNotification = "booking_complete_by_business"
    case none = ""
}

//let kNotificationType = "notification_type"
//let kBookingCreatedByCustomerNotification = "customer_create_booking"
//let kBookingCreatedByBusinessNotification = "business_create_booking"
//let kBookingCancelledByBusinessNotification = "booking_cancel_by_business"
//let kBookingCancelledByCustomerNotification = "booking_cancel_by_customer"
//let kBookingCompletedByNotification = "booking_complete_by_business"


let kUserDefaults = UserDefaults.standard

/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/


let kIsLoggedIN = "is_logged_in"
let kNormalUpdateShownOnDate = "normalUpdateShownOnDate"
let kUserName = "user_name"
let kPassword = "password"
let kDeviceToken = "DeviceToken"

let kSupportEmail = "admin@gpdock.com"
let kReportUsEmail = "admin@gpdock.com"
let tollFreeNumber = "1-888-202-3625"
let appID = "1347118363"
let kNotificationType = "notification_type"
let kSingleNotification = "single_notification"
let kBulkNotification = "bulk_notification"
let kReservationRemind = "reservation_remind"
let kNearByNotification = "near_by_marina"
let kIndividualOffer = "individual_offer"
let privateMarinaIconUrl = "http://res.cloudinary.com/dhnvrrzzk/image/upload/c_scale,w_37/v1515500173/map_icons/private_location_sel.png"
let publicMarinaIconUrl = "http://res.cloudinary.com/dhnvrrzzk/image/upload/c_scale,w_37/v1515500162/map_icons/marker_dark_blue.png"
let loginmessage = "Please Login or Sign up"
/*================== API URLs ====================================*/
let WEBSITE_URL = "https://www.gpdock.com/"

//String ABOUS_US_DEVELOPMENT="http://development.gpdock.com";
//String ABOUS_US_PRODUCTION="http://api.gpdock.com";
//String ABOUT_US_IN_USE = ABOUS_US_DEVELOPMENT;
let ABOUT_US_URL = DEBUG ? "http://development.gpdock.com/mobile/about" : "http://api.gpdock.com/mobile/about"

let TERMS_AND_CONDITIONS_URL = "\(WEBSITE_URL)user-term-service"

let BASE_URL = DEBUG ? "http://development.gpdock.com/api/v1/" : "https://www.gpdock.com/api/v1/" //

let NEW_VERSION_BASE_URL = DEBUG ? "http://development.gpdock.com/api/v2/" : "https://www.gpdock.com/api/v2/"

let MARINA_SHARING_URL = "\(WEBSITE_URL)marinaprofile/"
let FAQ_URL = "\(BASE_URL)faqs/role/customer"

let LOGIN_URL = "\(BASE_URL)user/login"
let LOGOUT_URL = "\(BASE_URL)logout"

let APP_VERSION = "\(NEW_VERSION_BASE_URL)app/versions"
let SIGN_UP_URL = "\(NEW_VERSION_BASE_URL)users"
let SOCIAL_AUTH_URL = "\(NEW_VERSION_BASE_URL)user/social/login"
let EDIT_PROFILE_URL = "\(NEW_VERSION_BASE_URL)user/profile/update"
let GENERATE_INVOICE_URL = "\(NEW_VERSION_BASE_URL)invoices"
let OFFERS_LIST_URL = "\(NEW_VERSION_BASE_URL)available/offers"

let UPDATE_DEVICE_TOKEN_URL = "\(BASE_URL)update/user/device"

let FORGOT_PASSWORD_URL = "\(BASE_URL)user/recovery"
let OTP_VARIFICATION_URL = "\(BASE_URL)user/otp/verification"
let PASSWORD_RECOVERY_URL = "\(BASE_URL)user/password/recovery"
let UPDATE_PASSWORD_URL = "\(BASE_URL)user/password/reset"

let WRITE_TO_US_URL = "\(BASE_URL)user/supports"
let BADGE_COUNT_URL = "\(BASE_URL)/badges"
let RATE_APP_ON_APPSTORE = "\(BASE_URL)user/rate/store"
let NOTIFICATIONS_LIST_URL = "\(BASE_URL)notifications"



let CLEAR_ALL_NOTIFICATIONS_LIST_URL = "\(BASE_URL)remove/notifications"

let NOTIFICATION_TOGGLE_STATUS_URL = "\(BASE_URL)notification/update"

let SERVICE_TRANSACTIONS_LIST_URL = "\(BASE_URL)service/payments/list"
let TRANSACTION_DETAILS = "\(BASE_URL)service/payment/info"

let SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL = "\(BASE_URL)marina/availablity"
let SEARCH_MARINA_AVAILABILITY_BY_USER_URL = "\(BASE_URL)user/boats/availablity"

let SEARCH_BY_TEXT_URL = "\(BASE_URL)suggest/search"

let MARINA_FLOOR_PLAN_URL = "\(BASE_URL)marina/availablity"
let NEARBY_CITIES_URL = "\(NEW_VERSION_BASE_URL)near/citites"
let CITIES_URL = "\(BASE_URL)cities"

let MARINA_LIST_URL = "\(BASE_URL)marinas"
let FAVORITES_MARINA_LIST_URL = "\(BASE_URL)favourites"
let FAVORITES_TOGGLE_URL = "\(BASE_URL)favourite"
let MARINA_FILTERS_URL = "\(BASE_URL)marina/search/filters"
let REVIEW_BOOKING_URL = "\(BASE_URL)marina/pricebydate"

let SERVICES_URL = "\(BASE_URL)services"
let AMENITIES_URL = "\(BASE_URL)amenities"

let CARDS_LIST_URL = "\(BASE_URL)card/list"
let ADD_CARD_URL = "\(BASE_URL)cards"
let REMOVE_CARD_URL = "\(BASE_URL)card/remove/"

let CREATE_BOOKING_URL = "\(BASE_URL)bookings"
let REQUEST_BOOKING_URL = "\(BASE_URL)booking/request"

let LIST_OF_BOOKINGS_URL = "\(BASE_URL)bookings/status"
let BOOKING_DETAILS_URL = "\(BASE_URL)bookings/"
let CANCEL_BOOKING_URL = "\(BASE_URL)bookings/cancel"
let DID_USER_BOOKED_EARLIER = "\(BASE_URL)user/booking"
let DID_USER_FAVORATE_EARLIER = "\(BASE_URL)user/favourite"

let MAIL_INVOICE_URL = "\(BASE_URL)invoice"
let PAY_AT_MARINA_URL = "\(BASE_URL)payments"
let REFUND_DETAILS_URL = "\(BASE_URL)refund/details"
let CHECK_REFUNDABLE_BOOKING_DETAILS_URL = "\(BASE_URL)refundable/details"
let USER_BOAT_LIST_URL = "\(BASE_URL)boat/user/boats"


let USER_PROFILE_URL = "\(BASE_URL)profile"
let USER_RIDE_HISTORY_URL = "\(BASE_URL)customer_booking_history"
let ALL_CARS_URL = "\(BASE_URL)all_cars"
let RIDE_REQUEST_URL = "\(BASE_URL)ride_request"
let CHANGE_RIDE_REQUEST_URL = "\(BASE_URL)change_ride_request"
let MARINA_DETAILS_URL = "\(BASE_URL)marina/details"
let LOCK_MARINA_URL = "\(BASE_URL)marins/select/space"

let MARINA_REVIEWS_URL = "\(BASE_URL)/reviews"
let MARINA_REVIEWS_FOR_RATINGS_URL = "\(BASE_URL)/reviews"

let ADD_BOAT_URL = "\(BASE_URL)boat/add"
let UPDATE_BOAT_URL = "\(BASE_URL)boat/update"

let DELETE_BOAT_URL = "\(BASE_URL)users/"//users/5/boats/31

let ADD_REVIEW_URL = "\(BASE_URL)reviews"
let COST_ESTIMATION_URL = "\(BASE_URL)fair_price"
let DRIVER_LOCATION_URL = "\(BASE_URL)driver_location"


let USER_CANCEL_RIDE_URL = "\(BASE_URL)user_cancel_ride"
let USER_CONTACT_VERIFIED = "\(BASE_URL)user/verify/contact"

/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "Facebook"
    case google = "Google"
}

/*======================== CONSTANT MESSAGES ==================================*/

let NETWORK_NOT_CONNECTED_MESSAGE = "Network is not connected!"
//let FUNCTIONALITY_PENDING_MESSAGE = "Under Development. Please ignore it!"


/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let FACEBOOK_URL_SCHEME = "fb1432514256876534"
let SELF_URL_SCHEME = "com.marinetechusa.GPDock"
let SELF_IDENTIFIER = "gpdock"

let STRIPE_KEY = DEBUG ?  "pk_test_s08TYuM2TqHyonJ8nqZUNq8d" : "pk_live_XSJyTGono74xfFkURjngEOmp"

let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.323647632593-h7jo4m5i9oeam4qs991a7sucgf29v7pu"
let GOOGLE_API_KEY = "AIzaSyCdR7ijSZ1kQS5oiVfJ57Ecq4uPMCcOhY0"
let kGoogleClientId = "323647632593-h7jo4m5i9oeam4qs991a7sucgf29v7pu.apps.googleusercontent.com"

/*============== PRINTING IN DEBUG MODE ==================*/
func print_debug <T>(_ object : T){
    if DEBUG_Log{
        print(object)
    }
}

func print_log <T>(_ object : T){
//    NSLog("\(object)")
}



enum fontSize : CGFloat {
    case small = 12.0
    case medium = 15.0
    case large = 16.0
    case xLarge = 18.0
    case xXLarge = 20.0
    case xXXLarge = 32.0
}

enum fonts {
    enum Raleway : String {
        case regular = "Raleway-Regular"
        case semiBold = "Raleway-Semibold"
        case medium = "Raleway-Medium"
        case bold = "Raleway-Bold"
        case light = "Raleway-Light"

        func font(_ size : fontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        }
    }
}

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }

        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }

    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}

//uses of fonts
//let fo = fonts.OpenSans.regular.font(.xXLarge)


enum segue : String{

    case unwindToDasboard  = "SegueUnwindToDasboard"
}

enum warningMessage : String{
    case alertTitle = "Important Message"
    case enterPassword = "Please enter your password"
    case validPassword = "Please enter a valid password. Passwords should be 6-20 characters long."

    case enterOldPassword = "Please enter your current password"
    case validOldPassword = "Please enter a valid current password. Passwords should be 6-20 characters long."

    case enterNewPassword = "Please enter your new password"
    case validNewPassword = "Please enter a valid new password. Passwords should be 6-20 characters long."
    
    case validPhoneNumber = "Please enter a valid phone number"
    case validName = "Please enter a valid name"
    case validEmailAddress = "Please enter a valid email address"
    case emailCanNotBeEmpty = "Please enter your email address"
    case restYourPassword = "An email was sent to you to rest your password"
    case changePassword = "Your password has been changed successfully"
    case logoutMsg = "You've been logged out successfully."
    case networkIsNotConnected = "Network is not connected!"
    case functionalityPending = "Under development. Please ignore it"
    case confirmPassword = "Please confirm your password"
    case passwordDidNotMatch = "Please enter matching passwords"

    case cardDeclined = "The card was declined. Please reenter the payment details"
    case enterCVV = "Please enter the CVV"
    case enterValidCVV = "Please enter a valid CVV"
    case cardHolderName = "Please enter the card holder's name"
    case expMonth = "Please enter the exp. month"
    case expYear = "Please enter the exp. year"
    case validExpMonth = "Please enter a valid exp. month"
    case validExpYear = "Please enter a valid exp. year"
    case validCardNumber = "Please enter a valid card number"
    case cardNumber = "Please enter the card number"
}


enum helperNames : String {
    case loader = "loader.gif"
    case oops = "opps"
    case placeholder = "placeholder"
    case facebook = "facebook"
    case Cancel = "Cancel"
    case VendorDetailNotFound = "Vendor Details Not Found"
}


/*============== SHOW MESSAGE ==================*/
//typedef void (^FBSDKLoginManagerRequestTokenHandler)(FBSDKLoginManagerLoginResult *result, NSError *error);

typealias GPDockCompletionBlock = (_ done: Bool)->Void

func showSuccessWithMessage(_ message: String)
{
//    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleSuccess)
    // TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.success)
}
func showErrorWithMessage(_ message: String)
{

    GPDock.showAlertWith(viewController: nil, message: message, title: warningMessage.alertTitle.rawValue, complitionBlock: nil)
}

func showAlertWith(viewController: UIViewController?,message:String,title:String, complitionBlock : ((_ done: Bool) ->Void)? = nil){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)

    let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
        guard let handler = complitionBlock else{
            alert.dismiss(animated: false, completion: nil)
            return
        }
        handler(true)
        alert.dismiss(animated: false, completion: nil)
    }
    alert.addAction(okayAction)
    var vc: UIViewController
    if let pvc = viewController{
        vc = pvc
    }else{
        vc = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!
    }
    vc.present(alert, animated: true, completion: nil)
}



func showLoginAlert(isFromMenu: Bool,shouldLogin:Bool,inViewController vc: UIViewController?) {

    let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
    let presentingVC = vc ?? appDelegate.window!.rootViewController!

    if !isFromMenu{
        askForLogin(isFromMenu: isFromMenu, shouldLogin: shouldLogin, inViewController: presentingVC)
    }else{
        SlideNavigationController.sharedInstance().closeMenu {
            openLoginModule(in: presentingVC, shouldLogin: shouldLogin)
        }
    }
}

func askForLogin(isFromMenu: Bool,shouldLogin:Bool,inViewController vc: UIViewController){
    let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "You have to login to access this feature. Would you like to login?", preferredStyle: UIAlertControllerStyle.alert)
    let okayAction = UIAlertAction(title: "Yes", style: .default) {[vc] (action) in
        openLoginModule(in: vc, shouldLogin: shouldLogin)
    }
    let cancelAction = UIAlertAction(title: "Nope", style: .cancel) { (action) in
        alert.dismiss(animated: true, completion: nil)
    }
    alert.addAction(okayAction)
    alert.addAction(cancelAction)
    vc.present(alert, animated: true, completion: nil)
}

func openLoginModule(in viewController: UIViewController,shouldLogin: Bool) {
    let nav = UINavigationController(rootViewController: shouldLogin ? AppStoryboard.Main.viewController(LoginViewController.self) : AppStoryboard.Main.viewController(RegisterViewController.self))
    nav.navigationBar.isTranslucent = false
    nav.navigationBar.barTintColor = .white
    nav.navigationBar.backgroundColor = .white
    viewController.present(nav, animated: true, completion: nil)
}





