//
//  UIViewControllerExtension.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

extension UINavigationController {
    func pop(_ animated: Bool) {
        _ = self.popViewController(animated: animated)
    }

    func popToRoot(_ animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
}

extension UIViewController {

    class var storyboardID : String {
        return "\(self)"
    }
    static func instantiate(fromAppStoryboard appStoryboard : AppStoryboard) -> Self {
        return appStoryboard.viewController(self)
    }
}


enum AppStoryboard : String {

    case Main,Booking, Profile, Home, Settings, Transactions

    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }

    func viewController<T : UIViewController>(_ viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }

    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}



extension UIViewController{

//    func setupNavigationBar() {
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.isTranslucent = true
//        navigationController?.navigationBar.topItem?.titleView?.tintColor = UIColor.white
//        navigationController?.navigationBar.tintColor = UIColor.white
//        let shadow = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
//        shadow.backgroundColor = UIColor.white//(red: 62.0 / 255.0, green: 81.0 / 255.0, blue: 119.0 / 255.0, alpha: 1.0)
//        shadow.layer.shadowColor = UIColor.groupTableViewBackground.cgColor//(red: 51 / 255, green: 76 / 255, blue: 104 / 255, alpha: 1.0).cgColor
//        shadow.layer.shadowOffset = CGSize(width: 0, height: 15)
//        shadow.layer.shadowOpacity = 0.12
//        shadow.layer.shadowRadius = 4.5
//        view.addSubview(shadow)
//    }



    func addLoginView(_ message:String)->LoginView{
        let loginView = LoginView.instanceFromNib()
        loginView.messageLabel.text = message
        self.view.addSubview(loginView)
        loginView.layer.zPosition = CGFloat(Int.max)
        let topContraints = NSLayoutConstraint(item: loginView, attribute:
            .top, relatedBy: .equal, toItem: self.view,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)

        let bottomContraints = NSLayoutConstraint(item: loginView, attribute:
            .bottom, relatedBy: .equal, toItem: self.view,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)

        let leftContraints = NSLayoutConstraint(item: loginView, attribute:
            .leading, relatedBy: .equal, toItem: self.view,
                            attribute: .leading, multiplier: 1.0,
                            constant: 0)

        let rightContraints = NSLayoutConstraint(item: loginView, attribute:
            .trailing, relatedBy: .equal, toItem: self.view,
                             attribute: .trailing, multiplier: 1.0,
                             constant: 0)

        loginView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([topContraints,rightContraints,leftContraints,bottomContraints])
        return loginView
    }

    


    func isModal() -> Bool {
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }

        if self.presentingViewController != nil {
            return true
        }

        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }

        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
}
