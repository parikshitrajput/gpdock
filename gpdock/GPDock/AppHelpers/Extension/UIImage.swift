//
//  UIIMage.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//


import UIKit

extension UIImage {
    func makeImageWithColorAndSize(_ color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        color.setFill()
        UIRectFill(CGRect(x:0, y:0, width:size.width, height:size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

 func drawAvatar(size: CGSize,
                            letters: String,
                            lettersFont: UIFont,
                            lettersColor: UIColor,
                            backgroundColor: CGColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)

        UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(backgroundColor)
            context.fill(rect)

            let style = NSParagraphStyle.default.mutableCopy()
            #if swift(>=4.0)
                let attributes = [
                    NSAttributedStringKey.paragraphStyle: style,
                    NSAttributedStringKey.font: lettersFont.withSize(min(size.height, size.width) / 2.0),
                    NSAttributedStringKey.foregroundColor: lettersColor
                ]

                let lettersSize = letters.size(withAttributes: attributes)
            #else
                let attributes = [
                NSParagraphStyleAttributeName: style,
                NSFontAttributeName: lettersFont.withSize(min(size.height, size.width) / 2.0),
                NSForegroundColorAttributeName: lettersColor
                ]

                let lettersSize = letters.size(attributes: attributes)
            #endif

            let lettersRect = CGRect(
                x: (rect.size.width - lettersSize.width) / 2.0,
                y: (rect.size.height - lettersSize.height) / 2.0,
                width: lettersSize.width,
                height: lettersSize.height
            )
            letters.draw(in: lettersRect, withAttributes: attributes)

            let avatarImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return avatarImage
        }
        return nil
    }

//    func trim(trimRect trimRect :CGRect) -> UIImage {
//        if CGRectContainsRect(CGRect(origin: CGPointZero, size: self.size), trimRect) {
//            if let imageRef = CGImageCreateWithImageInRect(self.CGImage, trimRect) {
//                return UIImage(CGImage: imageRef)
//            }
//        }
//        
//        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
//        self.drawInRect(CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
//        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        guard let image = trimmedImage else { return self }
//        
//        return image
//    }
}


public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}











private let ChannelDivider: CGFloat = 255

public class RGBA: NSObject {
    var red: CGFloat
    var green: CGFloat
    var blue: CGFloat
    var alpha: CGFloat

    init(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
    }

    init(intRed: Int, green: Int, blue: Int, alpha: Int) {
        self.red = CGFloat(intRed)/ChannelDivider
        self.green = CGFloat(green)/ChannelDivider
        self.blue = CGFloat(blue)/ChannelDivider
        self.alpha = CGFloat(alpha)/ChannelDivider
    }
}

public class Grayscale: NSObject {
    var white: CGFloat
    var alpha: CGFloat

    init(white: CGFloat, alpha: CGFloat) {
        self.white = white
        self.alpha = alpha
    }
}

public class GradientPoint<C>: NSObject {
    var location: CGFloat
    var color: C

    init(location: CGFloat, color: C) {
        self.location = location
        self.color = color
    }
}


