//
//  AppDelegate.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import CoreData
import Google
import GoogleMaps
import GooglePlaces

import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import Stripe
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import Fabric
import Crashlytics
import SwiftyJSON

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NotificationCenter.default.removeObserver(self)
        FirebaseApp.configure()
        Fabric.with([STPAPIClient.self, Crashlytics.self])
        
        self.registerForRemoteNotification()
        UIApplication.shared.statusBarStyle = .default
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: .FIRInstanceIDTokenRefreshNotification, object: nil)
        STPPaymentConfiguration.shared().publishableKey = STRIPE_KEY
        IQKeyboardManager.sharedManager().enable = true
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        self.logUser()
        if let _launchOptions = launchOptions{
            if let _ = _launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? Dictionary<String,AnyObject>{
                self.openNotification()
            }
        }else{
            let splashVC = AppStoryboard.Home.viewController(SplashAnimationViewController.self)
            splashVC.fromNotificationCenter = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil)
            self.window?.rootViewController = splashVC
        }

        UIApplication.shared.applicationIconBadgeNumber = 0
        self.unregisterFirebaseToken { (done) in
            if done{
                self.registerFirebaseToken()
            }
        }
       // window?.makeKeyAndVisible()
//        setupSiren()

        return true
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func openNotification(){
        let menu = AppStoryboard.Main.viewController(LeftMenuViewController.self)
        let nav = AppStoryboard.Home.viewController(SlideNavigationController.self)
        nav.avoidSwitchingToSameClassViewController = false
        nav.leftMenu = menu
        nav.enableSwipeGesture = true
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
        if CommonClass.isLoggedIn{
            let vc = AppStoryboard.Profile.viewController(NotificationListViewController.self)
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }

    }






    /*==============Notifications Handling================*/
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken

        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        kUserDefaults.set(deviceTokenString, forKey: kDeviceToken)

        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
        }else{
            print_debug("already registered to firebase with token : \(kUserDefaults.value(forKey: kDeviceToken) ?? "--")")
        }
    }


    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        Messaging.messaging().delegate = self
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            Messaging.messaging().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
        }

    }


    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            kUserDefaults.set(refreshedToken, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }
    }


    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print_debug(error.localizedDescription)
    }

    //Called when a notification is delivered to a foreground app.

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {

       NotificationCenter.default.post(name: .REFRESH_NOTIFICATION_LIST_NOTIFICATION, object: nil)
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }
    
 @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfoDict = response.notification.request.content.userInfo as? Dictionary<String,AnyObject>{
            if let notificationType = userInfoDict[kNotificationType] as? String{
                //print_debug("notification type: \(notificationType) and userInfo: \(userInfo)")
                let json = JSON.init(userInfoDict)
                print_debug("nitification json: \r\n \(json)")
                
                if notificationType == kBulkNotification{
                    goTOHomeScreen()
                }else if (notificationType == kSingleNotification){
                    goTOHomeScreen()
                }else if (notificationType == kIndividualOffer){
                    goTOHomeScreen()
                }else if (notificationType == kNearByNotification){
                    var newMarinaID = ""
                    var newmarinaTitle = ""
                    if let marinaID = userInfoDict["marina_id"] as? String{
                        newMarinaID = marinaID
                    }else if let marinaId = userInfoDict["marina_id"] as? Int{
                        newMarinaID = "\(marinaId)"
                    }
                    if let marinaTittle = userInfoDict["marina_title"] as? String{
                        newmarinaTitle = marinaTittle
                    }
                    if ((newMarinaID != "")&&(newmarinaTitle != "")){
                        goToMarinaScreen(marinaID: newMarinaID, marinaTittle: newmarinaTitle)
                    }
                    
                }else if (notificationType == kReservationRemind){
                    var newMarinaID = ""
                    var newmarinaTitle = ""

                    if let marinaID = userInfoDict["marina_id"] as? String{
                    newMarinaID = marinaID
                    }else if let marinaId = userInfoDict["marina_id"] as? Int{
                        newMarinaID = "\(marinaId)"
                    }
                    if let marinaTittle = userInfoDict["marina_title"] as? String{
                        newmarinaTitle = marinaTittle
                    }

                     if ((newMarinaID != "")&&(newmarinaTitle != "")){
                        goToMarinaScreen(marinaID: newMarinaID, marinaTittle: newmarinaTitle)
                    }
                }else{
                    let vc = AppStoryboard.Profile.viewController(NotificationListViewController.self)
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
                }
            }

            //self.application(UIApplication.shared, didReceiveRemoteNotification: userInfo)
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            print_debug(userInfoDict)

       }


        //completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
//            print_debug(userInfoDict)
//            let vc = AppStoryboard.Profile.viewController(NotificationListViewController.self)
//            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
//        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        if ( application.applicationState == .inactive || application.applicationState == .background)
        {

            if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
                if let notificationType = userInfoDict[kNotificationType] as? String{
                    let json = JSON.init(userInfoDict)
                    print_debug("nitification json: \r\n \(json)")
                    if notificationType == kBulkNotification{
                        goTOHomeScreen()
                    }else if (notificationType == kSingleNotification){
                        goTOHomeScreen()
                    }else if (notificationType == kIndividualOffer){
                        goTOHomeScreen()
                    }else if (notificationType == kNearByNotification){
                        var newMarinaID = ""
                        var newmarinaTitle = ""
                        if let marinaID = userInfoDict["marina_id"] as? String{
                            newMarinaID = marinaID
                        }else if let marinaId = userInfoDict["marina_id"] as? Int{
                            newMarinaID = "\(marinaId)"
                        }
                        if let marinaTittle = userInfoDict["marina_title"] as? String{
                            newmarinaTitle = marinaTittle
                        }
                        if ((newMarinaID != "")&&(newmarinaTitle != "")){
                            goToMarinaScreen(marinaID: newMarinaID, marinaTittle: newmarinaTitle)
                        }
                        
                    }else if (notificationType == kReservationRemind){
                        var newMarinaID = ""
                        var newmarinaTitle = ""
                        if let marinaID = userInfoDict["marina_id"] as? String{
                            newMarinaID = marinaID
                        }else if let marinaId = userInfoDict["marina_id"] as? Int{
                            newMarinaID = "\(marinaId)"
                        }
                        if let marinaTittle = userInfoDict["marina_title"] as? String{
                            newmarinaTitle = marinaTittle
                        }

                        if ((newMarinaID != "")&&(newmarinaTitle != "")){
                            goToMarinaScreen(marinaID: newMarinaID, marinaTittle: newmarinaTitle)
                        }
                    }else{
                        let vc = AppStoryboard.Profile.viewController(NotificationListViewController.self)
                        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
                    }
                }
            }
        }
    }



    /*=======================================Notifications Handling====================================*/
    func goTOHomeScreen() {
        let vc = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
    }
    
    func goToMarinaScreen(marinaID: String, marinaTittle: String) {
        let marinaVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        let marina = Marina()
        marina.ID = marinaID
        marina.title = marinaTittle

        marinaVC.marina = marina
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: marinaVC, withSlideOutAnimation: false, andCompletion: {            })

        //self.window?.rootViewController = marinaVC


        
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // NotificationCenter.default.removeObserver(self)

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: .SHOULD_USER_UPDATE_APPLICATION_VERSION, object: nil)
//        Siren.shared.checkVersion(checkType: .immediately)

        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
//        Siren.shared.checkVersion(checkType: .immediately)

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.

        self.saveContext()
    }

    // MARK: - Core Data stack
    // MARK: - Core Data Saving support
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }

        return coordinator
    }()
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.tecorb.Bloom_Trade" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return (urls[urls.count-1] as NSURL) as URL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "GPDock", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "GPDock")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()



    func saveContext () {
        var managedObjContext : NSManagedObjectContext!
        if #available(iOS 10.0, *) {
            managedObjContext = persistentContainer.viewContext
        } else {
            managedObjContext = self.managedObjectContext
        }

        if managedObjContext.hasChanges {
            do {
                try managedObjContext.save()

            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        var result = false
        if url.scheme == GOOGLE_URL_SCHEME{
            let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication]
            let annotation = options[UIApplicationOpenURLOptionsKey.annotation]
            result = GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication as! String!, annotation: annotation)
        }else if url.scheme == FACEBOOK_URL_SCHEME{
            result = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }else if url.scheme == SELF_URL_SCHEME || url.scheme == SELF_IDENTIFIER{
            result = true
        }
        return result
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        var result = false
        if url.scheme == GOOGLE_URL_SCHEME{
            result = GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        }else if url.scheme == FACEBOOK_URL_SCHEME{
            result = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }else if url.scheme == SELF_URL_SCHEME || url.scheme == SELF_IDENTIFIER{
            result = true
        }
        
        return result
    }



    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL{
                let pathComponents = url.pathComponents
                if pathComponents.contains("marinaprofile"){
                    let marinaID = url.lastPathComponent
                    if !marinaID.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
                        self.openMarinaProfileWithMarinaID(marinaID.trimmingCharacters(in: .whitespacesAndNewlines))
                    }
                }else if pathComponents.contains("privacy"){
                    self.navigateToPrivacyPolicy()
                }else if pathComponents.contains("aboutus"){
                    self.navigateToAboutUs()
                }
            }
        }
        return true
    }
    
    
//    func setupSiren() {
//        let siren = Siren.shared
//        siren.delegate = self
//        siren.alertType = .option
//        siren.alertMessaging = SirenAlertMessaging(updateTitle: warningMessage.alertTitle.rawValue,nextTimeButtonMessage: "OK, next time it is!",skipVersionButtonMessage: "A new version GPDock is available. Please update to version")
//        //siren.checkVersion(checkType: .immediately)
//          siren.debugEnabled = true
//
//
//    }


}

extension AppDelegate : MessagingDelegate{
    func logUser() {
        if CommonClass.isLoggedIn{
            let user = User(json: User.loadUserInfo()!)
            Crashlytics.sharedInstance().setUserEmail(user.email)
            Crashlytics.sharedInstance().setUserIdentifier(user.ID)
            Crashlytics.sharedInstance().setUserName(user.firstName+" "+user.lastName)
        }

    }

    func registerFirebaseToken() {
        if let token = InstanceID.instanceID().token() {
            kUserDefaults.set(token, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }
        Messaging.messaging().shouldEstablishDirectChannel = true
    }

    func unregisterFirebaseToken(completion: @escaping (Bool)->()) {
        InstanceID.instanceID().deleteID { (error) in
            completion(error == nil)
        }
    }

    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
            //self.registerFirebaseToken()
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    }
}

//Deep link handling

extension AppDelegate{
    func openMarinaProfileWithMarinaID(_ marinaID:String){
        let menu = AppStoryboard.Main.viewController(LeftMenuViewController.self)
        let nav = AppStoryboard.Home.viewController(SlideNavigationController.self)
        nav.avoidSwitchingToSameClassViewController = false
        nav.leftMenu = menu
        nav.enableSwipeGesture = true
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
        let marina = Marina()
        marina.ID = marinaID
        DispatchQueue.main.async {[marina] in
            self.navigateToMarinaProfile(marina, shouldDirectBooking: false)
        }
    }

    func navigateToMarinaProfile(_ marina:Marina,shouldDirectBooking:Bool) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        marinaProfileVC.shouldGoDirectlyToBooking = shouldDirectBooking
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: marinaProfileVC, withSlideOutAnimation: false, andCompletion: {            })
    }

    func navigateToPrivacyPolicy() {
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = true
        termsVc.isFromMenu = true
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: termsVc, withSlideOutAnimation: false, andCompletion: {            })
    }

    func navigateToAboutUs() {
        let aboutUsVC = AppStoryboard.Settings.viewController(AboutUsViewController.self)
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: aboutUsVC, withSlideOutAnimation: false, andCompletion: {            })
    }
}

//////////////
//enum DeepLinkType: String{
//    case none = ""
//    case marinaProfile = "marinaProfile"
//    case privacyPolicy = "privacyPolicy"
//    case aboutUs = "aboutUs"
//    case termsOfUses = "termsOfUses"
//    case support = "support"
//}
//
//class NKDeepLink: NSObject {
//    var url : URL!
//    override init() {
//        super.init()
//    }
//
//    init(url: URL) {
//        self.url = url
//        super.init()
//    }
//
//}





