//
//  UpdatePasswordViewController.swift
//  GPDock
//
//  Created by TecOrb on 03/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var user : User!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Reset Password"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo()!)
        self.setupNavigationTitle()
    }
    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        guard let oldPassword = oldPasswordTextField.text else {
            showAlertWith(viewController: self, message: warningMessage.enterOldPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if oldPassword.count == 0{
            showAlertWith(viewController: self, message: warningMessage.enterOldPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if oldPassword.count < 6{
            showAlertWith(viewController: self, message: warningMessage.validPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        guard let newPassword = newPasswordTextField.text else {
            showAlertWith(viewController: self, message: warningMessage.enterNewPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if newPassword.count == 0{
            showAlertWith(viewController: self, message: warningMessage.enterNewPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if newPassword.count < 6{
            showAlertWith(viewController: self, message: warningMessage.validPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        guard let confirmPassword = confirmedPasswordTextField.text else {
            showAlertWith(viewController: self, message: warningMessage.confirmPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if confirmPassword.count == 0{
            showAlertWith(viewController: self, message: warningMessage.confirmPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if confirmPassword != newPassword{
            showAlertWith(viewController: self, message: warningMessage.passwordDidNotMatch.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        self.updatePassword(oldPassword: oldPassword, newPassword: newPassword)
    }

    func updatePassword(oldPassword:String,newPassword:String) {
        LoginService.sharedInstance.updatePassword(self.user.ID, oldPassword: oldPassword, withNewPassword: newPassword){(success, message) in
            if success{
                self.showSuccessAlert(message: message, title: warningMessage.alertTitle.rawValue)
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    func showSuccessAlert(message:String,title:String){
        self.confirmedPasswordTextField.text = ""
        self.newPasswordTextField.text = ""
        self.oldPasswordTextField.text = ""
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.navigationController?.pop(true)
        }
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }


    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if sender == self.newPasswordTextField || sender == self.confirmedPasswordTextField{
            if let cnfrmPassword = self.confirmedPasswordTextField.text{
                if let newpswrd = self.newPasswordTextField.text{
                   // self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                    self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.lightGray : kApplicationRedColor
                }
            }
        }

    }

}
