//
//  ProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    var user : User!
    var placeHolders = ["Name","Email","Contact"]
    @IBOutlet weak var profileTableView: UITableView!
    // @IBOutlet weak var vcTitleLabel:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        // profileTableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    override func viewWillAppear(_ animated: Bool) {
        // self.vcTitleLabel.text = "\(user.firstName.capitalized) \(user.lastName.capitalized)"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileViewController{

   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return self.view.frame.size.height*649.0/1206.0
        }else{
            return self.view.frame.size.height*562.0/1206.0
        }
    }
}
extension ProfileViewController: SlideNavigationControllerDelegate{
    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}

