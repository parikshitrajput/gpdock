//
//  ContactUsViewController.swift
//  GPDock
//
//  Created by TecOrb on 05/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    @IBOutlet weak var skypeButton: UIButton!
    @IBOutlet weak var tollFreeButton: UIButton!
    @IBOutlet weak var containerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onclickSkype(_ sender: UIButton){
        let tollFreeUrl = "skype://\(tollFreeNumber)?call"
        self.showPhoneApp(tollFreeUrl: tollFreeUrl)
    }
    @IBAction func onclickTollFree(_ sender: UIButton){
        let tollFreeUrl = "telprompt://\(tollFreeNumber)"
        self.showPhoneApp(tollFreeUrl: tollFreeUrl)
    }
    func showPhoneApp(tollFreeUrl: String)  {
        if (UIApplication.shared.canOpenURL(URL(string:tollFreeUrl)!))
        {
            UIApplication.shared.openURL(URL(string:tollFreeUrl)!)
        } else
        {
            showAlertWith(viewController: nil, message: "Cann't able to call right now\r\nPlease try later!", title: warningMessage.alertTitle.rawValue)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
