//
//  ProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import Foundation
import UIKit
import Messages
import MessageUI

class ProfileViewController: UITableViewController,LoginViewDelegate {
    var user : User!
    var profileView : UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBackgroundView: UIImageView!
    @IBOutlet weak var profileContainner: UIVisualEffectView!


    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    var loginView : LoginView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        self.setUpUserDetails(user: user)
        self.addGestureRecognizer()

        self.clearsSelectionOnViewWillAppear = false
        self.profileView = self.tableView
        self.tableView.reloadData()

    }

    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(profileTapGestureRecognizer)
    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        self.profileContainner.addshadow(top: true, left: true, bottom: false, right: true, shadowRadius: 4, shadowOpacity: 0.4)
        if let profileCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)){
            profileCell.setNeedsLayout()
            profileCell.layoutIfNeeded()
        }
    }
    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            let userjson = User.loadUserInfo()
            self.user = User(json: userjson!)
            self.setUpUserDetails(user: self.user)
        }else{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view your Profile\r\nPlease Login or Sign up")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        self.profileView.setNeedsLayout()
        self.profileView.layoutIfNeeded()
        self.profileView.reloadData()
    }
    
    func setUpUserDetails(user: User) {
        profileImageView.setIndicatorStyle(.white)
        profileImageView.setShowActivityIndicator(true)
        profileImageView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName))
        //profileImageView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        profileImageView.addshadow(top: true, left: true, bottom: true, right: true)
        profileBackgroundView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName))
        fullNameLabel.text = user.firstName+" "+user.lastName
        phoneNumberLabel.text = user.contact
        emailLabel.text = user.email
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.tableView.reloadData()
        if let profileCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)){
            profileCell.setNeedsLayout()
            profileCell.layoutIfNeeded()
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer)
    {
        if let imageView = tapGestureRecognizer.view as? UIImageView{
            guard let profileImage = imageView.image else{
                return
            }
            let imageViewerVC = AppStoryboard.Profile.viewController(ImageViewerViewController.self)
            imageViewerVC.profileImage = profileImage
            self.present(imageViewerVC, animated: true, completion: nil)
        }
    }

    @IBAction func onClickMyBookings(_ sender: UIButton){
        if CommonClass.isLoggedIn{
            let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
            homeTab.selectedIndexFromMenu = 2
            homeTab.isFromMenu = true
            self.navigationController?.setViewControllers([homeTab], animated: true)
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }
    }
    @IBAction func onClickPaymentMethods(_ sender: UIButton){
        if CommonClass.isLoggedIn{
            let vc = AppStoryboard.Settings.viewController(PaymentsMethodsViewController.self)
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }

    }

    @IBAction func onClickMyBoats(_ sender: UIButton){

        if CommonClass.isLoggedIn{
            let vc = AppStoryboard.Profile.viewController(MyBoatsViewController.self)
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: false, andCompletion: {            })
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }

    }

    @IBAction func onClickNotifications(_ sender: UIButton){
        if CommonClass.isLoggedIn{
            let resetPasswordVC = AppStoryboard.Profile.viewController(UpdatePasswordViewController.self)
            self.navigationController?.pushViewController(resetPasswordVC, animated: true)
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }
    }

    func navigateToWriteToUs() {
        let writeToUsVC = AppStoryboard.Settings.viewController(WriteToUsViewController.self)
        self.navigationController?.pushViewController(writeToUsVC, animated: true)
    }

    @IBAction func onClickReportUs(_ sender: UIButton){
        self.composeEmail(reciepents: kReportUsEmail, subject: "Report of GPDock!")
    }
    @IBAction func onClickContactUs(_ sender: UIButton){
        self.showAlertToChoosePhone()
        //self.composeEmail(reciepents: kSupportEmail, subject: "GPDock, contact us!")
    }

    func composeEmail(reciepents:String,subject:String){
        if !MFMailComposeViewController.canSendMail(){
            showAlertWith(viewController: self, message: "Configure mail account", title: warningMessage.alertTitle.rawValue)
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([reciepents])
        composeVC.setSubject(subject)
        composeVC.setMessageBody("Hey Team Here is my feedback", isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }

    @IBAction func onClickEditProfile(_ sender: UIButton){
        let editProfileVC = AppStoryboard.Profile.viewController(EditProfileViewController.self)
        editProfileVC.user = self.user
        editProfileVC.userImage = self.profileImageView.image
        editProfileVC.delegate = self
        self.present(editProfileVC, animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return (tableView.frame.size.height-70)*464/1034 //(tableView.frame.size.height-49)/2//(tableView.frame.size.height - 49)*520/1138
        }else{
            return (tableView.frame.size.height-70)*564/1034 //(tableView.frame.size.height-49-10)/2//(tableView.frame.size.height - 49)*434/1138
        }


//        if indexPath.row == 0{
//            return (tableView.frame.size.height - 49)*520/1138
//        }else{
//            return (tableView.frame.size.height - 49)*434/1138
//        }

    }

}
extension ProfileViewController: EditProfileViewControllerDelegate{
    func user(didEditProfile viewController: EditProfileViewController, withUpdatedUser user: User) {
        self.user = user
        self.setUpUserDetails(user: user)
        viewController.dismiss(animated: true, completion: nil)
    }


    func showAlertToChoosePhone(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let phoneAction: UIAlertAction = UIAlertAction(title: "Phone", style: .default)
        { action -> Void in
            let tollFreeUrl = "telprompt://\(tollFreeNumber)"
            self.showPhoneApp(tollFreeUrl: tollFreeUrl)
        }
        actionSheet.addAction(phoneAction)

        let skypeAction: UIAlertAction = UIAlertAction(title: "Skype", style: .default)
        { action -> Void in
            self.callFromSkype()
        }
        actionSheet.addAction(skypeAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func callFromSkype()  {
        let installed = UIApplication.shared.canOpenURL(NSURL(string: "skype:")! as URL)
        if installed {
            UIApplication.shared.openURL(NSURL(string: "skype:\(tollFreeNumber)")! as URL)
        } else {
            UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/skype/id304878510?mt=8")! as URL)
        }
    }
    func showPhoneApp(tollFreeUrl: String)  {
        if (UIApplication.shared.canOpenURL(URL(string:tollFreeUrl)!))
        {
            UIApplication.shared.openURL(URL(string:tollFreeUrl)!)
        } else
        {
            showAlertWith(viewController: nil, message: "Cann't able to call right now\r\nPlease try later!", title: warningMessage.alertTitle.rawValue)
        }
    }

}

extension ProfileViewController: MFMailComposeViewControllerDelegate{

    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        var message = ""

        switch result {
        case .saved:
            message = "Saved as draft"
        case .cancelled:
            message = "Cancelled by you"
        case .sent:
            message = "Mail has been sent"
        case .failed:
            message = "Mail sent failed"
        }

        controller.dismiss(animated: true, completion: {
            print_debug(message)
           // showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
        })

    }
}
extension ProfileViewController: SlideNavigationControllerDelegate{

    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}


//@IBAction func onclickSkype(_ sender: UIButton){
//    let tollFreeUrl = "skype://\(tollFreeNumber)?call"
//    self.showPhoneApp(tollFreeUrl: tollFreeUrl)
//}
//@IBAction func onclickTollFree(_ sender: UIButton){
//    let tollFreeUrl = "telprompt://\(tollFreeNumber)"
//    self.showPhoneApp(tollFreeUrl: tollFreeUrl)
//}
//func showPhoneApp(tollFreeUrl: String)  {
//    if (UIApplication.shared.canOpenURL(URL(string:tollFreeUrl)!))
//    {
//        UIApplication.shared.openURL(URL(string:tollFreeUrl)!)
//    } else
//    {
//        showAlertWith(viewController: nil, message: "Cann't able to call right now\r\nPlease try later!", title: "Error!")
//    }
//}

