//
//  MyBoatsViewController.swift
//  GPDock
//
//  Created by TecOrb on 22/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import MGSwipeTableCell


class MyBoatsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,LoginViewDelegate {
    var boats = Array<Boat>()
    @IBOutlet weak var boatsTableView : UITableView!
    var loginView: LoginView!
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(MyBoatsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "My Boats"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(MyBoatsViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MyBoatsViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MyBoatsViewController.boatDidAddedHandler(_:)), name: .USER_DID_ADD_BOAT_NOTIFICATION, object: nil)
        self.boatsTableView.register(UINib(nibName: "BoatListTableViewCell", bundle: nil), forCellReuseIdentifier: "BoatListTableViewCell")
        self.setupNavigationTitle()
        self.setupViews()
        self.setupRightBarButtons()
        self.boatsTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
    }


    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    @objc func boatDidAddedHandler(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Boat>{
            if let boat = userInfo["boat"]{
                self.boats.insert(boat, at: 0)
                self.boatsTableView.reloadData()
            }
        }
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setUPContainer()
        }else{
            self.setupLoginView("To view your Boats\r\nPlease Login or Sign up")
        }
    }
    func setupRightBarButtons() {
        let addBoatButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        addBoatButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addBoatButton.setImage(#imageLiteral(resourceName: "add_grey"), for: .normal)
        addBoatButton.addTarget(self, action: #selector(onClickAddBoatButton(_:)), for: .touchUpInside)
        let addBoatBarButton = UIBarButtonItem(customView: addBoatButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([addBoatBarButton], animated: false)
    }


    func setUPContainer()  {
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        self.boats.removeAll()
        self.pageNumber = 1
        self.boatsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.boatsTableView.dataSource = self
        self.boatsTableView.delegate = self
        self.boatsTableView.addSubview(self.refreshControl)
        self.loadBoatsFor(self.user.ID, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func onClickMenuButton(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    @IBAction func onClickAddBoatButton(_ sender: UIButton){
        self.openAddBoatScreen()
    }

    func openAddBoatScreen() {
        if CommonClass.isLoggedIn{
            let addBoatVC = AppStoryboard.Home.viewController(AddBoatViewController.self)
            addBoatVC.indexPath = IndexPath(row:0,section:0)
            //addBoatVC.delegate = self
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(addBoatVC, animated: true)
        }else{
            showLoginAlert(isFromMenu: false, shouldLogin: true, inViewController: self)
        }
    }

    func openUpdateBoatScreen(boat:Boat,indexPath:IndexPath) {
        let addBoatVC = AppStoryboard.Home.viewController(AddBoatViewController.self)
        addBoatVC.indexPath = indexPath
        addBoatVC.userBoat = boat
        addBoatVC.delegate = self
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pushViewController(addBoatVC, animated: true)
    }

    func loadBoatsFor(_ userID:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            boats.removeAll()
            self.boatsTableView.reloadData()
        }


        MarinaService.sharedInstance.getUserBoatList(userID, page: pageNumber, perPage: recordPerPage) { (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newBoatArray = response{
                if newBoatArray.count == 0{
                    self.pageNumber = self.pageNumber - 1
                }
                self.boats.append(contentsOf: newBoatArray)
                self.boatsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            self.boatsTableView.reloadData()

        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.boatsTableView.reloadData()
        MarinaService.sharedInstance.getUserBoatList(self.user.ID, page: self.pageNumber, perPage: self.recordsPerPage) { (response) in
            self.boats.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBoatsArray = response{
                self.boats.append(contentsOf: newBoatsArray)
            }
            self.boatsTableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (boats.count > 0) ? boats.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if boats.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Boat found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatListTableViewCell", for: indexPath) as! BoatListTableViewCell
            let boat = boats[indexPath.row]
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.sd_setImage(with: URL(string:boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = boat.name
            cell.lengthLabel.text = "\(boat.boatSize.length) ft."
            cell.widthLabel.text = "\(boat.boatSize.width) ft."
            cell.depthLabel.text = "\(boat.boatSize.depth) ft."
            cell.rightButtons = self.rightButtonsForTableCell()
            cell.rightSwipeSettings.transition = MGSwipeTransition.drag
            cell.delegate = self
            return cell
        }
    }






    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if boats.count == 0{
            return
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == boatsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        self.loadBoatsFor(self.user.ID, pageNumber: pageNumber, recordPerPage: recordsPerPage)
                    }
                }
            }
        }
        
    }
    
}

extension MyBoatsViewController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}


extension MyBoatsViewController : MGSwipeTableCellDelegate,AddBoatViewControllerDelegate{
    func addBoatViewController(_ viewController: AddBoatViewController, didUpdateBoat boat: Boat, at indexPath: IndexPath) {
        self.boats[indexPath.row] = boat
        self.boatsTableView.reloadData()
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        return true
    }

    func rightButtonsForTableCell() -> [MGSwipeButton]{
        let editColor = kApplicationBrownColor
        let deleteColor = kApplicationRedColor
        //configure edit view

        let editSwipeNib = Bundle.main.loadNibNamed("SwipeButtonView", owner: self, options: nil)
        let edit = editSwipeNib?[1] as! SwipeButton
        edit.buttonIcon.image = #imageLiteral(resourceName: "edit_icon")
        edit.buttonNameLabel.text = "Edit"
        edit.backgroundColor = editColor
        edit.callback = { (cell) -> Bool in
            self.onClickEditBaot(cell: cell)
            return true
        }

        let swipeDeleteNib = Bundle.main.loadNibNamed("SwipeButtonView", owner: self, options: nil)
        let delete = swipeDeleteNib?[1] as! SwipeButton
        delete.buttonIcon.image = #imageLiteral(resourceName: "delete_icon")
        delete.buttonNameLabel.text = "Delete"
        delete.backgroundColor = deleteColor
        delete.callback = { (cell) -> Bool in
            self.onClickDeleteBoat(cell: cell)
            return true
        }

        let buttonArray = [delete,edit]

        return buttonArray
    }

    func onClickEditBaot(cell: MGSwipeTableCell){
        if let indexPath = cell.tableViewIndexPath(self.boatsTableView)as IndexPath?{
            let boat = self.boats[indexPath.row]
            self.openUpdateBoatScreen(boat: boat, indexPath: indexPath)
        }
    }

    func onClickDeleteBoat(cell: MGSwipeTableCell){
        if let indexPath = cell.tableViewIndexPath(self.boatsTableView)as IndexPath?{
            let boat = self.boats[indexPath.row]
            if boat.totalBooking != 0{
                showAlertWith(viewController: self, message: "There \(boat.totalBooking > 1 ? "are" : "is" ) \(boat.totalBooking) upcoming \(boat.totalBooking > 1 ? "bookings" : "booking" ) for this boat\r\nUnable to delete", title:warningMessage.alertTitle.rawValue)
            }else{
                self.askToDeleteBaot(boat: boat, atIndexPath: indexPath)
            }
        }
    }

    func askToDeleteBaot(boat : Boat,atIndexPath indexPath: IndexPath) {
            let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Would you really want to delete this boat?", preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Delete", style: .destructive){(action) in
                alert.dismiss(animated: true, completion: nil)
                self.deleteBoat(boat: boat, atIndexPath: indexPath)
            }

            let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(cancelAction)
            alert.addAction(okayAction)
            self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func deleteBoat(boat:Boat,atIndexPath indexPath: IndexPath){
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Deleting..")
        let boatID = boat.ID
        self.boats.remove(at: indexPath.row)
        self.boatsTableView.reloadData()
        MarinaService.sharedInstance.deleteBoatWith(self.user.ID, boatID: boatID) { (success, message) in
            CommonClass.hideLoader()
            showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)

        }
    }


}

