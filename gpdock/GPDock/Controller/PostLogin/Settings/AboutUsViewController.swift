//
//  AboutUsViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
import UIKit

class AboutUsViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = "About Us"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.webView.delegate = self
        let request = URLRequest(url: URL(string: ABOUT_US_URL)!)
        CommonClass.showLoader(withStatus: "Loading")
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if CommonClass.isLoaderOnScreen{
            CommonClass.hideLoader()
        }
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        if CommonClass.isLoaderOnScreen{
            CommonClass.hideLoader()
        }
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        if !CommonClass.isLoaderOnScreen{
            CommonClass.showLoader(withStatus: "Loading")
        }
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension AboutUsViewController: SlideNavigationControllerDelegate{
    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}
