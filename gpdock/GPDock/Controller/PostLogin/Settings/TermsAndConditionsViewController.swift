//
//  TermsAndConditionsViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var isPrivacyPolicy : Bool = false
    var isFromMenu = true

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = isPrivacyPolicy ? "Privacy Policy" : "Terms of Use"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.webView.delegate = self
        self.setUpLeftBarButton()
        if let path = Bundle.main.path(forResource: isPrivacyPolicy ? "GPDockPrivacyPolicy" :"GPDockTerms", ofType: "docx"){
            self.webView.delegate = self
            self.webView.scalesPageToFit = true
            let targetUrl = URL(fileURLWithPath: path)
            let request = URLRequest(url: targetUrl)
            self.webView.loadRequest(request)
        }
//        let request = URLRequest(url: URL(string: TERMS_AND_CONDITIONS_URL)!)
//        self.webView.loadRequest(request)
    }


    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if !isFromMenu{
            self.dismiss(animated: true, completion: nil)
        }else{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if CommonClass.isLoaderOnScreen{
            CommonClass.hideLoader()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {

        if CommonClass.isLoaderOnScreen{
            CommonClass.hideLoader()
        }
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !CommonClass.isLoaderOnScreen{
            CommonClass.showLoader(withStatus: "Loading")
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            
            UIApplication.shared.openURL(request.url!)
            return false
        }
        return true
    }
    
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension TermsAndConditionsViewController: SlideNavigationControllerDelegate{
    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}
