//
//  SupportsViewController.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var faqs = Array<FAQModel>()
    var user : User!
    var booking:Booking?

    var isFromMenu : Bool = false
    @IBOutlet weak var supportTableView: UITableView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        refreshControl.addTarget(self, action: #selector(MarinaListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = "Support"
        self.navigationItem.titleView = self.titleView
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.user = User(json: User.loadUserInfo()!)
        self.supportTableView.register(UINib(nibName: "SupportTableViewCell", bundle: nil), forCellReuseIdentifier: "SupportTableViewCell")
        self.navigationController?.navigationBar.isHidden = false
        self.supportTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.supportTableView.addSubview(refreshControl)
        self.supportTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.supportTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.setUpLeftBarButton()
        self.getFAQsFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }

        MarinaService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            self.refreshControl.endRefreshing()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.removeAll()
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }
        }
    }
    func getFAQsFromServer(){
        CommonClass.showLoader(withStatus: "Loading")
        MarinaService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            CommonClass.hideLoader()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }
        }
    }

    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftbutton
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if !isFromMenu{
            self.navigationController?.pop(true)
        }else{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return faqs.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 160
        }else if indexPath.section == faqs.count+1 {
            return 127
        }else{
            return 70
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let faqVC = AppStoryboard.Settings.viewController(FAQViewController.self)
            let faqModel = faqs[indexPath.section-1]
            faqVC.faqModel = faqModel
            self.navigationController?.pushViewController(faqVC, animated: true)
        }else{return}
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportUserNameTableViewCell", for: indexPath) as! SupportUserNameTableViewCell
            cell.userNameLabel.text = "Hi " + (CommonClass.isLoggedIn ? self.user.firstName : "Guest")
            return cell
        }else if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportTableViewCell", for: indexPath) as! SupportTableViewCell
            let faqModel = faqs[indexPath.section-1]
            cell.questionTextLabel.text = faqModel.category.capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportNotListedTableViewCell", for: indexPath) as! SupportNotListedTableViewCell
            cell.reachUsButton.addTarget(self, action: #selector(SupportsViewController.onClickReachUs(_:)), for: .touchUpInside)
            return cell
        }
    }

    @IBAction func onClickReachUs(_ sender: UIButton){
        if CommonClass.isLoggedIn{
            self.navigateToWriteToUs()
        }else{
            showLoginAlert(isFromMenu: false, shouldLogin: true, inViewController: self)
        }
    }

    func navigateToWriteToUs() {
        let writeToUsVC = AppStoryboard.Settings.viewController(WriteToUsViewController.self)
        writeToUsVC.booking = self.booking
        self.navigationController?.pushViewController(writeToUsVC, animated: true)
    }
}

extension SupportsViewController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return isFromMenu
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}
