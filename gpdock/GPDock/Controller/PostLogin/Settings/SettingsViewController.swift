//
//  SettingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
let kEmergencyContact = "EmergencyContact"
let kEmergencyEmail = "EmergencyEmail"
let kLengthUnit = "LengthUnit"
let kShouldShowRatingPopUp = "ShouldShowRatingPopUp"
let kPreviousRatedVersion = "PreviousRatedVersion"
protocol SettingsViewControllerDelegate{
    func settingsViewController(viewController:SettingsViewController, didChangeSettings changed: Bool)
}

class SettingsViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,LoginViewDelegate {
    let settingArray = ["Emergency Contact:","Emergency Email"]//,"Unit"]
    let settingIcons = [#imageLiteral(resourceName: "emergency_phone"),#imageLiteral(resourceName: "emergency_mail")]


    @IBOutlet weak var settingTableView: UITableView!
    var unitPicker = UIPickerView()
    var unitTextField : UITextField!
    var unitArray = ["Foot"]//"Meters"]
    var loginView: LoginView!
    var user: User!
    var isFromMenu : Bool = true
    var delegate : SettingsViewControllerDelegate?
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = "Settings"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        self.setupViews()
        self.setUpLeftBarButton()
    }

    func setupUnitPicker(_ textField: UITextField) {
        unitPicker = UIPickerView()
        unitPicker.showsSelectionIndicator = true
        unitPicker.delegate = self
        unitPicker.dataSource = self
        unitTextField = textField
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.barTintColor = kNavigationColor
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SettingsViewController.onClickDoneOfUnitPicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        spaceButton.title = "Unit"

        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SettingsViewController.onClickCancelOfUnitPicker(_:)))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        unitTextField.inputView = unitPicker
        unitTextField.inputAccessoryView = toolBar
    }

    func setUpLeftBarButton() {
        let leftbutton = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
        leftbutton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftbutton
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if !isFromMenu{
            delegate?.settingsViewController(viewController: self, didChangeSettings: true)
            self.dismiss(animated: true, completion: nil)
        }else{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }

    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setUPContainer()
        }else{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view your Settings\r\nPlease Login or Sign up")
        }
    }

    func setUPContainer()  {
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        self.settingTableView.dataSource = self
        self.settingTableView.delegate = self
        self.settingTableView.reloadData()
    }

    @objc func onClickDoneOfUnitPicker(_ sender: UIBarButtonItem) {
        let row = self.unitPicker.selectedRow(inComponent: 0)
        self.unitTextField.text = unitArray[row]
        Settings.sharedInstance.lengthUnit = unitArray[row]
        self.unitTextField.resignFirstResponder()
    }

    //cancel inputView handling
    @objc func onClickCancelOfUnitPicker(_ sender: UIBarButtonItem) {
        self.unitTextField.resignFirstResponder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SettingsViewController:UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " Your Settings"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.placeHolderLabel.text = settingArray[indexPath.row]
        cell.icon.image = settingIcons[indexPath.row]
        cell.textField.addTarget(self, action: #selector(SettingsViewController.textFieldEdited(_:)), for: .editingChanged)
        switch indexPath.row {
        case 0:
            cell.textField.keyboardType = .phonePad
            cell.textField.placeholder = "+919012345678"
            cell.textField.autocorrectionType = .no
            cell.textField.text = Settings.sharedInstance.emergencyContact
            cell.textField.tintColor = UIColor.darkText
        case 1:
            cell.textField.keyboardType = .emailAddress
            cell.textField.placeholder = "email@example.com"
            cell.textField.autocorrectionType = .no
            cell.textField.tintColor = UIColor.darkText
            cell.textField.text = Settings.sharedInstance.emergencyEmail
        case 2:
            cell.textField.keyboardType = .default
            cell.textField.autocorrectionType = .no
            cell.textField.tintColor = UIColor.clear
            if Settings.sharedInstance.lengthUnit.count == 0{
                Settings.sharedInstance.lengthUnit = "Foot"
            }
            cell.textField.text = Settings.sharedInstance.lengthUnit
            //self.unitTextField = cell.textField
            self.setupUnitPicker(cell.textField)
        default:
            break

        }
        cell.textField.delegate = self
        return cell
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(settingTableView) as IndexPath?{
            switch indexPath.row {
            case 0:
                Settings.sharedInstance.emergencyContact = textField.text!
            case 1:
                Settings.sharedInstance.emergencyEmail = textField.text!
            case 2:
                Settings.sharedInstance.lengthUnit = textField.text!
            default:
                print_debug("")
            }
            self.settingTableView.reloadData()
        }
    }


    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(settingTableView) as IndexPath?{
            switch indexPath.row {
            case 0:
                Settings.sharedInstance.emergencyContact = textField.text!
            case 1:
                Settings.sharedInstance.emergencyEmail = textField.text!
            case 2:
                Settings.sharedInstance.lengthUnit = textField.text!
            default:
                print_debug("")
            }
            //self.settingTableView.reloadData()
        }
    }


//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        var result = false
//        if let indexPath = textField.tableViewIndexPath(settingTableView) as IndexPath?{
//            switch indexPath.row {
//            case 0:
//                result = (string.characters.count > 10)
//            default:
//                result = false
//            }
//        }
//        return result
//    }


    @IBAction func textFieldEdited(_ textField : UITextField){
        if let indexPath = textField.tableViewIndexPath(settingTableView) as IndexPath?{
            switch indexPath.row {
            case 0:
                Settings.sharedInstance.emergencyContact = textField.text!
            case 1:
                Settings.sharedInstance.emergencyEmail = textField.text!
            case 2:
                Settings.sharedInstance.lengthUnit = textField.text!
            default:
                break;
            }
        }
    }
}
extension SettingsViewController: SlideNavigationControllerDelegate{


    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return unitArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return unitArray[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        unitTextField.text =  unitArray[row]
    }

    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return self.isFromMenu
    }

    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}


class Settings {
    static let sharedInstance = Settings()
    fileprivate init() {}
    var emergencyContact : String{
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kEmergencyContact) as? String{
                result = r
            }
            return result
        }

        set(newEmergencyContact){
            kUserDefaults.set(newEmergencyContact, forKey: kEmergencyContact)
        }
    }

    var emergencyEmail : String{
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kEmergencyEmail) as? String{
                result = r
            }
            return result
        }
        set(newEmergencyEmail){
            kUserDefaults.set(newEmergencyEmail, forKey: kEmergencyEmail)
        }
    }

    var lengthUnit : String {
        get {
            var result = "Foot"
            if let r = kUserDefaults.value(forKey:kLengthUnit) as? String{
                result = r
            }
            return result
        }
        set(newLengthUnit){
            kUserDefaults.set(newLengthUnit, forKey: kLengthUnit)
        }
    }

    var previousRatedVersion : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPreviousRatedVersion) as? String{
                result = r
            }
            return result
        }
        set(newpreviousRatedVersion){
            kUserDefaults.set(newpreviousRatedVersion, forKey: kPreviousRatedVersion)
        }
    }


    var shouldShowRatingPopUp : Bool {
        get {
            var result = true
            
            if let r = kUserDefaults.bool(forKey:kShouldShowRatingPopUp) as Bool?{
                result = r
            }
            return result
        }
        set(newShouldShowRatingPopUp){
            kUserDefaults.set(newShouldShowRatingPopUp, forKey: kShouldShowRatingPopUp)
        }
    }
    
    func reset(){
        kUserDefaults.set("", forKey: kEmergencyContact)
        kUserDefaults.set("", forKey: kLengthUnit)
        kUserDefaults.set("", forKey: kEmergencyEmail)
        kUserDefaults.set(true, forKey: kShouldShowRatingPopUp)


    }
    
}


