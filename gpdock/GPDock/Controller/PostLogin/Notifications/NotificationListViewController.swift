//
//  NotificationListViewController.swift
//  GPDock
//
//  Created by TecOrb on 13/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit


class NotificationListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,LoginViewDelegate {
    var notifications = Array<NotificationModel>()
    var isFromMenu = false
    var titleView : NavigationTitleView!

    @IBOutlet weak var notificationsTableView : UITableView!
    var loginView : LoginView!
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var selectedIndex = -1
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(NotificationListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = UIColor.groupTableViewBackground

        return refreshControl
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if notificationsTableView != nil{
            notificationsTableView.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
         self.notificationsTableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        self.notificationsTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.notificationsTableView.backgroundColor = UIColor.groupTableViewBackground
        self.notificationsTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)

        NotificationCenter.default.addObserver(self, selector: #selector(NotificationListViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationListViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(NotificationListViewController.refreshNotificationHandler(_:)), name: .REFRESH_NOTIFICATION_LIST_NOTIFICATION, object: nil)

        self.setupNavigationTitle()
        self.setupViews()
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Notifications".uppercased()
        self.navigationItem.titleView = self.titleView
    }


    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        GPDock.openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }
    
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    @objc func refreshNotificationHandler(_ notification: Notification) {
        if CommonClass.isLoggedIn{
            self.pageNumber = 1
            let userJSON = User.loadUserInfo()
            self.user = User(json: userJSON!)
            self.loadNotificationsFor(self.user.ID,pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        }
    }

    func setupViews(){
        self.setupLeftBarButtons(isFromMenu: self.isFromMenu)
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupContainer()
        }else{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view Notifications\r\nPlease Login or Sign up")
        }
    }
    func setupContainer(){
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        self.pageNumber = 1
        self.notifications.removeAll()
        self.notificationsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.notificationsTableView.dataSource = self
        self.notificationsTableView.delegate = self
        self.notificationsTableView.addSubview(self.refreshControl)
        self.loadNotificationsFor(self.user.ID,pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    func setupLeftBarButtons(isFromMenu:Bool) {
        let menuButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        menuButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        menuButton.setImage(isFromMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "back"), for: .normal)
        menuButton.addTarget(self, action: #selector(onclickBackButton(_:)), for: .touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        self.navigationItem.setLeftBarButtonItems([menuBarButton], animated: false)
    }

    func setupRightBarButtons() {
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        if self.notifications.count == 0{return}
        let deleteAllButton = UIButton(type: .system)
        deleteAllButton.frame = CGRect(x:0, y:0, width:35, height:35)
        deleteAllButton.tintColor = kApplicationRedColor
        deleteAllButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        deleteAllButton.setImage(#imageLiteral(resourceName: "delete_icon"), for: .normal)
        deleteAllButton.addTarget(self, action: #selector(onclickDeleteAllNotificationsButton(_:)), for: .touchUpInside)
        let deleteBarButton = UIBarButtonItem(customView: deleteAllButton)
        self.navigationItem.setRightBarButtonItems([deleteBarButton], animated: false)
    }

    @IBAction func onclickDeleteAllNotificationsButton(_ sender : UIButton){
        self.askToClearNotification()
    }

    func askToClearNotification(){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Are you sure to clear all the notifications?", preferredStyle: .alert)
        let weakAlert = alert
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { [weakAlert](action) in
            self.clearAllNotifications()
            weakAlert.dismiss(animated: true, completion: nil)
        }

        let nopeAction = UIAlertAction(title: "Nope", style: .cancel){ [weakAlert](action) in
            weakAlert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesAction)
        alert.addAction(nopeAction)
        self.present(alert, animated: true, completion: nil)
    }

    func clearAllNotifications() {
        CommonClass.showLoader(withStatus: "Please wait..")
        self.user.clearAllNotifications { (done) in
            CommonClass.hideLoader()
            if done{
                self.pageNumber = 1
                self.notifications.removeAll()
                self.notificationsTableView.reloadData()
                CommonClass.showSuccess(withStatus: "Done")
                self.setupRightBarButtons()
            }
        }
    }



    @IBAction func onclickBackButton(_ sender : UIButton){
        if isFromMenu{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }else{
           self.navigationController?.pop(true)
        }
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
    }


    func loadNotificationsFor(_ userID:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isNewDataLoading = true
        if pageNumber == 1{
            notifications.removeAll()
            if notificationsTableView != nil{
                notificationsTableView.reloadData()
            }
        }

        NotificationService.sharedInstance.getNotificationsForUser(userID,page:pageNumber,perPage: recordPerPage) { (success,response,message) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            self.isNewDataLoading = false
            if let newNotificationArray = response{
                if newNotificationArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.notifications.append(contentsOf: newNotificationArray)
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if self.notificationsTableView != nil{
                self.notificationsTableView.reloadData()
            }
            self.setupRightBarButtons()
        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        CommonClass.sharedInstance.clearAllPendingRequests()
        pageNumber = 1
        self.notifications.removeAll()
        self.isNewDataLoading = true
        self.notificationsTableView.reloadData()
        NotificationService.sharedInstance.getNotificationsForUser(self.user.ID,page: self.pageNumber,perPage: self.recordsPerPage) { (success,response,message) in
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newNotificationsArray = response{
                self.notifications.append(contentsOf: newNotificationsArray)
            }
            if self.notificationsTableView != nil{
                self.notificationsTableView.reloadData()
            }
            self.setupRightBarButtons()
        }
    }

    //for new notification comes into queue
    @objc func bookingDidCancelledByUser(_ notification: Notification) {
        /*
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                self.bookings.insert(removingBooking, at: 0)
                self.bookingsTableView.reloadData()
            }
        } */
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (notifications.count > 0) ? notifications.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  94 //UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if notifications.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No new notification found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
            let notification = notifications[indexPath.row]
             if notification.notificationType == kReservationRemind {
                cell.iconImageView.sd_setImage(with: URL(string:notification.pendingInvoice.marina.image.url))

            }else{
            cell.iconImageView.sd_setImage(with: URL(string:notification.booking.marina.image.url))

            }
            cell.iconImageView.setShowActivityIndicator(true)
            cell.iconImageView.setIndicatorStyle(.gray)
            cell.messageLabel.text = notification.message
            cell.timeOffSetLabel.text = notification.timeOffset
            cell.timeOffSetLabel.isGradientApplied = !notification.status
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notifications.count == 0{
            return
        }
        let notification = self.notifications[indexPath.row]
        notification.didReadbyUser(userID: self.user.ID) {(didRead) in
            if didRead{
                notification.status = true
            }
        }
        let booking = notification.booking

        
        if notification.notificationType == kReservationRemind {
          self.navigateToMarinaScreen(marina: notification.pendingInvoice.marina )
        }else{
            if booking.ID == "" || notification.notificationType == kSupportReplyByAdmin{
                return
            }else{
            self.navigateToBookingDetails(booking: booking)
            }
        }

    }

    func navigateToMarinaScreen(marina: Marina) -> Void {
        let marinaScreenVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaScreenVC.marina = marina
        self.navigationController?.pushViewController(marinaScreenVC, animated: true)
        
    }
    
    func navigateToBookingDetails(booking: Booking) -> Void {
        if !booking.isCancelled && !booking.isCompleted {
            let bookingDetailVC = AppStoryboard.Booking.viewController(UpcomingBookingDetailViewController.self)
            bookingDetailVC.booking = booking
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }else if booking.isCancelled && !booking.isCompleted{
            let bookingDetailVC = AppStoryboard.Booking.viewController(CancelledBookingDetailViewController.self)
            bookingDetailVC.booking = booking
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }else if !booking.isCancelled && booking.isCompleted{
            let bookingDetailVC = AppStoryboard.Booking.viewController(CompletedBookingDetailViewController.self)
            bookingDetailVC.booking = booking
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }else{
            showAlertWith(viewController: self, message: "A database error occourred\r\nPlease write us in support section", title: "Database Error!")
        }
    }



    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == notificationsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if self.notifications.count == 0{pageNumber=1}else{pageNumber+=1}
                        self.loadNotificationsFor(self.user.ID, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }

    }


}



