//
//  FiltersViewController.swift
//  GPDock
//
//  Created by TecOrb on 15/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
//FiltersViewController

protocol FiltersViewControllerDelegate {
    func filterViewController(_ viewController: FiltersViewController, didSelectFilter filters:(shouldShowNearby:Bool,userRating:Float,minPriceRange:Double,maxPriceMax:Double,marinaName:String,marinaType:MarinaTypeFilter,service:Set<Service>,amenities:Set<Aminity>))
    func filterViewController(_ viewController: FiltersViewController, didClearSelectedFilters cleared: Bool)
}

struct MarinaTypeFilter {
    var privateSeleted = true
    var publicSelected = true
    var filterTypeName :String{
        get{
            if (!self.privateSeleted && !self.publicSelected){
                return ""
            }else if (!self.privateSeleted && self.publicSelected){
                return "Public"
            } else if (self.privateSeleted && !self.publicSelected){
                return "Private"
            } else{return "Both"}
        }
    }
    init() {
        self.privateSeleted = true
        self.publicSelected = true
    }
    init(_ privateSelected:Bool,and publicSelected:Bool) {
        self.privateSeleted = privateSelected
        self.publicSelected = publicSelected
    }
}



class FiltersViewController: UIViewController {
    var sections = ["User Rating","Price(per foot)","Services","Amenities","Marina Type","Search Marina Name"]
    var delegate : FiltersViewControllerDelegate?
    var services = Array<Service>()
    var amenities = Array<Aminity>()
    var selectedSection: Int = -1
    var minPriceRange: Double = 0
    var maxPriceRange:Double = 10
    var sliderMaxValue: Double = 10
    var sliderMinValue:Double = 0
    var showNearByFirst = true
    var filteredUserRating: Float = 0
    var locationFilterType = LocationFilterType.nationWide
    var titleView : NavigationTitleView!
    var city : City!
    var marinaTypeFilter = MarinaTypeFilter()
    var marinaName = ""
    var selectedServices = Set<Service>()
    var selectedAmenities = Set<Aminity>()



    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyfilterButton: UIButton!

    var sizeOfBoats = { () -> Array<AHTag> in
        var tags = Array<AHTag>()
        tags.append(AHTag(dictionary: ["title":"20-30"]))
        tags.append(AHTag(dictionary: ["title":"31-40"]))
        tags.append(AHTag(dictionary: ["title":"41-50"]))
        tags.append(AHTag(dictionary: ["title":"50+"]))
        return tags
    }

    var filters:(shouldShowNearby:Bool,userRating:Float,minPriceRange:Double,maxPriceMax:Double,marinaName:String, marinaType:MarinaTypeFilter,service:Set<Service>,amenities:Set<Aminity>)?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupFilter()
        self.registerCells()
        self.getAllServices()
        self.getAllAmenities()
        self.filterTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.setupNavigationTitle()
        self.filterTableView.reloadData()
    }

    func setupFilter(){
        if let previousFilters = self.filters{
            self.showNearByFirst = previousFilters.shouldShowNearby
            self.filteredUserRating = previousFilters.userRating
            self.minPriceRange = previousFilters.minPriceRange
            self.maxPriceRange = previousFilters.maxPriceMax

            self.selectedServices =  previousFilters.service
            self.selectedAmenities = previousFilters.amenities
            self.marinaName = previousFilters.marinaName
            self.marinaTypeFilter = previousFilters.marinaType
            if (self.city.marinaMaxPrice == 0){
                self.sliderMinValue = 0
                self.sliderMaxValue = 10
            }else if city.marinaMinPrice == city.marinaMaxPrice{
                self.sliderMinValue = 0
                self.sliderMaxValue = (city.marinaMaxPrice > 0) ? city.marinaMaxPrice : 10
            }else{
                self.sliderMinValue = self.city.marinaMinPrice
                self.sliderMaxValue = self.city.marinaMaxPrice
            }
        }else{
            self.showNearByFirst = true
            self.filteredUserRating = 0.0
            self.marinaName = ""
            self.selectedServices = Set<Service>()
            self.selectedAmenities = Set<Aminity>()

            if (self.city.marinaMaxPrice == 0){
                self.minPriceRange = 0
                self.maxPriceRange = 10
                self.sliderMinValue = 0
                self.sliderMaxValue = 10
            }else if city.marinaMinPrice == city.marinaMaxPrice{
                self.minPriceRange = 0
                self.maxPriceRange = (city.marinaMaxPrice > 0) ? city.marinaMaxPrice : 10
                self.sliderMinValue = 0
                self.sliderMaxValue = (city.marinaMaxPrice > 0) ? city.marinaMaxPrice : 10
            }else{
                self.minPriceRange = self.city.marinaMinPrice
                self.maxPriceRange =  self.city.marinaMaxPrice
                self.sliderMinValue = self.city.marinaMinPrice
                self.sliderMaxValue = self.city.marinaMaxPrice
            }
        }
    }
    override func viewDidLayoutSubviews() {
        self.applyfilterButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Filters"
        self.navigationItem.titleView = self.titleView
    }
    @IBAction func onClickCancel(_ sender: UIBarButtonItem){
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func onClickClearFilter(_ sender: UIBarButtonItem){
        self.filters = nil
        self.setupFilter()
        delegate?.filterViewController(self, didClearSelectedFilters: true)
        self.filterTableView.reloadData()
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func onClickDone(_ sender: UIButton){
        let filters = (shouldShowNearby:self.showNearByFirst,userRating:self.filteredUserRating,minPriceRange:Double(self.minPriceRange),maxPriceMax:Double(maxPriceRange),marinaName:self.marinaName,marinaType:self.marinaTypeFilter,service:self.selectedServices,amenities:self.selectedAmenities)
        self.delegate?.filterViewController(self, didSelectFilter: filters)
        self.dismiss(animated: false, completion: nil)
    }

    func registerCells(){


        self.filterTableView.register(UINib(nibName: "MarinaTypeFilterCell", bundle: nil), forCellReuseIdentifier: "MarinaTypeFilterCell")

        self.filterTableView.register(UINib(nibName: "ShowNearByCell", bundle: nil), forCellReuseIdentifier: "ShowNearByCell")

        self.filterTableView.register(UINib(nibName: "FilterMarinaNameCell", bundle: nil), forCellReuseIdentifier: "FilterMarinaNameCell")

        self.filterTableView.register(UINib(nibName: "AHTagTableViewCell", bundle: nil), forCellReuseIdentifier: "AHTagTableViewCell")
        self.filterTableView.register(UINib(nibName: "FilterHeaderCell", bundle: nil), forCellReuseIdentifier: "FilterHeaderCell")
        self.filterTableView.register(UINib(nibName: "FilterListCell", bundle: nil), forCellReuseIdentifier: "FilterListCell")
        self.filterTableView.register(UINib(nibName: "DateSelectorCell", bundle: nil), forCellReuseIdentifier: "DateSelectorCell")
        self.filterTableView.register(UINib(nibName: "RatingFilterCell", bundle: nil), forCellReuseIdentifier: "RatingFilterCell")
        self.filterTableView.register(UINib(nibName: "PriceRangeFilterCell", bundle: nil), forCellReuseIdentifier: "PriceRangeFilterCell")
        self.filterTableView.register(UINib(nibName: "LocationFilterCell", bundle: nil), forCellReuseIdentifier: "LocationFilterCell")
    }

    func getAllServices() {
        MarinaService.sharedInstance.getAllServicesFromServer(completionBlock: { (resServices) in
            if let _services = resServices as Array<Service>?{
                self.services = _services
                //self.selectedServices = Set(self.services)
                //self.filterTableView.reloadData()
            }
        })
    }

    func getAllAmenities() {
        MarinaService.sharedInstance.getAllAmenitiesFromServer(completionBlock: { (resAmenities) in
            if let _amenities = resAmenities as Array<Aminity>?{
                self.amenities = _amenities
               // self.selectedAmenities = Set(self.amenities)
               // self.filterTableView.reloadData()
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FiltersViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0 || section == 1 || section == 4 || section == 5){
            return (section == self.selectedSection) ? 2 : 1
        }else if section == 2{
            return (section == self.selectedSection) ? self.services.count+1 : 1
        }else if section == 3{
            return (section == self.selectedSection) ? self.amenities.count+1 : 1
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            return self.ratingFilterCellFor(tableView, cellForRowAt: indexPath)
        }else if indexPath.section == 1{
            return self.priceFilterCellFor(tableView, cellForRowAt: indexPath)
        }else if indexPath.section == 5{
            return self.searchForMarinaNameCellFor(tableView, cellForRowAt: indexPath)
        }else if indexPath.section == 2{
            return self.serviceFilterCellFor(tableView, cellForRowAt: indexPath)
        }else if indexPath.section == 4{
            return self.marinaTypeFilterCellFor(tableView, cellForRowAt: indexPath)
        }else{
            return self.amenityFilterCellFor(tableView, cellForRowAt: indexPath)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if (indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 4 || indexPath.section == 5){
            if self.selectedSection == indexPath.section{
                if indexPath.row != 0{
                    return
                }
                self.selectedSection = -1
                tableView.reloadData()
                return
            }
            if indexPath.row == 0 {
                self.selectedSection = indexPath.section
            }
        }else{
            if indexPath.row == 0{
                if self.selectedSection == indexPath.section{
                    self.selectedSection = -1
                    tableView.reloadData()
                    return
                }
                self.selectedSection = indexPath.section
            }else{
                if indexPath.section == 2{
                    //selected services
                    let isContains = self.isContains(self.services[indexPath.row-1], in: self.selectedServices)
                    if isContains{
                        self.remove(service: self.services[indexPath.row-1], from: &self.selectedServices)
                    }else{
                        self.selectedServices.insert(self.services[indexPath.row-1])
                    }
                }else{
                    //selected Amenities
                    let isContains = self.isContains(self.amenities[indexPath.row-1], in: self.selectedAmenities)
                    if isContains{
                        self.remove(amenity: self.amenities[indexPath.row-1], from: &self.selectedAmenities)
                    }else{
                        self.selectedAmenities.insert(self.amenities[indexPath.row-1])
                    }
                }
            }
        }
        tableView.reloadData()
    }

    //For section 2
    func showNearByCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            cell.selectionLabel.text = showNearByFirst ? "Yes" : "No"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowNearByCell", for: indexPath) as! ShowNearByCell
            cell.toggleSwitch.isOn = self.showNearByFirst
            cell.toggleSwitch.addTarget(self, action: #selector(onSwitchToggle(_:)), for: .valueChanged)
            return cell
        }
    }
    func searchForMarinaNameCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            cell.selectionLabel.text = self.marinaName
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterMarinaNameCell", for: indexPath) as! FilterMarinaNameCell
            cell.marinaNameTextField.delegate = self
            cell.marinaNameTextField.text = self.marinaName
            cell.marinaNameTextField.addTarget(self, action: #selector(marinaNameDidChangedText(_:)), for: .editingChanged)
            return cell
        }
    }

    //For section 0
    func ratingFilterCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            cell.selectionLabel.text = (self.filteredUserRating > 0.0) ? "\(self.filteredUserRating)" : ""
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingFilterCell", for: indexPath) as! RatingFilterCell
            cell.ratingView.rating = self.filteredUserRating
            cell.ratingView.delegate = self
            return cell
        }
    }

    //For MarinaType Filter
    func marinaTypeFilterCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            cell.selectionLabel.text = self.marinaTypeFilter.filterTypeName
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaTypeFilterCell", for: indexPath) as! MarinaTypeFilterCell
            cell.publicButton.isSelected = self.marinaTypeFilter.publicSelected
            cell.privateButton.isSelected = self.marinaTypeFilter.privateSeleted
            cell.publicButton.addTarget(self, action: #selector(onClickPublicMarinaTypeFilter(_:)), for: .touchUpInside)
            cell.privateButton.addTarget(self, action: #selector(onClickPrivateMarinaTypeFilter(_:)), for: .touchUpInside)
            return cell
        }
    }

    //For section 1
    func priceFilterCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            let minprice = String(format: "$ %0.2f", minPriceRange)
            let maxprice = String(format: "$ %0.2f", maxPriceRange)
            cell.selectionLabel.text = "\(minprice) - \(maxprice)"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceRangeFilterCell", for: indexPath) as! PriceRangeFilterCell
            self.sliderSetup(cell.priceSliderView)
            return cell
        }
    }


    //For section 2
    func serviceFilterCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            let servicesArray : Array<Service> = Array(selectedServices)
            if servicesArray.count == 0{
                cell.selectionLabel.text = ""
            }else if servicesArray.count == 1{
                cell.selectionLabel.text = "\(servicesArray[0].title)"
            }else if servicesArray.count == 2{
                let first = "\(servicesArray[0].title)"
                let second = "\(servicesArray[1].title)"
                cell.selectionLabel.text =  first+" & "+second
            }else{
                let first = "\(servicesArray[0].title)"
                cell.selectionLabel.text =  first+" & "+"\(servicesArray.count-1) more"
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterListCell", for: indexPath) as! FilterListCell
            cell.filterDataLabel.text = services[indexPath.row-1].title
            cell.tickIcon.isHidden = !self.selectedServices.contains(services[indexPath.row-1])
            return cell
        }
    }
    //For section 3
    func amenityFilterCellFor(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell", for: indexPath) as! FilterHeaderCell
            cell.headerIcon.image = (indexPath.section == self.selectedSection) ? #imageLiteral(resourceName: "minus_gradient") : #imageLiteral(resourceName: "plus_gradient")
            cell.headerTitleLabel.text = sections[indexPath.section]
            let amenitiesArray : Array<Aminity> = Array(selectedAmenities)
            if amenitiesArray.count == 0{
                cell.selectionLabel.text = ""
            }else if amenitiesArray.count == 1{
                cell.selectionLabel.text = "\(amenitiesArray[0].title)"
            }else if amenitiesArray.count == 2{
                let first = "\(amenitiesArray[0].title)"
                let second = "\(amenitiesArray[1].title)"
                cell.selectionLabel.text =  first+" & "+second
            }else{
                let first = "\(amenitiesArray[0].title)"
                cell.selectionLabel.text =  first+" & "+"\(amenitiesArray.count-1) more"
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterListCell", for: indexPath) as! FilterListCell
            cell.filterDataLabel.text = amenities[indexPath.row-1].title
            let shouldHide = !self.isContains(amenities[indexPath.row-1], in: selectedAmenities)
            cell.tickIcon.isHidden = shouldHide
            return cell
        }
    }

    func isContains(_ amenity:Aminity,in selectedAmenities:Set<Aminity>) -> Bool{
        let amenitiesArray : Array<Aminity> = Array(selectedAmenities)
        return (amenitiesArray.filter({ (cAmenity) -> Bool in
            return cAmenity.ID == amenity.ID
        }).count != 0)
    }

    func isContains(_ service:Service,in selectedServices:Set<Service>) -> Bool{
        let serviceArray : Array<Service> = Array(selectedServices)
        return (serviceArray.filter({ (cService) -> Bool in
            return cService.ID == service.ID
        }).count != 0)
    }

    func remove(service: Service,from selectedServices:inout Set<Service>){
        for (index,cService) in selectedServices.enumerated(){
            if cService.ID == service.ID{
                selectedServices.remove(at: selectedServices.index(selectedServices.startIndex, offsetBy: index))
            }
        }
    }

    func remove(amenity: Aminity, from selectedAmenities:inout Set<Aminity>){
        for (index,cAmenity) in selectedAmenities.enumerated(){
            if cAmenity.ID == amenity.ID{
                selectedAmenities.remove(at: selectedAmenities.index(selectedAmenities.startIndex, offsetBy: index))
            }
        }
    }

    func sliderSetup(_ priceSliderView: NHRangeSliderView) {
        priceSliderView.delegate = self
        priceSliderView.thumbSize = 35

        priceSliderView.trackHighlightTintColor = UIColor.black
        priceSliderView.maximumValue = self.sliderMaxValue
        priceSliderView.minimumValue = self.sliderMinValue
        priceSliderView.lowerValue = self.minPriceRange
        priceSliderView.upperValue = self.maxPriceRange

        priceSliderView.gapBetweenThumbs = 1
        priceSliderView.thumbLabelStyle = .FOLLOW
        priceSliderView.titleLabel?.text = "Price Range"
        priceSliderView.lowerDisplayStringFormat = "$%.2f"
        priceSliderView.upperDisplayStringFormat = "$%.2f"
        priceSliderView.setNeedsLayout()
        priceSliderView.layoutIfNeeded()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return (indexPath.row == 0) ? 44 : 70
        }else if indexPath.section == 1{
            return (indexPath.row == 0) ? 44 : 84//100
        }else if indexPath.section == 4{
            return (indexPath.row == 0) ? 44 : 44
        }else if indexPath.section == 2{
            return (indexPath.row == 0) ? 44 : 44
        }else{
            return (indexPath.row == 0) ? 44 : 44
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return (indexPath.row == 0) ? 44 : 70
        }else if indexPath.section == 1{
            return (indexPath.row == 0) ? 44 : 84//100
        }else if indexPath.section == 4{
            return (indexPath.row == 0) ? 44 : 44
        }else if indexPath.section == 2{
            return (indexPath.row == 0) ? 44 : 44
        }else{
            return (indexPath.row == 0) ? 44 : 44
        }
    }

}
extension FiltersViewController: NHRangeSliderViewDelegate,NKFloatRatingViewDelegate,UITextFieldDelegate{

    func floatRatingView(ratingView: NKFloatRatingView, didUpdate rating: Float) {
        self.filteredUserRating = rating
        self.filterTableView.reloadData()
    }


    @IBAction func onClickNationWideFilter(_ sender: UIButton){
        self.locationFilterType = .nationWide
        if let indexpath = sender.tableViewIndexPath(self.filterTableView) as IndexPath?{
            if let cell = self.filterTableView.cellForRow(at: indexpath) as? LocationFilterCell{
                cell.toggleButtons(sender)
            }
        }

    }
    @IBAction func onClickCityFilter(_ sender: UIButton){
        self.locationFilterType = .city
        if let indexpath = sender.tableViewIndexPath(self.filterTableView) as IndexPath?{
            if let cell = self.filterTableView.cellForRow(at: indexpath) as? LocationFilterCell{
                cell.toggleButtons(sender)
            }
        }

    }

    @IBAction func onClickPrivateMarinaTypeFilter(_ sender: UIButton){
        self.marinaTypeFilter.privateSeleted = !self.marinaTypeFilter.privateSeleted
        self.filterTableView.reloadData()
    }

    @IBAction func onClickPublicMarinaTypeFilter(_ sender: UIButton){
        self.marinaTypeFilter.publicSelected = !self.marinaTypeFilter.publicSelected
        self.filterTableView.reloadData()
    }



    @IBAction func onSwitchToggle(_ sender: UISwitch){
        self.showNearByFirst = sender.isOn
        self.filterTableView.reloadData()
    }

    @IBAction func marinaNameDidChangedText(_ textField: UITextField){
        self.marinaName = textField.text ?? ""
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.marinaName = textField.text ?? ""
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.marinaName = textField.text ?? ""
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            let filters = (shouldShowNearby:self.showNearByFirst,userRating:self.filteredUserRating,minPriceRange:Double(self.minPriceRange),maxPriceMax:Double(maxPriceRange),marinaName:self.marinaName,marinaType:self.marinaTypeFilter,service:self.selectedServices,amenities:self.selectedAmenities)
            self.delegate?.filterViewController(self, didSelectFilter: filters)
            self.dismiss(animated: false, completion: nil)
        }
        return false
    }

    @IBAction func onClickStateFilter(_ sender: UIButton){
        self.locationFilterType = .state
        if let indexpath = sender.tableViewIndexPath(self.filterTableView) as IndexPath?{
            if let cell = self.filterTableView.cellForRow(at: indexpath) as? LocationFilterCell{
                cell.toggleButtons(sender)
            }
        }
    }

    func updatePriceOnFilterHeader(){
        let minprice = String(format: "$ %0.2f", minPriceRange)
        let maxprice = String(format: "$ %0.2f", maxPriceRange)
        if let priceHeaderCell = self.filterTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? FilterHeaderCell{
            priceHeaderCell.selectionLabel.text = "\(minprice) - \(maxprice)"
        }

    }


    func sliderValueChanged(slider: NHRangeSlider?) {
        if let sliderP = slider{
            self.maxPriceRange = sliderP.upperValue
            self.minPriceRange =  sliderP.lowerValue
            self.updatePriceOnFilterHeader()
        }
    }
}
