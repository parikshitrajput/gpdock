//
//  SortingSelectionViewController.swift
//  GPDock
//
//  Created by TecOrb on 07/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

protocol SortingSelectionViewControllerDelegate {
    func sort(viewController: SortingSelectionViewController,sortingKey:String,orderBy:String)
}

class SortingSelectionViewController: UIViewController {
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var cancelledButton : UIButton!

    var delegate: SortingSelectionViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithRespectToHeight(self.cancelledButton, borderColor: .clear, borderWidth: 0)
        self.containnerView.addshadow(top: true, left: false, bottom: false, right: false)
        self.cancelledButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view {
                UIView.animate(withDuration: 0.1, animations: {
                    self.view.alpha = 0.0
                }) { (done) in
                    if done{
                        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: false, completion: nil)
                    }
                }
            } else {
                return
            }

        }
    }
    @IBAction func onPriceHighToLow(_ sender: UIButton){
        delegate?.sort(viewController: self, sortingKey: "price", orderBy: "DESC")
        self.dismissSortingVC()
    }

    @IBAction func onPriceLowToHigh(_ sender: UIButton){
        delegate?.sort(viewController: self, sortingKey: "price", orderBy: "ASC")
        self.dismissSortingVC()
    }

    @IBAction func onReviews(_ sender: UIButton){
        delegate?.sort(viewController: self, sortingKey: "Reviews", orderBy: "DESC")
        self.dismissSortingVC()
    }

    @IBAction func onDistance(_ sender: UIButton){
        delegate?.sort(viewController: self, sortingKey: "Nearby", orderBy: "ASC")
        self.dismissSortingVC()
    }

    @IBAction func onCancelled(_ sender: UIButton){
        self.dismissSortingVC()

       // (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    func dismissSortingVC() {
        UIView.animate(withDuration: 0.1, animations: {
            self.view.alpha = 0.0
        }) { (done) in
            if done{
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: false, completion: nil)
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
