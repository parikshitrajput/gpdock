//
//  ReviewBookingDetailsViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ReviewBookingDetailsViewController: UIViewController {
    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var promotextField: UITextField!

     //var pagerCV: UICollectionView!
     var currentPage = 0
    var selectedIndex: Int = -1
    var offerView: OffersHeaderView!
    var offers = Array<OfferModal>()
    var discountPrice: Double?
    var selectedOffer : OfferModal?
    var pageNumber = 1
    var recordsPerPage = 5
    var isNewDataLoading = false
    var user : User!
    var marina: Marina!
    var bookingDates: BookingDates!
    var selectedSize : BoatSize!
    var boat : ParkingSpace!
    var priceToBePaid : Double!
    var userOwnBoat : Boat!
    var marinaPriceReview:MarinaPriceReview?
    var navigationTitleView : NavigationTitleView!
    var countForPriceDetails = 2
    let header = ["Marina Details","Slip Details","Boat Details","Booking Dates","Price Details","Cancellation Policy"]
    let data = ["Marina Details":["Name","Address"],
                "Booking Dates": ["From","To"],
                "Slip Details":["Slip Number","Rate"],
                "Boat Details":["Boat Name","Boat Size"],
                "Price Details":["Cost per night","Total Amount"],
                "Cancellation Policy":["Full refund 3 days prior to arrival. You will be charged full price if you cancel after that."]
    ]


    override func viewDidLoad() {
        super.viewDidLoad()
//        self.aTableView.dataSource = nil
//        self.aTableView.delegate = nil
        self.user = User(json: User.loadUserInfo()!)
        let headerCellNib = UINib(nibName: "HeaderTableViewCell", bundle: nil)
        self.aTableView.register(headerCellNib, forCellReuseIdentifier: "HeaderTableViewCell")

        self.aTableView.estimatedRowHeight = 44
        self.setUpNavigationView()
        if let _ = self.marinaPriceReview{
            self.countForPriceDetails = self.rowsCountForPriceDetailsSection()
            self.aTableView.dataSource = self
            self.aTableView.delegate = self
            self.setUpTableFooterView()
            let fromDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "YYYY-MM-dd")
            let toDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "YYYY-MM-dd")
            
      self.getGenerateInvoice(self.user.ID, marinaID:self.marina.ID, boatID:self.userOwnBoat.ID , fromDate: fromDate, toDate: toDate, price: "\(self.totalBookingAmount())")
        }else{
            self.getMarinaPrice()
        }
        self.offerView = OffersHeaderView.instanceFromNib()
        // self.setupHeader()
         aTableView.isHidden = true
        self.loadOfferFor(userID: self.user.ID, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)

    }
    override  func viewWillAppear(_ animated: Bool) {
        self.user = User(json: User.loadUserInfo()!)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
   // sizeHeaderToFit()
        self.payButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,0.7])
        
    }
    
    func sizeHeaderToFit() {

        let headerView = aTableView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        var frame = headerView.frame
        frame.size.height = 180
        headerView.frame = frame
        aTableView.tableHeaderView = headerView
    }

    
    func setupHeader() {
    //self.offerView = OffersHeaderView.instanceFromNib()
    self.offerView.frame = CGRect(x: 0, y:0, width: aTableView.frame.size.width, height:150)
    self.aTableView.tableHeaderView?.frame.size = CGSize(width: aTableView.frame.size.width, height: CGFloat(150))
    self.offerView.offerCollectionView.dataSource = self
    self.offerView.offerCollectionView.delegate = self
    self.offerView.offerCollectionView.allowsSelection = true
    self.offerView.offerCollectionView.allowsMultipleSelection = true
    aTableView.tableHeaderView = self.offerView
    self.offerView.offerCollectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    self.offerView.offerCollectionView.reloadData()
   }
    
    func loadOfferFor(userID :String,pageNumber:Int,recordPerPage:Int) {
        if self.pageNumber == 1{
            CommonClass.updateLoader(withStatus: "Update..")
        }
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isNewDataLoading = true
        if pageNumber == 1{
            offers.removeAll()
            if offerView.offerCollectionView != nil{
                offerView.offerCollectionView.reloadData()
            }
        }
        OfferService.sharedInstance.getOffersList(userID: userID, page: pageNumber, perPage: recordPerPage){(success, response,totalPages,totalRecords) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            
            self.isNewDataLoading = false
            if let newOfferArray = response{
                if newOfferArray.count == 0{
                    if self.pageNumber == 1{
                        self.aTableView.tableHeaderView = UIView(frame: CGRect.zero)
                    }

                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }else{
                    self.setupHeader()
                }
                self.aTableView.isHidden = false

                self.offers.append(contentsOf: newOfferArray)
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if self.offerView.offerCollectionView != nil{
                self.offerView.offerCollectionView.reloadData()
            }
        }
    }
    
    func getMarinaPrice(){
        CommonClass.showLoader(withStatus: "Calculating..")
        let fromDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "YYYY-MM-dd")
        self.marina.getMarinaPriceByDate(fromDate: fromDate, toDate: toDate) { (resMarinaPrice) in
            CommonClass.hideLoader()
            if let aMarinaPrice = resMarinaPrice{
                self.marinaPriceReview = aMarinaPrice
                self.countForPriceDetails = self.rowsCountForPriceDetailsSection()
                self.aTableView.dataSource = self
                self.aTableView.delegate = self
                self.setUpTableFooterView()
                self.getGenerateInvoice(self.user.ID, marinaID:self.marina.ID, boatID:self.userOwnBoat.ID , fromDate: fromDate, toDate: toDate, price: "\(self.totalBookingAmount())")
            }
        }
    }
    
    func getGenerateInvoice(_ userID: String, marinaID: String, boatID: String, fromDate: String, toDate: String, price: String) {
        BookingService.sharedInstance.generateInvoice(user.ID, marinaID: marinaID, boatID: boatID, fromDate: fromDate, toDate: toDate, price: price){(success, message) in
            if success{
                //showAlertWith(viewController: self, message: "Generate Invoice", title: warningMessage.alertTitle.rawValue)
            }
            
        }
    }
    

    

    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = "Review Your Booking"
        self.navigationItem.titleView = self.navigationTitleView
    }

    func totalBookingAmount() -> Double {
        var totalAmount = self.marina.pricePerFeet*Double(self.userOwnBoat.boatSize.length)*Double(bookingDates.totalDays)
        if let aMarinaPrices = self.marinaPriceReview{
           let costForRegularDays = Double(aMarinaPrices.regularCost.totalDays)*aMarinaPrices.regularCost.price*Double(self.userOwnBoat.boatSize.length)
            let costForWeekEnd = Double(aMarinaPrices.weekEndCost.totalDays)*aMarinaPrices.weekEndCost.price*Double(self.userOwnBoat.boatSize.length)

            let costsForHolidays = aMarinaPrices.specialCosts.map({ (priceForDate) -> Double in
                return priceForDate.price*Double(self.userOwnBoat.boatSize.length)
            })
            let totalCostForHolidays = costsForHolidays.map({$0}).reduce(0, +)
            totalAmount = costForRegularDays + costForWeekEnd + totalCostForHolidays
        }
        return totalAmount
    }

    func rowsCountForPriceDetailsSection() -> Int{
        var count = 2
        if let _aMarinaPrices = self.marinaPriceReview{
            if _aMarinaPrices.weekEndCost.totalDays != 0{
                count = 3
            }
            count = count + _aMarinaPrices.specialCosts.count
        }
        self.countForPriceDetails = count
        return count
    }

    func setUpTableFooterView() -> Void {
        let total = self.totalBookingAmount()
        let totalTitle = "$ "+String(format: "%0.2f", total)
        self.payButton.setTitle("Pay \(totalTitle)", for: .normal)
        self.payButton.addTarget(self, action: #selector(onClickPayButton(_:)), for: .touchUpInside)
        self.aTableView.reloadData()
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        if !user.isContactVerified {
            if ((user.countryCode == "") || (user.contact == "")) {
               self.navigateTONumberVerification()
            }else{
           self.navigateToPayment()
            }
        }else{
        let paymentOptionVC = AppStoryboard.Home.viewController(PaymentOptionViewController.self)
        let amount = self.totalBookingAmount()
        paymentOptionVC.amount = amount
        paymentOptionVC.isPayingForMarina = false
        paymentOptionVC.marina = self.marina
        paymentOptionVC.bookingDates = self.bookingDates
        paymentOptionVC.boat = self.boat
        paymentOptionVC.selectedSize = self.selectedSize
        paymentOptionVC.userOwnBoat = self.userOwnBoat
        paymentOptionVC.selectedOffer = self.selectedOffer
        paymentOptionVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(paymentOptionVC, animated: true)
        }
    }
    func navigateTONumberVerification() {
        let numberVerificationVC = AppStoryboard.Booking.viewController(NumberVerificationViewController.self)
        let amount = self.totalBookingAmount()
        numberVerificationVC.amount = amount
        numberVerificationVC.isPayingForMarina = false
        numberVerificationVC.marina = self.marina
        numberVerificationVC.bookingDates = self.bookingDates
        numberVerificationVC.boat = self.boat
        numberVerificationVC.selectedSize = self.selectedSize
        numberVerificationVC.userOwnBoat = self.userOwnBoat
        numberVerificationVC.discountPrice = self.discountPrice
        numberVerificationVC.selectedOffer = self.selectedOffer
        numberVerificationVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(numberVerificationVC, animated: true)
    }
    
    func navigateToPayment() {
        let mobileVerifiedVC = AppStoryboard.Booking.viewController(MobileOTPVerifyViewController.self)
        let amount = self.totalBookingAmount()
        mobileVerifiedVC.amount = amount
        mobileVerifiedVC.isPayingForMarina = false
        mobileVerifiedVC.marina = self.marina
        mobileVerifiedVC.bookingDates = self.bookingDates
        mobileVerifiedVC.boat = self.boat
        mobileVerifiedVC.selectedSize = self.selectedSize
        mobileVerifiedVC.userOwnBoat = self.userOwnBoat
        mobileVerifiedVC.selectedOffer = self.selectedOffer
        mobileVerifiedVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(mobileVerifiedVC, animated: true)
    }
}

extension ReviewBookingDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return header.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        headerView.headerLabel.text = header[section].capitalized
        headerView.headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        headerView.backgroundColor = UIColor(red: 240.0/255.0, green: 237.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 1{
            return 85
        }else
        if indexPath.section == 3{
            return 72 //60
        }else if indexPath.section == header.count-1{
            return UITableViewAutomaticDimension
        }else{
            return 72
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (section == (header.count - 2)) ? self.rowsCountForPriceDetailsSection() : (data[header[section]]?.count ?? 0)
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == header.count-1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCancellationPolicyCell", for: indexPath) as! ReviewCancellationPolicyCell
            if self.marina.cancellationPolicy.count == 0{
                cell.policyTextLabel.text = (data[header[indexPath.section]]!)[indexPath.row]
            }else{
                cell.policyTextLabel.text = self.marina.cancellationPolicy
            }
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewBookingDetailCell", for: indexPath) as! ReviewBookingDetailCell
            if(indexPath.section != (header.count - 2)){
                cell.itemTitle.text = (data[header[indexPath.section]]!)[indexPath.row]
            }
        if indexPath.section == 0{
            switch indexPath.row {
            case 0:
                cell.itemDetailText.text = self.marina.title
            case 1:
                cell.itemDetailText.text = "\(self.marina.address), \r\n\(self.marina.city.name), \(self.marina.city.state)"
            default:
                break
            }
        }else if indexPath.section == 1{
            switch indexPath.row {
            case 0:
                cell.itemDetailText.text = self.boat.title
            case 1:
                cell.itemDetailText.text = "$ "+String(format: "%0.2f/foot", self.marina.pricePerFeet)
            default:
                break
            }

        }else if indexPath.section == 2{
            switch indexPath.row {
            case 0:
                cell.itemDetailText.text = self.userOwnBoat?.name
            case 1:
                cell.itemDetailText.text = "\(self.userOwnBoat.boatSize.length)x\(self.userOwnBoat.boatSize.width)x\(self.userOwnBoat.boatSize.depth)"
            default:
                break
            }
        }
        else if indexPath.section == 3{
            switch indexPath.row {
            case 0:
                cell.itemDetailText.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "MM/dd/YY")+"  \(self.marina.checkIn)"
            case 1:
                cell.itemDetailText.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "MM/dd/YY")+"  \(self.marina.checkOut)"
            default:
                break
            }
        }else if indexPath.section == 4{
            if indexPath.row == 0{
                cell.itemTitle.text = "Weekday Price"
                if let aMarinaPrice = self.marinaPriceReview{
                    let str = "$\(aMarinaPrice.regularCost.price) * \(aMarinaPrice.regularCost.totalDays) * \(self.userOwnBoat.boatSize.length) = "
                    let total = aMarinaPrice.regularCost.price*Double(aMarinaPrice.regularCost.totalDays)*Double(self.userOwnBoat.boatSize.length)
                    cell.itemDetailText.text = str+"$ "+String(format: "%0.2f", total)
                }else{
                    let total = self.marina.pricePerFeet*Double(self.userOwnBoat.boatSize.length)*Double(bookingDates.totalDays)
                    cell.itemDetailText.text = "$ "+String(format: "%0.2f", total)
                }
            }else if indexPath.row == (countForPriceDetails-1){
                cell.itemTitle.text = "Grand Total"
                cell.itemDetailText.text = "$ "+String(format: "%0.2f", self.totalBookingAmount())
            }else{
                if let aMarinaPrice = self.marinaPriceReview{
                    if aMarinaPrice.weekEndCost.totalDays != 0{
                        if indexPath.row == 1{
                            cell.itemTitle.text = "Weekend Price"
                            let str = "$\(aMarinaPrice.weekEndCost.price) * \(aMarinaPrice.weekEndCost.totalDays) * \(self.userOwnBoat.boatSize.length) = "
                            let total = aMarinaPrice.weekEndCost.price*Double(aMarinaPrice.weekEndCost.totalDays)*Double(self.userOwnBoat.boatSize.length)
                            cell.itemDetailText.text = str+"$ "+String(format: "%0.2f", total)
                        }else{
                            cell.itemTitle.text = aMarinaPrice.specialCosts[indexPath.row-2].date
                            let total = aMarinaPrice.specialCosts[indexPath.row-2].price*Double(self.userOwnBoat.boatSize.length)
                            cell.itemDetailText.text = "$ "+String(format: "%0.2f * %d = $ %0.2f", aMarinaPrice.specialCosts[indexPath.row-2].price,self.userOwnBoat.boatSize.length,total)
                        }
                    }else{
                        cell.itemTitle.text = aMarinaPrice.specialCosts[indexPath.row-1].date
                        let total = aMarinaPrice.specialCosts[indexPath.row-1].price*Double(self.userOwnBoat.boatSize.length)
                        cell.itemDetailText.text = "$ "+String(format: "%0.2f * %d = $ %0.2f", aMarinaPrice.specialCosts[indexPath.row-1].price,self.userOwnBoat.boatSize.length,total)
                    }
                }
            }
        }
        return cell
        }
    }
}



extension ReviewBookingDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return (offers.count > 0) ? offers.count : 1 ;
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if offers.count == 0 {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCollectionViewCell", for: indexPath) as! NoDataCollectionViewCell
          cell.noDataLabel.text = self.isNewDataLoading ? "Loading.." : "No offers found"
         return cell
        }else{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OffersCollectionViewCell", for: indexPath) as!  OffersCollectionViewCell
        let offer = offers[indexPath.item]
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
//        cell.offerView.layer.borderWidth = 1.0
//        cell.offerView.layer.borderColor = UIColor.lightGray.cgColor
        cell.promoCodeLabel.text = " " + offer.coupanCode + " "
        cell.promoCodeLabel.layer.cornerRadius = 5
        cell.promoCodeLabel.layer.masksToBounds = true
        cell.usedCodeLabel.textColor = UIColor.white
        cell.layoutIfNeeded()
        cell.setNeedsLayout()
        cell.promCodeDescription.text = offer.descrption
        //cell.promCodeDescription.speed = .rate(50.0)
        //cell.promCodeDescription.tag = 101
        //cell.promCodeDescription.type = .continuous
        //cell.promCodeDescription.animationCurve = .easeInOut
        //cell.promCodeDescription.lineBreakMode = .byTruncatingTail
            cell.promoCodeTitle.text = offer.title
        cell.promoImage.setIndicatorStyle(.gray)
        cell.promoImage.setShowActivityIndicator(true)
        cell.promoImage.layer.cornerRadius = 5
        cell.promoImage.layer.masksToBounds = true
        cell.promoImage.sd_setImage(with: URL(string:offer.image),placeholderImage:#imageLiteral(resourceName: "offer_placeholder"))

        cell.offerPercentageLabel.text = String(format: "%0.0f", offer.offerPercentage) + " % OFF"
        cell.offerExpireLabel.text = CommonClass.dateFromBookingDateString(offer.expireAt)
        cell.usedOfferCount.text = "No. " + "\(offer.userUsedCount)" + "/" + "\(offer.perUserCount)"

            
            if selectedIndex == indexPath.item {
                cell.offerView.layer.backgroundColor = UIColor(red:58.0/255.0, green:74.0/255.0, blue:118.0/255, alpha:1.0).cgColor
                cell.promoCodeLabel.textColor = UIColor.white
                cell.promoCodeTitle.textColor = UIColor.white
                cell.offerExpireLabel.textColor = UIColor.white
                cell.offerPercentageLabel.textColor = UIColor.white
                cell.promCodeDescription.textColor = UIColor.white
                cell.promCodeDescription.textColor = UIColor.white
                cell.validUptoLabel.textColor = UIColor.white
                cell.usedCodeLabel.textColor = UIColor.white
                cell.usedOfferCount.textColor = UIColor.white
                cell.onFirstBookingLabel.textColor = UIColor.white

                cell.offerImage.image = #imageLiteral(resourceName: "wheel")
                cell.timeImage.image = #imageLiteral(resourceName: "time_sel")
                cell.selectOfferImage.image = #imageLiteral(resourceName: "tick_sel")
               
            }else{
                cell.offerView.layer.backgroundColor = UIColor.white.cgColor
                cell.promoCodeLabel.textColor = UIColor.black
                cell.promoCodeLabel.textColor = UIColor.white
                cell.promoCodeTitle.textColor = UIColor.black
                cell.offerExpireLabel.textColor = UIColor.black
                cell.offerPercentageLabel.textColor = UIColor.black
                cell.promCodeDescription.textColor = UIColor.black
                cell.validUptoLabel.textColor = UIColor.black
                cell.usedCodeLabel.textColor = UIColor.black
                cell.usedOfferCount.textColor = UIColor.black
                cell.onFirstBookingLabel.textColor = UIColor.black

                cell.offerImage.image = #imageLiteral(resourceName: "wheel_unsel")
                cell.timeImage.image = #imageLiteral(resourceName: "time_unsel")
                cell.selectOfferImage.image = #imageLiteral(resourceName: "tick_unsel")

            }
        cell.layoutIfNeeded()
        cell.setNeedsLayout()
        return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let aCell = cell as? OffersCollectionViewCell else {
            return
        }
        aCell.layoutIfNeeded()
        aCell.setNeedsLayout()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: scrollView)
        self.perform(#selector(self.scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.01)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if offers.count != 0{
        if selectedIndex == indexPath.item{
            selectedIndex = -1
            self.setUpTableFooterView()
            self.discountPrice = self.totalBookingAmount()
        }else{
            selectedIndex = indexPath.item
            self.selectPromoCode(indexpath: indexPath)
            
        }
        offerView.offerCollectionView.reloadData()
        }
    }


    func selectPromoCode(indexpath: IndexPath) {
            let offer = offers[indexpath.item]
          self.selectedOffer = offer
            let total = self.totalBookingAmount()
            let reducePercntage = offer.offerPercentage
            let reducePrice = Double((total * reducePercntage)/(100))
            let lastPrice = total - reducePrice
            let totalTitle = "$ "+String(format: "%0.2f", lastPrice)
            self.payButton.setTitle("Pay \(totalTitle)", for: .normal)
        
            self.discountPrice = lastPrice
            
        
    }

    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize = collectionView.bounds.size
        cellSize.width -= collectionView.contentInset.left
        cellSize.width -= collectionView.contentInset.right
        return cellSize
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
               if scrollView == offerView.offerCollectionView{
            if ((scrollView.contentOffset.x + scrollView.frame.size.width) >= scrollView.contentSize.width)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if self.offers.count == 0{pageNumber=1}else{pageNumber+=1}
                     self.loadOfferFor(userID: self.user.ID, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }
        
    }

    
    
    
    
    
    //func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if scrollView == offerView.offerCollectionView{
//            let pageWidth = scrollView.frame.width
//            self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
//            self.offerView.pageControl.currentPage = self.currentPage
//
//        }
        
    //}

}






