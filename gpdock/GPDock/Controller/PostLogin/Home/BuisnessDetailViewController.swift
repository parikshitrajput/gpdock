//
//  ViewController.swift
//  Twitter User Interface
//  Created by Saif Chaudhary on 1/22/17.
//  Copyright © 2017 Saif Chaudhary. All rights reserved.

import UIKit
import GoogleMaps
import SDWebImage

let offset_HeaderStop:CGFloat = 297 // At this offset the Header stops its transformations
let distance_W_LabelHeader:CGFloat = 30.0 // The distance between the top of the screen and the top of the White Label

enum contentTypes {
    case overView,reviews,animities
}

protocol BuisnessDetailViewControllerDelegate {
    func marinaDidRefresh(_ viewController: BuisnessDetailViewController, refreshedMarina marina: Marina)
}

class BuisnessDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, CustomerRatingTableViewCellDelegate {
    // MARK: Outlet properties
    @IBOutlet var tableView : UITableView!{
        didSet{
            tableView.estimatedRowHeight = 66
        }
    }
    var buisnessDetailViewControllerDelegate: BuisnessDetailViewControllerDelegate?
    var isForceFully = false
    @IBOutlet var pageControl: UIPageControl!

    @IBOutlet var headerView : UIView!
    @IBOutlet var profileView : UIView!
    @IBOutlet var isPrivateImageView : UIImageView!
    var titleView : NavigationTitleView!
    @IBOutlet var segmentedView : UIView!
    @IBOutlet var segmentedControl : SWSegmentedControl!

    @IBOutlet var profileContainnerView : UIView!

    //@IBOutlet var selectedDatesLabel : UILabel!
    @IBOutlet var bookNowButton : UIButton!

    @IBOutlet var marinaTitleLabel : UILabel!
    @IBOutlet var addressLabel : UILabel!
    @IBOutlet var frequencyLabel : UILabel!
    @IBOutlet var totalRatingLabel : UIButton!
    @IBOutlet var payForMarinaButton : UIButton!

    //@IBOutlet var totalVotesLabel : UILabel!
    @IBOutlet var headerPriceLabel : UILabel!
    @IBOutlet var viewPager : ViewPager!

    @IBOutlet weak var likeButton: SparkButton!

    var bookingDates : BookingDates = BookingDates(fromDate: Date(), toDate: Date().addingTimeInterval(24*60*60))
    var selectedBoatSize = BoatSize()
    // MARK: class properties
    var shouldGoDirectlyToBooking = false
    var user : User!
    var headerBlurImageView:UIImageView!
    var headerImageView:UIImageView!
    var contentToDisplay : contentTypes = .overView
    var marina : Marina!
    var reviewPaser = ReviewParser()
    var reviewPageNumber = 1
    var reviewPerPage = 10
    var isLoading = false
    var isDateSelected : Bool = false
    // MARK: The view
    override func viewDidLoad() {
        super.viewDidLoad()
        //add listener for notification
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(favoriteNotificationHandler(_:)), name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuisnessDetailViewController.userDidPayAtMarinaHandler(_:)), name: .USER_PAID_AT_MARINA_NOTIFICATION, object: nil)
        self.payForMarinaButton.isHidden = !self.marina.isSigned
        self.setupRightBarButtons()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.registerCells()

        tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        if shouldGoDirectlyToBooking{
            self.openCalendarScreen()
        }
        self.setupNavigationTitle()

        self.getMarinaDetailsFromServer(marina.ID)
        self.setupMarinaAddress(marina: marina)
        viewPager.dataSource = self;
    }

    override func viewDidLayoutSubviews() {
        self.segmentedControl.selectionIndicatorView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal)
        self.self.bookNowButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal)
        CommonClass.makeViewCircularWithCornerRadius(bookNowButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: bookNowButton.frame.size.height/10)

    }

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 50, y: 0, width: self.view.frame.size.width-100, height: 44)
        titleView.titleLabel.text = marina.title.uppercased()
        self.navigationItem.titleView = self.titleView
    }


    @objc func userDidPayAtMarinaHandler(_ notification: Notification) {
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func favoriteNotificationHandler(_ notification : Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,AnyObject>{
            if let marinaID = userInfo["marinaID"] as? String, let isFavorite = userInfo["isFavorite"] as? Bool{
                if marinaID == self.marina.ID{
                    self.marina.isFavorite = isFavorite
                    self.toggleLikeButton()
                }
            }
        }
    }

    func registerCells(){
        //CustomerRatingTableViewCell
        let ratingCellNib = UINib(nibName: "CustomerRatingTableViewCell", bundle: nil)
        self.tableView.register(ratingCellNib, forCellReuseIdentifier: "CustomerRatingTableViewCell")

        //PrivateMarinaCell

        let privateMarinaCellNib = UINib(nibName: "PrivateMarinaCell", bundle: nil)
        self.tableView.register(privateMarinaCellNib, forCellReuseIdentifier: "PrivateMarinaCell")

        let headerCellNib = UINib(nibName: "HeaderTableViewCell", bundle: nil)
        self.tableView.register(headerCellNib, forCellReuseIdentifier: "HeaderTableViewCell")
        //CustomerReviewTableViewCell
        let reviewCellNib = UINib(nibName: "CustomerReviewTableViewCell", bundle: nil)
        self.tableView.register(reviewCellNib, forCellReuseIdentifier: "CustomerReviewTableViewCell")

        let aminityTableViewCellNib = UINib(nibName: "AminityTableViewCell", bundle: nil)
        self.tableView.register(aminityTableViewCellNib, forCellReuseIdentifier: "AminityTableViewCell")

    }

    @IBAction func onClickShareMarina(_ sender: UIButton) {
        self.shareMarina()
    }

    func shareMarina(){
            let marinaNameToshare = self.marina.title+"\r\n"+self.marina.address+", "+self.marina.city.name+", "+self.marina.city.state
            let textToShare = marinaNameToshare+"\r\n"+MARINA_SHARING_URL+self.marina.ID
        if let imageurl = self.marina.images.first{
            guard let imageURL = URL(string: imageurl.url)else{
                return
            }
            if let image = SDImageCache.shared().imageFromMemoryCache(forKey: imageURL.absoluteString) {
                //let textToShare = userNameToshare+" "+referenceNumber+" "+self.user.qrImage
                let activityVC = UIActivityViewController(activityItems: [image,textToShare], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    ////
//    func shareImage(){
//        let userNameToshare = self.user.name
//        let referenceNumber = self.user.accountReference
//        guard let imageURL = URL(string: self.user.qrImage)else{
//            return
//        }
//
//        if let image = SDImageCache.shared().imageFromMemoryCache(forKey: imageURL.absoluteString) {
//            let textToShare = userNameToshare+" "+referenceNumber+" "+self.user.qrImage
//            let activityVC = UIActivityViewController(activityItems: [image,textToShare], applicationActivities: nil)
//            activityVC.popoverPresentationController?.sourceView = self.view
//            self.present(activityVC, animated: true, completion: nil)
//        }
//    }
    

    func toggleLikeButton() {
        if marina.isFavorite == true {
            likeButton.setImage(UIImage(named: "liked"), for: UIControlState())
        }
        else{
            likeButton.setImage(UIImage(named: "like"), for: UIControlState())
        }
    }

    @IBAction func likeButtonTapped(_ sender: SparkButton) {
        if !CommonClass.isLoggedIn{
            showLoginAlert(isFromMenu: false, shouldLogin: true, inViewController: self)
            return
        }

        self.marina.isFavorite = !self.marina.isFavorite
        self.toggleFavorate(userID: self.user.ID, marinaID: marina.ID)
        if marina.isFavorite == true {
            sender.setImage(UIImage(named: "liked"), for: UIControlState())
            sender.likeBounce(0.6)
            sender.animate()
        }
        else{
            sender.setImage(UIImage(named: "like"), for: UIControlState())
            sender.unLikeBounce(0.4)
        }
    }

    func toggleFavorate(userID: String,marinaID:String) {
        MarinaService.sharedInstance.toggleFavorites(userID, marinaID: marinaID) { (status, resMarinaID) in
            self.marina.isFavorite = status
                NotificationCenter.default.post(name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil, userInfo: ["marinaID":resMarinaID,"isFavorite":status])
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func setupRightBarButtons() {
        let payButton = UIButton(frame: CGRect(x:0, y:0, width:44, height:44))
        payButton.frame = CGRect(x:0, y:0, width:44, height:44)
        payButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        payButton.setImage(#imageLiteral(resourceName: "upload"), for: .normal)
        payButton.addTarget(self, action: #selector(onClickShareMarina(_:)), for: .touchUpInside)
        let addBoatBarButton = UIBarButtonItem(customView: payButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([addBoatBarButton], animated: false)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickGetDirectionButton(_ sender: UIButton){
        self.showLocationOfGoogleMap(marina: self.marina)
    }

    func showLocationOfGoogleMap(marina:Marina)  {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            if let url = URL(string:"comgooglemaps://?center=\(marina.latitude),\(marina.longitude)&zoom=16&q=\(marina.latitude),\(marina.longitude)" ){
                UIApplication.shared.openURL(url)
            }
        } else
        {
            if (UIApplication.shared.canOpenURL(URL(string:"https://maps.google.com")!))
            {
                if let url = URL(string:"https://www.google.com/maps/@\(marina.latitude),\(marina.longitude),18z"){
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

    @IBAction func onClickPayAtMarinaButton(_ sender: UIButton){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if !self.marina.isSigned{return}
            if self.marina.ID.isEmpty{
                GPDock.showAlertWith(viewController: self, message: "Some thing went wrong", title: warningMessage.alertTitle.rawValue, complitionBlock: { (done) in
                    self.navigationController?.pop(true)
                })
                return
            }
            let payToMarinaVCNavigation = AppStoryboard.Home.viewController(PayMoneyNavigationController.self)
            for vc in payToMarinaVCNavigation.viewControllers{
                if let payVC = vc as? PayForMarinaViewController{
                    payVC.marina = self.marina
                    break
                }
            }
            self.present(payToMarinaVCNavigation, animated: true, completion: nil)
        }
    }

    @IBAction func onClickBookNowButton(_ sender: UIButton){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            self.openCalendarScreen()
        }
    }

    func startBookingProcess()  {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.showSizePickerDialog()
    }

    func openLoginModule() {
        let nav = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        self.present(nav, animated: true, completion: nil)
    }
    func showSizePickerDialog() -> Void {
        let sizeSelectorVC = AppStoryboard.Home.viewController(SizeSelectorViewController.self)
        sizeSelectorVC.bookingDates = self.bookingDates
        sizeSelectorVC.marina = self.marina
        sizeSelectorVC.delegate = self
        sizeSelectorVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        CommonClass.makeViewCircularWithCornerRadius(sizeSelectorVC.containnerView, borderColor: UIColor.lightGray, borderWidth: 0, cornerRadius: 5)
        let nav = UINavigationController(rootViewController: sizeSelectorVC)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    /*
    @IBAction func onClickSelectDateButton(_ sender: UIButton){
        let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        calendarVC.delegate = self
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
*/

    func openCalendarScreen() {
        let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        //calendarVC.selectedBookingDate = self.bookingDates
        calendarVC.delegate = self
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func getMarinaDetailsFromServer(_ marinaID: String) {
        if marinaID != ""{
            CommonClass.showLoader(withStatus: "Loading..")
            MarinaService.sharedInstance.getMarinaDetails(marinaID,userID:self.user.ID, completionBlock: { (success,resMarina,message) in
                CommonClass.hideLoader()
                if success{
                    if let aMarina = resMarina{
                        self.marina = aMarina
                        self.buisnessDetailViewControllerDelegate?.marinaDidRefresh(self, refreshedMarina: aMarina)
                        self.setupNavigationTitle()
                        self.setupMarinaAddress(marina: self.marina)
                        self.tableView.reloadData()
                        DispatchQueue.main.async(execute: {
                            self.likeButton.setImage(self.marina.isFavorite ? UIImage(named: "liked") : UIImage(named: "like"), for: UIControlState())
                            self.setupMarinaAddress(marina: self.marina)
                            self.tableView.reloadData()
                            self.viewPager.reloadData()
                            self.tableView.reloadData()
                        })
                    }
                }else{
                    self.showAlertAndOut(message)
                }
            })
        }
    }

    func setupMarinaAddress(marina:Marina) {
        self.isPrivateImageView.image = marina.isPrivate ? #imageLiteral(resourceName: "ic_red_lock") : UIImage()
        self.marinaTitleLabel.text = marina.title
        self.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
        self.headerPriceLabel.text =  "$"+String(format: "%.2f/foot",marina.pricePerFeet)
        self.headerPriceLabel.isHidden = !self.marina.isSigned
        self.frequencyLabel.text = marina.radioFrequency.uppercased()
        //self.frequencyLabel.isHidden = !self.marina.isSigned
        let rating = (marina.averageRating == 0.0 ) ? "No Rating" : String(format: "%.1f",marina.averageRating)
        self.totalRatingLabel.setTitle(rating, for: .normal)
        self.payForMarinaButton.isHidden = !marina.isSigned
        self.toggleLikeButton()
        CommonClass.makeViewCircularWithRespectToHeight(self.frequencyLabel, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.totalRatingLabel, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.headerPriceLabel, borderColor: UIColor.clear, borderWidth: 0)
    }


    func getMarinaReviewsAndRatingFromServer(_ marinaID: String,page:Int,perPage:Int) {
        self.isLoading = true
        if marinaID != ""{
            if !CommonClass.isConnectedToNetwork{
                showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
                if self.reviewPageNumber > 1{self.reviewPageNumber = self.reviewPageNumber - 1}
                self.isLoading = false
                self.tableView.hideFooterSpinner()
                return
            }

            MarinaService.sharedInstance.getMarinaReviewsAndRating(marinaID, ratingWanted: (self.reviewPaser.reviews.count == 0), page: self.reviewPageNumber, perPage: reviewPerPage, completionBlock: { (resReviews) in
                self.tableView.hideFooterSpinner()
                self.isLoading = false

                if let aRParser = resReviews as? ReviewParser{
                    if aRParser.reviews.count == 0{
                        if self.reviewPageNumber > 1{self.reviewPageNumber = self.reviewPageNumber - 1}
                    }
                    if self.reviewPaser.reviews.count == 0{
                        self.reviewPaser = aRParser
                        self.tableView.reloadData()
                    }else{
                        self.reviewPaser.reviews.append(contentsOf: aRParser.reviews)
                        self.tableView.reloadData()
                    }
                }else{
                    if self.reviewPageNumber > 1{self.reviewPageNumber = self.reviewPageNumber - 1}
                }

            })
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.profileContainnerView.addshadow(top: true, left: true, bottom: true, right: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.highlightDates()
        self.tableView.reloadData()
    }

    func openReviewList(for rating:Int) {
        let reviewListVC = AppStoryboard.Home.viewController(MarinaReviewsViewController.self)
        reviewListVC.userRating = rating
        reviewListVC.marinaID = self.marina.ID
        self.navigationController?.pushViewController(reviewListVC, animated: true)
    }

    func highlightDates() -> Void {

       // self.selectedDatesLabel.alpha = isDateSelected ? 1.0 : 0.7
//        self.fromDateLabel.alpha = isDateSelected ? 1.0 : 0.7
//        self.toDateLabel.alpha = isDateSelected ? 1.0 : 0.7
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table view processing
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch contentToDisplay {
        case .overView:
            if self.marina.isSigned{
                rows = self.marina.isPrivate ? 5 : 4
            }else{
                rows = self.marina.isPrivate ? 2 : 2
            }
        case .reviews:
            rows = self.reviewPaser.reviews.count+1
        case .animities:
            //rows = self.marina.aminities.count+1
            rows = self.marina.aminities.count+self.marina.services.count+2
        }
        return rows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 44.0
        switch contentToDisplay {
        case .overView:
           height = self.heightForCell(indexPath: indexPath)
        case .reviews:
            if indexPath.row == 0{
                height = 160
            }else{
                height = UITableViewAutomaticDimension
            }
        case .animities:
            if indexPath.row == 0 || indexPath.row == self.marina.services.count+1{
                height = 35
            }else{
                height = 44
            }
        }
        return height
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch contentToDisplay {
        case .overView:
            if self.marina.isSigned{
                let cell = self.marina.isPrivate ? self.cellForSignedPrivateMarinaOverView(tableView, cellForRowAt: indexPath) : self.cellForSignedRegularOverView(tableView, cellForRowAt: indexPath)
                return cell
            }else{
                let cell = self.marina.isPrivate ? self.cellForUnsignedPrivateMarinaOverView(tableView, cellForRowAt: indexPath) : self.cellForUnsignedRegularOverView(tableView, cellForRowAt: indexPath)
                return cell
            }
        case .reviews:
            let cell = self.reviewCellForTableView(tableView, cellForRowAt: indexPath)
            return cell
        case .animities:
            let cell = self.cellForOverAmenities(tableView, cellForRowAt: indexPath)
            return cell
        }
    }

    func cellForOverAmenities(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
            cell.headerLabel.text = "  Services"
            cell.headerLabel.isGradientApplied = true
            return cell
        }else if indexPath.row == self.marina.services.count+1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
            cell.headerLabel.text = "  Amenities"
            cell.headerLabel.isGradientApplied = true
            
            return cell
        }else if ((indexPath.row > 0) && (indexPath.row <= self.marina.services.count)){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AminityTableViewCell", for: indexPath) as! AminityTableViewCell
            cell.selectionStyle = .none
            let service = self.marina.services[indexPath.row-1]
            cell.aminityIcon.setIndicatorStyle(.gray)
            cell.aminityIcon.setShowActivityIndicator(true)
            cell.aminityIcon.sd_setImage(with: URL(string:service.icon),placeholderImage:#imageLiteral(resourceName: "placeholderamenity"))
            cell.aminityTitleLabel.text = service.title
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AminityTableViewCell", for: indexPath) as! AminityTableViewCell
            cell.selectionStyle = .none
            let row = indexPath.row - (self.marina.services.count+2)
            let aminity = self.marina.aminities[row]
            cell.aminityIcon.setIndicatorStyle(.gray)
            cell.aminityIcon.setShowActivityIndicator(true)
            cell.aminityIcon.sd_setImage(with: URL(string:aminity.icon),placeholderImage:#imageLiteral(resourceName: "placeholderamenity"))
            cell.aminityTitleLabel.text = aminity.title
            return cell
        }

    }




    
    func urlForMarinaLocationStaticImage(_ marina: Marina,size:CGSize) -> URL{
        let iconurl = marina.isPrivate ? privateMarinaIconUrl : publicMarinaIconUrl
        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=icon:\(iconurl)|\(marina.latitude),\(marina.longitude)&\("zoom=16&size=\(Int(size.width))x\(Int(size.height))")&sensor=true"
        let url = URL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
        return url
    }







    func reviewCellForTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return ratingCellForTableView(tableView, cellForRowAt: indexPath)
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerReviewTableViewCell", for: indexPath) as! CustomerReviewTableViewCell
           // cell.customerImage.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
            let review = reviewPaser.reviews[indexPath.row-1]
            cell.reviewDateLabel.text = review.createdAt
            cell.customerImage.sd_setImage(with: URL(string:review.user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:review.user.firstName+" "+review.user.lastName))
            cell.customerNameLabel.text = review.user.firstName+" "+review.user.lastName
            cell.ratingView.rating = Float(review.rating)
            cell.reviewTextLabel.text = review.reviewDescription
            //weather call to server to send next batch of review
            self.callForNextBatchOfReview(indexPath: indexPath)
            return cell
        }
    }

    func callForNextBatchOfReview(indexPath:IndexPath){
        if !((indexPath.row >= (self.reviewPaser.reviews.count-5)) && (self.reviewPaser.reviews.count < self.reviewPaser.totalReviews)){return}
        if self.isLoading{return}
        self.reviewPageNumber += 1
        self.tableView.showFooterSpinner()
        self.getMarinaReviewsAndRatingFromServer(self.marina.ID, page: self.reviewPageNumber, perPage: self.reviewPerPage)
    }
    func ratingCellForTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerRatingTableViewCell", for: indexPath) as! CustomerRatingTableViewCell
        cell.oneProgressBar.progressValue = CGFloat(reviewPaser.ratings.oneStars)//*10)
        cell.twoProgressBar.progressValue = CGFloat(reviewPaser.ratings.twoStars)//*10)
        cell.threeProgressBar.progressValue = CGFloat(reviewPaser.ratings.threeStars)//*10)
        cell.fourProgressBar.progressValue = CGFloat(reviewPaser.ratings.fourStars)//*10)
        cell.fiveProgressBar.progressValue = CGFloat(reviewPaser.ratings.fiveStars)//*10)
        cell.overAllRatingView.rating = Float(marina.averageRating)
        cell.overAllRatingCountLabel.text = String(format: "%.1f", Float(marina.averageRating))
        cell.delegate = self
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    
    func customerRatingTableViewCell(_ cell: CustomerRatingTableViewCell, didTapOnRating rating: Int) {
        self.openReviewList(for: rating)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let rCell = cell as? CustomerRatingTableViewCell{
//            rCell.oneProgressBar.progressValue = 20
//            rCell.twoProgressBar.progressValue = 40
//            rCell.threeProgressBar.progressValue = 60
//            rCell.fourProgressBar.progressValue = 80
//            rCell.fiveProgressBar.progressValue = 100
//            rCell.overAllRatingView.rating = Float(marina.averageRating)
//            rCell.overAllRatingCountLabel.text = String(format: "%.1f", Float(marina.averageRating))
//        }
    }

    // MARK: Scroll view delegate

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isForceFully = false
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if isForceFully{
            let offset = scrollView.contentOffset.y + headerView.bounds.height
            //var avatarTransform = CATransform3DIdentity
            var headerTransform = CATransform3DIdentity
            // PULL DOWN -----------------
            if offset < 0 {
                let headerScaleFactor:CGFloat = -(offset) / headerView.bounds.height
                let headerSizevariation = ((headerView.bounds.height * (1.0 + headerScaleFactor)) - headerView.bounds.height)/2
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                // Hide views if scrolled super fast
                headerView.layer.zPosition = 0
            }
                // SCROLL UP/DOWN ------------
            else {
                // Header -----------
                headerTransform = CATransform3DTranslate(headerTransform, 0,-offset_HeaderStop, 0)
            }
            // Apply Transformations
            headerView.layer.transform = headerTransform
            // Segment control
            _ = profileView.frame.height - segmentedView.frame.height - offset
            var segmentTransform = CATransform3DIdentity
            // Scroll the segment view until its offset reaches the same offset at which the header stopped shrinking
            segmentTransform = CATransform3DTranslate(segmentTransform, 0, -offset_HeaderStop, 0)
            segmentedView.layer.transform = segmentTransform
            tableView.scrollIndicatorInsets = UIEdgeInsetsMake(segmentedView.frame.maxY, 0, 0, 0)
        }else{

            //}
        let offset = scrollView.contentOffset.y + headerView.bounds.height
        //var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        // PULL DOWN -----------------
        if offset < 0 {
            let headerScaleFactor:CGFloat = -(offset) / headerView.bounds.height
            let headerSizevariation = ((headerView.bounds.height * (1.0 + headerScaleFactor)) - headerView.bounds.height)/2
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            // Hide views if scrolled super fast
            headerView.layer.zPosition = 0
        }
        // SCROLL UP/DOWN ------------
        else {
            // Header -----------
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
        }
        // Apply Transformations
        headerView.layer.transform = headerTransform
        // Segment control
        let segmentViewOffset = profileView.frame.height - segmentedView.frame.height - offset
        var segmentTransform = CATransform3DIdentity
        // Scroll the segment view until its offset reaches the same offset at which the header stopped shrinking
        segmentTransform = CATransform3DTranslate(segmentTransform, 0, max(segmentViewOffset, -offset_HeaderStop), 0)
        
        segmentedView.layer.transform = segmentTransform
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(segmentedView.frame.maxY, 0, 0, 0)
        }
    }
    
    // MARK: Interface buttons
    
    @IBAction func selectContentType(_ sender: UISegmentedControl) {
//        isForceFully = true
        if sender.selectedSegmentIndex == 0 {
            contentToDisplay = .overView
            self.tableView.reloadData()
            //self.tableView.setContentOffset(CGPoint(x:0,y:offset_HeaderStop), animated: false)
        }else if sender.selectedSegmentIndex == 1{
            contentToDisplay = .reviews
            if self.reviewPaser.reviews.count == 0{
                tableView.showFooterSpinner()
                self.getMarinaReviewsAndRatingFromServer(self.marina.ID, page: self.reviewPageNumber, perPage: self.reviewPerPage)
            }
            self.tableView.reloadData()

        }else if sender.selectedSegmentIndex == 2{
            contentToDisplay = .animities
            self.tableView.reloadData()
        }

//        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
//        self.tableView.setContentOffset(CGPoint(x:0,y:offset_HeaderStop), animated: false)
    }

    func addMarkerForMarina(_ marina : Marina, mapView : GMSMapView) {
        let marinaMarker = MarinaMarker(marina: marina, index: 0)
        DispatchQueue.main.async {
        marinaMarker.icon = marinaMarker.imageForSelectedMarinaMarker()
        marinaMarker.isTappable = true
        marinaMarker.map = mapView;

        let camera = GMSCameraPosition.camera(withTarget: marinaMarker.position, zoom:15.5)//kMapZoomLevel)
        mapView.mapType = .normal
        mapView.camera = camera
        var point = mapView.projection.point(for: marinaMarker.position)
        point.y = point.y - (mapView.frame.size.height/2) + 30
        let newPoint = mapView.projection.coordinate(for: point)
        let camera1 = GMSCameraUpdate.setTarget(newPoint)
        mapView.animate(with: camera1)
        mapView.isUserInteractionEnabled = false
        }
    }
}

extension BuisnessDetailViewController:NKCalenderViewControllerDelegate,SizeSelectorViewControllerDelegate,FloorPlanViewControllerDelegate{
    func nkcalendarViewController(_ viewController:NKCalenderViewController, didSelectedDate selectedDates:BookingDates)
    {
        self.bookingDates = selectedDates
        //var dateText = "From "+CommonClass.formattedDateWith(selectedDates.fromDate, format: "MMM dd")
       // dateText = dateText+"  To "+CommonClass.formattedDateWith(selectedDates.toDate, format: "MMM dd")
        //self.selectedDatesLabel.text = dateText
        self.isDateSelected = (self.bookingDates.totalDays>0)
        viewController.dismiss(animated: false) {
            if self.isDateSelected{
                self.startBookingProcess()
            }
        }
    }

    func sizeSelectorViewController(_ viewController: SizeSelectorViewController, didSelectBoatSize boatSize: BoatSize, withUserOwnBoat userBoat: Boat?) {
        self.selectedBoatSize = boatSize
        guard let boat = userBoat else{
            return;
        }

        if self.marina.isSigned{
            self.navigateToFloorPlan(boat)
        }else{
            self.navigateToRequestBookingFor(boat)
        }
    }
    
    func navigateToFloorPlan(_ userBoat: Boat?){
        let floorPlanVC = AppStoryboard.Home.viewController(FloorPlanViewController.self)
        floorPlanVC.marina = self.marina
        floorPlanVC.bookingDates = self.bookingDates
        floorPlanVC.selectedBoatSize = self.selectedBoatSize
        floorPlanVC.floorPlanDelegate = self
        floorPlanVC.userBoat = userBoat
        self.navigationController?.pushViewController(floorPlanVC, animated: true)
    }

    func navigateToRequestBookingFor(_ userBoat: Boat?){
        guard let boat = userBoat else {
            return
        }
        let requestBookingVC = AppStoryboard.Home.viewController(RequestBookingViewController.self)
        requestBookingVC.marina = self.marina
        requestBookingVC.bookingDates = self.bookingDates
        requestBookingVC.userBoat = boat
        self.navigationController?.pushViewController(requestBookingVC, animated: true)
    }

    func floorPlanViewControllerShouldEditBooking(_ viewController: FloorPlanViewController,willEditWith selectedDate:BookingDates?){
        self.navigationController?.pop(false)
        self.openCalendarScreen()
    }

}

extension BuisnessDetailViewController:ViewPagerDataSource{
    func showAlertAndOut(_ message:String){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            self.navigationController?.popToRoot(true)
        }
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }




    func numberOfItems(viewPager:ViewPager) -> Int {
        return marina.images.count;
    }


    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = view;
        var imageView:UIImageView?
        if(newView == nil){
            newView = UIView(frame: CGRect(x: 0, y: 0, width: self.viewPager.frame.width, height:  self.viewPager.frame.height))
            imageView = UIImageView(frame: newView!.bounds)
            imageView!.tag = 1
            imageView!.autoresizingMask =  [.flexibleWidth, .flexibleHeight]
            imageView?.contentMode = .scaleAspectFill
            imageView?.clipsToBounds = true
            newView?.addSubview(imageView!)
        }else{
            imageView = newView?.viewWithTag(1) as? UIImageView
        }
        imageView?.setIndicatorStyle(.gray)
        imageView?.setShowActivityIndicator(true)
        imageView?.sd_setImage(with: URL(string:marina.images[index].url))
        return newView ?? UIView()
    }

@nonobjc func didSelectedItem(_ index: Int) {
    }
}


extension BuisnessDetailViewController{//here goes extension for all four cases of marina type
    //Mark: Cell for signed private marina
    func cellForSignedPrivateMarinaOverView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMarinaTableViewCell", for: indexPath) as! AboutMarinaTableViewCell
            cell.aboutMarinaLabel.text = marina.marinaDescription
            //cell.cellIcon.image = #imageLiteral(resourceName: "aboutmarina")
            return cell
        }else if indexPath.row == 1{
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaInfoTableViewCell", for: indexPath) as! MarinaInfoTableViewCell
            cell.checkInLabel.text = marina.checkIn
            cell.checkOutLabel.text = marina.checkOut
            cell.pricePerFeetLabel.text = "$"+String(format: "%.2f/foot",marina.pricePerFeet)
            return cell
        }else if indexPath.row == 2{
            //private marina additional info
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateMarinaCell", for: indexPath) as! PrivateMarinaCell
            cell.locationTypeLabel.text = marina.locationType
            let accessTime = marina.fromAccessTime+" to "+marina.toAccessTime
            cell.accessTimeLabel.text = (accessTime == "- to -") ? "24x7" : marina.fromAccessTime+" to "+marina.toAccessTime
            cell.insuranceRequiredLabel.text = self.marina.isAdditionalInsuranceRequired ? "Yes" : "No"
            return cell
        }else if indexPath.row == 3{
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaMapDetailsCell", for: indexPath) as! MarinaMapDetailsCell
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.mapViewImageView.setIndicatorStyle(.gray)
            cell.mapViewImageView.setShowActivityIndicator(true)
            let height = (self.view.frame.width*6/10)+1
            let width = self.view.frame.width-28
            cell.mapViewImageView.sd_setImage(with: self.urlForMarinaLocationStaticImage(self.marina, size: CGSize(width:width,height:height)))
            cell.directionsButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaPolicyTableViewCell", for: indexPath) as! MarinaPolicyTableViewCell
            cell.cellIcon.image = #imageLiteral(resourceName: "listBullet")
            cell.policyLabel.text = marina.cancellationPolicy+"\r\n"
            return cell
        }
    }
    //Mark: Cell for unsigned private marina
    func cellForUnsignedPrivateMarinaOverView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMarinaTableViewCell", for: indexPath) as! AboutMarinaTableViewCell
            cell.aboutMarinaLabel.text = marina.marinaDescription
            return cell
        }else {
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaMapDetailsCell", for: indexPath) as! MarinaMapDetailsCell
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.mapViewImageView.setIndicatorStyle(.gray)
            cell.mapViewImageView.setShowActivityIndicator(true)
            let height = (self.view.frame.width*6/10)+1
            let width = self.view.frame.width-28
            cell.mapViewImageView.sd_setImage(with: self.urlForMarinaLocationStaticImage(self.marina, size: CGSize(width:width,height:height)))
            cell.directionsButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            return cell
        }
    }

    //Mark: Cell for signed regular marina
    func cellForSignedRegularOverView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMarinaTableViewCell", for: indexPath) as! AboutMarinaTableViewCell
            cell.aboutMarinaLabel.text = marina.marinaDescription
            return cell
        }else if indexPath.row == 1{
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaInfoTableViewCell", for: indexPath) as! MarinaInfoTableViewCell
            cell.checkInLabel.text = marina.checkIn
            cell.checkOutLabel.text = marina.checkOut
            cell.pricePerFeetLabel.text = "$"+String(format: "%.2f/foot",marina.pricePerFeet)
            return cell
        }else if indexPath.row == 2{
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaMapDetailsCell", for: indexPath) as! MarinaMapDetailsCell
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.mapViewImageView.setIndicatorStyle(.gray)
            cell.mapViewImageView.setShowActivityIndicator(true)
            let height = (self.view.frame.width*6/10)+1
            let width = self.view.frame.width-28

            cell.mapViewImageView.sd_setImage(with: self.urlForMarinaLocationStaticImage(self.marina, size: CGSize(width:width,height:height)))
            cell.directionsButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaPolicyTableViewCell", for: indexPath) as! MarinaPolicyTableViewCell
            cell.cellIcon.image = #imageLiteral(resourceName: "listBullet")
            cell.policyLabel.text = marina.cancellationPolicy+"\r\n"
            return cell
        }
    }


    //Mark: Cell for unsigned regular marina
    func cellForUnsignedRegularOverView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutMarinaTableViewCell", for: indexPath) as! AboutMarinaTableViewCell
            cell.aboutMarinaLabel.text = marina.marinaDescription
            //cell.cellIcon.image = #imageLiteral(resourceName: "aboutmarina")
            return cell
        }else{
            //details row with closing
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaMapDetailsCell", for: indexPath) as! MarinaMapDetailsCell
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.mapViewImageView.setIndicatorStyle(.gray)
            cell.mapViewImageView.setShowActivityIndicator(true)
            let height = (self.view.frame.width*6/10)+1
            let width = self.view.frame.width-28

            cell.mapViewImageView.sd_setImage(with: self.urlForMarinaLocationStaticImage(self.marina, size: CGSize(width:width,height:height)))
            cell.directionsButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            return cell
        }
    }


    func heightForCell(indexPath:IndexPath) -> CGFloat{
        var height:CGFloat = 44
        if self.marina.isSigned{
            if self.marina.isPrivate{
                if indexPath.row == 0{
                    height = UITableViewAutomaticDimension //about marina cell
                }else if indexPath.row == 1{
                    height = CGFloat(126)// + (self.view.frame.width*6/10) //checkin checkout and price
                }else if indexPath.row == 2{
                    height = CGFloat(189)// + (self.view.frame.width*6/10) //private marina cell
                }
                else if indexPath.row == 3{
                    height = (self.view.frame.width*6/10)+1 //map cell
                }else if indexPath.row == 4{
                    height = UITableViewAutomaticDimension //cancellation policy
                }
            }else{
                if indexPath.row == 0{
                    height = UITableViewAutomaticDimension //about marina cell
                }else if indexPath.row == 1{
                    height = CGFloat(126)// + (self.view.frame.width*6/10) //checkin checkout and price
                }else if indexPath.row == 2{
                    height = (self.view.frame.width*6/10)+1 //map cell
                }else if indexPath.row == 3{
                    height = UITableViewAutomaticDimension //cancellation policy
                }
            }

        }else{
            if self.marina.isPrivate{
                if indexPath.row == 0{
                    height = UITableViewAutomaticDimension //about marina cell
                }else if indexPath.row == 1{
                    height = (self.view.frame.width*6/10)+1 //map cell
                }
            }else{
                if indexPath.row == 0{
                    height = UITableViewAutomaticDimension //about marina cell
                }else if indexPath.row == 1{
                    height = (self.view.frame.width*6/10)+1 //map cell
                }
            }

        }
        return height
    }

}
extension UITableView{
    func showFooterSpinner(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 44)
        self.tableFooterView = spinner;
    }

    func hideFooterSpinner(){
        self.tableFooterView = nil;
    }

}

