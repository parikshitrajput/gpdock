//
//  FloorPlanViewController.swift
//  GPDock
//
//  Created by TecOrb on 14/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
protocol FloorPlanViewControllerDelegate {
    func floorPlanViewControllerShouldEditBooking(_ viewController: FloorPlanViewController,willEditWith selectedDate:BookingDates?)
}

class FloorPlanViewController: UIViewController,ZSeatSelectorDelegate {
    var marina: Marina!
    var selectedBoatSize: BoatSize!
    var bookingDates: BookingDates!
    var selectedBoat : ParkingSpace!
    var userBoat : Boat?
    var floorPlanDelegate: FloorPlanViewControllerDelegate?
    @IBOutlet weak var parkingPlanView : UIView!
    @IBOutlet weak var mainPierContainner : UIView!

   // @IBOutlet weak var rangeImage : UIImageView!
    @IBOutlet weak var dateFromLabel : UILabel!
    @IBOutlet weak var dateToLabel : UILabel!
    @IBOutlet weak var boatNameLabel : UILabel!

    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var bookMarinaButton : UIButton!
    var floorPlan = FloorPlan()
    var user: User!
    var navigationTitleView : NavigationTitleView!
    var marinaPriceReview : MarinaPriceReview?

    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.setUpNavigationView()

        self.getMarinaFloorPlan(self.selectedBoatSize, bookingDates: self.bookingDates)
        self.parkingPlanView.backgroundColor = UIColor.white
        self.boatNameLabel.text = "Boat: "+(userBoat?.name ?? "--")

        self.dateFromLabel.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "MM/dd/YY")
        self.dateToLabel.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "MM/dd/YY")
        self.getMarinaPrice()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    func getMarinaPrice(){
        let fromDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "YYYY-MM-dd")
        self.marina.getMarinaPriceByDate(fromDate: fromDate, toDate: toDate) { (resMarinaPrice) in
            if let aMarinaPrice = resMarinaPrice{
                self.marinaPriceReview = aMarinaPrice
            }
        }
    }
    

    @IBAction func refreshFloorPlan(_ sender: UIButton){
        CommonClass.sharedInstance.clearAllPendingRequests()
        self.resetFloor()
        self.getMarinaFloorPlan(self.selectedBoatSize, bookingDates: self.bookingDates)
    }


    override func viewDidLayoutSubviews() {
        self.bookMarinaButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        self.mainPierContainner.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 4)
        CommonClass.makeViewCircularWithRespectToHeight(editButton, borderColor: UIColor.clear, borderWidth: 0)
        self.view.bringSubview(toFront: self.mainPierContainner)
        self.mainPierContainner.layer.zPosition = 10
    }


    override func viewWillAppear(_ animated: Bool) {
        //self.selectedBoat = nil
    }
    func seatMapping(){
        let seats = ZSeatSelector()
        
        seats.frame = CGRect(x: 0, y: 0, width: self.parkingPlanView.frame.size.width, height:  self.parkingPlanView.frame.size.height)
        seats.maximumZoomScale = 5.0

        seats.setSeatSize(CGSize(width: 40, height: 40))
        seats.setAvailableImage()
        seats.floorPlan(map: floorPlan)
        seats.selected_seat_limit  = 1
        seats.seatSelectorDelegate = self
        self.parkingPlanView.addSubview(seats)
        seats.isDirectionalLockEnabled = true
    }


    func resetFloor() {
        for view in self.parkingPlanView.subviews{
            if view is ZSeatSelector{
                for v in view.subviews{
                    v.removeFromSuperview()
                }
            }
        }
    }


    func seatSelected(_ seat: ZSeat) {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if seat.boat.isBooked{
            return
        }

        if !seat.boat.isAvailable{
            return
        }

        if seat.boat.isAvailable{
            self.locakParking(self.user.ID,parkingID: seat.boat.ID, completion: { (selected) in
                seat.selected_seat = selected
                if selected{
                    self.selectedBoat = seat.boat
                }else{
                    self.selectedBoat = nil
                }
            })
        }
    }

    func locakParking(_ userID: String, parkingID: String,completion:@escaping (Bool)->Void) {
        MarinaService.sharedInstance.lockParkingSpaceFor(userID,parkingID: parkingID) { (done) in
            completion(done)
        }
    }

    func getSelectedSeats(_ seats: NSMutableArray) {
        if seats.count == 0{
            self.selectedBoat = nil
        }else{
            if let selSeat = seats.lastObject as? ZSeat{
                self.selectedBoat = selSeat.boat
            }else{
                self.selectedBoat = nil
            }
        }
    }


    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = marina.title.uppercased()
        self.navigationItem.titleView = self.navigationTitleView
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickEditButton(_ sender: UIButton){
        self.floorPlanDelegate?.floorPlanViewControllerShouldEditBooking(self, willEditWith: bookingDates)
        //self.navigationController?.pop(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getMarinaFloorPlan(_ boatSize: BoatSize,bookingDates:BookingDates) -> Void {
        CommonClass.showLoader(withStatus: "Getting..")
        MarinaService.sharedInstance.getFloorPlanForMarina(self.marina.ID, boatSize: boatSize, bookingDate: bookingDates) { (resFloorPlan) in
            CommonClass.hideLoader()
            if let aFloorPlan = resFloorPlan{
                //self.resetFloor()
                self.floorPlan = aFloorPlan
                self.seatMapping()
            }
        }
    }


    @IBAction func onClickBookNowButton(_ sender: UIButton){
        if self.selectedBoat != nil{
            let reviewDetailsVC = AppStoryboard.Home.viewController(ReviewBookingDetailsViewController.self)
            reviewDetailsVC.bookingDates = self.bookingDates
            reviewDetailsVC.selectedSize = self.selectedBoatSize
            reviewDetailsVC.marina = self.marina
            reviewDetailsVC.boat = self.selectedBoat
            reviewDetailsVC.userOwnBoat = userBoat
            reviewDetailsVC.marinaPriceReview = self.marinaPriceReview
            self.navigationController?.pushViewController(reviewDetailsVC, animated: true)
        }else{
            showAlertWith(viewController: self, message: "Please select a slip", title: warningMessage.alertTitle.rawValue)
        }
    }
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

