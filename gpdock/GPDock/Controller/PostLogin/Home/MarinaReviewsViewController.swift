//
//  MarinaReviewsViewController.swift
//  GPDock
//
//  Created by TecOrb on 30/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaReviewsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var userRating : Int!
    @IBOutlet weak var aTableView: UITableView!


    var page = 1
    var perPage = 15
    var user: User!
    var isLoading = false
    var marinaID: String!
    var reviews = [Review]()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(MarinaReviewsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.aTableView.addSubview(refreshControl)
        self.registerCells()
        self.aTableView.estimatedRowHeight = 80
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        let ratingView = NKFloatRatingView(frame: CGRect(x:0,y:0,width:200, height:30))
        ratingView.emptyImage = #imageLiteral(resourceName: "star_icon_empty")
        ratingView.fullImage = #imageLiteral(resourceName: "star_icon")
        ratingView.rating = Float(userRating)
        ratingView.editable = false
        self.navigationItem.titleView = ratingView
        self.isLoading = true
        self.getMarinaReviewsAndRatingFromServer(self.marinaID,rating: self.userRating,page:self.page,perPage:self.perPage)
        self.aTableView.reloadData()
    }

    func registerCells(){
        //CustomerRatingTableViewCell
        let ratingCellNib = UINib(nibName: "CustomerRatingTableViewCell", bundle: nil)
        self.aTableView.register(ratingCellNib, forCellReuseIdentifier: "CustomerRatingTableViewCell")
        //CustomerReviewTableViewCell
        let reviewCellNib = UINib(nibName: "CustomerReviewTableViewCell", bundle: nil)
        self.aTableView.register(reviewCellNib, forCellReuseIdentifier: "CustomerReviewTableViewCell")
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaReviewsForRatings(marinaID, reviewsForRating: self.userRating, page: page, perPage: perPage, completionBlock: { (resReviews) in
            self.isLoading = false
            self.reviews.removeAll()
            self.refreshControl.endRefreshing()
            if let someReviews = resReviews{
                self.reviews.append(contentsOf: someReviews)
                self.aTableView.reloadData()
            }
        })
    }

    func getMarinaReviewsAndRatingFromServer(_ marinaID: String,rating:Int,page:Int,perPage:Int) {
        self.isLoading = true
        if page > 1{
            self.aTableView.showFooterSpinner()
        }

        MarinaService.sharedInstance.getMarinaReviewsForRatings(marinaID, reviewsForRating: rating, page: page, perPage: perPage, completionBlock: { (resReviews) in
            self.isLoading = false
            self.aTableView.hideFooterSpinner()
            if let someReviews = resReviews{
                if someReviews.count == 0{
                    if self.page >= 2{self.page -= 1}
                }
                self.reviews.append(contentsOf: someReviews)
                self.aTableView.reloadData()
            }else{
                if self.page >= 2{self.page -= 1}
                self.aTableView.reloadData()
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.reviews.count == 0) ? 1 : self.reviews.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.reviews.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text =  isLoading ? "Loading.." : "No Review to show\r\nPull to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerReviewTableViewCell", for: indexPath) as! CustomerReviewTableViewCell
            let review = reviews[indexPath.row]
            cell.reviewDateLabel.text = review.createdAt
            cell.customerImage.sd_setImage(with: URL(string:review.user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:review.user.firstName+" "+review.user.lastName))
            cell.customerNameLabel.text = review.user.firstName+" "+review.user.lastName
            cell.ratingView.rating = Float(review.rating)
            cell.reviewTextLabel.text = review.reviewDescription
            return cell
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        self.page+=1
                        self.getMarinaReviewsAndRatingFromServer(self.marinaID,rating:self.userRating,page:self.page,perPage:self.perPage)
                    }
                }
            }
        }

    }



}


