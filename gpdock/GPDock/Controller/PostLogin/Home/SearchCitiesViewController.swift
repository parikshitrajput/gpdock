//
//  SearchCitiesViewController.swift
//  GPDock
//
//  Created by TecOrb on 22/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SearchCitiesViewController: UIViewController {
    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var aSearchBar: UISearchBar!

    var titleView : NavigationTitleView!

    var keyword : String = ""
    var page = 1
    var perPage = 15
    var searchCount = 0
    var user: User!
    var isLoading = true
    var isSeachActive = false
    var suggestions = Array<Suggestion>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.aTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.aSearchBar.delegate = self
        self.aTableView.reloadData()
        self.aSearchBar.becomeFirstResponder()
        self.loadCitiesFromServer("", page: self.page, perPage: self.perPage)
        self.setupNavigationTitle()
    }
    override func viewDidLayoutSubviews() {
        for s in aSearchBar.subviews[0].subviews {
            if s is UITextField {
                s.addshadow(top: true, left: true, bottom: true, right: true)
            }
        }
    }

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Search".uppercased()
        self.navigationItem.titleView = self.titleView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    func loadCitiesFromServer(_ keyword: String, page: Int,perPage:Int) -> Void {
        self.searchCount += 1
        self.isLoading = true

        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }

        if page == 1{
            suggestions.removeAll()
            aTableView.reloadData()
        }
        MarinaService.sharedInstance.searchBySearchText(searchText: keyword, pageNumber: page, perPage: perPage) { (success,resSuggestions,message) in
            self.isLoading = false

            if let someSuggestions = resSuggestions{
                if someSuggestions.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.suggestions.append(contentsOf:someSuggestions)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.aTableView.reloadData()
        }
    }

}
extension SearchCitiesViewController: UISearchBarDelegate{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSeachActive = false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text!.count==0{
            return
        }
        CommonClass.sharedInstance.clearAllPendingRequests()
        self.page = 1
        isSeachActive = true
        self.isLoading = true
        self.suggestions.removeAll()
        self.aTableView.reloadData()
        self.loadCitiesFromServer(searchBar.text!, page: self.page, perPage: self.perPage)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count==0{
            return
        }
        CommonClass.sharedInstance.clearAllPendingRequests()
        self.page = 1
        self.isLoading = true
        isSeachActive = true
        self.suggestions.removeAll()
        self.aTableView.reloadData()
        self.loadCitiesFromServer(searchBar.text!, page: self.page, perPage: self.perPage)
    }
}

extension SearchCitiesViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if suggestions.count != 0{
            rows = suggestions.count
        }
        return rows
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.suggestions.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            if searchCount == 0{
                cell.messageLabel.text = "Loading.."
            }else{
                if isLoading{
                    cell.messageLabel.text = "Searching.."
                }else{
                    cell.messageLabel.text = "Searching.."//"Enter city/marina to search"
                }
            }
            return cell
        }else{

        let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaLocationHomeCell", for: indexPath) as! MarinaLocationHomeCell
        let suggestion = suggestions[indexPath.row]
        cell.locationPhoto.setIndicatorStyle(.gray)
        cell.locationPhoto.setShowActivityIndicator(true)
        cell.locationPhoto.sd_setImage(with: URL(string:suggestion.image), placeholderImage: #imageLiteral(resourceName: "placeholderamenity"))
        cell.locationNameLabel.text = suggestion.name
        cell.categoryLabel.text = suggestion.category.capitalized
            cell.categoryLabel.textColor = (suggestion.suggestionType == .city) ? kApplicationRedColor.withAlphaComponent(0.9) : kNavigationColor.withAlphaComponent(0.9)
        CommonClass.makeViewCircularWithRespectToHeight(cell.locationNameLabel, borderColor: UIColor.clear, borderWidth: 0)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if suggestions.count == 0{return}
        let suggestion = self.suggestions[indexPath.row]
        navigateToSuggestedObject(suggestion: suggestion)
    }

    func navigateToSuggestedObject(suggestion:Suggestion) {
        if suggestion.suggestionType == .city{
            self.navigateToMarinaListByCity(suggestion: suggestion)
        }else{
            let marina = Marina()
            marina.ID = suggestion.ID
            marina.title = suggestion.name
            self.navigateToMarinaProfile(marina, shouldDirectBooking: false)
        }
    }

    func navigateToMarinaListByCity(suggestion:Suggestion) {
        let marinaListVC = AppStoryboard.Home.viewController(MarinaListViewController.self)
        marinaListVC.city.ID = suggestion.ID
        marinaListVC.city.name = suggestion.name
        marinaListVC.city.image = suggestion.image
        self.navigationController?.pushViewController(marinaListVC, animated: true)
    }

    func navigateToMarinaProfile(_ marina:Marina,shouldDirectBooking:Bool) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        marinaProfileVC.shouldGoDirectlyToBooking = shouldDirectBooking
        self.navigationController?.pushViewController(marinaProfileVC, animated: !shouldDirectBooking)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        self.page+=1
                        self.loadCitiesFromServer(keyword, page: page, perPage: perPage)
                    }
                }
            }
        }
        
    }
}
