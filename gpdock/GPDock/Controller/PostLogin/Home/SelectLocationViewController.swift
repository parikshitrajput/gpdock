//
//  SelectLocationViewController.swift
//  Sinr
//
//  Created by TecOrb on 01/07/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
protocol SelectLocationViewControllerDelegateProtocol {
    func placeDidSelected(_ place:GMSPlace)
}

class SelectLocationViewController: UIViewController {

    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var delegate : SelectLocationViewControllerDelegateProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = UIRectEdge.top
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        resultsViewController?.primaryTextColor = UIColor.darkGray
        resultsViewController?.primaryTextHighlightColor = UIColor.black
        resultsViewController?.secondaryTextColor = UIColor.darkGray
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        //filter.country = "USA"
        resultsViewController!.autocompleteFilter = filter
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        let subView = UIView(frame: CGRect(x:0, y:64, width:self.view.frame.size.width,height: 44))
        subView.addSubview((searchController?.searchBar)!)
        self.view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.placeholder = "Search by City"
        searchController?.hidesNavigationBarDuringPresentation = false
        self.navigationController!.navigationBar.isTranslucent = false
        searchController!.hidesNavigationBarDuringPresentation = false
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        self.definesPresentationContext = true
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        //self.searchController?.isActive = false
        self.navigationController?.pop(true)
    }
}

// Handle the user's selection.
extension SelectLocationViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        // searchController?.isActive = false
        let marinaListVC = AppStoryboard.Home.viewController(MarinaListViewController.self)
        let city = City()
        city.name = place.name
        self.navigationController?.pushViewController(marinaListVC, animated: true)
    }

    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription
        )
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

}

extension SelectLocationViewController{
func getTitleOfGMSPlace(_ place: GMSPlace) -> String {
    var title = ""
    if let addressComponents = place.addressComponents{
        for ac in addressComponents{
            if ac.type == "premise"{
                title = ac.name
                break
            }
            if title.isEmpty{
                if ac.type == "sublocality_level_2"{
                    title = ac.name
                    break
                }
            }

            if title.isEmpty{
                if ac.type == "sublocality_level_1"{
                    title = ac.name
                    break
                }
            }
            if title.isEmpty{
                if ac.type == "locality"{
                    title = ac.name
                    break;
                }
            }
        }
    }
    if title.isEmpty{
        return "Unknown"
    }else
    {
        return title
    }
}


func getSubTitleOfGMSPlace(_ place: GMSPlace) -> String {
    var title = ""
    if let addressComponents = place.addressComponents{
        for ac in addressComponents{
            if ac.type == "locality"{
                title = ac.name
            }
            if ac.type == "administrative_area_level_2"{
                title = title + ", \(ac.name)"
                break;
            }
            if title.isEmpty{
                if ac.type == "administrative_area_level_1"{
                    title = ac.name
                    break;
                }
                if ac.type == "country"{
                    title = ac.name
                    break;
                }

            }

        }
    }
    if title.isEmpty{
        return "Unknown"
    }else
    {
        return title
    }
}
 
 @IBAction func getCurrentLocationOfUser(_ sender:UIButton) {
    //CommonClass.showLoaderWithStatus("Getting your current location")
    let placesClient =  GMSPlacesClient()
    placesClient.currentPlace(callback: { (placeLikelihoods, error) -> Void in
        CommonClass.hideLoader()
        guard error == nil else {
            print("Current Place error: \(error!.localizedDescription)")
            return
        }

        if let placeLikelihoods = placeLikelihoods {
            if let likelihood = placeLikelihoods.likelihoods.max(by: { (lh1, lh2) -> Bool in
                return lh1.likelihood < lh2.likelihood
            }) {

                let place = likelihood.place
                let title = self.getTitleOfGMSPlace(place)
                let subTitle = self.getSubTitleOfGMSPlace(place)
                let alert = UIAlertController(title: "Sinr", message: "Your current location is \(title), \(subTitle)", preferredStyle: UIAlertControllerStyle.alert)

                let okayAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { (okayAction) in
                    self.navigationController?.dismiss(animated: true, completion: {
                        //self.delegate?.locationDidSelected((title,subTitle), coordinate: place.coordinate)
                    })
                    self.delegate?.placeDidSelected(place)
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (cancelAction) in
                    alert.dismiss(animated: true, completion: {
                    })
                })
                alert.addAction(okayAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }

    })
}
}
