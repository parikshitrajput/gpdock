//
//  AddBoatViewController.swift
//  GPDock
//
//  Created by TecOrb on 21/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddBoatViewControllerDelegate{
    //func addBoatViewController(_ viewController: AddBoatViewController, didAddedBoat: ParkingSpace)
    func addBoatViewController(_ viewController: AddBoatViewController,didUpdateBoat boat: Boat,at indexPath:IndexPath)
}

class AddBoatViewController: UIViewController,SettingsViewControllerDelegate {
    var user = User()
    var userBoat : Boat?
    var indexPath: IndexPath!
    var isFromMenu = true
    var delegate : AddBoatViewControllerDelegate?
    var titleView : NavigationTitleView!

    @IBOutlet weak var lengthTextField: UITextField!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var depthTextField: UITextField!

    @IBOutlet weak var lengthUnitLabel: UILabel!
    @IBOutlet weak var widthUnitLabel: UILabel!
    @IBOutlet weak var depththUnitLabel: UILabel!

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var boatImageView: UIImageView!
    @IBOutlet weak var boatNameTextField: UITextField!

    var imageUpdate = false
    var selectedBoatSize : BoatSize = BoatSize(length: 0, width:0 , depth: 0)
    var imagePickerController : UIImagePickerController!
    var boatImage : UIImage?

    override func loadView() {
        super.loadView()
        //NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdateHomeScreen(_:)), name: .DISMISS_VIEW, object: nil)
        self.navigationController?.navigationBar.isHidden = false
        refreshUnit()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = !self.isFromMenu
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = (self.userBoat != nil) ? "Edit Boat Details" : "Add Boat"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLayoutSubviews() {
        self.addButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    func refreshUnit(){
        let unittext = Settings.sharedInstance.lengthUnit
        if unittext == "Foot"{
            self.lengthUnitLabel.text = "ft"
            self.widthUnitLabel.text = "ft"
            self.depththUnitLabel.text = "ft"
        }else{
            self.lengthUnitLabel.text = "ft"
            self.widthUnitLabel.text = "ft"
            self.depththUnitLabel.text = "ft"
        }
    }
    @IBAction func onClickSettingButtons(_ sender: UIButton){
        self.openSettingsScreen()
    }

    func openSettingsScreen() {
        let settingsVC = AppStoryboard.Settings.viewController(SettingsViewController.self)
        settingsVC.isFromMenu = false
        settingsVC.delegate = self
        let nav = UINavigationController(rootViewController: settingsVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func settingsViewController(viewController: SettingsViewController, didChangeSettings changed: Bool) {
        if changed{
            self.refreshUnit()
        }
        viewController.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickUploadScreenShot(_ sender:UIButton){
        self.showAlertToChooseAttachmentOption()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.removeObserver(self)

        if let userJson = User.loadUserInfo() as JSON?{
            self.user = User(json: userJson)
        }

        self.lengthTextField.delegate = self
        self.widthTextField.delegate = self
        self.depthTextField.delegate = self
        self.lengthTextField.addTarget(self, action: #selector(textFieldTextDidChangeEditing(_:)), for: .editingChanged)
        self.widthTextField.addTarget(self, action: #selector(textFieldTextDidChangeEditing(_:)), for: .editingChanged)
        self.depthTextField.addTarget(self, action: #selector(textFieldTextDidChangeEditing(_:)), for: .editingChanged)

        NotificationCenter.default.addObserver(self, selector:#selector(userDidLoggedIn(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
       // NotificationCenter.default.addObserver(self, selector:#selector(gotoSupportPageSucceefully(_:)), name: Notification.Name(USER_SUPPORT_NOTIFICATION), object: nil)


        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.setupNavigationTitle()
        if let boat = userBoat{
            //self.navigationItem.title = "Update Boat"
            self.boatNameTextField.text = boat.name
            self.lengthTextField.text = "\(boat.boatSize.length)"
            self.widthTextField.text = "\(boat.boatSize.width)"
            self.depthTextField.text = "\(boat.boatSize.depth)"
           // self.addButton.setTitle("Edit Boat", for: .normal)
            self.addButton.setTitle("Done", for: .normal)

            if boat.image.count != 0 {
                self.boatImageView.setIndicatorStyle(.white)
                self.boatImageView.setShowActivityIndicator(true)
                self.boatImageView.sd_setImage(with: URL(string:boat.image), placeholderImage: #imageLiteral(resourceName: "upload_image"),options:[],completed: { (image, error, cacheType, url) in
                    self.boatImage = image
                    self.refreshUploadImageButton()
                })
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    @objc func userDidLoggedIn(_ notification: Notification)  {
        if notification.name == .USER_DID_LOGGED_IN_NOTIFICATION{
            if let aUser = notification.userInfo?["user"] as? User, aUser.ID != ""{
                self.user = aUser
            }
        }
    }

//    func collectionViewSetUp() {
//        CommonClass.makeViewCircularWithCornerRadius(addButton, borderColor: UIColor.lightGray, borderWidth: 0, cornerRadius: 5)
//        CommonClass.makeViewCircularWithCornerRadius(boatImageView, borderColor: UIColor.lightGray, borderWidth: 0, cornerRadius: 2)
//
//    }

    func registerCells() -> Void {
//        let nibName = UINib(nibName: "BoatSizeCVCell", bundle:nil)
//        self.lengthCV.register(nibName, forCellWithReuseIdentifier: "BoatSizeCVCell")
//        self.widthCV.register(nibName, forCellWithReuseIdentifier: "BoatSizeCVCell")
//        self.depthCV.register(nibName, forCellWithReuseIdentifier: "BoatSizeCVCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBackButton(_ sender:UIBarButtonItem){
        self.navigationController?.navigationBar.isHidden = !self.isFromMenu
        self.navigationController?.pop(true)
    }
    @IBAction func onClickUploadImage(_ sender:UIButton){
        self.showAlertToChooseAttachmentOption()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickAddBoat(_ sender:UIButton){

        let boatName = self.boatNameTextField.text!
        self.selectedBoatSize.length = Int(self.lengthTextField.text!) ?? 0
        self.selectedBoatSize.width = Int(self.widthTextField.text!) ?? 0
        self.selectedBoatSize.depth = Int(self.depthTextField.text!) ?? 0

        let validatetionResult = self.validateParams(boatname: boatName)
        if !validatetionResult.success{
            showAlertWith(viewController: self, message: validatetionResult.message, title: warningMessage.alertTitle.rawValue)
            return
        }
        
        let sizeValidate = self.validateBoatSizeParams(boatName: boatName)
        if !sizeValidate.success {
            self.MaximumBoatSizeAlert(title: warningMessage.alertTitle.rawValue, message: sizeValidate.message)
            return
        
        }
        
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: NETWORK_NOT_CONNECTED_MESSAGE, title:warningMessage.alertTitle.rawValue)
            return
        }

        if let boat = userBoat{
            self.updateBoatOnServer(self.user.ID,boatID:boat.ID, boatName: boatName, boatSize: self.selectedBoatSize, boatImage: boatImage) { (success) in
                if success{
                    self.navigationController?.pop(true)
                }else{
                    self.showSuccessAlertForBoatAdding(title: warningMessage.alertTitle.rawValue, message: "Unable to edit boat details with given parameters!\r\nWould you like to add boat with other paramaters?")
                }
            }
        }else{
        self.addBoatToServer(self.user.ID, boatName: boatName, boatSize: self.selectedBoatSize, boatImage: boatImage) { (success) in
            if success{
                self.navigationController?.pop(true)
            }else{
                self.showSuccessAlertForBoatAdding(title: warningMessage.alertTitle.rawValue, message: "Unable to add boat with given parameters!\r\nWould you like to add boat with other paramaters?")
            }
        }
        }

    }

    func refreshUploadImageButton() -> Void {
        //self.uploadImageButton.isSelected  = (self.boatImage != nil)
    }

    func addBoatToServer(_ userID: String, boatName:String, boatSize: BoatSize, boatImage:UIImage?, completionBlock: @escaping (_ success: Bool)->Void){
        CommonClass.showLoader(withStatus: "Adding..")
        MarinaService.sharedInstance.addBoatWith(userID, name: boatName, boatSize: boatSize,boatImage: boatImage) { (success,resBoat,message)  in
            CommonClass.hideLoader()
            if success{
            if let aBoat = resBoat {
                NotificationCenter.default.post(name: .USER_DID_ADD_BOAT_NOTIFICATION, object: nil, userInfo: ["boat":aBoat])
                completionBlock(true)
            }else{
                completionBlock(false)
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            }else{
                completionBlock(false)
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func updateBoatOnServer(_ userID: String,boatID:String, boatName:String, boatSize: BoatSize, boatImage:UIImage?, completionBlock: @escaping (_ success: Bool)->Void){
        CommonClass.showLoader(withStatus: "Updating..")
        MarinaService.sharedInstance.updateBoatWith(userID,boatID: boatID, name: boatName, boatSize: boatSize,boatImage: boatImage){ (success,resBoat,message)  in
                CommonClass.hideLoader()
        if success{
                    if let aBoat = resBoat {
                        self.userBoat = aBoat
                        self.delegate?.addBoatViewController(self, didUpdateBoat: aBoat, at: self.indexPath)
                        completionBlock(true)
                    }else{
                        completionBlock(false)
                        showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                    }
                }else{
                    completionBlock(false)
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
        }
    }


    func showSuccessAlertForBoatAdding(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            //self.resetFields()
            alert.dismiss(animated: true, completion: nil)
        }

        let nopeAction = UIAlertAction(title: "Nope", style: .cancel) { (action) in
            self.navigationController?.pop(false)
            alert.dismiss(animated: false, completion: {
            })
        }
        alert.addAction(yesAction)
        alert.addAction(nopeAction)
        self.present(alert, animated: true, completion: nil)

    }
    func resetFields(){
        self.boatImageView.image = #imageLiteral(resourceName: "upload_image")
        self.boatNameTextField.text = ""
        self.lengthTextField.text = ""
        self.widthTextField.text = ""
        self.depthTextField.text = ""

    }
    func validateParams(boatname:String) -> (success:Bool,message:String) {
        if boatname.count == 0{
            return(false,"Please enter the boat's name")
        }
        if self.selectedBoatSize.length == 0 {
            return(false,"Please enter the boat's length")
        }
        if self.selectedBoatSize.width == 0 {
            return(false,"Please enter the boat's width")
        }
        if self.selectedBoatSize.depth == 0 {
            return(false,"Please enter the boat's depth")
        }
        return(true," ")
    }
    func validateBoatSizeParams(boatName:String) -> (success:Bool,message:String) {
        if self.selectedBoatSize.length > 200 {
            return(false,"The boat length you have entered is more than 200 ft.  Please Check support for assistance.")
        }
        if self.selectedBoatSize.width > 200{
            return(false,"The boat width you have entered is more than 200 ft.  Please Check support for assistance.")
        }
        if self.selectedBoatSize.depth > 200{
            return(false,"The boat depth you have entered is more than 200 ft.  Please Check support for assistance.")
        }
        return(true," ")
    }
    
//    @objc func gotoSupportPageSucceefully(_ notification: Notification){
//        if let sInfo = notification.userInfo as? [String:AnyObject]{
//            if sInfo["support"] != nil{
//                let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
//                self.navigationController?.pushViewController(supportVC, animated: false)
//
//            }
//        }
//    }
    
   
    
    func MaximumBoatSizeAlert(title:String,message:String){
//        let supportVC = AppStoryboard.Settings.viewController(SupportPopUpViewController.self)
//        supportVC.sizeBoat = message
//        supportVC.modalPresentationStyle = .overFullScreen
//
//        self.navigationController?.present(supportVC, animated: true, completion: nil)
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Support", style: .default) { (action) in
           self.navigateToSupportView()
//            self.dismiss(animated: true, completion: nil)
        }

        let nopeAction = UIAlertAction(title: "Nope", style: .cancel) { (action) in
            alert.dismiss(animated: false, completion: {
            })
        }
        alert.addAction(yesAction)
        alert.addAction(nopeAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
//    @objc func getUpdateHomeScreen(_ notification: Notification) {
//        self.navigateToSupportView()
//    }
//
    func navigateToSupportView() {
//        NotificationCenter.default.post(name: Notification.Name(USER_SUPPORT_NOTIFICATION), object: nil, userInfo: ["support":"support"])
        let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        self.navigationController?.pushViewController(supportVC, animated: false)


    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddBoatViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if(textField == self.lengthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }else if(textField == self.widthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }else if(textField == self.depthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.lengthTextField{
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
        }else if textField == self.widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
        }else if textField == self.depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.lengthTextField{
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
        }else if textField == self.widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
        }else if textField == self.depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
        }
    }
    @IBAction func textFieldTextDidChangeEditing(_ textField: UITextField) {
        if textField == self.lengthTextField{
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
        }else if textField == self.widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
        }else if textField == self.depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
        }
    }
}

extension AddBoatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = false
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = false
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.boatImage = tempImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.boatImage = tempImage
        }

        let imageCropVC = RSKImageCropViewController(image: self.boatImage ?? UIImage(), cropMode: .square)
        imageCropVC.delegate = self
        self.navigationController?.pushViewController(imageCropVC, animated: true)
        picker.dismiss(animated: true) {}
    }
}

//MARK: - RSKImageCropViewControllerDelegate
extension AddBoatViewController:RSKImageCropViewControllerDelegate{
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.navigationController?.pop(true)
    }


    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        self.boatImage = croppedImage
        self.boatImageView.image = croppedImage
        self.refreshUploadImageButton()
        self.navigationController?.pop(true)
    }

}





