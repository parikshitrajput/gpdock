//
//  MarinaListViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Crashlytics

class MarinaListViewController:UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var aTableView: UITableView!

    @IBOutlet weak var filterContainner: UIView!
    @IBOutlet weak var filterIndicator: UIView!

    @IBOutlet weak var sortButtonContainner: UIVisualEffectView!

    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!


    var titleView : NavigationTitleView!

    var page = 1
    var perPage = 15
    var user: User!
    var isLoading = false
    var city = City()
    var sortingKey: String?
    var orderBy: String?
    var filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)?
    var marinas = [Marina]()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        refreshControl.addTarget(self, action: #selector(MarinaListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json:User.loadUserInfo()!)
        NotificationCenter.default.addObserver(self, selector: #selector(favoriteNotificationHandler(_:)), name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuisnessDetailViewController.userDidPayAtMarinaHandler(_:)), name: .USER_PAID_AT_MARINA_NOTIFICATION, object: nil)

        let nib = UINib(nibName: "NoDataAndReloadCell", bundle: nil)
        self.aTableView.register(nib, forCellReuseIdentifier: "NoDataAndReloadCell")
        self.aTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.aTableView.addSubview(refreshControl)
        self.aTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.aTableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        self.setupNavigationTitle()
        self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
        self.aTableView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithRespectToHeight(self.filterContainner, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.filterButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.filterIndicator, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.sortButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.sortButtonContainner, borderColor: UIColor.clear, borderWidth: 0)
        self.sortButtonContainner.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])


        self.filterContainner.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = city.name.uppercased()
        self.navigationItem.titleView = self.titleView
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.filterIndicator.isHidden = (self.filters == nil)
        if titleView != nil{
            titleView.titleLabel.text = (self.filters == nil) ? city.name.uppercased() : "Filtered Result"
        }
//        self.sortingKey = nil
//        self.orderBy = nil
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func favoriteNotificationHandler(_ notification : Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,AnyObject>{
            if let marinaID = userInfo["marinaID"] as? String, let isFavorite = userInfo["isFavorite"] as? Bool{
                for i in 0..<self.marinas.count{
                    if marinaID == self.marinas[i].ID{
                        self.marinas[i].isFavorite = isFavorite
                        self.aTableView.reloadData()
                        break
                    }
                }
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        self.sortingKey = nil
        self.orderBy = nil
        self.filters = nil
        self.filterIndicator.isHidden = (self.filters == nil)

        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaList(self.city.ID,userID:self.user.ID, latitude: nil, longitude: nil, page: page, perPage: perPage) { (success,resMarinas,resCity,message)  in
            self.isLoading = false
            self.marinas.removeAll()
            refreshControl.endRefreshing()
            if let someMarinas = resMarinas{
                self.marinas.append(contentsOf: someMarinas)
            }
            if let aCity = resCity{
                self.city = aCity
            }
            
            self.aTableView.reloadData()
        }
    }


    @IBAction func onClickPayAtMarinaButton(_ sender: UIButton){
        if let indexpath = sender.tableViewIndexPath(self.aTableView) as IndexPath?{
            self.payAtMarina(marina: self.marinas[indexpath.row])
        }
    }
    
    func payAtMarina(marina:Marina){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if !marina.isSigned{
                return
            }

            let payToMarinaVCNavigation = AppStoryboard.Home.viewController(PayMoneyNavigationController.self)
            for vc in payToMarinaVCNavigation.viewControllers{
                if let payVC = vc as? PayForMarinaViewController{
                    payVC.marina = marina
                    break
                }
            }
            self.present(payToMarinaVCNavigation, animated: true, completion: nil)
        }
    }
    @objc func userDidPayAtMarinaHandler(_ notification: Notification) {
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickMessageButton(_ sender: UIButton){
       // if CommonClass.isLoggedIn{
            let notificationVC = AppStoryboard.Profile.viewController(NotificationListViewController.self)
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: notificationVC, withSlideOutAnimation: false, andCompletion: { })
        //}else{
          //  showLoginAlert(isFromMenu: true,inViewController: nil)
        //}
    }

    @IBAction func onClickBookNowButton(_ sender: UIButton){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if let indexPath = sender.tableViewIndexPath(aTableView) as IndexPath?{
                let marina = self.marinas[indexPath.row]
                self.navigateToMarinaProfile(marina,shouldDirectBooking:true)
            }
        }
    }
    func openLoginModule() {
        let nav = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController

        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onclickMapButton(_ sender: UIButton){
        let marinasOnMapVC = AppStoryboard.Home.viewController(MarinasOnMapViewController.self)
        marinasOnMapVC.city = self.city
        marinasOnMapVC.marinas = self.marinas
        marinasOnMapVC.filters = self.filters
        self.navigationController?.pushViewController(marinasOnMapVC, animated: false)
    }



    @IBAction func onClickFilterButton(_ sender: UIButton){
        let filterVC = AppStoryboard.Home.viewController(FiltersViewController.self)
        filterVC.delegate = self
        filterVC.city = self.city
        filterVC.filters = self.filters
        let nav = UINavigationController(rootViewController: filterVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: {
        })
    }

    @IBAction func onClickSortButton(_ sender: UIButton){
        CommonClass.sharedInstance.showSortingOptions(withDelegate: self)
    }

    func loadFilteredMarinasListFromServer(_ sortingKey: String?,orderBy:String?,filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double,marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)?,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getFilteredMarinaList(sortingKey, sortingOrder: orderBy, cityID: self.city.ID, userID:self.user.ID, filters: filters,page: page, perPage: perPage, minPriceRange:self.city.marinaMinPrice, maxPriceMax: self.city.marinaMaxPrice) { (resMarinas) in
            self.isLoading = false
            if let someMarinas = resMarinas as? [Marina]{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.aTableView.reloadData()
        }
    }



    func loadMarinasListFromServer(_ cityID: String,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaList(cityID,userID:self.user.ID, latitude: nil, longitude: nil, page: page, perPage: perPage) { (success,resMarinas,resCity,message) in
            self.isLoading = false
            if let someMarinas = resMarinas{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            if let aCity = resCity{
                self.city = aCity
            }
            self.aTableView.reloadData()
        }
    }

    @IBAction func onReloadButton(_ sender: UIButton){
        self.page = 1
        self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
    }


    // MARK: - UITableViewDataSource Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if marinas.count != 0{
            rows = marinas.count
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.marinas.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text =  isLoading ? "Loading.." : "No Marina found\r\nRefresh to try again"
            return cell
        }else{

            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaCell", for: indexPath) as! MarinaCell
            let marina = marinas[indexPath.row]
            cell.marinaPhoto.setIndicatorStyle(.gray)
            cell.marinaPhoto.setShowActivityIndicator(true)
            cell.isPrivateImageView.image = marina.isPrivate ? #imageLiteral(resourceName: "ic_red_lock") : UIImage()
            cell.marinaPhoto.sd_setImage(with: URL(string:marina.image.url), completed: {[cell] (image, error, cacheType, url) in
                cell.marinaPhoto.animateImage(duration:0.5)
            })

            cell.marinaNameLabel.text = marina.title
            cell.priceLabel.text =  "$"+String(format: "%.2f/foot",marina.pricePerFeet)
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            cell.bookingButton.addTarget(self, action: #selector(onClickBookNowButton(_:)), for: .touchUpInside)
            cell.payForServiceButton.addTarget(self, action: #selector(onClickPayAtMarinaButton(_:)), for: .touchUpInside)
            cell.payForServiceButton.isHidden = !marina.isSigned
            cell.priceLabel.isHidden = !marina.isSigned

            CommonClass.makeViewCircularWithRespectToHeight(cell.distanceLabel, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.ratingButton, borderColor: UIColor.clear, borderWidth: 0)

            CommonClass.makeViewCircularWithRespectToHeight(cell.bookingButton, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.priceLabel, borderColor: UIColor.clear, borderWidth: 0)
            cell.likeButton.setImage(marina.isFavorite ? UIImage(named: "liked") : UIImage(named: "like"), for: UIControlState())
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }


    @IBAction func likeButtonTapped(_ sender: SparkButton) {
        if !CommonClass.isLoggedIn{
            showLoginAlert(isFromMenu: false, shouldLogin: true, inViewController: self)
            return
        }
        if let indexPath = sender.tableViewIndexPath(aTableView) as IndexPath?{
            self.marinas[indexPath.row].isFavorite = !self.marinas[indexPath.row].isFavorite
            let marina = self.marinas[indexPath.row]
            self.toggleFavorate(userID: self.user.ID, marinaID: marina.ID)

            if marina.isFavorite == true {
                sender.setImage(UIImage(named: "liked"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
            }
            else{
                sender.setImage(UIImage(named: "like"), for: UIControlState())
                sender.unLikeBounce(0.4)
            }
        }

    }

    func toggleFavorate(userID: String,marinaID:String) {
        MarinaService.sharedInstance.toggleFavorites(userID, marinaID: marinaID) { (status, marinaID) in
            for marina in self.marinas{
                if marinaID == marina.ID{
                    marina.isFavorite = status
                    self.aTableView.reloadData()
                    NotificationCenter.default.post(name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil, userInfo: ["marinaID":marinaID,"isFavorite":status])
                    break
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return   marinas.count != 0 ? screenWidth*4.5/7 : 90
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if let ccell = cell as? MarinaCell{
                let marina = marinas[indexPath.row]
                ccell.distanceLabel.text = marina.radioFrequency.uppercased()
                ccell.distanceLabel.textAlignment = .left
                let rating = (marina.averageRating == 0.0 ) ? "No Rating" : String(format: "%.1f",marina.averageRating)
                ccell.ratingButton.setTitle(rating, for: .normal)

                ccell.setNeedsLayout()
                ccell.layoutIfNeeded()
            }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.marinas.count == 0{
            return
        }
        let marina = self.marinas[indexPath.row]
        self.navigateToMarinaProfile(marina,shouldDirectBooking:false)
    }

    func navigateToMarinaProfile(_ marina:Marina,shouldDirectBooking:Bool) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        marinaProfileVC.shouldGoDirectlyToBooking = shouldDirectBooking
        marinaProfileVC.buisnessDetailViewControllerDelegate = self
        self.navigationController?.pushViewController(marinaProfileVC, animated: !shouldDirectBooking)
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.city.ID == "" {return}
        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        if ((self.filters != nil) || (self.sortingKey != nil && self.self.orderBy != nil)){
                            self.page+=1
                            self.loadFilteredMarinasListFromServer(self.sortingKey, orderBy: self.orderBy, filters: self.filters, page: self.page, perPage: self.perPage)
                        }else{
                            self.page+=1
                            self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
                        }
                    }
                }
            }
        }

    }
}

extension MarinaListViewController: FiltersViewControllerDelegate{
    func filterViewController(_ viewController: FiltersViewController, didClearSelectedFilters cleared: Bool) {
        if cleared{
            self.filters = nil
            self.sortingKey = nil
            self.orderBy = nil
            self.page = 1
            self.marinas.removeAll()
            self.isLoading = true
            self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
            self.aTableView.reloadData()
        }
    }

    func filterViewController(_ viewController: FiltersViewController, didSelectFilter filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter,service: Set<Service>, amenities: Set<Aminity>)) {
        self.filters = filters
        self.page = 1
        self.marinas.removeAll()
        self.isLoading = true
        self.loadFilteredMarinasListFromServer(sortingKey, orderBy: orderBy, filters: self.filters, page: self.page, perPage: self.perPage)
        self.aTableView.reloadData()
    }
}

extension MarinaListViewController: SortingSelectionViewControllerDelegate{
    func sort(viewController: SortingSelectionViewController, sortingKey: String, orderBy: String) {

        if ((sortingKey.count == 0) || (orderBy.count == 0)){
            return
        }
        self.page = 1
        self.sortingKey = sortingKey
        self.orderBy = orderBy
        self.marinas.removeAll()
        self.isLoading = true
        self.loadFilteredMarinasListFromServer(sortingKey, orderBy: orderBy, filters: self.filters, page: self.page, perPage: self.perPage)
        self.aTableView.reloadData()
    }
}

extension MarinaListViewController: BuisnessDetailViewControllerDelegate{
    func marinaDidRefresh(_ viewController: BuisnessDetailViewController, refreshedMarina marina: Marina) {
        for (index,_marina) in self.marinas.enumerated(){
            if _marina.ID == marina.ID{
                self.marinas[index].isSigned = marina.isSigned
                self.aTableView.reloadData()
                break
            }
        }
    }
}







