//
//  MarinaProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
//enum contentTypes {
//    case tweets, media
//}
//let offset_HeaderStop:CGFloat = 0//220 - 64  // At this offset the Header stops its transformations

class MarinaProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var header : StretchHeader!
    @IBOutlet var segmentedView : UIView!
    var contentToDisplay : contentTypes = .overView
    var marina : Marina!

    @IBOutlet weak var marinaTableView: UITableView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = marina.title
        setupHeaderView()
    }


    
    func setupHeaderView() {
        let options = StretchHeaderOptions()
        options.position = .underNavigationBar

        header = StretchHeader()
        header.stretchHeaderSize(headerSize: CGSize(width: view.frame.size.width, height: 220),
                                 imageSize: CGSize(width: view.frame.size.width, height: 220),
                                 controller: self,
                                 options: options)
        header.imageView.image = UIImage(named: "0")

        // custom
        let label = UILabel()
        label.frame = CGRect(x: 10, y: header.frame.size.height - 40, width: header.frame.size.width - 20, height: 40)
        label.textColor = UIColor.white
        label.text = marina.title
        label.font = UIFont.boldSystemFont(ofSize: 16)
        header.addSubview(label)
        marinaTableView.tableHeaderView = header
    }

    // MARK: - ScrollView Delegate
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
        header.updateScrollViewOffset(scrollView)
        let offset = scrollView.contentOffset.y + header.bounds.height
        // Segment control

        let segmentViewOffset = header.frame.height - segmentedView.frame.height - offset

        var segmentTransform = CATransform3DIdentity

        // Scroll the segment view until its offset reaches the same offset at which the header stopped shrinking
        segmentTransform = CATransform3DTranslate(segmentTransform, 0, max(segmentViewOffset, -offset_HeaderStop), 0)

        segmentedView.layer.transform = segmentTransform


        // Set scroll view insets just underneath the segment control
        marinaTableView.scrollIndicatorInsets = UIEdgeInsetsMake(segmentedView.frame.maxY, 0, 0, 0)
    }

    // MARK: - Table view data source
    // MARK: Table view processing

    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {


        if section == 0 || section == 2 || section == 3 || section == 4{
            return 1
        }else {

            return 3
        }

    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return  indexPath.section == 4 ? ScreenSize.SCREEN_WIDTH * 0.5628 + 32  : UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewStaticCell", for: indexPath)

            return cell
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath)

            return cell
        }

        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath)

            return cell
        }

        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealsCell", for: indexPath)

            return cell
        }

        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewDynamicCell", for: indexPath)

            return cell
        }



    }


    // MARK: Interface buttons


    @IBAction func selectContentType(_ sender: UISegmentedControl) {

        // crap code I know
        if sender.selectedSegmentIndex == 0 {
            contentToDisplay = .overView
        }
        else {
            contentToDisplay = .animities
        }

        marinaTableView.reloadData()
    }



}
