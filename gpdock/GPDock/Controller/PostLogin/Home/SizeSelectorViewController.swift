//
//  SizeSelectorViewController.swift
//  GPDock
//
//  Created by TecOrb on 12/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

public struct BoatSize {

    public var length: Int{

        didSet(prevlength){
            if prevlength != length {
                if length >= 20 && length <= 30{
                    range = "20-30"
                }else if length >= 31 && length <= 40{
                    range = "31-40"
                }else if length >= 41 && length <= 50{
                    range = "41-50"
                }else{
                    range = "50+"
                }
            }
        }
    }

    public var width: Int
    public var depth: Int
    public var range : String = "20-30"
    public init(){
        self.length = 0
        self.width = 0
        self.depth = 0
    }

    public init(length: Int, width: Int,depth:Int){
        self.length = length
        self.width = width
        self.depth = depth
    }
}


protocol SizeSelectorViewControllerDelegate {
    func sizeSelectorViewController(_ viewController: SizeSelectorViewController, didSelectBoatSize boatSize: BoatSize, withUserOwnBoat userBoat:Boat?)
//    func sizeSelectorViewController(viewController: SizeSelectorViewController, shouldProceedToFloorPlan proceed: Bool)
}
class SizeSelectorViewController: UIViewController {
    @IBOutlet weak var containnerView: UIView!
    @IBOutlet weak var lengthTextField: UITextField!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var depthTextField: UITextField!

    var delegate : SizeSelectorViewControllerDelegate?
    @IBOutlet weak var rangeCV: UICollectionView!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var lengthUnitLabel: UILabel!
    @IBOutlet weak var widthUnitLabel: UILabel!
    @IBOutlet weak var depththUnitLabel: UILabel!

    @IBOutlet weak var leftCountLabel: NKCustomLabel!
    var isFirstTime = true
    //@IBOutlet weak var selectedBoatImage: UIImageView!


    var marina : Marina!
    var bookingDates: BookingDates!

    var selectedBoatSize : BoatSize = BoatSize(length: 21, width:8 , depth: 6)

//    var lengthSource = Array<Int>()
//    var widthSource = Array<Int>()
//    var depthSource = Array<Int>()
    //var rangeSource = ["20-30","31-40","41-50","50+"]
//    var lenthOffSetsToMove = [0,11,21,31]

    var boatsArray = Array<Boat>()
    var userOwnBoat : Boat!

    var user : User!
    var selectedBoatIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        rangeCV.dataSource = self
        rangeCV.delegate = self
        refreshUnit()
        CommonClass.makeViewCircularWithCornerRadius(containnerView, borderColor: UIColor.lightGray, borderWidth: 1, cornerRadius: 2)
        self.leftCountLabel.text = self.marina.isSigned ? "Loading slips available.." : "Please select a boat and proceed"
        //self.getMarinaAvailability(self.selectedBoatSize, bookingDates: self.bookingDates)
        self.proceedButton.setNeedsLayout()
        self.proceedButton.layoutSubviews()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.proceedButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func viewDidLayoutSubviews() {
        self.proceedButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    @IBAction func onClickSettingButtons(_ sender: UIButton){
        self.openSettingsScreen()
    }
    
    func refreshUnit(){
        let unittext = Settings.sharedInstance.lengthUnit
        if unittext == "Foot"{
            self.lengthUnitLabel.text = "ft"
            self.widthUnitLabel.text = "ft"
            self.depththUnitLabel.text = "ft"
        }else{
            self.lengthUnitLabel.text = "ft"
            self.widthUnitLabel.text = "ft"
            self.depththUnitLabel.text = "ft"
        }
    }

    func openSettingsScreen() {
        let settingsVC = AppStoryboard.Settings.viewController(SettingsViewController.self)
        settingsVC.isFromMenu = false
        settingsVC.delegate = self
        let nav = UINavigationController(rootViewController: settingsVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

    func getMarinaAvailabilityForUser(_ userID:String,bookingDates:BookingDates) -> Void {
        CommonClass.showLoader(withStatus: "Loading slips available..")
        MarinaService.sharedInstance.getParkingAvailabilityCountForUser(userID, marinaID: self.marina.ID, bookingDate: bookingDates, completionBlock: { (resBoats) in
            CommonClass.hideLoader()

            if let someBoats = resBoats{
                self.boatsArray.removeAll()
                self.boatsArray.append(contentsOf: someBoats)
                self.rangeCV.reloadData()
                if self.boatsArray.count == 0{
                    self.leftCountLabel.text = "Please add a boat to proceed"
                    if self.isFirstTime{
                        self.openAddBoatScreen()
                    }
                }else{
                    self.selectedBoatIndex = 0
                    let indexPath = IndexPath(item: self.selectedBoatIndex, section: 0)
                    self.collectionView(self.rangeCV, didSelectItemAt: indexPath)
                }
            }
        })
    }

    func getMarinaAvailability(_ boatSize: BoatSize,bookingDates:BookingDates) -> Void {
        if !self.marina.isSigned{
            self.proceedButton.isEnabled = self.marina.isSigned ? false : true
            self.leftCountLabel.text = " "
            return
        }

        MarinaService.sharedInstance.getParkingAvailabilityCountForMarina(self.marina.ID, boatSize: boatSize, bookingDate:bookingDates) { (resCount) in
            //  CommonClass.hideLoader()
            if let aCount = resCount as Int?{
                self.proceedButton.isEnabled = (aCount > 0)
                self.proceedButton.alpha = (aCount > 0) ? 1.0 : 0.5
                if (aCount < 5)&&(aCount > 0){
                    if self.selectedBoatIndex != -1{
                        self.leftCountLabel.text = self.marina.isSigned ? "Hurry up!\r\n\(aCount) slips left for this boat" : ""
                    }else{
                        self.leftCountLabel.text = "Hurry up!\r\n\(aCount) slips left for this boat"
                    }
                }else if aCount > 4{
                    if self.selectedBoatIndex != -1{
                        self.leftCountLabel.text = "\(aCount) slips left for this boat"
                    }else{
                        self.leftCountLabel.text = "\(aCount) slips left for this boat"
                    }
                }else{
                    if self.selectedBoatIndex != -1{
                        self.leftCountLabel.text = "We are sorry but the size of your boat exceeds the measures authorized for this marina, please check the dimensions of your boat and try again."
                    }else{
                        self.leftCountLabel.text = "We are sorry but the size of your boat exceeds the measures authorized for this marina, please check the dimensions of your boat and try again."
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.proceedButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

        if self.marina.isSigned{
            self.getMarinaAvailabilityForUser(self.user.ID, bookingDates: self.bookingDates)
        }else{
            self.proceedButton.isEnabled = false
            self.loadBoatsFor(self.user.ID, pageNumber: 1, recordPerPage: 1000)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        super.viewWillDisappear(animated)
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickProceed(_ sender: UIButton){
        if userOwnBoat != nil{
            self.dismiss(animated: false, completion: nil)
            delegate?.sizeSelectorViewController(self, didSelectBoatSize: selectedBoatSize, withUserOwnBoat: userOwnBoat)
        }else{
            showAlertWith(viewController: self, message: "Please select a boat\r\nIf you don't have any boat added please add first", title: warningMessage.alertTitle.rawValue)
        }

    }

    @IBAction func onClickAddBoat(_ sender: UIButton){
        self.openAddBoatScreen()
    }
    func openAddBoatScreen() {
        self.isFirstTime = false
        let addBaotCV = AppStoryboard.Home.viewController(AddBoatViewController.self)
        addBaotCV.isFromMenu = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pushViewController(addBaotCV, animated: true)
    }

}

extension SizeSelectorViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,SettingsViewControllerDelegate{
    func settingsViewController(viewController: SettingsViewController, didChangeSettings changed: Bool) {
        if changed{
            self.refreshUnit()
        }
        viewController.dismiss(animated: true, completion: nil)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.boatsArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BoatSizeRangeCell", for: indexPath) as! BoatSizeRangeCell
        let boat = boatsArray[indexPath.item]
        cell.boatRangeIcon.sd_setImage(with: URL(string:boat.image), placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
        cell.leftCountLabel.text = boat.name
        if self.selectedBoatIndex == indexPath.item{
            CommonClass.makeViewCircularWithCornerRadius(cell.boatRangeIcon, borderColor: kApplicationGreenColor, borderWidth: 0, cornerRadius: 0)
            cell.removeBoatButton.isHidden = true
            cell.removeBoatButton.addTarget(self, action: #selector(removeButton(_:)), for: .touchUpInside)
        }else{
            CommonClass.makeViewCircularWithCornerRadius(cell.boatRangeIcon, borderColor: UIColor.clear, borderWidth: 2, cornerRadius: 0)
            cell.removeBoatButton.isHidden = true
        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    @IBAction func removeButton(_ sender: UIButton) -> Void {
        if let indexPath = sender.collectionViewIndexPath(self.rangeCV){
            _ = boatsArray[indexPath.item]

        }
    }

    func removeBoat(baot: Boat){

    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let boat = boatsArray[indexPath.item]
        self.selectedBoatIndex = indexPath.item
        self.userOwnBoat = boat
        self.leftCountLabel.text = "Loading slips available.."
        self.selectedBoatSize = boat.boatSize
        self.lengthTextField.text = "\(self.selectedBoatSize.length)"
        self.widthTextField.text = "\(self.selectedBoatSize.width)"
        self.depthTextField.text = "\(self.selectedBoatSize.depth)"
        collectionView.reloadData()
        self.proceedButton.isEnabled = self.marina.isSigned ? false : true
        self.getMarinaAvailability(self.selectedBoatSize, bookingDates: self.bookingDates)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height = collectionView.frame.size.height*17/20 - 2
        if self.selectedBoatIndex == indexPath.item{
            height = collectionView.frame.size.height - 2
        }
        return CGSize(width: height, height: height)
    }

    func isLastOf(_ dataSource:Array<Int>,item: Int) -> Bool {
        var result = false
        if let last = dataSource.last{
            result = (last == item)
        }
        return result
    }
}


extension SizeSelectorViewController{
    func loadBoatsFor(_ userID:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }



        MarinaService.sharedInstance.getUserBoatList(userID, page: pageNumber, perPage: recordPerPage) { (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            if let someBoats = response{
                self.boatsArray.removeAll()
                self.boatsArray.append(contentsOf: someBoats)
                self.rangeCV.reloadData()
                if self.boatsArray.count == 0{
                    self.leftCountLabel.text = "Please add a boat to proceed"
                    if self.isFirstTime{
                        self.openAddBoatScreen()
                    }
                }else{
                    self.selectedBoatIndex = 0
                    let indexPath = IndexPath(item: self.selectedBoatIndex, section: 0)
                    self.collectionView(self.rangeCV, didSelectItemAt: indexPath)
                }
            }
        }
    }
}









