//
//  HomeTabBarViewController.swift
//  GPDock
//
//  Created by TecOrb on 09/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Foundation
import StoreKit

class HomeTabBarViewController: UITabBarController {
    var user : User!

    var exploreTabButton : UITabBarItem!
    var favoritesTabButton : UITabBarItem!
    var bookingTabButton : UITabBarItem!
    var profileTabButton : UITabBarItem!
    var statusBarView : UIView!
    let titles = ["Explore","Favorites","Bookings","Account"]
    var titleView : NavigationTitleView!
    //let titles = ["Near By","GPDock","Profile"]
    var isFromMenu = false
    var selectedIndexFromMenu:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeTabBarViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeTabBarViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

         self.user = User(json: User.loadUserInfo()!)
        self.tabBar.barTintColor = UIColor.groupTableViewBackground//UIColor(colorLiteralRed: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 1.0)
        self.tabBar.backgroundColor = UIColor.groupTableViewBackground//UIColor(colorLiteralRed: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 1.0)
        self.tabBar.tintColor = UIColor.white
        self.tabBar.selectionIndicatorImage = UIImage().makeImageWithColorAndSize(UIColor.clear, size: CGSize(width:tabBar.frame.width/4, height: tabBar.frame.height))

        exploreTabButton = UITabBarItem()
        exploreTabButton.title = "Explore"
        favoritesTabButton = UITabBarItem()
        favoritesTabButton.title = "Favorites"
        bookingTabButton = UITabBarItem()
        bookingTabButton.title = "Bookings"
        profileTabButton = UITabBarItem()
        profileTabButton.title = "Account"
        setUpTabBarElements()

        let homeVC = AppStoryboard.Home.viewController(HomeViewController.self)
        homeVC.tabBarItem = exploreTabButton

        let favoritesVC = AppStoryboard.Home.viewController(FavoritesMarinaListViewController.self)
        favoritesVC.tabBarItem = favoritesTabButton

        let bookingVC = AppStoryboard.Booking.viewController(MyBookingsViewController.self)
        bookingVC.tabBarItem = bookingTabButton

        let profileVC = CommonClass.isLoggedIn ? AppStoryboard.Profile.viewController(ProfileViewController.self) : AppStoryboard.Profile.viewController(TempProfileViewController.self)

        profileVC.tabBarItem = profileTabButton

        self.setupRightBarButtons()

        let vcs = [homeVC,favoritesVC,bookingVC,profileVC]

        self.viewControllers = vcs
        self.selectedIndex = 0
        self.selectedViewController = vcs[0]
        if !isFromMenu{
            self.selectedIndex = 0
            self.selectedViewController = vcs[0]
        }else{
            self.selectedIndex = self.selectedIndexFromMenu
            self.selectedViewController = vcs[self.selectedIndexFromMenu]
        }
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = titles[self.selectedIndexFromMenu].uppercased()
        self.navigationItem.titleView = self.titleView
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
//        self.tabBar.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.middle,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,0.5,1.0])
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {

        var newTabBarFrame = tabBar.frame
        let newTabBarHeight: CGFloat = UIDevice.isIphoneX ? 94 : 55
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        tabBar.frame = newTabBarFrame
    }

    override func viewDidLayoutSubviews() {
        self.tabBar.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.user = User(json: User.loadUserInfo()!)
        self.setupViews()
    }

    func setupViews(){
            if var vcs = self.viewControllers {
                let profileVC = CommonClass.isLoggedIn ? AppStoryboard.Profile.viewController(ProfileViewController.self) : AppStoryboard.Profile.viewController(TempProfileViewController.self)
                profileVC.tabBarItem = profileTabButton
            vcs.remove(at: 3)
            vcs.append(profileVC)
            self.setViewControllers(vcs, animated: false)
            }
        }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        super.viewWillAppear(animated)
        if !self.user.ID.isEmpty{
            self.getBadgesCount()
        }
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        self.statusBarView = statusBar
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
            statusBar.backgroundColor = UIColor.white
        }
    }

    func setUpTabBarElements() {
        let selAttDict = [NSAttributedStringKey.foregroundColor:UIColor.white]//UIColor(colorLiteralRed: 211.0/255.0, green: 86.0/255.0, blue: 6.0/255.0, alpha: 1.0)]
        let normalAttDict = [NSAttributedStringKey.foregroundColor:UIColor.lightGray]

        self.exploreTabButton.image = UIImage(named:"explore_unsel")?.withRenderingMode(.alwaysOriginal)
        self.exploreTabButton.selectedImage = UIImage(named: "explore_sel")?.withRenderingMode(.alwaysOriginal)
        self.exploreTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.exploreTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

        self.favoritesTabButton.image = UIImage(named: "favorites_unsel")?.withRenderingMode(.alwaysOriginal)
        self.favoritesTabButton.selectedImage = UIImage(named: "favorites_sel")?.withRenderingMode(.alwaysOriginal)
        self.favoritesTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.favoritesTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

        self.bookingTabButton.image = UIImage(named: "bookings_unsel")?.withRenderingMode(.alwaysOriginal)
        self.bookingTabButton.selectedImage = UIImage(named: "bookings_sel")?.withRenderingMode(.alwaysOriginal)
        self.bookingTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.bookingTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

        self.profileTabButton.image = UIImage(named: "account_unsel")?.withRenderingMode(.alwaysOriginal)
        self.profileTabButton.selectedImage = UIImage(named: "account_sel")?.withRenderingMode(.alwaysOriginal)
        self.profileTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.profileTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == exploreTabButton{
            titleView.titleLabel.text = "Explore".uppercased()
        }else if item == favoritesTabButton{
            titleView.titleLabel.text = "My Favorites".uppercased()
        }else if item == bookingTabButton{
            titleView.titleLabel.text = "My Bookings".uppercased()
        }else{
            titleView.titleLabel.text = "Account".uppercased()
        }
    }



    // MARK: - Actions
    func setupRightBarButtons() {
        let mailButton = UIButton(type: .system)
        mailButton.frame = CGRect(x:0, y:0, width:35, height:35)
        mailButton.tintColor = UIColor.black
        mailButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        mailButton.setImage(#imageLiteral(resourceName: "message"), for: .normal)
        mailButton.addTarget(self, action: #selector(onClickMessageButton(_:)), for: .touchUpInside)
        let mailBarButton = UIBarButtonItem(customView: mailButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([mailBarButton], animated: false)
    }


    @IBAction func onClickMessageButton(_ sender: UIButton){
        let notificationVC = AppStoryboard.Profile.viewController(NotificationListViewController.self)
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: notificationVC, withSlideOutAnimation: false, andCompletion: { })
    }

    @IBAction func onClickFilterButton(_ sender: UIButton){

    }
}



extension HomeTabBarViewController{
    func getBadgesCount(){
        if CommonClass.isLoggedIn{
            LoginService.sharedInstance.badgecounts(self.user.ID, completionBlock: { (badgesCount, shouldShowRatingPopUp) in
                let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
                if shouldShowRatingPopUp && (Settings.sharedInstance.previousRatedVersion != currentVersion){
                    if #available(iOS 10.3, *) {
                        SKStoreReviewController.requestReview()
                        Settings.sharedInstance.shouldShowRatingPopUp = false
                        let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
                        Settings.sharedInstance.previousRatedVersion = currentVersion
                    } else {
                        self.openDoYouLovePopUp()
                    }
                }
            })
        }
    }

    func openDoYouLovePopUp() {
        //(UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: true, completion: nil)
        let isLovingAlert = UIAlertController(title: "Do you love GPDock?", message: "", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .default) { (action) in
            Settings.sharedInstance.shouldShowRatingPopUp = false
            let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
            Settings.sharedInstance.previousRatedVersion = currentVersion
            isLovingAlert.dismiss(animated: true, completion: nil)
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
            Settings.sharedInstance.previousRatedVersion = currentVersion
            self.askForRatingOnAppStore()
        }
        isLovingAlert.addAction(noAction)
        isLovingAlert.addAction(yesAction)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(isLovingAlert, animated: true, completion: nil)
    }

    func askForRatingOnAppStore() {
        let isLovingAlert = UIAlertController(title: "Rate GPDock", message: "We’re glad you love GPDock!\r\nPlease take moment to rate your experience.\r\nThanks so much!", preferredStyle: .alert)
        let rateGPDock = UIAlertAction(title: "Rate GPDock", style: .default) { (action) in
            Settings.sharedInstance.shouldShowRatingPopUp = false
            self.openAppStoreReviewPage()
        }

        let mayBeLater = UIAlertAction(title: "Maybe Later", style: .default) { (action) in
            Settings.sharedInstance.shouldShowRatingPopUp = true
            isLovingAlert.dismiss(animated: true, completion: nil)
        }

        let noThanks = UIAlertAction(title: "No, Thanks", style: .default) { (action) in
            Settings.sharedInstance.shouldShowRatingPopUp = false
            let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
            Settings.sharedInstance.previousRatedVersion = currentVersion
            isLovingAlert.dismiss(animated: true, completion: nil)
        }

        isLovingAlert.addAction(rateGPDock)
        isLovingAlert.addAction(mayBeLater)
        isLovingAlert.addAction(noThanks)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(isLovingAlert, animated: true, completion: nil)
    }

    func openAppStoreReviewPage(){
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appID)?action=write-review"
        let url = URL(string: urlStr)!

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:]){(done) in
            }
            self.user.ratedOnAppStore()

        } else {
            UIApplication.shared.openURL(url)
            self.user.ratedOnAppStore()
        }

    }

    
}

extension HomeTabBarViewController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}

