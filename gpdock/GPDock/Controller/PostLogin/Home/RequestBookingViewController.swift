//
//  RequestBookingViewController.swift
//  GPDock
//
//  Created by TecOrb on 12/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class RequestBookingViewController: UIViewController {
    var marina: Marina!
    var bookingDates: BookingDates!
    var userBoat : Boat!
    var user : User!
    var navigationTitleView : NavigationTitleView!
    var chechRequestBooking: Bool = false

    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var requestBookingButton: UIButton!


    let header = ["Marina Details","Boat Details","Booking Dates","Note"]
    let data = ["Marina Details":["Name","Address"],
                "Booking Dates": ["From","To"],
                "Boat Details":["Boat Name","Boat Size"],
                "Note":["Full refund 3 days prior to arrival. You will be charged full price if you cancel after that."]
    ]


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationView()
        self.user = User(json: User.loadUserInfo()!)
        let headerCellNib = UINib(nibName: "HeaderTableViewCell", bundle: nil)
        self.aTableView.register(headerCellNib, forCellReuseIdentifier: "HeaderTableViewCell")
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.aTableView.estimatedRowHeight = 44
        self.aTableView.reloadData()
    }
    override  func viewWillAppear(_ animated: Bool) {
        self.user = User(json: User.loadUserInfo()!)
    }
    override func viewWillLayoutSubviews() {
        self.requestBookingButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func viewDidLayoutSubviews() {
        self.requestBookingButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = "Request For Booking"
        self.navigationItem.titleView = self.navigationTitleView
    }


    @IBAction func onClickRequestForBook(_ sender: UIButton){
        if !user.isContactVerified {
            if ((user.countryCode == "") || (user.contact == "")) {
                self.navigateTONumberVerification()
            }else{
                self.navigateToOtpMobile()
            }
        }else{
            self.createBookingFor(self.user.ID, marinaID: self.marina.ID, boatID: self.userBoat.ID, bookingDates: self.bookingDates)

        }
    }
    func navigateTONumberVerification() {
        let numberVerificationVC = AppStoryboard.Booking.viewController(NumberVerificationViewController.self)
        numberVerificationVC.checkRequestBooking = true
        self.navigationController?.pushViewController(numberVerificationVC, animated: true)
    }
    
    func navigateToOtpMobile() {
        let mobileVerifiedVC = AppStoryboard.Booking.viewController(MobileOTPVerifyViewController.self)
        mobileVerifiedVC.checkRequestBooking = true
        self.navigationController?.pushViewController(mobileVerifiedVC, animated: true)
    }

    func createBookingFor(_ userID:String,marinaID: String,boatID:String,bookingDates: BookingDates){
        CommonClass.showLoader(withStatus: "Requesting..")
        PaymentService.sharedInstance.requestBookingFor(userID, marinaID: marinaID ,bookingDates: bookingDates, boatID: boatID) {(success, respBooking, message) in
            if success{
                if let _ = respBooking{
                    CommonClass.hideLoader()
                    let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                    homeTab.selectedIndexFromMenu = 0
                    homeTab.isFromMenu = true
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
                        GPDock.showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your request has been sent. We will notify you as soon as we confirm your booking. Thank you!", title: warningMessage.alertTitle.rawValue)
                    })
                }
            }else{
                CommonClass.hideLoader()
                GPDock.showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: message, title: warningMessage.alertTitle.rawValue)
            }

        }
    }
}


extension RequestBookingViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return header.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        headerView.headerLabel.text = header[section].capitalized
        headerView.headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        headerView.backgroundColor = UIColor(red: 240.0/255.0, green: 237.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 1{
            return 85
        }else if indexPath.section == header.count-1{
            return UITableViewAutomaticDimension
        }else{
            return 72
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (data[header[section]]?.count ?? 0)
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == header.count-1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCancellationPolicyCell", for: indexPath) as! ReviewCancellationPolicyCell
            if self.marina.cancellationPolicy.count == 0{
                cell.policyTextLabel.text = "We will notify you as soon as we confirm your booking. Thank you!"//"Your request will send to \(self.marina.title). You will notify as your booking will confirm." //(data[header[indexPath.section]]!)[indexPath.row]
            }else{
                cell.policyTextLabel.text = "We will notify you as soon as we confirm your booking. Thank you!" //"Your request will send to \(self.marina.title). You will notify as your booking will confirm."//self.marina.cancellationPolicy
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewBookingDetailCell", for: indexPath) as! ReviewBookingDetailCell
            cell.itemTitle.text = (data[header[indexPath.section]]!)[indexPath.row]
            if indexPath.section == 0{
                switch indexPath.row {
                case 0:
                    cell.itemDetailText.text = self.marina.title
                case 1:
                    cell.itemDetailText.text = "\(self.marina.address), \r\n\(self.marina.city.name), \(self.marina.city.state)"
                default:
                    break
                }
            }else if indexPath.section == 1{
                switch indexPath.row {
                case 0:
                    cell.itemDetailText.text = self.userBoat.name
                case 1:
                    cell.itemDetailText.text = "\(self.userBoat.boatSize.length)x\(self.userBoat.boatSize.width)x\(self.userBoat.boatSize.depth)"
                default:
                    break
                }

            } else{
                switch indexPath.row {
                case 0:
                    cell.itemDetailText.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.fromDate, format: "MM/dd/YY")+"  \(self.marina.checkIn)"
                case 1:
                    cell.itemDetailText.text = CommonClass.sharedInstance.formattedDateWith(self.bookingDates.toDate, format: "MM/dd/YY")+"  \(self.marina.checkOut)"
                default:
                    break
                }
            }
            return cell
        }
    }
}
