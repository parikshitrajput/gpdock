//
//  CalenderViewController.swift
//  CarDetailingApp
//
//  Created by TecOrb on 15/03/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import FSCalendar

protocol CalendarViewControllerDelegate {
    func calendarViewController(_ viewController:CalendarViewController, didSelectedDate selectedDates:BookingDates)
}






public struct BookingDates {
    public var totalDays : UInt = 0
    
    public var fromDate: Date {
        didSet{
            self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))
        }
    }

    public var toDate: Date{
        didSet{
            self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))
        }
    }


    public init(){
        let startDate = Date().addingTimeInterval(24*60*60)
        let tillDate = Date().addingTimeInterval(24*60*60)

        self.fromDate = startDate
        self.toDate = tillDate
        self.totalDays = UInt(abs(tillDate.interval(ofComponent: .day, fromDate: startDate)))
    }

    public init(fromDate: Date, toDate: Date){
        self.fromDate = fromDate
        self.toDate = toDate
        self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))
    }
}











class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
    var delegate : CalendarViewControllerDelegate?
    @IBOutlet weak var dateTitleLabel : UILabel!
    @IBOutlet weak var calenarView : UIView!
    var previousSelectedDate : Date?
    var bookingDates = BookingDates()
    var statusBarView : UIView!
    var isSelectingFrom = false
    var calendar: FSCalendar!

    //MARK:- @IBOutlet for to show selected dates
    @IBOutlet weak var toDayLabel : UILabel!
    @IBOutlet weak var toDateLabel : UILabel!
    @IBOutlet weak var toYearLabel : UILabel!

    @IBOutlet weak var fromDateLabel : UILabel!
    @IBOutlet weak var fromDayLabel : UILabel!
    @IBOutlet weak var fromYearLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateTitleLabel.text = "Select Dates"

        let height: CGFloat = self.view.frame.size.width
        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: height, height: height))

        calendar.dataSource = self
        calendar.delegate = self
        calendar.backgroundColor = UIColor.white
        self.calendar = calendar
        self.calenarView.addSubview(calendar)

        if self.isSelectingFrom{
            self.calendar.select(bookingDates.fromDate)
        }else{
            self.calendar.select(bookingDates.toDate)
        }
        self.isSelectingFrom = !self.isSelectingFrom
        self.toggleCalenderDateSelection()
    }

    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        self.statusBarView = statusBar
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor(red: 31.0/255.0, green: 121.0/255.0, blue: 139.0/255.0, alpha: 1)
//        }
    }

    override func viewWillDisappear(_ animated: Bool) {
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor.black
//        }
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickDone(_ sender: UIButton){
        if !(bookingDates.fromDate < bookingDates.toDate){
            showAlertWith(viewController: self, message: "From Date should always be smaller then To Date", title: warningMessage.alertTitle.rawValue)
            return
        }
        delegate?.calendarViewController(self, didSelectedDate: self.bookingDates)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickFromDateButton(_ sender: UIButton){
        self.isSelectingFrom = true
        self.toggleCalenderDateSelection()
    }
    @IBAction func onClickToDateButton(_ sender: UIButton){
        self.isSelectingFrom = false
        self.toggleCalenderDateSelection()
    }

    func maximumDate(for calendar: FSCalendar) -> Date {

        if isSelectingFrom {
            let maxdate = Date(timeInterval: 24*60*60*60, since: Date())
            return maxdate
        }else{
            let maxdate = Date(timeInterval: 24*60*60*60, since: Date())
            return maxdate
        }
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        if isSelectingFrom {
            let tommorrow = Date(timeInterval: 24*60*60, since: Date())
            return tommorrow
        }else{
            let tommorrow = Date(timeInterval: 24*60*60, since: Date())//bookingDates.fromDate)
            return tommorrow
        }
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if isSelectingFrom{
            self.bookingDates.fromDate = date
        }else{
            self.bookingDates.toDate = date
        }

        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }

        self.isSelectingFrom = !self.isSelectingFrom
        calendar.reloadData()
        self.perform(#selector(toggleCalenderDateSelection), with: nil, afterDelay: 0.5)
        //self.toggleCalenderDateSelection()
    }

    @objc func toggleCalenderDateSelection() -> Void {
        self.setDateTexts()
        self.calendar.reloadData()
        if self.isSelectingFrom{
            self.calendar.select(bookingDates.fromDate)
        }else{
            self.calendar.select(bookingDates.toDate)
        }
        self.calendar.reloadData()
        UIView.animate(withDuration: 0.2) { 
            self.toDayLabel.alpha = self.isSelectingFrom ? 0.5 : 1.0
            self.toDateLabel.alpha = self.isSelectingFrom ? 0.5 : 1.0
            self.toYearLabel.alpha = self.isSelectingFrom ? 0.5 : 1.0
            self.fromDayLabel.alpha = self.isSelectingFrom ? 1.0 : 0.5
            self.fromDateLabel.alpha = self.isSelectingFrom ? 1.0 : 0.5
            self.fromYearLabel.alpha = self.isSelectingFrom ? 1.0 : 0.5
        }
    }

    func setDateTexts() -> Void {
        let fromText = self.dayDateYear(fromDate: self.bookingDates.fromDate)
        let toText = self.dayDateYear(fromDate: self.bookingDates.toDate)

        self.fromDayLabel.text = fromText.day
        self.fromDateLabel.text = fromText.date
        self.fromYearLabel.text = fromText.year
        self.toDayLabel.text = toText.day
        self.toDateLabel.text = toText.date
        self.toYearLabel.text = toText.year

    }

    func dayDateYear(fromDate date: Date) -> (day:String,date:String,year:String) {
        var rday = ""
        var rdate = ""
        var ryear = ""

        let df  = DateFormatter()
        df.dateFormat = "EEEE"
        rday = df.string(from: date)
        df.dateFormat = "MMM dd"
        rdate = df.string(from: date)
        df.dateFormat = "YYYY"
        ryear = df.string(from: date)
        return (rday,rdate,ryear)

    }

    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        if ((date > bookingDates.fromDate) && (date < bookingDates.toDate)){
//            return 1
//        }
        return 0
    }

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if !isSelectingFrom{
           if self.bookingDates.fromDate >= date {return false}
            showErrorWithMessage("To Date should be greater then From Date")
        }

        return true
    }
}

