//
//  SplashAnimationViewController.swift
//  GPDock
//
//  Created by TecOrb on 23/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SplashAnimationViewController: UIViewController,SKSplashDelegate {
    @IBOutlet weak var imageView : UIImageView!
    var splashView : SKSplashView!
    var fromNotificationCenter : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.animationGPDock()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func animationGPDock(){
        let splashIcon = SKSplashIcon(image: #imageLiteral(resourceName: "logo"), animationType: .bounce)
        splashIcon?.iconSize = self.imageView.frame.size
        let color = UIColor.white
        self.splashView = SKSplashView(splashIcon: splashIcon, animationType: .none, frame: self.view.frame)
        self.splashView.delegate = self;
        self.splashView.backgroundColor = color;
        self.splashView.animationDuration = 2.5
        self.view.addSubview(splashView)
//        DispatchQueue.main.async {
            self.splashView.startAnimation()
       // }
    }


    func splashView(_ splashView: SKSplashView!, didBeginAnimatingWithDuration duration: Float) {

    }

    func splashViewDidEndAnimating(_ splashView: SKSplashView!) {

            let menu = AppStoryboard.Main.viewController(LeftMenuViewController.self)
            let nav = AppStoryboard.Home.viewController(SlideNavigationController.self)
            nav.avoidSwitchingToSameClassViewController = false
            nav.leftMenu = menu
            nav.enableSwipeGesture = true
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
            UINavigationBar.appearance().addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 10, shadowOpacity: 0.7)

    }

}
