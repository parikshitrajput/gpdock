//
//  FavoritesMarinaListViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit


class FavoritesMarinaListViewController:UIViewController,UITableViewDataSource,UITableViewDelegate, LoginViewDelegate {

    @IBOutlet weak var aTableView: UITableView!
    var page = 1
    var perPage = 15
    var user: User!
    var isFromMenu = false
    var isLoading = false
    var marinas = [Marina]()
    var loginView : LoginView!
    var noFavoratesView: NoFavoritesView!

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        refreshControl.addTarget(self, action: #selector(FavoritesMarinaListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self)

        self.user = User(json: User.loadUserInfo()!)

        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesMarinaListViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FavoritesMarinaListViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(favoriteNotificationHandler(_:)), name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuisnessDetailViewController.userDidPayAtMarinaHandler(_:)), name: .USER_PAID_AT_MARINA_NOTIFICATION, object: nil)

        self.aTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.aTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 9, right: 0)

        self.aTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.setupViews()
    }



    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        GPDock.openLoginModule(in: self,shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        if self.loginView != nil{
            self.loginView.removeFromSuperview()
        }
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }

    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.user = User(json: User.loadUserInfo()!)
        self.setupViews()
    }

    @IBAction func onClickPayAtMarinaButton(_ sender: UIButton){
        if let indexpath = sender.tableViewIndexPath(self.aTableView) as IndexPath?{
            self.payAtMarina(marina: self.marinas[indexpath.row])
        }
    }

    func payAtMarina(marina:Marina){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if !marina.isSigned{
                return
            }
            let payToMarinaVCNavigation = AppStoryboard.Home.viewController(PayMoneyNavigationController.self)
            for vc in payToMarinaVCNavigation.viewControllers{
                if let payVC = vc as? PayForMarinaViewController{
                    payVC.marina = marina
                    break
                }
            }
            self.present(payToMarinaVCNavigation, animated: true, completion: nil)
        }
    }
    @objc func userDidPayAtMarinaHandler(_ notification: Notification) {
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.dismiss(animated: true, completion: nil)
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupContainer()
        }else{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view your Favorites\r\nPlease Login or Sign up")
        }
    }

    func setupContainer(){
        self.aTableView.addSubview(refreshControl)
        self.page = 1
        self.marinas.removeAll()

        self.loadMarinasListFromServer(self.user.ID, page: self.page, perPage: self.perPage)
    }

    func setupFavoritesContainer(){
        self.user = User(json: User.loadUserInfo()!)
        CommonClass.showLoader(withStatus: "Please wait..")
        self.user.isUserHaveFavorites{ (apiResult, result, message) in
            CommonClass.hideLoader()
            if result{
                self.setupContainer()
            }else{
                self.setupNoFavoritesFoundView()
            }
        }
    }

    func setupNoFavoritesFoundView(){
        if self.noFavoratesView != nil{
            self.noFavoratesView.removeFromSuperview()
        }
        self.addNoFavoritesView()
        self.noFavoratesView.exploreMarinasButton.addTarget(self, action: #selector(onClickExploreMarinasButton(_:)), for: .touchUpInside)
    }

    func addNoFavoritesView(){
        if self.noFavoratesView != nil{
            self.noFavoratesView.removeFromSuperview()
        }
        self.noFavoratesView = NoFavoritesView.instanceFromNib()
        self.view.addSubview(noFavoratesView)
        noFavoratesView.layer.zPosition = CGFloat(Int.max)
        let topContraints = NSLayoutConstraint(item: noFavoratesView, attribute:
            .top, relatedBy: .equal, toItem: self.view,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)

        let bottomContraints = NSLayoutConstraint(item: noFavoratesView, attribute:
            .bottom, relatedBy: .equal, toItem: self.view,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)

        let leftContraints = NSLayoutConstraint(item: noFavoratesView, attribute:
            .leading, relatedBy: .equal, toItem: self.view,
                      attribute: .leading, multiplier: 1.0,
                      constant: 0)

        let rightContraints = NSLayoutConstraint(item: noFavoratesView, attribute:
            .trailing, relatedBy: .equal, toItem: self.view,
                       attribute: .trailing, multiplier: 1.0,
                       constant: 0)

        noFavoratesView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([topContraints,rightContraints,leftContraints,bottomContraints])
        noFavoratesView.exploreMarinasButton.addTarget(self, action: #selector(onClickExploreMarinasButton(_:)), for: .touchUpInside)
    }



    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func favoriteNotificationHandler(_ notification : Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,AnyObject>{
            if let marinaID = userInfo["marinaID"] as? String, let isFavorite = userInfo["isFavorite"] as? Bool{
                if isFavorite{
                    self.handleRefresh(self.refreshControl)
                }else{
                    for i in 0..<self.marinas.count{
                        if marinaID == self.marinas[i].ID{
                            self.marinas.remove(at:i)
                            self.aTableView.reloadData()
                            break
                        }
                    }
                    if self.marinas.count == 0{
                        self.setupNoFavoritesFoundView()
                    }
            }
        }
        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isLoading = true

        MarinaService.sharedInstance.getFavoritesMarinaList(self.user.ID, page: page, perPage: perPage) { (resMarinas) in
            self.isLoading = false
            self.marinas.removeAll()
            refreshControl.endRefreshing()
            if let someMarinas = resMarinas as? [Marina]{
                self.marinas.append(contentsOf: someMarinas)
            }
            self.aTableView.reloadData()
            if self.marinas.count == 0{
                self.setupNoFavoritesFoundView()
            }else{
                if self.noFavoratesView != nil{
                    self.noFavoratesView.removeFromSuperview()
                }
            }
        }
    }


    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickExploreMarinasButton(_ sender: UIButton){
        let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
        homeTab.selectedIndexFromMenu = 0
        homeTab.isFromMenu = true
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
        })
    }

    @IBAction func onClickBookNowButton(_ sender: UIButton){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if let indexPath = sender.tableViewIndexPath(aTableView) as IndexPath?{
                let marina = self.marinas[indexPath.row]
                self.navigateToMarinaProfile(marina,shouldDirectBooking:true)
            }
        }
    }
    func openLoginModule() {
        let nav = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController

        self.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickFilterButton(_ sender: UIButton){
        let filterVC = AppStoryboard.Home.viewController(FiltersViewController.self)
        let nav = UINavigationController(rootViewController: filterVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.backgroundColor = kNavigationColor
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: {
        })
        //showAlertWith(viewController: self, message: FUNCTIONALITY_PENDING_MESSAGE, title: "Under Development!")
    }
//    @IBAction func onClickSortButton(_ sender: UIButton){
//        CommonClass.sharedInstance.showSortingOptions(in: self) { (orderBy) in
//            print_debug(orderBy)
//        }
//    }

    func loadMarinasListFromServer(_ userID: String,page: Int,perPage:Int) -> Void {
        if userID.isEmpty{
            return
        }
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getFavoritesMarinaList(self.user.ID, page: page, perPage: perPage)  { (resMarinas) in
            self.isLoading = false
            if let someMarinas = resMarinas as? [Marina]{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.aTableView.reloadData()
            if self.marinas.count == 0{
                self.setupNoFavoritesFoundView()
            }else{
                if self.noFavoratesView != nil{
                    self.noFavoratesView.removeFromSuperview()
                }
            }
        }
    }

    @IBAction func onReloadButton(_ sender: UIButton){
        self.page = 1
        self.loadMarinasListFromServer(self.user.ID, page: self.page, perPage: self.perPage)
    }


    // MARK: - UITableViewDataSource Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if marinas.count != 0{
            rows = marinas.count
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.marinas.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text =  isLoading ? "Loading.." : "No Marina found\r\nRefresh to try again"
            return cell
        }else{

            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaCell", for: indexPath) as! MarinaCell
            let marina = marinas[indexPath.row]
            cell.marinaPhoto.setIndicatorStyle(.gray)
            cell.marinaPhoto.setShowActivityIndicator(true)
            cell.isPrivateImageView.image = marina.isPrivate ? #imageLiteral(resourceName: "ic_red_lock") : UIImage()
            cell.marinaPhoto.sd_setImage(with: URL(string:marina.image.url), completed: {[cell] (image, error, cacheType, url) in
                cell.marinaPhoto.animateImage(duration:0.5)
            })
            cell.marinaNameLabel.text = marina.title
            cell.priceLabel.text =  "$"+String(format: "%.2f/foot",marina.pricePerFeet)
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            cell.bookingButton.addTarget(self, action: #selector(onClickBookNowButton(_:)), for: .touchUpInside)
            cell.payForServiceButton.isHidden = !marina.isSigned
            cell.priceLabel.isHidden = !marina.isSigned

            cell.payForServiceButton.addTarget(self, action: #selector(onClickPayAtMarinaButton(_:)), for: .touchUpInside)

            CommonClass.makeViewCircularWithRespectToHeight(cell.distanceLabel, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.bookingButton, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.priceLabel, borderColor: UIColor.clear, borderWidth: 0)
            cell.likeButton.setImage(marina.isFavorite ? UIImage(named: "liked") : UIImage(named: "like"), for: UIControlState())
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }


    @IBAction func likeButtonTapped(_ sender: SparkButton) {
        if let indexPath = sender.tableViewIndexPath(aTableView) as IndexPath?{
            self.marinas[indexPath.row].isFavorite = !self.marinas[indexPath.row].isFavorite
            let marina = self.marinas[indexPath.row]
            self.toggleFavorate(userID: self.user.ID, marinaID: marina.ID)
            sender.setImage(UIImage(named: "like"), for: UIControlState())
            sender.unLikeBounce(0.4)
            if self.marinas.count == 1{
                self.setupNoFavoritesFoundView()
                self.marinas.remove(at: indexPath.row)
                self.aTableView.reloadData()
            }else{
                self.marinas.remove(at: indexPath.row)
                self.aTableView.beginUpdates()
                self.aTableView.deleteRows(at: [indexPath], with: .middle)
                self.aTableView.endUpdates()
            }
        }

    }

    func toggleFavorate(userID: String,marinaID:String) {
        MarinaService.sharedInstance.toggleFavorites(userID, marinaID: marinaID) { (status, marinaID) in
            for marina in self.marinas{
                if marinaID == marina.ID{
                    NotificationCenter.default.post(name: .USER_FAVORATES_MARINA_NOTIFICATION, object: nil, userInfo: ["marinaID":marinaID,"isFavorite":status])
                    marina.isFavorite = status
                    self.aTableView.reloadData()
                    break
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return   marinas.count != 0 ? screenWidth*4.5/7 : 90
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let ccell = cell as? MarinaCell{
            let marina = marinas[indexPath.row]
            ccell.distanceLabel.text = marina.radioFrequency.uppercased()
            ccell.distanceLabel.textAlignment = .left
            let rating = (marina.averageRating == 0.0 ) ? "No Rating" : String(format: "%.1f",marina.averageRating)
            ccell.ratingButton.setTitle(rating, for: .normal)
            ccell.setNeedsLayout()
            ccell.layoutIfNeeded()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.marinas.count == 0{
            return
        }
        let marina = self.marinas[indexPath.row]
        self.navigateToMarinaProfile(marina,shouldDirectBooking:false)
    }

    func navigateToMarinaProfile(_ marina:Marina,shouldDirectBooking:Bool) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        marinaProfileVC.shouldGoDirectlyToBooking = shouldDirectBooking
        marinaProfileVC.buisnessDetailViewControllerDelegate = self
        self.navigationController?.pushViewController(marinaProfileVC, animated: !shouldDirectBooking)
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.user.ID == "" {return}
        if scrollView == aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        self.page+=1
                        self.loadMarinasListFromServer(self.user.ID, page: self.page, perPage: self.perPage)
                    }
                }
            }
        }

    }



}

extension FavoritesMarinaListViewController: BuisnessDetailViewControllerDelegate{
    func marinaDidRefresh(_ viewController: BuisnessDetailViewController, refreshedMarina marina: Marina) {
        for (index,_marina) in self.marinas.enumerated(){
            if _marina.ID == marina.ID{
                self.marinas[index].isSigned = marina.isSigned
                self.aTableView.reloadData()
                break
            }
        }
    }
}

extension FavoritesMarinaListViewController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}



