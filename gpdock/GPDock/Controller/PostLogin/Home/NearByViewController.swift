//
//  HomeViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import UICollectionViewLeftAlignedLayout

let kMapZoomLevel:Float = 16.0

class NearByViewController: QMBParallaxScrollViewController,QMBParallaxScrollViewControllerDelegate,CLLocationManagerDelegate{
    var locationManager: CLLocationManager!
    var mapVC:MapViewController!
    var marinaListVC:MarinaListViewWithMapController!

    var page = 1
    var perPage = 50
    var user: User!
    var isLoading = false
    var marinas = [Marina]()
    var markers = [MarinaMarker]()
    @IBOutlet weak var filterOptionsView: UIView!
    @IBOutlet weak var showListButton: UIButton!
    @IBOutlet weak var pagerCV: UICollectionView!

    var currentCoordinate : CLLocationCoordinate2D!
    var isLocationFound : Bool = false
    var isShowListButtonHidden: Bool = false
    var selectedMarkerIndex = 0
    var isPagerHidden: Bool = false{
        didSet{
            if isPagerHidden{
                let frame = CGRect(x: 0, y: self.view.frame.size.height+92, width: self.view.frame.size.width, height: 92)
                UIView.animate(withDuration: 0.5, animations: {
                    self.pagerCV.frame = frame
                    self.view.sendSubview(toBack: self.pagerCV)

                })
            }else{
                let frame = CGRect(x: 0, y: self.view.frame.size.height-92, width: self.view.frame.size.width, height: 92)
                UIView.animate(withDuration: 0.5, animations: {
                    self.pagerCV.frame = frame
                    self.view.bringSubview(toFront: self.pagerCV)
                })
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        //DispatchQueue.main.async {


        self.user = User(json:User.loadUserInfo()!)

        self.mapVC = AppStoryboard.Home.viewController(MapViewController.self)
        self.marinaListVC = AppStoryboard.Home.viewController(MarinaListViewWithMapController.self)
        self.setup(withTopViewController: self.mapVC, andTopHeight: 44/*(self.view.frame.size.height*1.75/5)*/, andBottomViewController: self.marinaListVC)


        self.delegate = self
        self.maxHeight = self.view.frame.size.height-104
        self.view.bringSubview(toFront: self.filterOptionsView)
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.enableTapGestureTopView(true)

        self.marinaListVC.aTableView.dataSource = self
        self.marinaListVC.aTableView.delegate = self
        self.marinaListVC.aTableView.tableFooterView = UIView(frame:CGRect.zero)

        self.pagerCV.dataSource = self
        self.pagerCV.delegate = self
        self.pagerCV.isPagingEnabled = true
        self.mapVC.gMapView.delegate = self
        self.showListButton.isHidden = false
        self.showFullTopView(true)

        self.view.bringSubview(toFront: self.showListButton)

        //let frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 92)
        //self.pagerCV.frame = frame
        self.isPagerHidden = false
        //  }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func toggleShowListButton() -> Void {
        self.showFullTopView(false)

    }

    @IBAction func onClickFilterButton(_ sender: UIButton){
        let filterVC = AppStoryboard.Home.viewController(FiltersViewController.self)
        let nav = UINavigationController(rootViewController: filterVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.backgroundColor = kNavigationColor
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: {

        })
        //showAlertWith(viewController: self, message: FUNCTIONALITY_PENDING_MESSAGE, title: "Under Development!")
    }
    @IBAction func onClickSortButton(_ sender: UIButton){
//        CommonClass.sharedInstance.showSortingOptions(in: self) { (orderBy) in
//            print_debug(orderBy)
//        }
    }

    @IBAction func onClickShowListButton(_ sender: UIButton){
        showListButton.isHidden = true
        self.isShowListButtonHidden = true
        self.isPagerHidden = true

        //self.mapVC.gMapView.selectedMarker = nil
        for marker in markers{
            if marker.index == selectedMarkerIndex{
                marker.zIndex = 10
                self.mapVC.gMapView.selectedMarker = marker
                marker.icon = marker.imageForSelectedMarinaMarker()
                let indexPath = IndexPath(item: selectedMarkerIndex, section: 0)
                self.pagerCV.scrollToItem(at: indexPath, at: .left, animated: false)
            }else{
                marker.icon = marker.imageForMarinaMarker()
                marker.zIndex = 1
            }
        }
        self.view.sendSubview(toBack: pagerCV)
        toggleShowListButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showFullTopView(true)
        locationManager.startUpdatingLocation()
    }


    // MARK: - CLLocationManagerDelegate Methods
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .restricted:
            break
        case .denied:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationFound{
            if let location = locations.last{
                self.isLocationFound = true
                self.currentCoordinate = location.coordinate
                manager.stopUpdatingLocation()
                self.page = 1
                self.marinas.removeAll()
                marinaListVC.aTableView.reloadData()
                mapVC.gMapView.clear()
                self.loadMarinasListFromServer(self.currentCoordinate.latitude, longitude:self.currentCoordinate.longitude, page: self.page, perPage:self.perPage)

            }
        }
    }


    func parallaxScrollViewController(_ controller: QMBParallaxScrollViewController!, didChange state: QMBParallaxState) {
        //print_debug("state: \(state)")
    }

    func parallaxScrollViewController(_ controller: QMBParallaxScrollViewController!, didHideList isHidden: Bool) {
        self.isPagerHidden = !isHidden
        self.pagerCV.isHidden = !isHidden
        self.showListButton.isHidden = !isHidden
        if isHidden{
            self.view.bringSubview(toFront: pagerCV)
            self.view.bringSubview(toFront: showListButton)
            for marker in markers{
                if marker.index == selectedMarkerIndex{
                    marker.zIndex = 10
                    self.mapVC.gMapView.selectedMarker = marker
                    marker.icon = marker.imageForSelectedMarinaMarker()
                    let indexPath = IndexPath(item: selectedMarkerIndex, section: 0)
                    self.pagerCV.scrollToItem(at: indexPath, at: .left, animated: false)
                }else{
                    marker.zIndex = 1
                    marker.icon = marker.imageForMarinaMarker()
                }
            }
        }else{
            self.view.sendSubview(toBack: pagerCV)
            self.view.sendSubview(toBack: showListButton)
        }
    }



    func parallaxScrollViewController(_ controller: QMBParallaxScrollViewController!, didChangeTopHeight height: CGFloat) {
        //print_debug("height: \(height)")
    }

    func parallaxScrollViewController(_ controller: QMBParallaxScrollViewController!, didChange newGesture: QMBParallaxGesture, oldGesture: QMBParallaxGesture) {
    }

    @IBAction func onClickBookNowButton(_ sender: UIButton){
        if !CommonClass.isLoggedIn{
            self.openLoginModule()
        }else{
            if let indexPath = sender.tableViewIndexPath(self.marinaListVC.aTableView) as IndexPath?{
                let marina = self.marinas[indexPath.row]
                self.navigateToMarinaProfile(marina,shouldDirectBooking:true)
            }
        }
    }
    func openLoginModule() {
        let nav = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController

        self.present(nav, animated: true, completion: nil)
    }
    func navigateToMarinaProfile(_ marina:Marina,shouldDirectBooking:Bool) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        marinaProfileVC.shouldGoDirectlyToBooking = shouldDirectBooking
        self.navigationController?.pushViewController(marinaProfileVC, animated: !shouldDirectBooking)
    }
}

extension NearByViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return marinas.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagerCollectionViewCell", for: indexPath) as! PagerCollectionViewCell
        let marina = marinas[indexPath.item]

        cell.marinaImage.setIndicatorStyle(.gray)
        cell.marinaImage.setShowActivityIndicator(true)
        cell.marinaImage.sd_setImage(with: URL(string:marina.image.url))

        cell.marinaName.text = marina.title
        cell.ratingView.rating = Float(marina.averageRating)
        cell.distanceLabel.text = marina.radioFrequency
        var tags = ""
        for service in marina.services{
            if tags == ""{tags = service.title}else{tags=tags+", "+service.title}
        }
        cell.tagsLabel.text = tags
        cell.nextmarinaImage.image = nil
        if indexPath.item < marinas.count-1{
            let nextMarina = marinas[indexPath.item+1]
            cell.nextmarinaImage.setIndicatorStyle(.gray)
            cell.nextmarinaImage.setShowActivityIndicator(true)
            cell.nextmarinaImage.sd_setImage(with: URL(string:nextMarina.image.url))

        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.marinas.count == 0{
            return
        }
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = self.marinas[indexPath.row]
        self.navigationController?.pushViewController(marinaProfileVC, animated: true)
    }
    func changeMarkerToSelected(_ marina: Marina) -> Void {
        let marker = markers.filter { (marker) -> Bool in
            return marker.marina.ID == marina.ID
        }
        if let markerToBeSelected = marker.first{
            markerToBeSelected.zIndex = 10
            markerToBeSelected.icon = markerToBeSelected.imageForSelectedMarinaMarker()
            self.mapVC.gMapView.selectedMarker = markerToBeSelected
            self.selectedMarkerIndex = markerToBeSelected.index
            let rMarkers = markers.filter({ (mmarker) -> Bool in
                return mmarker.marina.ID != markerToBeSelected.marina.ID
            })
            for m in rMarkers{
                m.zIndex = 1
                m.icon = m.imageForMarinaMarker()

            }
        }
    }
    func changeMarkerFromSelectedToDeSelect(_ marina: Marina) -> Void {
        let marker = markers.filter { (marker) -> Bool in
            return marker.marina.ID == marina.ID
        }
        if let markerToBeSelected = marker.first{
            markerToBeSelected.zIndex = 10
            markerToBeSelected.icon = markerToBeSelected.imageForMarinaMarker()
        }

    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 90)
    }
}



extension NearByViewController : UITableViewDataSource,UITableViewDelegate{

    func loadMarinasListFromServer(_ latitude: Double,longitude:Double,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaList(nil,userID: self.user.ID, latitude: latitude, longitude: longitude, page: page, perPage: perPage) { (success,resMarinas,resCity,message) in
            if self.page == 1{self.marinas.removeAll()}
            self.isLoading = false
            if let someMarinas = resMarinas{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
//            if let aCity = resCity{
//                //self.city = aCity
//            }
            self.marinaListVC.aTableView.reloadData()
            self.refreshMarkers()
            self.pagerCV.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if marinas.count != 0{
            rows = marinas.count
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.marinas.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text =  isLoading ? "Loading.." : "No Marina found\r\nRefresh to try again"
            return cell
        }else{

            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaCell", for: indexPath) as! MarinaCell
            let marina = marinas[indexPath.row]
            cell.marinaPhoto.setIndicatorStyle(.gray)
            cell.marinaPhoto.setShowActivityIndicator(true)
            cell.isPrivateImageView.image = marina.isPrivate ? #imageLiteral(resourceName: "ic_red_lock") : UIImage()

            cell.marinaPhoto.sd_setImage(with: URL(string:marina.image.url))//, placeholderImage:UIImage(named:"\(indexPath.row%4)"))
            cell.marinaNameLabel.text = marina.title
            cell.priceLabel.text =  "$"+String(format: "%.2f/foot",marina.pricePerFeet)
            cell.addressLabel.text = "\(marina.address), \(marina.city.name), \(marina.city.state)"
            cell.likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
            cell.bookingButton.addTarget(self, action: #selector(onClickBookNowButton(_:)), for: .touchUpInside)

            // cell.configureServices(services: marina.services)
            CommonClass.makeViewCircularWithRespectToHeight(cell.distanceLabel, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.bookingButton, borderColor: UIColor.clear, borderWidth: 0)
            CommonClass.makeViewCircularWithRespectToHeight(cell.priceLabel, borderColor: UIColor.clear, borderWidth: 0)
            cell.likeButton.setImage(marina.isFavorite ? UIImage(named: "liked") : UIImage(named: "like"), for: UIControlState())
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }

    @IBAction func likeButtonTapped(_ sender: SparkButton) {
        if let indexPath = sender.tableViewIndexPath(self.marinaListVC.aTableView) as IndexPath?{
            self.marinas[indexPath.row].isFavorite = !self.marinas[indexPath.row].isFavorite
            let marina = self.marinas[indexPath.row]
            self.toggleFavorate(userID: self.user.ID, marinaID: marina.ID)
            if marina.isFavorite == true {
                sender.setImage(UIImage(named: "liked"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
            }
            else{
                sender.setImage(UIImage(named: "like"), for: UIControlState())
                sender.unLikeBounce(0.4)
            }
        }

    }

    func toggleFavorate(userID: String,marinaID:String) {
        MarinaService.sharedInstance.toggleFavorites(userID, marinaID: marinaID) { (status, marinaID) in
            for marina in self.marinas{
                if marinaID == marina.ID{
                    marina.isFavorite = status
                    self.marinaListVC.aTableView.reloadData()
                    break
                }
            }
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return   marinas.count != 0 ? screenWidth*4.5/7 : 90
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let ccell = cell as? MarinaCell{
            let marina = marinas[indexPath.row]
            ccell.distanceLabel.text = marina.radioFrequency.uppercased()
            let rating =  String(format: "%.1f",marina.averageRating)
            ccell.ratingButton.setTitle(rating, for: .normal)
//            var tags = [String]()
//            for service in marina.services{
//                tags.append(service.title)
//            }
//            ccell.tagView.tags = tags
//            ccell.setNeedsLayout()
//            ccell.layoutIfNeeded()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.marinas.count == 0{
            return
        }
        self.navigateToMarinaProfile(self.marinas[indexPath.row],shouldDirectBooking:false)
    }


    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //let offsetY = pagerCV.contentOffset.x
        if scrollView == self.pagerCV{
            if let visibleCells = pagerCV.visibleCells as? [PagerCollectionViewCell]{
                if let cell = visibleCells.first {
                    let x = cell.nextmarinaImage.frame.maxX
                    let w = 0
                    let h = cell.nextmarinaImage.frame.size.height
                    let y = cell.marinaImage.frame.origin.y
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.nextmarinaImage.frame = CGRect(x: x, y: y, width: CGFloat(w), height: h)

                    })
                }
            }

        }
    }

    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.pagerCV{
            if let visibleCells = pagerCV.visibleCells as? [PagerCollectionViewCell]{
                if let cell = visibleCells.first{
                    if let indexPath = pagerCV.indexPath(for: cell){
                        //if !self.isPagerHidden{
                        changeMarkerToSelected(marinas[indexPath.item])
                        selectedMarkerIndex = indexPath.item
                        //}
                    }
                    let x = cell.contentView.frame.maxX-20
                    let w = 20
                    let h = cell.nextmarinaImage.frame.size.height
                    let y = cell.marinaImage.frame.origin.y
                    UIView.animate(withDuration: 0.2, animations: {
                        cell.nextmarinaImage.frame = CGRect(x: x, y: y, width: CGFloat(w), height: h)
                        
                    })
                }
            }
        }else if scrollView == self.marinaListVC.aTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        self.page+=1
                        // self.loadMarinasListFromServer(coordinate?.latitude ?? 0.0, longitude: coordinate?.longitude ?? 0.0, page:page, perPage: perPage)
                    }
                }
            }
        }
        
    }






    func refreshMarkers() {
        self.mapVC.gMapView.clear()
        self.markers.removeAll()
        let path = GMSMutablePath()
        for i in 0..<marinas.count{
            let marina = marinas[i]
            self.addMarkerForMarina(marina,index: i)
            path.add(CLLocationCoordinate2D(latitude: marina.latitude, longitude: marina.longitude))
        }
        let bounds = GMSCoordinateBounds(path: path)
        if let camera = self.mapVC.gMapView.camera(for: bounds, insets:UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)){
            self.mapVC.gMapView.camera = camera;
        }
    }


    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.mapVC.gMapView.mapType = .normal
        self.mapVC.gMapView.camera = camera
    }


    func addMarkerForMarina(_ marina : Marina,index:Int) {
        let marinaMarker = MarinaMarker(marina: marina, index: index)
        self.markers.append(marinaMarker)
        marinaMarker.isTappable = true
        marinaMarker.zIndex = 1
        marinaMarker.map = self.mapVC.gMapView;
    }
}

extension NearByViewController : GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if isPagerHidden{
            isPagerHidden = false
            self.showFullTopView(true)
            self.view.bringSubview(toFront: pagerCV)
            self.showListButton.isHidden = false
            self.view.bringSubview(toFront: showListButton)
        }
        if let marinaMarker = marker as? MarinaMarker{
            marinaMarker.icon = marinaMarker.imageForSelectedMarinaMarker()
            marinaMarker.zIndex = 10
            self.mapVC.gMapView.selectedMarker = marinaMarker
            let rMarkers = markers.filter({ (mmarker) -> Bool in
                return mmarker.marina.ID != marinaMarker.marina.ID
            })
            for m in rMarkers{
                m.icon = m.imageForMarinaMarker()
                m.zIndex = 1
            }
            self.selectedMarkerIndex = marinaMarker.index
            let indexPath = IndexPath(item: marinaMarker.index, section: 0)
            self.pagerCV.scrollToItem(at: indexPath, at: .left, animated: false)
        }

        return true
    }
}




class MapViewController: UIViewController {
    @IBOutlet weak var gMapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gMapView.isMyLocationEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }






}












class MarinaListViewWithMapController:UIViewController,QMBParallaxScrollViewHolder {
    @IBOutlet weak var aTableView: UITableView!

    var page = 1
    var perPage = 15
    var user: User!
    var isLoading = false
    var marinas = [Marina]()
    var coordinate : CLLocationCoordinate2D?{
        didSet{
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.aTableView.translatesAutoresizingMaskIntoConstraints = false;
        self.aTableView.reloadData()
//        self.loadMarinasListFromServer(coordinate?.latitude ?? 0.0, longitude: coordinate?.longitude ?? 0.0, page:page, perPage: perPage)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // MARK: - QMBParallaxScrollViewHolder Methods
    func scrollViewForParallaxController() -> UIScrollView! {
        return self.aTableView
    }

}




//{
//    var locationManager: CLLocationManager!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.view.setNeedsLayout()
//        self.view.layoutIfNeeded()
//    }
//
//    // MARK: - CLLocationManagerDelegate Methods
//
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        switch status {
//        case .notDetermined:
//            manager.requestWhenInUseAuthorization()
//            break
//        case .authorizedWhenInUse:
//            manager.startUpdatingLocation()
//            break
//        case .authorizedAlways:
//            manager.startUpdatingLocation()
//            break
//        case .restricted:
//            break
//        case .denied:
//            break
//        }
//    }
//}

/*extension HomeViewController: CLLocationManagerDelegate, GMSMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            self.locationManager.startUpdatingLocation()
        }else{
            self.showGotoLocationSettingAlert()
        }
    }


    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationCalled{
            isLocationCalled = true
            if let location = locations.last{
                currentLocation = location
                self.getCarCategoriesFromServer()
                self.refreshCustomerLocationWithCoordinate(location.coordinate)
                self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                    if self.pickUpFromAddressView.isHidden{
                        self.dropOffCoordinates = location.coordinate
                        self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                            self.dropOffAddress = address
                            self.droppToAddressView.pinLocationLabel.text = address
                            self.pickUpFromAddressView.dropOffPinLocationLabel.text = address
                        })
                    }else if self.droppToAddressView.isHidden{
                        self.pickUpCoordinates = location.coordinate
                        self.nameOfPlaceWithCoordinates(coordinate: location.coordinate, complition: { (address) in
                            self.pickUpAddress = address
                            self.pickUpFromAddressView.pinLocationLabel.text = address
                            self.droppToAddressView.pickUpPinLocationLabel.text = address
                        })
                    }
                })
                self.getAllCarsFromServer()
            }
        }
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        mapViewWillMove = gesture
        animateHeaderAndRideView(true)
    }


    func refreshMarkers() {
        self.googleMapView.clear()
        for car in carArray{
            self.addMarkerForDriver(car: car)
        }
    }




    func addMarkerForDriver(car : Car) {
        let driverMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude))
        driverMarker.icon = #imageLiteral(resourceName: "taxiIcon")
        driverMarker.isTappable = true
        driverMarker.title = car.name
        driverMarker.map = self.googleMapView;
    }


    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        var currentAddress = ""
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                currentAddress = lines.joined(separator: ",")
                complition(currentAddress)
            }
        }

    }


    func showGotoLocationSettingAlert(){
        let alert = UIAlertController(title: "Opps", message: "Location access seems disabled\r\nGo to settings to enabled", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if #available(iOS 10.0, *){
                let url = UIApplicationOpenSettingsURLString

//            if UIApplication.shared.canOpenURL(URL(string: "App-prefs:root=LOCATION_SERVICES")!){
//                UIApplication.shared.openURL(URL(string: "App-prefs:root=LOCATION_SERVICES")!)
//                }
//            }else if #available(iOS 9.0, *){
//                if UIApplication.shared.canOpenURL(URL(string: "prefs:root=LOCATION_SERVICES")!){
//                    UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
//                }
//            }

                if UIApplication.shared.canOpenURL(URL(string: url)!){
                    UIApplication.shared.openURL(URL(string: url)!)
                }
            }else if #available(iOS 9.0, *){
                let url = UIApplicationOpenSettingsURLString

                if UIApplication.shared.canOpenURL(URL(string: url)!){
                    UIApplication.shared.openURL(URL(string: url)!)
                }
            }
        }

        alert.addAction(okayAction)
        alert.addAction(settingsAction)

        self.present(alert, animated: true, completion: nil)
    }

}*/

//MARK:- Calculate time to arriaval

/*extension HomeViewController{
    func getArrivalTimeOfAllCategory(from userLocationCoordinate: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D]) -> Void {
        var destinationsStr = ""
        let sourceStr = "\(userLocationCoordinate.latitude),\(userLocationCoordinate.longitude)"
        for dest in destinations{
            let destStr = "\(dest.latitude)%2C\(dest.longitude)"
            if destinationsStr == ""{
                destinationsStr = destStr
            }else{
                destinationsStr = "\(destinationsStr)%7C\(destStr)"
            }
        }

        getTravelTime(from: sourceStr, to: destinationsStr) { (timeArray) in
            for t in 0..<timeArray.count{
                self.carCategoryArray[t].time = timeArray[t]
            }
            self.categoryCollectionView.reloadData()
        }
    }


    func getTravelTime(from source:String,to destination: String,completion:@escaping (_ times: [String])->Void)->Void{
        let url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(source)&destinations=\(destination)&key=\(GOOGLE_API_KEY)"

        Alamofire.request(url).responseJSON { response in
            let json = JSON(data: response.data!)
            print_debug("traver time json : \n \(json)")
            let rows = json["rows"].arrayValue
            var timeArray = [String]()
            if rows.count > 0{
                if let arrValues = rows.first?["elements"].array as [JSON]?{
                    for dict in arrValues{
                        if let notFound = dict["status"].string as String?,notFound != "OK"{
                            timeArray.append("no cab")
                        }else if let durationDict = dict["duration"].dictionaryObject as [String:AnyObject]?{
                            if let durationf = durationDict["text"] as? String{
                                timeArray.append(durationf)
                            }
                        }
                    }
                }
            }
            completion(timeArray)
        }
    }

}*/

/*extension HomeViewController:GMSAutocompleteViewControllerDelegate{

    func showGooglePlaceAutocomplete(){
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        var fullAddress = ""
        fullAddress = place.name
        if let formattedAddress = place.formattedAddress{
            //fullAddress =  formattedAddress
            fullAddress = fullAddress + ", \(formattedAddress)"
        }
        if pickUpFromAddressView.isHidden{
            self.dropOffAddress = fullAddress
            self.dropOffCoordinates = place.coordinate
            self.droppToAddressView.pinLocationLabel.text = fullAddress
            self.pickUpFromAddressView.dropOffPinLocationLabel.text = fullAddress
            self.centerPinImageView.image = #imageLiteral(resourceName: "redPin")
        }else if droppToAddressView.isHidden{

            self.pickUpAddress = fullAddress
            self.pickUpCoordinates = place.coordinate
            self.pickUpFromAddressView.pinLocationLabel.text = fullAddress
            self.droppToAddressView.pickUpPinLocationLabel.text = fullAddress
            self.centerPinImageView.image = #imageLiteral(resourceName: "greenPin")
        }

        self.getAllCarsByCategoryFromServer(catetegoryID: selectedCategory.ID, latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        refreshCustomerLocationWithCoordinate(place.coordinate)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        showErrorWithMessage(error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}*/


extension NearByViewController{//: SlideNavigationControllerDelegate{
    @IBAction func onClickMenu(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
//    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
//        return true
//    }
//    
//    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
//        return false
//    }
}
