//
//  NKCalenderViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 23/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import FSCalendar
import EasyTipView

protocol NKCalenderViewControllerDelegate {
    func nkcalendarViewController(_ viewController:NKCalenderViewController, didSelectedDate selectedDates:BookingDates)
}


class NKCalenderViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
    var delegate : NKCalenderViewControllerDelegate?
    @IBOutlet weak var doneContainnerView : UIView!
    @IBOutlet weak var calenarView : UIView!
    @IBOutlet weak var headerLabel : NKCustomLabel!

    var confirmShowToolTip : Bool!

    var shouldShowToolTip = false{
        didSet{
            self.confirmShowToolTip  = shouldShowToolTip
        }
    }

    var showTipAfter: Int = 0
    var modifyFromDate = false
    var modifyToDate = false
    var monthPositionToShowTip : FSCalendarMonthPosition = .current
    var selectedBookingDate : BookingDates!
    var calendar: FSCalendar!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    var tipView : EasyTipView!//(text: "Some text", preferences: preferences)
    var dateFormatter: DateFormatter?
    var gregorian: Calendar!
    // The start date of the range
    var date1: Date?
    // The end date of the range
    var date2: Date?
    var tempDates = BookingDates()

    @IBAction func previousClicked(_ sender: UIButton) {
        self.dismissTipView()
        let currentMonth: Date? = calendar?.currentPage
        let previousMonth: Date? = gregorian?.date(byAdding: .month, value: -1, to: currentMonth!)
        calendar?.setCurrentPage(previousMonth!, animated: true)
    }
    @IBAction func onClickClose(_ sender: UIButton){
        self.dismissTipView()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onclickDone(_ sender: UIButton) {
        self.dismissTipView()

        if selectedBookingDate != nil{
            if selectedBookingDate.totalDays == 0{
                showAlertWith(viewController: self, message: "Please select a check in and check out date", title: warningMessage.alertTitle.rawValue)
                return
            }
            delegate?.nkcalendarViewController(self, didSelectedDate: self.selectedBookingDate)
        }else{
            showAlertWith(viewController: self, message: "Please select a check in and check out date", title: warningMessage.alertTitle.rawValue)
        }
        //self.dismiss(animated: true, completion: nil)
    }


    @IBAction func nextClicked(_ sender: UIButton) {
        self.dismissTipView()
        let currentMonth: Date? = calendar?.currentPage
        let nextMonth: Date? = gregorian.date(byAdding: .month, value: 1, to: currentMonth!)
        calendar?.setCurrentPage(nextMonth!, animated: true)
    }


    override func viewDidLoad() {
        gregorian = Calendar(identifier: .iso8601)
        let width: CGFloat = self.view.frame.size.width-30
        let height: CGFloat = width + 35

//        if self.selectedBookingDate == nil{
//            self.selectedBookingDate = BookingDates(fromDate: Date(), toDate: (gregorian?.date(byAdding: .day, value: 2, to: Date()))!)
//        }
//        date1 = self.selectedBookingDate.fromDate
//        date2 = self.selectedBookingDate.toDate

        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: width, height: height))
        calendar.appearance.titleTodayColor = kNavigationColor
        calendar.backgroundColor = UIColor.white
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.headerHeight = 50
        calendar.appearance.caseOptions = .weekdayUsesSingleUpperCase
        calendar.appearance.weekdayTextColor = UIColor.black
        calendar.appearance.titleWeekendColor = UIColor.black
        calendar.appearance.weekdayFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)

        calendar.pagingEnabled = true
        calendar.allowsMultipleSelection = true
        calendar.rowHeight = 40
        calendar.placeholderType = FSCalendarPlaceholderType.none

        self.calendar = calendar
        self.calenarView.addSubview(calendar)
        calendar.dataSource = self
        calendar.delegate = self

        CommonClass.makeViewCircularWithCornerRadius(self.calenarView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)
        calendar.appearance.titleDefaultColor = UIColor.black
        calendar.appearance.titleSelectionColor = kNavigationColor
        calendar.appearance.headerTitleColor = kNavigationColor
        calendar.appearance.titleFont = UIFont(name: fonts.Raleway.regular.rawValue, size: fontSize.large.rawValue)
        calendar.appearance.headerTitleFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)

        calendar.weekdayHeight = 40
        calendar.swipeToChooseGesture.isEnabled = true
        calendar.scrollDirection = .horizontal
        calendar.scrollEnabled = false
        calendar.register(RangePickerCell.self, forCellReuseIdentifier: "cell")

        let datefrom = CommonClass.sharedInstance.formattedDateWith(calendar.currentPage, format: "MMMM YYYY")
        self.headerLabel.text = datefrom.uppercased()
    }

    override func viewDidLayoutSubviews() {
        self.doneContainnerView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal)
    }
    // MARK: - FSCalendarDataSource
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()//.addingTimeInterval(24*60*60)
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return (gregorian?.date(byAdding: .month, value: 4, to: Date()))!
    }


//    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
////        if (gregorian?.isDateInToday(date))! {
//           // return "Today"
////        }
//        return nil
//    }

    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at monthPosition: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell: RangePickerCell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: monthPosition) as! RangePickerCell
        return cell
    }

    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        configureCell(cell, for: date, at: monthPosition)
    }

    // MARK: - FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if monthPosition != .current{self.dismissTipView()}
        //return monthPosition == .current
        if (date > (gregorian?.date(byAdding: .day, value: -1, to: Date()))! && date < (gregorian?.date(byAdding: .month, value: 2, to: Date()))!) && monthPosition == .current{
            return true
        }
        return false //monthPosition == .current
    }

    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            self.monthPositionToShowTip = monthPosition
            showTipAfter = 1
        }else{
            showTipAfter = 0
        }

        if self.tipView != nil{
            self.dismissTipView()
        }

        if calendar.swipeToChooseGesture.state == .changed{
            if monthPosition == .previous || monthPosition == .next {
                showTipAfter = 1
            }else{
                showTipAfter = 0
            }
            self.monthPositionToShowTip = monthPosition

            // If the selection is caused by swipe gestures
            if (date1 == nil) {
                date1 = date
            }else {
                if (date2 != nil) {
                    calendar.deselect(date2!)
                }
                date2 = date
            }
        }else {
            if (date2 != nil) {
                calendar.deselect(date1!)
                calendar.deselect(date2!)
                date1 = date
                date2 = nil
            }else if (date1 == nil) {
                date1 = date
            }else {
                date2 = date
            }
        }

        if let startDate = date1{
            self.selectedBookingDate = BookingDates(fromDate: startDate, toDate: startDate)
            self.shouldShowToolTip = false
            if let toDate = date2{
                let sortedDate = CommonClass.shortSelectedDate(startDate, toDate: toDate)
                self.selectedBookingDate = BookingDates(fromDate: sortedDate.fromDate, toDate: sortedDate.toDate)
                self.date1 = self.selectedBookingDate.fromDate
                self.date2 = self.selectedBookingDate.toDate
                self.shouldShowToolTip = true
            }
        }
        configureVisibleCells()
    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
       // print("did deselect date \(String(describing: dateFormatter?.string(from: date)))")
        configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor] {
        if gregorian.isDateInToday(date){
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }

    func calendarCurrentMonthDidChange(_ calendar: FSCalendar) {
        self.dismissTipView()
        let datefrom = CommonClass.sharedInstance.formattedDateWith(calendar.currentPage, format: "MMMM YYYY")
        self.headerLabel.text = datefrom.uppercased()
    }
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let datefrom = CommonClass.sharedInstance.formattedDateWith(calendar.currentPage, format: "MMMM YYYY")
        self.headerLabel.text = datefrom.uppercased()
    }


    // MARK: - Private methods
    func configureVisibleCells() {
        for cell in (calendar.visibleCells()){
            let date: Date? = calendar.date(for: cell)
            let position: FSCalendarMonthPosition = calendar.monthPosition(for: cell)
            self.configureCell(cell, for: date!, at: position)
        }
    }

    func configureCell(_ cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let rangeCell: RangePickerCell? = cell as? RangePickerCell
        if position != .current {
            rangeCell?.rightMiddleLayer?.isHidden = true
            rangeCell?.leftMiddleLayer?.isHidden = true
            //rangeCell?.middleLayer?.isHidden = true
            rangeCell?.selectionLayer?.isHidden = true
            return
        }

        if (date1 != nil) && (date2 != nil) {
            // The date is in the middle of the range
            let isMiddle: Bool = date.compare(date1!) != date.compare(date2!)

            //rangeCell?.middleLayer?.isHidden = !isMiddle
            rangeCell?.rightMiddleLayer?.isHidden = !isMiddle
            rangeCell?.leftMiddleLayer?.isHidden = !isMiddle

            if date == self.selectedBookingDate.fromDate{
                rangeCell?.rightMiddleLayer?.isHidden = false
                rangeCell?.leftMiddleLayer?.isHidden = true
            }else if date == self.selectedBookingDate.toDate{
                rangeCell?.rightMiddleLayer?.isHidden = true
                rangeCell?.leftMiddleLayer?.isHidden = false
            }else{
                rangeCell?.rightMiddleLayer?.isHidden = !isMiddle
                rangeCell?.leftMiddleLayer?.isHidden = !isMiddle
            }

            if shouldShowToolTip{
                if gregorian.isDate(date, equalTo: selectedBookingDate.toDate, toGranularity: .day) && (self.monthPositionToShowTip != .previous){
                    let when = showTipAfter*800
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(when)) {
                        self.showTip(forView: cell, withinSuperview: self.calenarView)
                    }
                    self.shouldShowToolTip = false
                    self.monthPositionToShowTip = .current
                }else if gregorian.isDate(date, equalTo: selectedBookingDate.fromDate, toGranularity: .day) && self.monthPositionToShowTip == .previous{
                    let when = showTipAfter*800
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(when)) {
                        self.showTip(forView: cell, withinSuperview: self.calenarView)
                    }
                    self.shouldShowToolTip = false
                    self.monthPositionToShowTip = .current
                }
            }

        }
        else {
            //rangeCell?.middleLayer?.isHidden = true
            rangeCell?.rightMiddleLayer?.isHidden = true
            rangeCell?.leftMiddleLayer?.isHidden = true
        }
        var isSelected: Bool = false
        isSelected = isSelected || ( (date1 != nil) && (gregorian?.isDate(date, inSameDayAs: date1!))!)
        isSelected = isSelected || ((date2 != nil) && (gregorian?.isDate(date, inSameDayAs: date2!))!)
        rangeCell?.selectionLayer?.isHidden = !isSelected
    }
}

extension NKCalenderViewController: EasyTipViewDelegate{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismissTipView()
    }

    func showTip(forView: UIView,withinSuperview superview: UIView) -> Void {
            if self.tipView != nil{ self.tipView.dismiss()}
        //if confirmShowToolTip{
            let fromDate = CommonClass.sharedInstance.formattedDateWith(selectedBookingDate.fromDate, format: "dd MMM")
            let toDate = CommonClass.sharedInstance.formattedDateWith(selectedBookingDate.toDate, format: "dd MMM")
            let titleText = "\(fromDate) - \(toDate) \(selectedBookingDate.totalDays) nights\r\nDrag to modify"
            self.tipView = EasyTipView(text: titleText, preferences: self.configurationForEasyTip())
            self.tipView.addshadow(top: true, left: true, bottom: true, right: true)
            self.tipView.show(forView: forView)
        //}
    }

    func dismissTipView() {
        if self.tipView != nil{ self.tipView.dismiss()}
    }

    func configurationForEasyTip()-> EasyTipView.Preferences {
        var preferences = EasyTipView.Preferences()
        // preferences.drawing.font = UIFont(name: fonts.OpenSans.regular.rawValue, size: fontSize.small.rawValue)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.applyGradient = false
        preferences.drawing.backgroundColor = kNavigationColor//UIColor(red: 60.0/255.0, green: 154.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 0
        return preferences
    }

    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
//        let tipView = EasyTipView(text: "Some text", preferences: preferences)
//        tipView.show(forView: someView, withinSuperview: someSuperview)
//
//        // later on you can dismiss it
//        tipView.dismiss()
    }
}

