//
//  MapViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import UICollectionViewLeftAlignedLayout


class MarinasOnMapViewController: UIViewController,CLLocationManagerDelegate {
    @IBOutlet weak var gMapView: GMSMapView!
    @IBOutlet weak var filterContainner: UIView!
    @IBOutlet weak var filterIndicator: UIView!

    @IBOutlet weak var sortButtonContainner: UIVisualEffectView!

    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!

    var locationManager: CLLocationManager!
    var page = 1
    var perPage = 50
    var user: User!
    var isLoading = false
    var city : City!
    var marinas = [Marina]()
    var markers = [MarinaMarker]()
    var sortingKey: String?
    var orderBy: String?

    var filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)?

    var currentCoordinate : CLLocationCoordinate2D!
    @IBOutlet weak var pagerCV: UICollectionView!
    @IBOutlet weak var listButton: UIButton!
    var titleView: NavigationTitleView!

    var isLocationFound : Bool = false
    var isShowListButtonHidden: Bool = false
    var selectedMarkerIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo()!)
        self.gMapView.isMyLocationEnabled = true
        self.setupNavigationTitle()
        self.pagerCV.register(UINib(nibName: "PagerCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "PagerCollectionViewCell")
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self

        self.pagerCV.dataSource = self
        self.pagerCV.delegate = self
        self.pagerCV.isPagingEnabled = true
        self.gMapView.delegate = self

        self.refreshMarkers()
        self.pagerCV.reloadData()
        if self.marinas.count == 0{
            self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.filterIndicator.isHidden = (self.filters == nil)

        //locationManager.startUpdatingLocation()
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithRespectToHeight(self.filterContainner, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.filterButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.filterIndicator, borderColor: UIColor.clear, borderWidth: 0)

        CommonClass.makeViewCircularWithRespectToHeight(self.sortButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(self.sortButtonContainner, borderColor: UIColor.clear, borderWidth: 0)
        self.sortButtonContainner.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

        self.filterContainner.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }


    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = city.name.uppercased()
        self.navigationItem.titleView = self.titleView
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickMessageButton(_ sender: UIButton){
        let notificationVC = AppStoryboard.Profile.viewController(NotificationListViewController.self)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func onclickListButton(_ sender: UIButton){
        self.navigationController?.pop(false)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .restricted:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if self.marinas.count == 0 && isEnabling{
                    self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
                }
            }
        case .denied:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if self.marinas.count == 0 && isEnabling{
                    self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
                }
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationFound{
//            if let location = locations.last{
//                self.isLocationFound = true
//                /*
//                self.currentCoordinate = location.coordinate
//                manager.stopUpdatingLocation()
//                self.page = 1
//                self.marinas.removeAll()
//                marinaListVC.aTableView.reloadData()
//                mapVC.gMapView.clear()
//                self.loadMarinasListFromServer(self.currentCoordinate.latitude, longitude:self.currentCoordinate.longitude, page: self.page, perPage:self.perPage)
//                */
//            }
        }
    }
}

extension MarinasOnMapViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return marinas.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagerCollectionViewCell", for: indexPath) as! PagerCollectionViewCell
        let marina = marinas[indexPath.item]

        cell.marinaImage.setIndicatorStyle(.gray)
        cell.marinaImage.setShowActivityIndicator(true)
        cell.marinaImage.sd_setImage(with: URL(string:marina.image.url), completed: {[cell] (image, error, cacheType, url) in
            cell.marinaImage.animateImage(duration:0.5)
        })
        cell.isPrivateImage.isHidden = !marina.isPrivate
        cell.marinaName.text = marina.title
        cell.ratingView.rating = Float(marina.averageRating)
        cell.distanceLabel.text = marina.address+", "+marina.city.name+", "+marina.city.state
        var tags = ""
        for service in marina.services{
            if tags == ""{tags = service.title}else{tags=tags+", "+service.title}
        }
        cell.tagsLabel.text = tags
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.marinas.count == 0{
            return
        }
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = self.marinas[indexPath.row]
        self.navigationController?.pushViewController(marinaProfileVC, animated: true)
    }

    func changeMarkerToSelected(_ marina: Marina) -> Void {
        let marker = markers.filter { (marker) -> Bool in
            return marker.marina.ID == marina.ID
        }
        if let markerToBeSelected = marker.first{
            markerToBeSelected.zIndex = 10
            markerToBeSelected.icon = markerToBeSelected.imageForSelectedMarinaMarker()
            self.gMapView.selectedMarker = markerToBeSelected
            self.selectedMarkerIndex = markerToBeSelected.index
            let rMarkers = markers.filter({ (mmarker) -> Bool in
                return mmarker.marina.ID != markerToBeSelected.marina.ID
            })
            for m in rMarkers{
                m.zIndex = 1
                m.icon = m.imageForMarinaMarker()
            }
        }
    }
    func changeMarkerFromSelectedToDeSelect(_ marina: Marina) -> Void {
        let marker = markers.filter { (marker) -> Bool in
            return marker.marina.ID == marina.ID
        }
        if let markerToBeSelected = marker.first{
            markerToBeSelected.zIndex = 10
            markerToBeSelected.icon = markerToBeSelected.imageForMarinaMarker()
        }

    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/*546/750*/, height: (self.view.frame.size.height*320/1206))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension MarinasOnMapViewController:SortingSelectionViewControllerDelegate,FiltersViewControllerDelegate {

    func filterViewController(_ viewController: FiltersViewController, didClearSelectedFilters cleared: Bool) {
        if cleared{
            self.filters = nil
            self.page = 1
            self.marinas.removeAll()
            self.isLoading = true
            self.loadMarinasListFromServer(self.city.ID, page: self.page, perPage: self.perPage)
        }
    }

    func sort(viewController: SortingSelectionViewController, sortingKey: String, orderBy: String) {
        if ((sortingKey.count == 0) || (orderBy.count == 0)){
            return
        }
        self.page = 1
        self.sortingKey = sortingKey
        self.orderBy = orderBy
        self.marinas.removeAll()
        self.isLoading = true
        self.loadFilteredMarinasListFromServer(sortingKey, orderBy: orderBy, filters: self.filters, page: self.page, perPage: self.perPage)
    }

    @IBAction func onClickFilterButton(_ sender: UIButton){
        let filterVC = AppStoryboard.Home.viewController(FiltersViewController.self)
        filterVC.delegate = self
        filterVC.city = self.city
        filterVC.filters = self.filters
        let nav = UINavigationController(rootViewController: filterVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: {
        })
    }

    func filterViewController(_ viewController: FiltersViewController, didSelectFilter filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)) {
            self.filters = filters
            self.page = 1
            self.marinas.removeAll()
            self.isLoading = true
            self.loadFilteredMarinasListFromServer(sortingKey, orderBy: orderBy, filters: self.filters, page: self.page, perPage: self.perPage)
        }



    @IBAction func onClickSortButton(_ sender: UIButton){
        CommonClass.sharedInstance.showSortingOptions(withDelegate: self)
    }

    func loadFilteredMarinasListFromServer(_ sortingKey: String?,orderBy:String?,filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)?,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getFilteredMarinaList(sortingKey, sortingOrder: orderBy, cityID: self.city.ID, userID:self.user.ID, filters: filters,page: page, perPage: perPage, minPriceRange:self.city.marinaMinPrice, maxPriceMax: self.city.marinaMaxPrice) { (resMarinas) in
            self.isLoading = false
            if let someMarinas = resMarinas as? [Marina]{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.refreshMarkers()
            self.pagerCV.reloadData()
        }
    }

    func loadMarinasListFromServer(_ cityID: String,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaList(cityID,userID:self.user.ID ,latitude: nil, longitude: nil, page: page, perPage: perPage) { (success,resMarinas,resCity,message)  in
            self.isLoading = false
            if let someMarinas = resMarinas{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            if let aCity = resCity{
                self.city = aCity
            }
            self.refreshMarkers()
            self.pagerCV.reloadData()
        }
    }


    func loadMarinasListFromServer(_ latitude: Double,longitude:Double,page: Int,perPage:Int) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getMarinaList(nil, userID:self.user.ID, latitude: latitude, longitude: longitude, page: page, perPage: perPage) { (success,resMarinas,resCity,message) in
            if self.page == 1{self.marinas.removeAll()}
            self.isLoading = false
            if let someMarinas = resMarinas{
                if someMarinas.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.marinas.append(contentsOf:someMarinas)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            if let aCity = resCity{
                self.city = aCity
            }
            self.refreshMarkers()
            self.pagerCV.reloadData()
        }
    }




    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        if scrollView == self.pagerCV{
            if let visibleCells = pagerCV.visibleCells as? [PagerCollectionViewCell]{
                if let cell = visibleCells.first{
                    if let indexPath = pagerCV.indexPath(for: cell){
                        changeMarkerToSelected(marinas[indexPath.item])
                        selectedMarkerIndex = indexPath.item
                    }
                }
            }
        }

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: scrollView)
        self.perform(#selector(self.scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
    }

    func refreshMarkers() {
        self.gMapView.clear()
        self.markers.removeAll()
        let path = GMSMutablePath()
        for i in 0..<marinas.count{
            let marina = marinas[i]
            self.addMarkerForMarina(marina,index: i)
            path.add(CLLocationCoordinate2D(latitude: marina.latitude, longitude: marina.longitude))
        }


        let bounds = GMSCoordinateBounds(path: path)
        if let camera = self.gMapView.camera(for: bounds, insets:UIEdgeInsets(top: 15, left: 15, bottom: self.pagerCV.bounds.height, right: 15)){
            self.gMapView.camera = camera;
        }

        if marinas.count == 1{
            if let marina = marinas.last{
                let coordinate = CLLocationCoordinate2D(latitude: marina.latitude, longitude: marina.longitude)
                self.refreshCustomerLocationWithCoordinate(coordinate)
            }
        }
    }


    func refreshCustomerLocationWithCoordinate(_ coordinate:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: kMapZoomLevel)
        self.gMapView.mapType = .normal
        self.gMapView.camera = camera
    }


    func addMarkerForMarina(_ marina : Marina,index:Int) {
        let marinaMarker = MarinaMarker(marina: marina, index: index)
        self.markers.append(marinaMarker)
        marinaMarker.isTappable = true
        marinaMarker.map = self.gMapView;

        if self.selectedMarkerIndex == index{
            self.gMapView.selectedMarker = marinaMarker
            marinaMarker.zIndex = 10
            marinaMarker.icon = marinaMarker.imageForSelectedMarinaMarker()
        }else{
            marinaMarker.zIndex = 1
            marinaMarker.icon = marinaMarker.imageForMarinaMarker()
        }
    }
}
extension MarinasOnMapViewController : GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let marinaMarker = marker as? MarinaMarker{
            marinaMarker.zIndex = 10
            marinaMarker.icon = marinaMarker.imageForSelectedMarinaMarker()
            self.gMapView.selectedMarker = marinaMarker
            let rMarkers = markers.filter({ (mmarker) -> Bool in
                return mmarker.marina.ID != marinaMarker.marina.ID
            })
            for m in rMarkers{
                m.icon = m.imageForMarinaMarker();
                m.zIndex = 1
            }
            self.selectedMarkerIndex = marinaMarker.index
            let indexPath = IndexPath(item: marinaMarker.index, section: 0)
            self.pagerCV.scrollToItem(at: indexPath, at: .left, animated: false)
        }

        return true
    }
}
