//
//  HomeViewController.swift
//  GPDock
//
//  Created by TecOrb on 09/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import ADTransitionController
import CoreLocation
import SDWebImage

class HomeViewController: UIViewController,CLLocationManagerDelegate {
    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchContainner: UIView!

    var page = 1
    var perPage = 15
    var totalPage = 0
    var user: User!
    var isLoading = false
    var canLoadMore = true

    var cities = [City]()
    var currentCoordinate : CLLocationCoordinate2D?
    var locationManager : CLLocationManager!
    var isLocationFound : Bool = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.white//UIColor(patternImage: #imageLiteral(resourceName: "background"))
        refreshControl.addTarget(self, action: #selector(HomeViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        return refreshControl
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
       self.getAppVersion()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdateHomeScreen(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdateHomeScreen(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificarionAppVersion(_:)), name: .SHOULD_USER_UPDATE_APPLICATION_VERSION, object: nil)
        
        let nib = UINib(nibName: "NoDataAndReloadCell", bundle: nil)
        self.aTableView.register(nib, forCellReuseIdentifier: "NoDataAndReloadCell")
        self.aTableView.addSubview(refreshControl)
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        //self.addFooterSpinner()
        self.aTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.aTableView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 9, right: 0)
    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
//    override func viewWillAppear(_ animated: Bool) {
//        let userjson = User.loadUserInfo()
//        self.user = User(json: userjson!)T
//    }
    
    @objc func getUpdateHomeScreen(_ notification: Notification) {
        if notification.name == .USER_DID_LOGGED_IN_NOTIFICATION{
            if let aUser = notification.userInfo?["user"] as? User, aUser.ID != ""{
                self.user = aUser
                self.page = 1
                self.loadCitiesFromServer(self.page, perPage: perPage)

            }
        }

        
    }

    func addFooterSpinner(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: self.aTableView.frame.width, height: 44)
        self.aTableView.tableFooterView = spinner;
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.searchButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.searchButton.frame.size.height/2)
        self.searchButton.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 10)//self.searchButton.frame.size.height/2)
    }
    func getAppVersion() {
        
        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    @objc func getNotificarionAppVersion(_ notification: Notification) {

        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
                
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func checkVersionAndUpdate(currentVersion: String, versionLevel: String) {
        let appVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
        if currentVersion != appVersion {
            if versionLevel.lowercased() != "Normal".lowercased() {
                self.forceUPdateVersion(currentVersion: currentVersion, versionLevel: versionLevel)
            }else{
                self.askForUpdateVersion(currentVersion: currentVersion, versionLevel: versionLevel, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    
    func askForUpdateVersion(currentVersion: String, versionLevel: String, title: String) {
        if !CommonClass.shouldShowNormalUpdate{return}
        let alert = UIAlertController(title: title, message: "A new version GPDock is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.goTOAppStore()
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }

        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
            CommonClass.setNormalUpdateKey()
        })
    }
    
    
        func forceUPdateVersion(currentVersion: String, versionLevel: String) {
    
            let alert = UIAlertController(title: "Important message", message: "A new version GPDock is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Update", style: .default){(action) in
                alert.dismiss(animated: true, completion: nil)
                self.goTOAppStore()
            }
            
            alert.addAction(okayAction)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
                CommonClass.setNormalUpdateKey()
            })
        }
    
    func goTOAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/gpdock/id1347118363"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate Methods
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .restricted:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if !isEnabling{
                    self.loadCitiesFromServer(self.page, perPage: self.perPage)
                }
            }
            break
        case .denied:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if !isEnabling{
                    self.loadCitiesFromServer(self.page, perPage: self.perPage)
                }
            }
            break
        }
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isLocationFound{
            if let location = locations.last{
                self.isLocationFound = true
                self.currentCoordinate = location.coordinate
                manager.stopUpdatingLocation()
                self.page = 1
                self.loadCitiesFromServer(self.page, perPage: self.perPage)
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMessageButton(_ sender: UIButton){
        if CommonClass.isLoggedIn{
            let notificationVC = AppStoryboard.Profile.viewController(NotificationListViewController.self)
            self.navigationController?.pushViewController(notificationVC, animated: true)
            
        }else{
            showLoginAlert(isFromMenu: true, shouldLogin: true,inViewController: nil)
        }
    }


       @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.page = 1
        self.aTableView.tableFooterView = nil
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isLoading = true
        MarinaService.sharedInstance.getCitiesFromServer(keyword: nil, userID: self.user.ID,coordinates: self.currentCoordinate, pageNumber: page, perPage: perPage) { (success,resCities,resTotalPage,message)  in
            self.isLoading = false
            self.cities.removeAll()
            refreshControl.endRefreshing()
            self.totalPage = resTotalPage
            self.canLoadMore = true//((self.totalPage > self.page) || (self.cities.count == 0))
            if let someCities = resCities{
                self.cities.append(contentsOf: someCities)
            }
            self.aTableView.reloadData()
        }
    }


    func loadCitiesFromServer(_ page: Int,perPage:Int) -> Void {
        if (self.totalPage != 0) && (self.page > self.totalPage){
            self.aTableView.tableFooterView = nil
            return
        }
        self.addFooterSpinner()

        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            if self.page > 1{
                self.page = self.page - 1
            }
            return
        }

        self.isLoading = true
        MarinaService.sharedInstance.getCitiesFromServer(keyword: nil, userID: self.user.ID, coordinates: self.currentCoordinate, pageNumber: page, perPage: perPage) { (success,resCities,resTotalPage,message) in
            self.isLoading = false
            self.totalPage = resTotalPage
            self.canLoadMore = true//((self.totalPage > self.page) || (self.cities.count == 0))
            if let someCities = resCities{
                if someCities.count == 0{
                    if self.page > 1{
                        self.page = self.page - 1
                    }
                }
                self.cities.append(contentsOf:someCities)
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.aTableView.reloadData()
        }
    }

    @IBAction func onReloadButton(_ sender: UIButton){
        self.page = 1
        self.loadCitiesFromServer(self.page, perPage: perPage)
    }

    @IBAction func onClickSearchButton(_ sender: UIButton){
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }

        let selectLocation = AppStoryboard.Home.viewController(SelectLocationViewController.self)
        selectLocation.title = "Search"
        self.navigationController?.pushViewController(selectLocation, animated: false)
    }
}
extension HomeViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if cities.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataAndReloadCell", for: indexPath) as! NoDataAndReloadCell
            cell.reloadButton.isHidden = isLoading
            cell.messageLabel.text =  isLoading ? "Loading.." : "No Marina found\r\nRefresh to try again"
            cell.messageLabel.text = "Opps! No city found\r\nRefresh to try again"
            cell.reloadButton.addTarget(self, action: #selector(onReloadButton(_:)), for: .touchUpInside)
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaLocationHomeCell", for: indexPath) as! MarinaLocationHomeCell
            let city = cities[indexPath.row]
            cell.locationPhoto.setIndicatorStyle(.gray)
            cell.locationPhoto.setShowActivityIndicator(true)
            let imageUrl = URL(string:city.image) ?? URL(string:BASE_URL)!
            cell.locationPhoto.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholder_city"), options:[], completed: {[cell] (image, error, cacheType, url) in
                cell.locationPhoto.animateImage(duration:0.5)
            })
            cell.locationNameLabel.text = city.name.capitalized
            cell.containnerView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 0.5)
            DispatchQueue.main.async {
                self.loadNextBatch(indexPath: indexPath)
            }
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
            }
        }

    


    func loadNextBatch(indexPath:IndexPath) {
        if self.page > self.totalPage {
            self.aTableView.tableFooterView = nil
            return
        }
        if cities.count >= self.perPage && canLoadMore {
            if !isLoading && ((cities.count - indexPath.row) <= self.perPage/2){
                if CommonClass.isConnectedToNetwork{
                    isLoading = true
                    self.page+=1
                    self.loadCitiesFromServer(self.page, perPage: perPage)
                }
            }
        }
    }


    func addParallaxToView(vw: UIView) {
        let hAmount = 0
        let vAmount = 20

        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -hAmount
        horizontal.maximumRelativeValue = hAmount

        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -vAmount
        vertical.maximumRelativeValue = vAmount

        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        vw.addMotionEffect(group)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.frame.size.height-49)/3
//        return self.view.frame.size.width*25/40
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let marinaListVC = AppStoryboard.Home.viewController(MarinaListViewController.self)
        let city = self.cities[indexPath.row]
        marinaListVC.city = city
        self.navigationController?.pushViewController(marinaListVC, animated: true)
    }




}



