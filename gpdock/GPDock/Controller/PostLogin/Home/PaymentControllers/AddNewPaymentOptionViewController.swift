//
//  AddNewPaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe


class AddNewPaymentOptionViewController: UIViewController,PaymentSuccessViewControllerDelegate {
    @IBOutlet weak var cardNumberTextField : BKCardNumberField!
    @IBOutlet weak var cardHolderNameTextField : UITextField!
    @IBOutlet weak var ccvTextField : UITextField!
    @IBOutlet weak var expTextField : BKCardExpiryField!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var payButton : UIButton!
//    @IBOutlet weak var payableAmountLabel : UILabel!
    @IBOutlet weak var saveCardButton : UIButton!
    var isPayingForMarina = false
    var paymentNote : String?
    var amountsArray : Array<Double>?//(category:String,amount:Double)>?

    var payableAmout : Double!
    var user : User!
    var marina: Marina!
    var bookingDates: BookingDates!
    var selectedSize : BoatSize!
    var boat : ParkingSpace!
    var ownBoat : Boat?
    var selectedOffer : OfferModal?
    var discountPrice: Double?
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Add New Card"
        self.navigationItem.titleView = self.titleView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preloadCardIO()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.setupNavigationTitle()
        self.setupAllView()
        self.navigateToScanCard()
    }
    func setupAllView(){
       // self.payableAmountLabel.text = "Enter Card Details"//"$ "+String(format: "%0.2lf", self.payableAmout)
        self.payButton.setTitle("Pay $ "+String(format: "%0.2lf",self.discountPrice ?? self.payableAmout), for: .normal)
        cardNumberTextField.showsCardLogo = true
        cardNumberTextField.cardNumberFormatter.groupSeparater = " "
        self.saveCardButton.isSelected = true
        CommonClass.makeViewCircularWithCornerRadius(self.containnerView, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardNumberTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardHolderNameTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.cardHolderNameTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.ccvTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.ccvTextField.setLeftPaddingPoints(5)
        self.ccvTextField.setRightPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.expTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.expTextField.setLeftPaddingPoints(5)

    }

    override func viewDidLayoutSubviews() {
        self.payButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    @IBAction func onToggleSaveCardForFasterCheckOut(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        //validate card params
        self.payButton.isEnabled = false
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            showAlertWith(viewController: self, message: warningMessage.validCardNumber.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            showAlertWith(viewController: self, message: warningMessage.cardHolderName.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            showAlertWith(viewController: self, message: warningMessage.cardHolderName.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        guard let brand = self.cardNumberTextField.cardNumberFormatter.cardPatternInfo.shortName  else {
            self.payButton.isEnabled = true
            showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            showAlertWith(viewController: self, message: warningMessage.enterCVV.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            showAlertWith(viewController: self, message: warningMessage.enterCVV.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            showAlertWith(viewController: self, message: warningMessage.expMonth.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            showAlertWith(viewController: self, message: warningMessage.expYear.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv)
        if !inputValidation.success{
            showAlertWith(viewController: self, message: inputValidation.message, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true
            return
        }

        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv)
        if !success{
            showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }


        self.createTokenAndProceedBookingWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv)
    }

    func createTokenAndProceedBookingWith(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String){

        CommonClass.showLoader(withStatus: "Connecting..")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV

        STPAPIClient.shared().createToken(withCard: cardParams) { (stpToken, error) in
            if error != nil{
                CommonClass.hideLoader()
                showAlertWith(viewController: self, message: "\(String(describing: error!.localizedDescription))", title: warningMessage.alertTitle.rawValue)
                self.payButton.isEnabled = true
                return
            }else{
                guard let token = stpToken else{
                    CommonClass.hideLoader()
                    showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
                    self.payButton.isEnabled = true
                    return
                }

                if self.isPayingForMarina{
                    CommonClass.updateLoader(withStatus: "Paying..")
                    if let amounts = self.amountsArray{
                        self.payForDirectPayment(marinaID: self.marina.ID, cardID:"", sourceToken: token.tokenId, amounts: amounts, paymentNotes:self.paymentNote ?? "", coupanCode: self.selectedOffer?.coupanCode ?? "")
                    }
                }else{
                    CommonClass.updateLoader(withStatus: "Creating..")
                    self.payAndCreateBookingFor(self.user.ID, bookingDates: self.bookingDates, parkingSpaceID: self.boat.ID, pricePerFeet: self.marina.pricePerFeet, boatID: self.ownBoat?.ID, amount: self.payableAmout, sourceToken: token.tokenId, cardID: "", willSave: self.saveCardButton.isSelected) {success, respBooking in
                        if success{
                            if let _ = respBooking{
                                let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                                homeTab.selectedIndexFromMenu = 2
                                homeTab.isFromMenu = true
                                self.navigationController?.setViewControllers([homeTab], animated: false)
                                CommonClass.hideLoader()

                                GPDock.showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your reservation is confirmed!", title: warningMessage.alertTitle.rawValue)
//                                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
//                                    CommonClass.hideLoader()
//                                    GPDock.showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your reservation is confirmed!", title: warningMessage.alertTitle.rawValue)
//                                })
                            }
                        }else{
                            self.payButton.isEnabled = true
                            CommonClass.hideLoader()
                        }

                    }}
            }

        }
    }


    func validteCardInputs(_ cardNumber:String,cardHolderName:String,brand:String,expMonth: Int?,expYear:Int?,CVV:String) -> (success:Bool,message:String) {
        if cardNumber.count == 0{
            return(false,warningMessage.validCardNumber.rawValue)
        }

        if cardHolderName.count == 0{
            return(false,warningMessage.cardHolderName.rawValue)
        }

        if let expMon = expMonth{
            if (expMon < 1) || (expMon > 12){return(false,warningMessage.validExpMonth.rawValue)}
        }else{
            return(false,warningMessage.expMonth.rawValue)
        }

        if let expY = expYear{
            if (expY < Date().year){return(false,warningMessage.validExpYear.rawValue)}
        }else{
            return(false,warningMessage.expYear.rawValue)
        }

        if !brand.lowercased().contains("maestro"){
            if (CVV.count < 3) || (CVV.count > 4){return(false,warningMessage.enterCVV.rawValue)}
        }
        return(true,"")

    }

    func validateCardParams(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String) -> Bool {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        let validationState = STPCardValidator.validationState(forCard: cardParams)
        return (validationState != .invalid)
    }




    func payForDirectPayment(marinaID:String,cardID:String,sourceToken:String,amounts:[Double],paymentNotes:String, coupanCode:String){
        if self.user.ID.count != 0{
            self.user.payAtMarina(marinaID: marinaID, cardID: cardID, sourceToken: sourceToken, amounts: amounts, paymentNote: paymentNotes, coupanCode: coupanCode, completionBlock: { (success, payment, message) in
                if success{
                    CommonClass.hideLoader()
                    if let paymentDone = payment{
                        self.showPaymentSuccess(paymentDone)
                    }
                }else{
                    CommonClass.hideLoader()
                    self.payButton.isEnabled = true
                    GPDock.showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            })
        }
    }

    func showPaymentSuccess(_ payment: Payment){
        let paymentSuccessVC = AppStoryboard.Home.viewController(PaymentSuccessViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.delegate = self
        self.present(paymentSuccessVC, animated: true, completion: nil)
    }

    func paymentSuccessViewController(viewController:PaymentSuccessViewController, didFinishPayment done: Bool) {
        viewController.dismiss(animated: false) {
            self.dismiss(animated: false, completion: nil)
        }
    }

    func payAndCreateBookingFor(_ userID: String,bookingDates:BookingDates,parkingSpaceID: String,pricePerFeet:Double,boatID:String?,amount:Double,sourceToken:String,cardID:String?,willSave :Bool,completionBlock:@escaping (_ success:Bool,_ booking :Booking?)->Void){
        CommonClass.showLoader(withStatus: "Creating..")
        PaymentService.sharedInstance.payAndCreateBookingFor(userID, bookingDates: bookingDates, parkingSpaceID: parkingSpaceID, pricePerFeet: pricePerFeet, boatID: boatID ?? "", amount: amount, sourceToken: sourceToken, cardID: cardID ?? "", willSave: willSave, coupanCode: self.selectedOffer?.coupanCode ?? "") { (success,responseBooking,message) in
            if let booking = responseBooking{
                CommonClass.updateLoader(withStatus: "Creating..")
                completionBlock(true,booking)
            }else{
                GPDock.showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                completionBlock(false,nil)
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickScanCard(_ sender: UIButton){
        //open card scanner screen
        self.navigateToScanCard()
    }

    func navigateToScanCard(){
        let scanCardVC = AppStoryboard.Settings.viewController(ScanCardViewController.self)
        scanCardVC.selectedOffer = self.selectedOffer
        scanCardVC.discountPrice = self.discountPrice
        scanCardVC.delegate = self
        let nav = UINavigationController(rootViewController: scanCardVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

}

extension AddNewPaymentOptionViewController: ScanCardViewControllerDelegate{
    func cardDidScan(viewcontroller: ScanCardViewController, withCardInfo cardInfo: CardIOCreditCardInfo) {
        if let cardNumber = cardInfo.cardNumber{
            self.cardNumberTextField.cardNumber = cardNumber
        }else{
            self.cardNumberTextField.cardNumber = ""
        }
        if let ccv = cardInfo.cvv{
            self.ccvTextField.text = ccv
        }else{
            self.ccvTextField.text = ""
        }
        if let cardholderName = cardInfo.cardholderName{
            self.cardHolderNameTextField.text = cardholderName
        }else{
            self.cardHolderNameTextField.text = ""
        }

        if let expiryMonth = cardInfo.expiryMonth as UInt?{
            if expiryMonth > 0{
                self.expTextField.dateComponents.month = Int(expiryMonth)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        if let expiryYear = cardInfo.expiryYear as UInt?{
            if expiryYear > 0{
                self.expTextField.dateComponents.year = Int(expiryYear)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
    }

    func cardDidScan(viewcontroller: ScanCardViewController, isUserChooseManual wantManual: Bool) {
        if wantManual{
            self.cardNumberTextField.cardNumber = ""
            self.ccvTextField.text = ""
            self.cardHolderNameTextField.text = ""
            self.expTextField.text = ""
        }
    }

}
