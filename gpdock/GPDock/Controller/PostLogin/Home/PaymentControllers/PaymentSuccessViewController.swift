//
//  PaymentSuccessViewController.swift
//  GPDock
//
//  Created by TecOrb on 03/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
protocol PaymentSuccessViewControllerDelegate {
    func paymentSuccessViewController(viewController:PaymentSuccessViewController, didFinishPayment done: Bool)
}

class PaymentSuccessViewController: UIViewController {
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!

    @IBOutlet weak var transactionIDLabel : UILabel!
    @IBOutlet weak var okayButton : UIButton!
    var delegate : PaymentSuccessViewControllerDelegate?
    var payment : Payment!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.amountLabel.text =  String(format: "$ %0.2lf", (payment.amount/100))
        self.messageLabel.text = "Your payment has been sent\r\n\nA/c no.: \(payment.accountNumber)"
        self.transactionIDLabel.text = self.payment.CustomerTransactionID
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        okayButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.okayButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickOkay(_ sender:UIButton){
        NotificationCenter.default.post(name: .USER_PAID_AT_MARINA_NOTIFICATION, object: nil, userInfo: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
