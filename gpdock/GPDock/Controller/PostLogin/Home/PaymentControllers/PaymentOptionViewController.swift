//
//  PaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 27/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class PaymentOptionViewController: UIViewController {
    @IBOutlet weak var optionTableView : UITableView!

    //let customerContext = GPDockCustomerContext()
    //let customerContext = STPCustomerContext()
    var user:User!
    var cards = Array<Card>()
    var amountsArray : Array<Double>?//(category:String,amount:Double)>?
    var amount : Double!
    var marina: Marina!
    var paymentNote : String?
    var bookingDates: BookingDates!
    var selectedSize : BoatSize!
    var boat : ParkingSpace!
    var userOwnBoat : Boat?
    var payableTitle : String?
    var isPayingForMarina = false
    var selectedIndex = -1
    var discountPrice: Double?
    var selectedOffer : OfferModal?
   // var cvvForSelectedCard = ""

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Select Payment Option"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.setupNavigationTitle()
        optionTableView.dataSource = self
        optionTableView.delegate = self
        optionTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.optionTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        self.getUserCards(self.user.ID)
//        self.getUserCards(userID: self.user.ID)
    }



    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func getUserCards(_ userID:String) -> Void {
        CommonClass.showLoader(withStatus: "Fetching..")
        PaymentService.sharedInstance.getCardsForUser(self.user.ID) { (resCards) in
            CommonClass.hideLoader()
            if let someCards = resCards{
                self.cards.removeAll()
                self.cards.append(contentsOf: someCards)
                self.optionTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        let desiredViewController = self.navigationController!.viewControllers.filter { $0 is ReviewBookingDetailsViewController }.first!

        if let dVC = desiredViewController as? ReviewBookingDetailsViewController{
            self.navigationController!.popToViewController(dVC, animated: true)
        }
        //self.navigationController?.pop(true)
    }


}

extension PaymentOptionViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 72
        if indexPath.section == 1{
            if indexPath.row == selectedIndex{
                height = 72
            }
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 12

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 12}
        }else if section == 2 {
            height = 12
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height:CGFloat = 0

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 0}
        }else if section == 2 {
            height = 0
        }
        return height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = 1
        case 1:
            rows = cards.count //customerContext.customer.gpdockSources.count
        case 2:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PayableAmountDescriptionCell", for: indexPath) as! PayableAmountDescriptionCell
            //if let title = self.payableTitle{
                cell.payableTitleLabel.text = ""//title
            //}
            cell.amountLabel.text = String(format: "$ %0.2lf", discountPrice ?? amount)
            return cell

        }else if indexPath.section == 1{

            if selectedIndex == indexPath.row{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCardTableViewCell", for: indexPath) as! SelectedCardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                cell.doneButton.addTarget(self, action: #selector(onClickPayButton(_:)), for: .touchUpInside)
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                cell.detailTextLabel?.text = "\(card.expMonth)/\(card.expYear)"
                cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)

                return cell
            }

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if self.selectedIndex == indexPath.row{
                self.selectedIndex = -1
                //self.cvvForSelectedCard = ""
            }else{
                self.selectedIndex = indexPath.row
            }
            tableView.reloadData()
            //confirmByPaying(indexPath: indexPath)
        }else if indexPath.section == 2{
            self.navigateToAddNewCard()
        }
    }


    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.Home.viewController(AddNewPaymentOptionViewController.self)
        addNewPaymentOptionVC.payableAmout = self.amount
        addNewPaymentOptionVC.amountsArray = amountsArray
        addNewPaymentOptionVC.isPayingForMarina = self.isPayingForMarina
        addNewPaymentOptionVC.marina = self.marina
        addNewPaymentOptionVC.boat = self.boat
        addNewPaymentOptionVC.bookingDates = self.bookingDates
        addNewPaymentOptionVC.selectedSize = self.selectedSize
        addNewPaymentOptionVC.ownBoat = self.userOwnBoat
        addNewPaymentOptionVC.paymentNote = self.paymentNote
        addNewPaymentOptionVC.selectedOffer = self.selectedOffer
        addNewPaymentOptionVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(addNewPaymentOptionVC, animated: true)
    }

    @IBAction func onTextDidChanged(_ textField: UITextField){
        //self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        //self.cvvForSelectedCard = ""
        if let indexPath = sender.tableViewIndexPath(self.optionTableView) as IndexPath?{
            if indexPath.row == selectedIndex{self.selectedIndex = -1}
            let card = cards[indexPath.row]
            self.askToDeleteCard(card: card, atIndexPath: indexPath)
           // self.removeCard(card, indexPath: indexPath)
        }
    }

    func askToDeleteCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Would you really want to delete this card?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeCard(card, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }

        CommonClass.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeCard(card.ID) { (success, message) in
            CommonClass.hideLoader()
            if success{
                self.cards.remove(at: indexPath.row)
                self.optionTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
        }
    }

    func confirmByPaying(indexPath:IndexPath) {
        if isPayingForMarina{
            showAlertWith(viewController: self, message: warningMessage.functionalityPending.rawValue, title: "Under Development")
            return
        }
        let card = cards[indexPath.row]
        let YY = CommonClass.sharedInstance.formattedDateWith(Date(), format: "YY")
        let MM = CommonClass.sharedInstance.formattedDateWith(Date(), format: "MM")

        let expValidation = STPCardValidator.validationState(forExpirationYear: YY, inMonth: MM)
        if expValidation != .valid{
            showAlertWith(viewController: self, message: "The card was declined. Please reenter the payment details", title: warningMessage.alertTitle.rawValue)
            return
        }

        CommonClass.showLoader(withStatus: "Booking...")
        self.payAndCreateBookingFor(self.user.ID, bookingDates: self.bookingDates, parkingSpaceID: self.boat.ID, pricePerFeet: self.marina.pricePerFeet, boatID: self.userOwnBoat?.ID, amount: self.amount, sourceToken: "", cardID: card.ID, willSave: false) {success, respBooking in
            CommonClass.hideLoader()
            if success{
                if let _ = respBooking{
                    let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                    homeTab.selectedIndexFromMenu = 2
                    homeTab.isFromMenu = true
                    self.navigationController?.setViewControllers([homeTab], animated: false)
                    CommonClass.hideLoader()

                    GPDock.showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your reservation is confirmed!", title: warningMessage.alertTitle.rawValue)
//                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
//                        showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your reservation is confirmed!", title:warningMessage.alertTitle.rawValue)
//                    })
                }
            }
        }
    }
    @IBAction func onClickPayButton(_ sender: UIButton){

        if let indexPath = sender.tableViewIndexPath(self.optionTableView){
            let card = cards[indexPath.row]
            
            //let stpcard = STPCard(id: card.cardToken, brand: STPCard.brand(from: card.brand), last4: card.last4, expMonth: card.expMonth, expYear: card.expYear, funding: STPCard.funding(from: card.fundingType))

            let YY = CommonClass.sharedInstance.formattedDateWith(Date(), format: "YY")
            let MM = CommonClass.sharedInstance.formattedDateWith(Date(), format: "MM")

            let expValidation = STPCardValidator.validationState(forExpirationYear: YY, inMonth: MM)
            if expValidation != .valid{
                showAlertWith(viewController: self, message: "The card was declined. Please reenter the payment details", title: warningMessage.alertTitle.rawValue)
                return
            }
            if isPayingForMarina{
                if let amounts = self.amountsArray{
                    self.payForDirectPayment(marinaID: self.marina.ID, cardID: card.ID, sourceToken: "", amounts: amounts, paymentNotes:self.paymentNote ?? "", coupancode: self.selectedOffer?.coupanCode ?? "")
                }
            }else{
                CommonClass.showLoader(withStatus: "Booking...")
                self.payAndCreateBookingFor(self.user.ID, bookingDates: self.bookingDates, parkingSpaceID: self.boat.ID, pricePerFeet: self.marina.pricePerFeet, boatID: self.userOwnBoat?.ID, amount: self.amount, sourceToken: "", cardID: card.ID, willSave: false) {success, respBooking in
                    CommonClass.hideLoader()

                    if success{
                        if let _ = respBooking{
                            let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                            homeTab.selectedIndexFromMenu = 2
                            homeTab.isFromMenu = true
                            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
                                showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your reservation is confirmed!", title: warningMessage.alertTitle.rawValue)
                            })
                        }
                    }

                }
            }
            
        }
    }

}


extension PaymentOptionViewController:UITextFieldDelegate,PaymentSuccessViewControllerDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func payForDirectPayment(marinaID:String,cardID:String,sourceToken:String,amounts:[Double],paymentNotes:String, coupancode: String){
        if self.user.ID.count != 0{
            self.user.payAtMarina(marinaID: marinaID, cardID: cardID, sourceToken: sourceToken, amounts: amounts, paymentNote: paymentNotes, coupanCode: coupancode, completionBlock: { (success, payment, message) in
                if success{
                    if let paymentDone = payment{
                        self.showPaymentSuccess(paymentDone)
                    }
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            })
        }
    }

    func showPaymentSuccess(_ payment: Payment){
        let paymentSuccessVC = AppStoryboard.Home.viewController(PaymentSuccessViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.delegate = self
        self.present(paymentSuccessVC, animated: true, completion: nil)
    }

    func paymentSuccessViewController(viewController:PaymentSuccessViewController, didFinishPayment done: Bool) {
        viewController.dismiss(animated: false) {
            self.dismiss(animated: false, completion: nil)
        }
    }

    func payAndCreateBookingFor(_ userID: String,bookingDates:BookingDates,parkingSpaceID: String,pricePerFeet:Double,boatID:String?,amount:Double,sourceToken:String,cardID:String?,willSave :Bool,completionBlock:@escaping (_ success:Bool,_ booking :Booking?)->Void){
        PaymentService.sharedInstance.payAndCreateBookingFor(userID, bookingDates: bookingDates, parkingSpaceID: parkingSpaceID, pricePerFeet: pricePerFeet, boatID: boatID ?? "", amount: amount, sourceToken: sourceToken, cardID: cardID ?? "", willSave: willSave, coupanCode: self.selectedOffer?.coupanCode ?? "") { (success,responseBooking,message) in
            if let booking = responseBooking{
                completionBlock(true,booking)
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                completionBlock(false,nil)
                
            }
            
        }
    }

}








