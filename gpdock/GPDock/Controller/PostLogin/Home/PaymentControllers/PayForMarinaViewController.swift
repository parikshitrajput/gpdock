//
//  PayForMarinaViewController.swift
//  GPDock
//
//  Created by TecOrb on 09/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PayForMarinaViewController: UIViewController {
    let payingOptions = ["Groceries","Gas","Pump Out","Other"]
    let payingOptionIcons = ["grocerry","gass","pump","othersPay"]
    var notes = ""
    var amounts = [0.0,0.0,0.0,0.0]
    var marina : Marina!
    var isAppered = false
    @IBOutlet weak var optionTableView: UITableView!
    @IBOutlet weak var payButton: UIButton!

    var toolPayButton: UIButton!

    var toolBar: UIToolbar!

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Send Money to \(marina.title)"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToolBarButton()
        self.setupNavigationTitle()
        self.optionTableView.register(UINib(nibName: "MarinaPayingCategoryCell", bundle: nil), forCellReuseIdentifier: "MarinaPayingCategoryCell")
        self.optionTableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        self.optionTableView.dataSource = self
        self.optionTableView.delegate = self
        self.refreshPayButton()
    }

    override func viewDidLayoutSubviews() {
        self.payButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    func setupToolBarButton() {
        toolPayButton = UIButton(type: .custom)
        toolPayButton.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        toolPayButton.setTitleColor(.white, for: UIControlState())
        toolPayButton.titleLabel?.font = fonts.Raleway.bold.font(.xLarge)
        self.toolPayButton.addTarget(self, action: #selector(PayForMarinaViewController.onClickPayButton(_:)), for: .touchUpInside)
        //CommonClass.makeViewCircularWithCornerRadius(toolPayButton, borderColor: kApplicationGreenColor, borderWidth: 0, cornerRadius: 2)
        self.toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.isOpaque = true
        toolBar.tintColor = .white
        toolBar.barTintColor = .white
        toolBar.backgroundColor = .white
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        let doneButton = UIBarButtonItem(customView: self.toolPayButton)
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolPayButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        toolBar.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.isAppered{
            return
        }
        if let cell = self.optionTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? MarinaPayingCategoryCell{
            cell.amountTextField.becomeFirstResponder()
            self.isAppered = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func refreshPayButton(){
        let amountsArray = amounts
        let totalAmount = amountsArray.reduce(0) { $0 + $1 }
        self.payButton.alpha = (totalAmount > 0) ? 1.0 : 0.5
        self.payButton.isUserInteractionEnabled = (totalAmount > 0)

        let totalStr = String(format: "%0.2f",totalAmount)
        self.payButton.setTitle("Pay $ \(totalStr)", for: UIControlState())
        self.toolPayButton.alpha = (totalAmount > 0) ? 1.0 : 0.5
        self.toolPayButton.isUserInteractionEnabled = (totalAmount > 0)
        self.toolPayButton.setTitle("Pay $ \(totalStr)", for: UIControlState())
    }


    func validateNotes() -> (result:Bool,message:String) {
        if let otherPaying = self.amounts.last{
            if ((otherPaying*100) > 0.0) && (self.notes.trimmingCharacters(in: .whitespacesAndNewlines).count == 0){
                return(false,"Please add a note indicating what you are purchasing")
            }else{
                return(true,"success")
            }
        }else{
            return(false,"Unknown Error!")
        }
    }

    func validateTotalAmounts() -> (result:Bool,message:String) {
        let amountsArray = amounts
        let amount = amountsArray.reduce(0) { $0 + $1 }
        if amount < StripeLimit.lowerLimit{
            let lowerLimit = String(format:"%.0f",StripeLimit.lowerLimit)
            return(false,"There is a minimum $\(lowerLimit) payment")
        }else if amount > StripeLimit.upperLimit{
            let upperLimit = String(format:"%.0f",StripeLimit.upperLimit)
            return(false,"There is a maximum $\(upperLimit) payment")
        }else{
            return(true,"success")
        }
    }
    @IBAction func onClickPayButton(_ sender:UIButton){
        let totalValidation = self.validateTotalAmounts()
        if !totalValidation.result{
            showAlertWith(viewController: self, message: totalValidation.message, title: warningMessage.alertTitle.rawValue)
            return
        }

        let noteValidation = self.validateNotes()
        if !noteValidation.result{
            showAlertWith(viewController: self, message: noteValidation.message, title: warningMessage.alertTitle.rawValue)
            return
        }

        let amountsArray = amounts
        self.view.endEditing(true)
        let amount = amountsArray.reduce(0) { $0 + $1 }
            if amount >= StripeLimit.lowerLimit{
            let paymentOptionVC = AppStoryboard.Home.viewController(PaymentOptionViewController.self)
            paymentOptionVC.payableTitle = "Pay at \(self.marina.title)"
            paymentOptionVC.amount = amount
            paymentOptionVC.amountsArray = amountsArray
            paymentOptionVC.isPayingForMarina = true
            paymentOptionVC.marina = self.marina
            paymentOptionVC.bookingDates = BookingDates()
            paymentOptionVC.boat = ParkingSpace()
            paymentOptionVC.selectedSize = BoatSize()
            paymentOptionVC.userOwnBoat = Boat()
            paymentOptionVC.paymentNote = self.notes
            self.navigationController?.pushViewController(paymentOptionVC, animated: true)
        }else{
            showAlertWith(viewController: self, message: "Please enter an amount greater than $0.50", title: warningMessage.alertTitle.rawValue)
        }

    }

    @IBAction func closeButtonClick(_ sender:UIBarButtonItem){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PayForMarinaViewController: UITextFieldDelegate,UITextViewDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.optionTableView){
            if let txtFiled = textField as? BKCurrencyTextField{
                if txtFiled.numberValue != nil{
                    amounts[indexPath.row] = txtFiled.numberValue.doubleValue
                }else{
                    amounts[indexPath.row] = 0.0
                }
            }
            self.refreshPayButton()
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.optionTableView){
            if let txtFiled = textField as? BKCurrencyTextField{
                if txtFiled.numberValue != nil{
                    amounts[indexPath.row] = txtFiled.numberValue.doubleValue
                }else{
                    amounts[indexPath.row] = 0.0
                }
            }
            self.refreshPayButton()
        }
    }
    
    @IBAction func textFieldTextDidChangeEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.optionTableView){
            if let txtFiled = textField as? BKCurrencyTextField{
                if txtFiled.numberValue != nil{
                amounts[indexPath.row] = txtFiled.numberValue.doubleValue
                }else{
                    amounts[indexPath.row] = 0.0
                }
            }
            self.refreshPayButton()
        }
    }


    func textViewDidChange(_ textView: UITextView) {
        self.notes = textView.text
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.notes = textView.text
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.notes = textView.text
    }

}


extension PayForMarinaViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return (section == 0) ? 4 : 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaPayingCategoryCell", for: indexPath) as! MarinaPayingCategoryCell
        cell.categoryIcon.image = UIImage(named: self.payingOptionIcons[indexPath.row])
        cell.categoryLabel.text = self.payingOptions[indexPath.row]
        cell.amountTextField.addTarget(self, action: #selector(textFieldTextDidChangeEditing(_:)), for: .editingChanged)
        cell.amountTextField.numberValue = NSDecimalNumber(value: self.amounts[indexPath.row])
        cell.amountTextField.inputAccessoryView = self.toolBar
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
            cell.inputTextView.text = notes
            cell.inputTextView.delegate = self
            //cell.amountTextField.inputAccessoryView = self.toolBar
            return cell
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        }else{
            let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
            headerView.headerLabel.text = "  Notes"
            headerView.headerLabel.font = fonts.Raleway.bold.font(.large)
            headerView.headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            headerView.backgroundColor = UIColor.groupTableViewBackground
            return headerView
        }

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 0 : 35
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 50 : 100
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at:indexPath) as? MarinaPayingCategoryCell{
            cell.amountTextField.becomeFirstResponder()
        }else if let cell = tableView.cellForRow(at:indexPath) as? TextViewCell{
            cell.inputTextView.becomeFirstResponder()
        }
    }


}











class PayMoneyNavigationController: UINavigationController {
    var statusBarView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}




