//
//  AddCardViewController.swift
//  GPDock
//
//  Created by TecOrb on 31/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class AddCardViewController : UIViewController {
    @IBOutlet weak var cardNumberTextField : BKCardNumberField!
    @IBOutlet weak var cardHolderNameTextField : UITextField!
    @IBOutlet weak var ccvTextField : UITextField!
    @IBOutlet weak var expTextField : BKCardExpiryField!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var payButton : UIButton!

    var user : User!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Add New Card"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preloadCardIO()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.setupNavigationTitle()
        self.setupAllView()
        self.navigateToScanCard()
    }
    override func viewDidLayoutSubviews() {
        self.payButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    func setupAllView(){
        cardNumberTextField.showsCardLogo = true
        cardNumberTextField.cardNumberFormatter.groupSeparater = " "
        CommonClass.makeViewCircularWithCornerRadius(self.containnerView, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.cardNumberTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardHolderNameTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.cardHolderNameTextField.setLeftPaddingPoints(5)
        CommonClass.makeViewCircularWithCornerRadius(self.ccvTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.ccvTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.expTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.expTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.payButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)
    }

    @IBAction func onClickScanCard(_ sender: UIButton){
        //open card scanner screen
       self.navigateToScanCard()
    }

    func navigateToScanCard(){
        //open card scanner screen
        let scanCardVC = AppStoryboard.Settings.viewController(ScanCardViewController.self)
        scanCardVC.delegate = self
        let nav = UINavigationController(rootViewController: scanCardVC)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    @IBAction func onToggleSaveCardForFasterCheckOut(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        //validate card params
        self.payButton.isEnabled = false
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            showAlertWith(viewController: self, message: warningMessage.validCardNumber.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            showAlertWith(viewController: self, message: warningMessage.cardHolderName.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            showAlertWith(viewController: self, message: warningMessage.cardHolderName.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        guard let brand = self.cardNumberTextField.cardNumberFormatter.cardPatternInfo.shortName  else {
            self.payButton.isEnabled = true
            showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            showAlertWith(viewController: self, message: warningMessage.enterCVV.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            showAlertWith(viewController: self, message: warningMessage.enterCVV.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            showAlertWith(viewController: self, message: warningMessage.expMonth.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            showAlertWith(viewController: self, message: warningMessage.expYear.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true

            return
        }

        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv)
        if !inputValidation.success{
            showAlertWith(viewController: self, message: inputValidation.message, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true
            return
        }

        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv)
        if !success{
            showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
            self.payButton.isEnabled = true
            return
        }
        self.createTokenAndProceedBookingWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv)
    }

    func createTokenAndProceedBookingWith(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String){

        CommonClass.showLoader(withStatus: "Creating..")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV

        STPAPIClient.shared().createToken(withCard: cardParams) { (stpToken, error) in
            if error != nil{
                CommonClass.hideLoader()
                showAlertWith(viewController: self, message: "\(String(describing: error!.localizedDescription))", title: warningMessage.alertTitle.rawValue)
                self.payButton.isEnabled = true

                return
            }else{
                guard let token = stpToken else{
                    CommonClass.hideLoader()
                    showAlertWith(viewController: self, message: warningMessage.cardDeclined.rawValue, title: warningMessage.alertTitle.rawValue)
                    self.payButton.isEnabled = true
                    return
                }
                CommonClass.updateLoader(withStatus: "Saving..")
                self.createCardFor(self.user.ID, sourceToken: token.tokenId)
            }
        }
    }


    func validteCardInputs(_ cardNumber:String,cardHolderName:String,brand:String,expMonth: Int?,expYear:Int?,CVV:String) -> (success:Bool,message:String) {
        if cardNumber.count == 0{
            return(false,warningMessage.validCardNumber.rawValue)
        }

        if cardHolderName.count == 0{//Please enter the card holder's name
            return(false,warningMessage.cardHolderName.rawValue)
        }

        if let expMon = expMonth{
            if (expMon < 1) || (expMon > 12){return(false,warningMessage.validExpMonth.rawValue)}
        }else{
            return(false,warningMessage.expMonth.rawValue)
        }

        if let expY = expYear{
            if (expY < Date().year){return(false,warningMessage.validExpYear.rawValue)}
        }else{
            return(false,warningMessage.expYear.rawValue)
        }

        if !brand.lowercased().contains("maestro"){
            if (CVV.count < 3) || (CVV.count > 4){return(false,warningMessage.enterCVV.rawValue)}
        }
        return(true,"")

    }

    func validateCardParams(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String) -> Bool {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        let validationState = STPCardValidator.validationState(forCard: cardParams)
        return (validationState != .invalid)
    }

    func createCardFor(_ userID: String,sourceToken:String){
        PaymentService.sharedInstance.addCardForUser(userID, cardToken: sourceToken) { (success, resCard,message)  in
            CommonClass.hideLoader()
            if success{
                if let aCard = resCard{
                    NotificationCenter.default.post(name: .USER_DID_ADD_NEW_CARD_NOTIFICATION, object: nil, userInfo: ["card":aCard])
                    //self.navigationController?.pop(true)
                }else{
                    self.payButton.isEnabled = true
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                self.payButton.isEnabled = true
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddCardViewController: ScanCardViewControllerDelegate{
    func cardDidScan(viewcontroller: ScanCardViewController, withCardInfo cardInfo: CardIOCreditCardInfo) {
        if let cardNumber = cardInfo.cardNumber{
            self.cardNumberTextField.cardNumber = cardNumber
        }else{
            self.cardNumberTextField.cardNumber = ""
        }
        if let ccv = cardInfo.cvv{
            self.ccvTextField.text = ccv
        }else{
            self.ccvTextField.text = ""
        }
        if let cardholderName = cardInfo.cardholderName{
            self.cardHolderNameTextField.text = cardholderName
        }else{
            self.cardHolderNameTextField.text = ""
        }

        if let expiryMonth = cardInfo.expiryMonth as UInt?{
            if expiryMonth > 0{
                self.expTextField.dateComponents.month = Int(expiryMonth)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        if let expiryYear = cardInfo.expiryYear as UInt?{
            if expiryYear > 0{
                self.expTextField.dateComponents.year = Int(expiryYear)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
    }

    func cardDidScan(viewcontroller: ScanCardViewController, isUserChooseManual wantManual: Bool) {
        if wantManual{
            self.cardNumberTextField.cardNumber = ""
            self.ccvTextField.text = ""
            self.cardHolderNameTextField.text = ""
            self.expTextField.text = ""
        }
    }

}


