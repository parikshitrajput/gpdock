//
//  PaymentsMethodsViewController.swift
//  GPDock
//
//  Created by TecOrb on 31/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class PaymentsMethodsViewController: UIViewController,LoginViewDelegate {
    @IBOutlet weak var optionTableView : UITableView!
    var loginView : LoginView!
    var user:User!
    var cards = Array<Card>()
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "My Payment Methods"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentsMethodsViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentsMethodsViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)


        NotificationCenter.default.addObserver(self, selector: #selector(PaymentsMethodsViewController.userDidAddNewCardHandler(_:)), name: .USER_DID_ADD_NEW_CARD_NOTIFICATION, object: nil)
        //self.getUserCards(userID: self.user.ID)
        self.setupViews()
        self.setupNavigationTitle()

    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        GPDock.openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    @objc func userDidAddNewCardHandler(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Card>{
            if let aCard = userInfo["card"]{
                let duplicateCards = self.cards.filter({ (card) -> Bool in
                    return aCard.ID == card.ID
                })

                if duplicateCards.count == 0{
                    self.cards.append(aCard)
                    self.optionTableView.reloadData()
                    self.navigationController?.pop(true)
                }else{
                    GPDock.showAlertWith(viewController: nil, message: "The card you have entered is already exists", title: warningMessage.alertTitle.rawValue, complitionBlock: { (done) in
                        self.navigationController?.pop(true)
                    })
                }
            }
        }
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupContainer()
        }else{
            self.cards.removeAll()
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view your cards\r\nPlease Login or Sign up")
        }
    }
    func setupContainer(){
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson!)
        self.cards.removeAll()

        optionTableView.dataSource = self
        optionTableView.delegate = self
        optionTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.optionTableView.contentInset = UIEdgeInsets(top: -18, left: 0, bottom: 0, right: 0)
        self.getUserCards(self.user.ID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if CommonClass.isLoggedIn{
//        }
    }

    func getUserCards(_ userID:String) -> Void {
        CommonClass.showLoader(withStatus: "Fetching..")
        PaymentService.sharedInstance.getCardsForUser(self.user.ID) { (resCards) in
            CommonClass.hideLoader()
            if let someCards = resCards{
                self.cards.removeAll()
                self.cards.append(contentsOf: someCards)
                self.optionTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
}

extension PaymentsMethodsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 18
        if section == 0{
            if cards.count == 0{height = 0}else{height = 0}
        }else if section == 1 {
            height = 18
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = cards.count
        case 1:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                cell.detailTextLabel?.text = "\(card.expMonth)/\(card.expYear)"
                cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
                return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            return
        }else if indexPath.section == 1{
            self.navigateToAddNewCard()
        }
    }


    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.Settings.viewController(AddCardViewController.self)
        self.navigationController?.pushViewController(addNewPaymentOptionVC, animated: true)
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.optionTableView) as IndexPath?{
            let card = cards[indexPath.row]
            self.askToDeleteCard(card: card, atIndexPath: indexPath)
        }
    }

    func askToDeleteCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Would you really want to delete this card?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeCard(card, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }

        CommonClass.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeCard(card.ID) { (success, message) in
            CommonClass.hideLoader()
            if success{
                self.cards.remove(at: indexPath.row)
                self.optionTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
        }
    }

}

extension PaymentsMethodsViewController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}







