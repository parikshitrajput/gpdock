//
//  MenuViewController.h
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//


import UIKit
import SDWebImage
import FBSDKCoreKit
import FBSDKLoginKit
import Google.SignIn


let kDarkLeftMenuCellColor = UIColor(red:46.0/255.0, green:53.0/255.0, blue:67.0/255, alpha:1.0)
let kLightLeftMenuCellColor = UIColor(red:50.0/255.0, green:58.0/255.0, blue:71.0/255, alpha:1.0)


class LeftMenuViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var tableView: UITableView!
    var slideOutAnimationEnabled : Bool!
    let leftMenuOptions = ["Profile","Explore Marinas","My Bookings","My Boats","My Favorites","Notifications","My Payment Methods","Service Transactions","Share","About Us","Terms of Use","Privacy Policy","Support","Logout"]
    let leftMenuIcons = ["user_img","explore_marina","my_bookings","my_boat","my_favourite","notifications","my_payment_method","servicetransactions","share","about_us","terms_condition","privacy_policy","menu_support","logout"]

    var user : User!

    override func viewDidLoad() {
        slideOutAnimationEnabled = false
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)


        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(slideNavigationDidClose(_:)), name: .GPDockSlideNavigationControllerDidClose, object: nil)

        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        //self.myBookingVC = AppStoryboard.Home.viewController(MyBookingsViewController.self)
        SlideNavigationController.sharedInstance().enableShadow = false
        SlideNavigationController.sharedInstance().enableSwipeGesture = true
        SlideNavigationController.sharedInstance().portraitSlideOffset = UIScreen.main.bounds.size.width * 1 / 4
        self.tableView.separatorColor = UIColor.white
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.delegate = self
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView != nil{
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuOptions.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 90 : 48
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuProfileCell", for: indexPath) as! MenuProfileCell
            cell.profileIcon.sd_setImage(with: URL(string:self.user.profileImage), placeholderImage: CommonClass.sharedInstance.userAvatarImage(username:"\(self.user.firstName) \(self.user.lastName)"))
            cell.nameLabel.text = CommonClass.isLoggedIn ? "\(self.user.firstName) \(self.user.lastName)" : "Guest"
            CommonClass.makeViewCircular(cell.profileIcon, borderColor: kNavigationColor, borderWidth: 0)
            return cell
        }else{

        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
        if indexPath.row == leftMenuOptions.count-1{
            cell.cellIcon.setImage(UIImage(named:leftMenuIcons[indexPath.row]), for: UIControlState())
            cell.nameLabel.text = CommonClass.isLoggedIn ? leftMenuOptions[indexPath.row] : "Login"
        }else{
            cell.cellIcon.setImage(UIImage(named:leftMenuIcons[indexPath.row]), for: UIControlState())
            cell.nameLabel.text = leftMenuOptions[indexPath.row]
        }
        return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        let colorView = UIView()
        colorView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.selectedBackgroundView = colorView
        var vc : UIViewController!

        switch indexPath.row {
        case 0:
            //if CommonClass.isLoggedIn{
                let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                homeTab.selectedIndexFromMenu = 3
                homeTab.isFromMenu = true
                vc = homeTab
//            }else{
//                showLoginAlert(isFromMenu: true,inViewController: nil)
//            }
            cell.selectedBackgroundView = nil
        case 1:
            let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
            homeTab.isFromMenu = true
            homeTab.selectedIndexFromMenu = 0
            vc = homeTab
        case 2:
//            if CommonClass.isLoggedIn{
                let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
                homeTab.selectedIndexFromMenu = 2
                homeTab.isFromMenu = true
                vc = homeTab
//            }else{
//                showLoginAlert(isFromMenu: true,inViewController: nil)
//            }
            cell.selectedBackgroundView = nil

        case 3:
           // if CommonClass.isLoggedIn{
                let myBoatsVC = AppStoryboard.Profile.viewController(MyBoatsViewController.self)
                vc = myBoatsVC
//            }else{
//                showLoginAlert(isFromMenu: true,inViewController: nil)
//            }
            cell.selectedBackgroundView = nil
        case 4:
            let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
            homeTab.selectedIndexFromMenu = 1
            homeTab.isFromMenu = true
            vc = homeTab
            cell.selectedBackgroundView = nil
        case 5:
            let notificationVC = AppStoryboard.Profile.viewController(NotificationListViewController.self)
            notificationVC.isFromMenu = true
            vc = notificationVC
            cell.selectedBackgroundView = nil
        case 6:
            vc = AppStoryboard.Settings.viewController(PaymentsMethodsViewController.self)
            cell.selectedBackgroundView = nil
        case 7:
            let transactionVC = AppStoryboard.Transactions.viewController(ServiceTransactionListViewController.self)
            transactionVC.isFromMenu = true
            vc = transactionVC
            cell.selectedBackgroundView = nil
        case 8:
            self.shareApplication()
            cell.selectedBackgroundView = nil
            return
        case 9:
            vc = AppStoryboard.Settings.viewController(AboutUsViewController.self)
            cell.selectedBackgroundView = nil
        case 10:
            let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
            termsVc.isPrivacyPolicy = false
            termsVc.isFromMenu = true
            vc = termsVc
            cell.selectedBackgroundView = nil
        case 11:
            let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
            termsVc.isPrivacyPolicy = true
            termsVc.isFromMenu = true
            vc = termsVc
            cell.selectedBackgroundView = nil
        case 12:
            let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
            supportVC.isFromMenu = true
            vc = supportVC
            cell.selectedBackgroundView = nil
        case 13:

            if CommonClass.isLoggedIn{
                askForLogout()
            }else{
                showLoginAlert(isFromMenu: true, shouldLogin: true, inViewController: nil)
            }
            cell.selectedBackgroundView = nil
            return
        default:
            cell.selectedBackgroundView = nil
            return
        }
        cell.selectedBackgroundView = nil
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: {[cell,tableView] in
            cell.selectedBackgroundView = nil
            tableView.reloadData()
        })
    }

    
    func shareApplication(){
        tableView.reloadData()
        SlideNavigationController.sharedInstance().closeMenu {
            let textToShare = "Check out GPDock! A new way to reserve your next sailing adventure! \(WEBSITE_URL)"
            //let url = URL(string:WEBSITE_URL)!
            var objectsToShare = [Any]()
            objectsToShare.append(textToShare)
            //objectsToShare.append(url)

            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
            appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
        }
    }

    func rateTheAppApplication(){
        tableView.reloadData()
        SlideNavigationController.sharedInstance().closeMenu {
            self.rateApp(appId: "1241899105", completion: { (success) in
                if !success{
                    showErrorWithMessage("Couldn't be reviewed")
                }
            })
        }
    }

    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id=" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }


    func askForLogout() {

        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Do you really want to logout?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.logout()
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            SlideNavigationController.sharedInstance().closeMenu {
                self.tableView.reloadData()
            }
            alert.dismiss(animated: true, completion: nil)
        }

            alert.addAction(okayAction)
            alert.addAction(cancelAction)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    @objc func userDidLoggedIn(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let lUser = userInfo["user"] as? User{
                self.user = lUser
                self.tableView.reloadData()
            }
        }
    }

    @objc func slideNavigationDidClose(_ notification:Notification) {
        if self.tableView != nil{
            self.tableView.reloadData()
        }
    }

    func logout() {
        SlideNavigationController.sharedInstance().closeMenu {
            if !CommonClass.isConnectedToNetwork{
                let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                showAlertWith(viewController:appDelegate.window!.rootViewController!, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
                self.tableView.reloadData()
                return
            }
            self.logoutFromSocialMedia()
            User.logOut({ (success, resUser, message) in
                if success{
                    kUserDefaults.set(false, forKey: kIsLoggedIN)
                    let user = User()
                    user.saveUserInfo(user)
                    Settings.sharedInstance.reset()
                    let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                    let menu = AppStoryboard.Main.viewController(LeftMenuViewController.self)
                    let nav = AppStoryboard.Home.viewController(SlideNavigationController.self)
                    nav.avoidSwitchingToSameClassViewController = false
                    nav.leftMenu = menu
                    nav.enableSwipeGesture = true
                    appDelegate.window?.rootViewController = nav
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    showAlertWith(viewController:appDelegate.window!.rootViewController!, message: "You have logged out successfully", title: warningMessage.alertTitle.rawValue)
                }else{
                    let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                    showAlertWith(viewController:appDelegate.window!.rootViewController!, message: message, title: warningMessage.alertTitle.rawValue)
                }
            })

        }
    }

    func logoutFromSocialMedia() {
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.systemAccount
        login.logOut()
        GIDSignIn.sharedInstance().signOut()
    }
}




