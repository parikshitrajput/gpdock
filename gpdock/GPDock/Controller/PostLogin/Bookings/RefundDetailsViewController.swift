//
//  RefundDetailsViewController.swift
//  GPDock
//
//  Created by TecOrb on 15/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class RefundDetailsViewController: UIViewController {
    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var supportView: UIView!

    var booking: Booking!
    var navigationTitleView : FloorPlanNavigationTitleView!
    var refundDetails = Refund()
    var titleView: NavigationTitleView!
    //  bookingTableView

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        //self.setUpNavigationView()
        self.setupNavigationTitle()
        self.bookingTableView.dataSource = self
        self.bookingTableView.delegate = self
        self.bookingTableView.backgroundColor = UIColor.groupTableViewBackground
        self.bookingTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.getRefundDataFromServer(withBookingID: self.booking.ID)
    }

    override func viewDidLayoutSubviews() {
        self.supportView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    func getRefundDataFromServer(withBookingID bookingID: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingRefundDetails(bookingID, completionBlock: { (resRefund) in
            if let aRefund = resRefund{
                CommonClass.hideLoader()
                self.refundDetails = aRefund
                self.bookingTableView.reloadData()
            }
        })
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    

//    func setUpNavigationView() -> Void {
//        self.navigationTitleView = FloorPlanNavigationTitleView.instanceFromNib()
//        self.navigationTitleView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width-90, height: 44)
//        self.navigationTitleView.titleLabel.text = "Refund Details"
//        self.navigationTitleView.addressLabel.text = "Booking ID : \(booking.ID)"
//        self.navigationItem.titleView = self.navigationTitleView
//    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Refund Details" //"Booking "+self.booking.ID
        self.navigationItem.titleView = self.titleView
    }

    func registerCells() {

        self.bookingTableView.register(UINib(nibName: "TimeToCreditCell", bundle: nil), forCellReuseIdentifier: "TimeToCreditCell")

        self.bookingTableView.register(UINib(nibName: "CurrentRefundStatusCell", bundle: nil), forCellReuseIdentifier: "CurrentRefundStatusCell")
        self.bookingTableView.register(UINib(nibName: "RefundAmountAndStausOnDateCell", bundle: nil), forCellReuseIdentifier: "RefundAmountAndStausOnDateCell")
        self.bookingTableView.register(UINib(nibName: "RateAndReviewCell", bundle: nil), forCellReuseIdentifier: "RateAndReviewCell")

        self.bookingTableView.register(UINib(nibName: "RefundSourceCell", bundle: nil), forCellReuseIdentifier: "RefundSourceCell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RefundDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    @IBAction func navigateToSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        supportVC.booking = self.booking
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentRefundStatusCell", for: indexPath) as! CurrentRefundStatusCell
            cell.configureButtons()
            cell.cancellationInitiatedButton.isSelected = self.refundDetails.cancellationInitiated
            cell.cancellationSuccessButton.isSelected = self.refundDetails.cancellationSuccessful
            cell.refundInitiatedButton.isSelected = self.refundDetails.refundInitiated
            cell.refundProcessedButton.isSelected = self.refundDetails.refundCompleted
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeToCreditCell", for: indexPath) as! TimeToCreditCell
            cell.timeLabel.text = refundDetails.creditTime
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RefundAmountAndStausOnDateCell", for: indexPath) as! RefundAmountAndStausOnDateCell
            cell.amountLabel.text = "$ "+String(format: "%0.2lf", (refundDetails.amount))
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RefundSourceCell", for: indexPath) as! RefundSourceCell
            cell.amountLabel.text = "$ "+String(format: "%0.2lf", (refundDetails.amount))
            cell.statusLabel.text = refundDetails.progressStage
            cell.sourceTypeLabel.text = refundDetails.card.brand + " xxxx-\(refundDetails.card.last4)"
            cell.dateLabel.text = CommonClass.formattedDateWithString(refundDetails.updatedAt, format: "MM/dd/YY")
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://CurrentRefundStatusCell
            height = 158
        case 1: //TimeToCreditCell
            height = 43
        case 2: //RefundAmountAndStatusCell
            height = 72
        case 3: //RefundSourceCell
            height = 129
        default:
            break
        }
        return height
    }


    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM/dd/YY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }
}











