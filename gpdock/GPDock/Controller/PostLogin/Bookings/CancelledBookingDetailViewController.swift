//
//  BookingDetailViewController.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CancelledBookingDetailViewController: UIViewController {
    var booking: Booking = Booking()
    var refundDetails = Refund()
    var titleView : NavigationTitleView!

    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var tabView: UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.setupNavigationTitle()
        self.bookingTableView.dataSource = self
        self.bookingTableView.delegate = self
        self.bookingTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.getRefundDataFromServer(withBookingID: self.booking.ID)
    }
    override func viewDidLayoutSubviews() {
        self.tabView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Booking "+self.booking.ID
        self.navigationItem.titleView = self.titleView
    }

    func getRefundDataFromServer(withBookingID bookingID: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingRefundDetails(bookingID, completionBlock: { (resRefund) in
            if let aRefund = resRefund{
                CommonClass.hideLoader()
                self.refundDetails = aRefund
                self.bookingTableView.reloadData()
            }
        })
    }



    func registerCells() {
        self.bookingTableView.register(UINib(nibName: "BookingIDRepresentingCell", bundle: nil), forCellReuseIdentifier: "BookingIDRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "MarinaRepresentingCell", bundle: nil), forCellReuseIdentifier: "MarinaRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "GoToRefundCell", bundle: nil), forCellReuseIdentifier: "GoToRefundCell")

        self.bookingTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.bookingTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.bookingTableView.register(UINib(nibName: "DiscountOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountOfferTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    func getBookingDataFromServer(withBookingID bookingID: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingDetails(self.booking.ID) { (resBooking) in
            if let aBooking = resBooking{
                self.booking = aBooking
            }
        }
    }

    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        supportVC.booking = self.booking
        self.navigationController?.pushViewController(supportVC, animated: true)
    }
    @IBAction func onClickMailInvoice(_ sender: UIButton){

        //        self.booking.emailInvoice{ (sueccess, message) in
        //            showAlertWith(viewController: self, message: message, title: sueccess ? "GPDock" : "Error")
        //        }
        self.shareMarina()
    }
    
    func shareMarina(){
        let fromDate = self.getDateFromString(self.booking.fromDate)
        let toDate = self.getDateFromString(self.booking.toDate)
        let marinaNameToshare = self.booking.marina.title+"\r\n"+self.booking.marina.address+", "+self.booking.marina.city.name+", "+self.booking.marina.city.state

        let staticText = "Hi,\r\nI have booked \(marinaNameToshare) from \(fromDate) to \(toDate)"

        let textToShare = staticText+"\r\n"+MARINA_SHARING_URL+self.booking.marina.ID

        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }

    @IBAction func onClickGetDirectionButton(_ sender: UIButton){
        self.showLocationOfGoogleMap(marina: self.booking.marina)
    }

    func showLocationOfGoogleMap(marina:Marina)  {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            if let url = URL(string:"comgooglemaps://?center=\(marina.latitude),\(marina.longitude)&zoom=16&q=\(marina.latitude),\(marina.longitude)" ){
                UIApplication.shared.openURL(url)
            }
        } else
        {
            if (UIApplication.shared.canOpenURL(URL(string:"https://maps.google.com")!))
            {
                if let url = URL(string:"https://www.google.com/maps/@\(marina.latitude),\(marina.longitude),18z"){
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    @IBAction func onClickMarinaNameLabel(_ sender: UIButton){
        self.navigateToMarinaProfile(marina: self.booking.marina)
    }

    @IBAction func onClickRebook(_ sender: UIButton){
        self.navigateToMarinaProfile(marina: self.booking.marina)
    }
    func navigateToMarinaProfile(marina: Marina) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        SlideNavigationController.sharedInstance().pushViewController(marinaProfileVC, animated: true)
    }


    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM/dd/YY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

    func getDateFromString(_ dateString: String,format: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = format
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

}

extension CancelledBookingDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if booking.offer.mainPrice == 0.0 {
            return 9
        }else{
        return 10
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingIDRepresentingCell", for: indexPath) as! BookingIDRepresentingCell
            cell.bookingIDLabel.text = self.booking.parkingSpace.title
            cell.statusLabel.setTitle(booking.bookingStatus.rawValue.capitalized, for: UIControlState())
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaRepresentingCell", for: indexPath) as! MarinaRepresentingCell
            cell.marinaTitleLabel.text = booking.marina.title.capitalized
            cell.marinaAddressLabel.text = "\(booking.marina.address), \(booking.marina.city.name), \(booking.marina.city.state)"
            cell.directionButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            cell.marinaNameLabelButton.addTarget(self, action: #selector(onClickMarinaNameLabel(_:)), for: .touchUpInside)

            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = booking.marina.checkIn
            cell.checkOutTime.text = booking.marina.checkOut
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.lengthLabel.text = "\(booking.boat.boatSize.length) ft."
            cell.widthLabel.text = "\(booking.boat.boatSize.width) ft."
            cell.depthLabel.text = "\(booking.boat.boatSize.depth) ft."
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "GoToRefundCell", for: indexPath) as! GoToRefundCell
            return cell

        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell

            cell.otherIcon.image = #imageLiteral(resourceName: "refund")
            cell.otherTitleLabel.text = refundDetails.progressStage
            cell.otherDetailsLabel.text = " "
            cell.needsUpdateConstraints()
            cell.updateConstraints()
            cell.setNeedsLayout()
            cell.layoutIfNeeded()

            return cell

        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "amount_refrence")
            cell.otherTitleLabel.text = "Refund Amount"
            if (!self.refundDetails.cancellationInitiated && !self.refundDetails.cancellationSuccessful && !self.refundDetails.refundInitiated && !self.refundDetails.refundCompleted && !self.refundDetails.refundRejected){
                cell.otherDetailsLabel.text = "Not Applicable"
            }else{
                cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", self.refundDetails.amount)
            }
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "process_date")
            cell.otherTitleLabel.text = "Last Updated On"
            cell.otherDetailsLabel.text = self.getDateFromString(booking.toDate,format:"MM/dd/YY")
            cell.setNeedsLayout()
            cell.layoutIfNeeded()

            return cell
        case 8:
            if booking.offer.mainPrice == 0.0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
                cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
                cell.otherTitleLabel.text = (self.booking.bookingType == .online) ? "Amount Paid" : "Booking Mode"
                cell.otherDetailsLabel.text = (self.booking.bookingType == .online) ? "$ "+String(format: "%0.2lf", booking.amount) : "Offline"
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountOfferTableViewCell", for: indexPath) as! DiscountOfferTableViewCell
                cell.discountImage.image = #imageLiteral(resourceName: "offer")
                cell.discountOfferLabel.text = String(format: "%0.0f", booking.offer.offerPercentage) + " %"
                let offerPrice = booking.offer.mainPrice - booking.offer.offerPrice
                cell.discountOfferAmount.text = "$ "+String(format: "%0.2lf", offerPrice)
                cell.mainPriceAmount.text = "$ "+String(format: "%0.2lf", booking.offer.mainPrice)
                
                return cell
            }
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
            cell.otherTitleLabel.text = (self.booking.bookingType == .online) ? "Amount Paid" : "Booking Mode"
            cell.otherDetailsLabel.text = (self.booking.bookingType == .online) ? "$ "+String(format: "%0.2lf", booking.amount) : "Offline"
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
//        case 9:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
//            cell.otherIcon.image = #imageLiteral(resourceName: "offers_line")
//            cell.otherTitleLabel.text = "Discount Offer"
//            cell.otherDetailsLabel.text = String(format: "%0.0f", booking.offer.offerPercentage) + " %"
//            cell.setNeedsLayout()
//            cell.layoutIfNeeded()
//            return cell

        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://bookingid
            height = 89
        case 1: //MarinaRepresentingCell
            height = 89
        case 2: //CheckInCheckOutCell
            height = 89
        case 3: //Baot
            height = 111
        case 4: //Go to refund cell
            height = 64
        case 5: //OtherCell (Refund Processed)
            height = 64
        case 6: //OtherCell (Refund Amount)
            height = 64
        case 7: //OtherCell (Prcessed date)
            height = 64
        case 8: //OtherCell (Amount Paid)
            if booking.offer.mainPrice == 0.0 {
                height = 64
            }else{
                height = 94
            }
        case 9: //OtherCell (Discount Offer)
            height = 64
        default:
            break
        }
        return height
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4{
            if refundDetails.refundRejected{
                showAlertWith(viewController: self, message: "Refund for your booking with ID:\(self.booking.ID) has been rejected by admin", title: warningMessage.alertTitle.rawValue)
            }else if (!self.refundDetails.cancellationInitiated && !self.refundDetails.cancellationSuccessful && !self.refundDetails.refundInitiated && !self.refundDetails.refundCompleted && !self.refundDetails.refundRejected){
                showAlertWith(viewController: self, message: "Refund on your booking with ID:\(self.booking.ID) is Not Applicable", title: warningMessage.alertTitle.rawValue)
            }else{
                let refuncDetailsVC = AppStoryboard.Booking.viewController(RefundDetailsViewController.self)
                refuncDetailsVC.booking = self.booking
                self.navigationController?.pushViewController(refuncDetailsVC, animated: true)
            }
        }else if indexPath.row == 1{
            return
        }
    }
    
}
