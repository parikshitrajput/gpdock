//
//  UpcomingBookingDetailViewController.swift
//  GPDock
//
//  Created by TecOrb on 14/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class UpcomingBookingDetailViewController: UIViewController,CancellationAlertViewControllerDelegate {
    var booking: Booking = Booking()
    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var tabView: UIView!

    var titleView: NavigationTitleView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(UpcomingBookingDetailViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        self.registerCells()
        self.setupNavigationTitle()
        self.bookingTableView.dataSource = self
        self.bookingTableView.delegate = self
        self.bookingTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)

    }

//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }

//    @objc func userDidLoggedInHandler(_ notification: Notification) {
//        if notification.name == .SESSION_EXPIRED_NOTIFICATION{
//            self.navigationController?.viewControllers.removeLast()
//        }
//    }

    override func viewDidLayoutSubviews() {
        self.tabView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Booking "+self.booking.ID
        self.navigationItem.titleView = self.titleView
    }


    func registerCells() {
        self.bookingTableView.register(UINib(nibName: "BookingIDRepresentingCell", bundle: nil), forCellReuseIdentifier: "BookingIDRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "MarinaRepresentingCell", bundle: nil), forCellReuseIdentifier: "MarinaRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "QRCodeCell", bundle: nil), forCellReuseIdentifier: "QRCodeCell")
        
        self.bookingTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.bookingTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.bookingTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.bookingTableView.register(UINib(nibName: "DiscountOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountOfferTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        supportVC.booking = self.booking
        self.navigationController?.pushViewController(supportVC, animated: true)
    }

    @IBAction func onClickMailInvoice(_ sender: UIButton){

        //        self.booking.emailInvoice{ (sueccess, message) in
        //            showAlertWith(viewController: self, message: message, title: sueccess ? "GPDock" : "Error")
        //        }
        self.shareMarina()
    }
    func shareMarina(){
        let fromDate = self.getDateFromString(self.booking.fromDate)
        let toDate = self.getDateFromString(self.booking.toDate)
        let marinaNameToshare = self.booking.marina.title+"\r\n"+self.booking.marina.address+", "+self.booking.marina.city.name+", "+self.booking.marina.city.state

        let staticText = "Hi,\r\nI have booked \(marinaNameToshare) from \(fromDate) to \(toDate)"

        let textToShare = staticText+"\r\n"+MARINA_SHARING_URL+self.booking.marina.ID

        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    @IBAction func onClickGetDirectionButton(_ sender: UIButton){
        self.showLocationOfGoogleMap(marina: self.booking.marina)
    }

    func showLocationOfGoogleMap(marina:Marina)  {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            if let url = URL(string:"comgooglemaps://?center=\(marina.latitude),\(marina.longitude)&zoom=16&q=\(marina.latitude),\(marina.longitude)" ){
                UIApplication.shared.openURL(url)
            }
        } else
        {
            if (UIApplication.shared.canOpenURL(URL(string:"https://maps.google.com")!))
            {
                if let url = URL(string:"https://www.google.com/maps/@\(marina.latitude),\(marina.longitude),18z"){
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    @IBAction func onClickCancel(_ sender: UIButton){
        self.showCancellationAlert()
    }

    @IBAction func onClickMarinaNameLabel(_ sender: UIButton){
        self.navigateToMarinaProfile(marina: self.booking.marina)
    }

    func navigateToMarinaProfile(marina: Marina) {
        let marinaProfileVC = AppStoryboard.Home.viewController(BuisnessDetailViewController.self)
        marinaProfileVC.marina = marina
        SlideNavigationController.sharedInstance().pushViewController(marinaProfileVC, animated: true)
    }

    func cancellationAlertViewController(viewController: CancellationAlertViewController, bookingDidCancel success: Bool) {
        if success{
            self.navigationController?.pop(true)
//            showAlertWith(viewController: ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Your booking has been cancelled\r\nAmount(if refundable) would be credit as per policy!", title: warningMessage.alertTitle.rawValue)
        }
    }

    func showCancellationAlert() -> Void {
        if booking.bookingType == .offline{
            GPDock.showAlertWith(viewController: self, message: "This reservation was done directly with the marina. Please contact the marina directly to request a refund.", title: warningMessage.alertTitle.rawValue)
            return
        }else{
            let cancellationAlert = AppStoryboard.Booking.viewController(CancellationAlertViewController.self)
            cancellationAlert.booking = self.booking
            cancellationAlert.delegate = self
            cancellationAlert.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            CommonClass.makeViewCircularWithCornerRadius(cancellationAlert.containnerView, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
            let nav = UINavigationController(rootViewController: cancellationAlert)
            nav.navigationBar.barTintColor = kNavigationColor
            nav.navigationBar.isTranslucent = false
            nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nav.navigationBar.isHidden = true
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
    }

    func showPriceBreakUpsAlert() -> Void {
        let priceBreakupsVC = AppStoryboard.Booking.viewController(PriceBreakupViewController.self)
        priceBreakupsVC.booking = self.booking
        let nav = UINavigationController(rootViewController: priceBreakupsVC)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM/dd/YY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension UpcomingBookingDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if booking.offer.mainPrice == 0.0 {
            return  6
        }else{
        return 7
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingIDRepresentingCell", for: indexPath) as! BookingIDRepresentingCell
            cell.bookingIDLabel.text = self.booking.parkingSpace.title
            cell.statusLabel.setTitle(booking.bookingStatus.rawValue.capitalized, for: UIControlState())
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaRepresentingCell", for: indexPath) as! MarinaRepresentingCell
            cell.marinaTitleLabel.text = booking.marina.title.capitalized
            cell.marinaAddressLabel.text = "\(booking.marina.address), \(booking.marina.city.name), \(booking.marina.city.state)"
            cell.directionButton.addTarget(self, action: #selector(onClickGetDirectionButton(_:)), for: .touchUpInside)
            cell.marinaNameLabelButton.addTarget(self, action: #selector(onClickMarinaNameLabel(_:)), for: .touchUpInside)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QRCodeCell", for: indexPath) as! QRCodeCell
            cell.qrcodeImageView.setIndicatorStyle(.gray)
            cell.qrcodeImageView.setShowActivityIndicator(true)
            cell.qrcodeImageView.sd_setImage(with: URL(string:booking.QRImage))
            cell.slipNumberLabel.text = booking.ID
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = booking.marina.checkIn
            cell.checkOutTime.text = booking.marina.checkOut
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.lengthLabel.text = "\(booking.boat.boatSize.length) ft."
            cell.widthLabel.text = "\(booking.boat.boatSize.width) ft."
            cell.depthLabel.text = "\(booking.boat.boatSize.depth) ft."
            return cell
        case 5:
            if booking.offer.mainPrice == 0.0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
                cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
                cell.otherTitleLabel.text = (self.booking.bookingType == .online) ? "Amount Paid" : "Booking Mode"
                cell.otherDetailsLabel.text = (self.booking.bookingType == .online) ? "$ "+String(format: "%0.2lf", booking.amount) : "Offline"
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountOfferTableViewCell", for: indexPath) as! DiscountOfferTableViewCell
                cell.discountImage.image = #imageLiteral(resourceName: "offer")
                cell.discountOfferLabel.text = String(format: "%0.0f", booking.offer.offerPercentage) + " %"
                let offerPrice = booking.offer.mainPrice - booking.offer.offerPrice
                cell.discountOfferAmount.text = "$ "+String(format: "%0.2lf", offerPrice)
                cell.mainPriceAmount.text = "$ "+String(format: "%0.2lf", booking.offer.mainPrice)
                return cell
            }

        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
            cell.otherTitleLabel.text = (self.booking.bookingType == .online) ? "Amount Paid" : "Booking Mode"
            cell.otherDetailsLabel.text = (self.booking.bookingType == .online) ? "$ "+String(format: "%0.2lf", booking.amount) : "Offline"
            return cell
//        case 6:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
//            cell.otherIcon.image = #imageLiteral(resourceName: "offers_line")
//            cell.otherTitleLabel.text = "Discount Offer"//(self.booking.bookingType == .online) ? "Amount Paid" : "Booking Mode"
//            cell.otherDetailsLabel.text = String(format: "%0.0f", booking.offer.offerPercentage) + " %"
//            return cell

        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://bookingid
            height = 89
        case 1: //MarinaRepresentingCell
            height = 89
        case 2: //QRCodeCell
            height = 110
        case 3: //CheckInCheckOutCell
            height = 89
        case 4: //BoatRepresentingCell
            height = 111
        case 5: //OtherCell
            if booking.offer.mainPrice == 0.0 {
                height = 78
            }else{
              height = 94
            }
           
        case 6: //offerCell
            height = 78
        default:
            break
        }
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            return
            //self.showPriceBreakUpsAlert()
        }else if indexPath.row == 1{
            return
           // self.showLocationOfGoogleMap(marina: self.booking.marina)
        }
    }

}









