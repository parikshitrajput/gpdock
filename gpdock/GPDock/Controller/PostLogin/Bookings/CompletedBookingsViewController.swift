//
//  CompletedBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CompletedBookingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var bookings = Array<Booking>()
    @IBOutlet weak var bookingsTableView : UITableView!

    let noBookingMessage = "Oh no!\r\n\r\nWe don's see any completed bookings! Follow this link to embark on your next GPDock adventure!"
    
    var noBookingView: NoBookingView!
    
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var selectedIndex = -1

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CompletedBookingsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil)

        self.bookingsTableView.register(UINib(nibName: "BookingCell", bundle: nil), forCellReuseIdentifier: "BookingCell")


        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookingsTableView.addSubview(self.refreshControl)
        self.bookingsTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 59, right: 0)

        self.loadBookingFor(self.user.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        // self.bookingsTableView.reloadData()
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
    }

    func setupNoBookingFoundView(){
        if self.noBookingView != nil{
            self.noBookingView.removeFromSuperview()
        }
        self.addNoBookingView()
    }
    @IBAction func onClickBookNowButton(_ sender: UIButton){
        let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
        homeTab.selectedIndexFromMenu = 0
        homeTab.isFromMenu = true
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
        })
    }

    @objc func bookingDidCancelledByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let addingBooking = userInfo["booking"]{
                addingBooking.isCancelled = true
                self.bookings.insert(addingBooking, at: 0)
                self.bookingsTableView.reloadData()
            }
        }
    }

    func addNoBookingView(){
        self.noBookingView = NoBookingView.instanceFromNib()
        self.noBookingView.messageLabel.text = noBookingMessage
        self.view.addSubview(noBookingView)

        noBookingView.layer.zPosition = CGFloat(Int.max)
        let topContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .top, relatedBy: .equal, toItem: self.view,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)

        let bottomContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .bottom, relatedBy: .equal, toItem: self.view,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)

        let leftContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .leading, relatedBy: .equal, toItem: self.view,
                      attribute: .leading, multiplier: 1.0,
                      constant: 0)

        let rightContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .trailing, relatedBy: .equal, toItem: self.view,
                       attribute: .trailing, multiplier: 1.0,
                       constant: 0)

        noBookingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([topContraints,rightContraints,leftContraints,bottomContraints])
        noBookingView.bookNowButton.addTarget(self, action: #selector(onClickBookNowButton(_:)), for: .touchUpInside)
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
////        if self.bookingsTableView != nil{
////            self.bookingsTableView.reloadData()
////        }
//    }
//

    func loadBookingFor(_ userID:String,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }

        self.isNewDataLoading = true

        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }

        BookingService.sharedInstance.getBookingsForUser(userID, status: status,page:pageNumber,perPage: recordPerPage) { (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    self.pageNumber = self.pageNumber - 1
                }
                self.bookings.append(contentsOf: newJobArray)
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            self.bookingsTableView.reloadData()
            if self.bookings.count == 0{
                self.setupNoBookingFoundView()
            }
        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.bookingsTableView.reloadData()
        BookingService.sharedInstance.getBookingsForUser(self.user.ID, status: "completed",page: self.pageNumber,perPage: self.recordsPerPage) { (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
            }
            self.bookingsTableView.reloadData()
            if self.bookings.count == 0{
                self.setupNoBookingFoundView()
            }
        }
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100//(indexPath.row == selectedIndex) ? 138 : 82
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingCell", for: indexPath) as! BookingCell
        let booking = bookings[indexPath.row] //BookingCell
        cell.marinaImageView.setShowActivityIndicator(true)
        cell.marinaImageView.setIndicatorStyle(.gray)
        cell.marinaImageView.sd_setImage(with: URL(string:booking.marina.image.url),placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
        cell.cancelledStamp.isHidden = !booking.isCancelled
        cell.marinaTitle.text = booking.marina.title
        cell.marinaAddress.text = booking.marina.address+", "+booking.marina.city.name+", "+booking.marina.city.state
        cell.bookingFromDate.text = CommonClass.dateFromBookingDateString(booking.fromDate)
        cell.bookinToDate.text = CommonClass.dateFromBookingDateString(booking.toDate)
        return cell
        }
    }

    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        self.navigateToBookingDetails(booking: booking)
    }

    func navigateToBookingDetails(booking: Booking) -> Void {
        if booking.isCancelled{
            let bookingDetailVC = AppStoryboard.Booking.viewController(CancelledBookingDetailViewController.self)
            bookingDetailVC.booking = booking
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }else{
            let bookingDetailVC = AppStoryboard.Booking.viewController(CompletedBookingDetailViewController.self)
            bookingDetailVC.booking = booking
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }
    }



    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == bookingsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        self.loadBookingFor(self.user.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }
        
    }
    
    
}
