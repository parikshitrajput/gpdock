//
//  CancellationAlertViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
protocol CancellationAlertViewControllerDelegate {
    func cancellationAlertViewController(viewController: CancellationAlertViewController,bookingDidCancel success: Bool)
}

class CancellationAlertViewController: UIViewController {
    var refundable : RefundableDetails!
    var booking: Booking!
    var delegate: CancellationAlertViewControllerDelegate?
    @IBOutlet weak var cancellationPercentageLabel : UILabel!
    @IBOutlet weak var cancellationChargeLabel : UILabel!
    @IBOutlet weak var amountToBeRefundLabel : UILabel!
    @IBOutlet weak var chargeApplyAfterHoursLabel : UILabel!
    @IBOutlet weak var hoursPassedTillNowLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var closeButton : UIButton!
    var user : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        self.getRefundableDetails(booking: self.booking)
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.headerView.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        self.closeButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getRefundableDetails(booking: Booking) {
        CommonClass.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.checkAndGetBookingRefundableDetails(booking.ID) { (resRefundableDetails) in
            CommonClass.hideLoader()
            if let refDetails = resRefundableDetails{
                self.refundable = refDetails
                self.setupBookingCancellationPolicyData(reufundaleDetails: self.refundable)
            }else{
                self.setupBookingCancellationPolicyData(reufundaleDetails: RefundableDetails())
            }
        }
    }

    func setupBookingCancellationPolicyData(reufundaleDetails: RefundableDetails) {
        self.cancellationPercentageLabel.text = String(format: "%0.2lf", reufundaleDetails.cancellationPercentage)+"%"
        self.cancellationChargeLabel.text = String(format: "$ %0.2lf", reufundaleDetails.cancellationCharge/100.0)
        self.amountToBeRefundLabel.text = String(format: "$ %0.2lf", reufundaleDetails.amountToBeRefund/100.0)
        self.chargeApplyAfterHoursLabel.text = String(format: "%0.2lf Hrs", reufundaleDetails.chargeApplyAfterHours)
        self.hoursPassedTillNowLabel.text = String(format: "%0.2lf Hrs", reufundaleDetails.hoursPassedTillNow)
    }


    @IBAction func onClickCloseButton(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickCancelNow(_ sender: UIButton){
        sender.isEnabled = false
        self.cancelBooking(booking, completionBlock: { (done) in
            sender.isEnabled = true
            if done{
                self.dismiss(animated:false, completion: nil)
                self.delegate?.cancellationAlertViewController(viewController: self, bookingDidCancel: true)
            }else{
                self.delegate?.cancellationAlertViewController(viewController: self, bookingDidCancel: false)
            }
        })
    }


    func cancelBooking(_ booking:Booking,completionBlock:@escaping (_ done :Bool)->Void) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            completionBlock(false)
            return
        }
        CommonClass.showLoader(withStatus: "Cancelling..")
        BookingService.sharedInstance.cancelBookings(with: booking.ID,userID: self.user.ID) { (success,booking,message) in
            CommonClass.hideLoader()
            if success{
                if let cancelledBooking = booking{
                    NotificationCenter.default.post(name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil, userInfo: ["booking":cancelledBooking])
                }
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            completionBlock(success)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
