//
//  RateAndReviewBookingViewController.swift
//  GPDock
//
//  Created by TecOrb on 29/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView

class RateAndReviewBookingViewController: UIViewController {
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var bottomConstriant: NSLayoutConstraint!

    @IBOutlet weak var submitButton: UIButton!
    var titleView : NavigationTitleView!

    var review: Review!
    var booking:Booking = Booking()
    var user : User!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo()!)
        self.submitButton.isHidden = (self.review.rating == 0.0) ? false : true
        self.bottomConstriant.constant = (self.review.rating == 0.0) ? self.submitButton.frame.size.height : 0

        self.reviewTableView.dataSource = self
        self.reviewTableView.delegate = self
        self.reviewTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraints()
        self.setupNavigationTitle()
    }

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = (self.review.rating == 0.0) ? "Rate us" : "You rated"
        self.navigationItem.titleView = self.titleView
    }
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
        let validationResult = self.validateParams(rating: self.review.rating, review: review.reviewDescription)
        if !validationResult.result{
            showAlertWith(viewController: self, message: validationResult.message, title: warningMessage.alertTitle.rawValue)
            return
        }
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        self.submitReview(rating: review.rating, review: review.reviewDescription)
    }
    override func viewDidLayoutSubviews() {
        self.submitButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func submitReview(rating: Double,review:String){
        CommonClass.showLoader(withStatus: "Sending..")
        BookingService.sharedInstance.addReviewForBookingByUser(self.user.ID, marinaID: self.booking.marina.ID, bookingID: self.booking.ID, rating: self.review.rating, title: self.user.firstName+" "+self.user.lastName, reviewDesc: self.review.reviewDescription) { (result, resReview, message) in
            if result{
                if let postedReview = resReview{
                    self.review = postedReview
                    self.booking.review = self.review
                    self.navigationController?.pop(true)
                    showAlertWith(viewController: nil, message: message, title: "Thank you")
                }
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func validateParams(rating:Double,review:String) -> (result:Bool,message:String) {
        if rating < 1.0{
            return (false,"Minimum rating should not less than 1.0")
        }
        if review.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return (false,"Enter your review")
        }
        return (true,"")
    }
}

extension RateAndReviewBookingViewController: NKFloatRatingViewDelegate,UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        self.review.reviewDescription = textView.text
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.review.reviewDescription = textView.text
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.review.reviewDescription = textView.text
    }
    func floatRatingView(ratingView: NKFloatRatingView, didUpdate rating: Float) {
        self.review.rating = Double(rating)
    }
}


extension RateAndReviewBookingViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingStarCell", for: indexPath) as! RatingStarCell
            cell.ratingView.rating = Float(self.review.rating)
            cell.rateUsLabel.text = (self.review.rating == 0.0) ? "Rate us" : "You rated:"
            cell.ratingView.isUserInteractionEnabled = (self.review.rating == 0.0)
            cell.ratingView.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
            cell.inputTextView.text = self.review.reviewDescription
            cell.inputTextView.delegate = self
            cell.inputTextView.isUserInteractionEnabled = (self.review.rating == 0.0)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 114 : (tableView.frame.size.height-126)
    }

}

class RatingStarCell: UITableViewCell {
    @IBOutlet weak var ratingView : NKFloatRatingView!
    @IBOutlet weak var rateUsLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}





