//
//  MyBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//cash current
//card completed

import UIKit

class MyBookingsViewController: UIViewController,LoginViewDelegate{
    var loginView: LoginView!
    var noBookingView: NoBookingView!

    var titleView : NavigationTitleView!
    var containnerVC : YSLContainerViewController?
    var user: User!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo()!)
        NotificationCenter.default.addObserver(self, selector: #selector(MyBookingsViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MyBookingsViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)

        self.setupNavigationTitle()
        self.setupViews()
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "My Bookings"
        self.navigationItem.titleView = self.titleView
    }
    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        GPDock.openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }
    @objc func userDidLoggedInHandler(_ notification: Notification) {
        self.setupViews()
    }

    func setupViews(){
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.user = User(json: User.loadUserInfo()!)
            self.setupBookingContainer()
        }else{
            self.removeBookingContainer()
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupLoginView("To view your Bookings\r\nPlease Login or Sign up")
        }
    }



    func setupBookingContainer(){
        CommonClass.showLoader(withStatus: "Please wait..")
        self.user.isUserHaveBooking { (apiResult, result, message) in
            CommonClass.hideLoader()
            if result{
                self.setUPContainer()
            }else{
                self.setupNoBookingFoundView()
            }
        }
    }

    func removeBookingContainer(){
        self.containnerVC?.view.removeFromSuperview()
        self.containnerVC = nil
    }

    func setupNoBookingFoundView(){
        if self.noBookingView != nil{
            self.noBookingView.removeFromSuperview()
        }
        self.addNoBookingView()
    }

    func setUPContainer()  {
        let comingUpBookingVC = AppStoryboard.Booking.viewController(CurrentBookingsViewController.self)
        comingUpBookingVC.title = "Coming Up"

        let completedBookingVC = AppStoryboard.Booking.viewController(CompletedBookingsViewController.self)
        completedBookingVC.title = "Completed"

//        let cancelledBookingVC = AppStoryboard.Booking.viewController(CancelledBookingsViewController.self)
//        cancelledBookingVC.title = "Cancelled"
        let menuItemColor = UIColor.gray
        let menuItemSelectedColor = UIColor.black
        let menuBGColor = UIColor.white

        let statusHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
        let navigationHeight:CGFloat = self.navigationController?.navigationBar.frame.size.height ?? 55
        self.containnerVC = YSLContainerViewController(controllers: [comingUpBookingVC,completedBookingVC], topBarHeight: 0,subtractableHeight:(statusHeight + navigationHeight), parentViewController: self)

        //Customize the containner menu
        self.containnerVC?.menuBackGroudColor = menuBGColor
        self.containnerVC?.menuItemSelectedTitleColor = menuItemSelectedColor
        self.containnerVC?.menuItemTitleColor = menuItemColor
        self.containnerVC?.menuItemFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)

        self.containnerVC?.delegate = self
        self.view.addSubview((containnerVC?.view)!)
        self.containnerVC?.view.setNeedsLayout()
        self.containnerVC?.view.layoutIfNeeded()
    }
    override func viewDidLayoutSubviews() {
        self.containnerVC?.view.setNeedsLayout()
        self.containnerVC?.view.layoutIfNeeded()
    }


    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMenuButton(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    @IBAction func onClickBookNowButton(_ sender: UIButton){
            let homeTab = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
            homeTab.selectedIndexFromMenu = 0
            homeTab.isFromMenu = true
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeTab, withSlideOutAnimation: false, andCompletion: {
            })
    }


    func addNoBookingView(){
         self.noBookingView = NoBookingView.instanceFromNib()
        self.view.addSubview(noBookingView)
        noBookingView.layer.zPosition = CGFloat(Int.max)
        let topContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .top, relatedBy: .equal, toItem: self.view,
                  attribute: NSLayoutAttribute.top, multiplier: 1.0,
                  constant: 0)

        let bottomContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .bottom, relatedBy: .equal, toItem: self.view,
                     attribute: NSLayoutAttribute.bottom, multiplier: 1.0,
                     constant: 0)

        let leftContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .leading, relatedBy: .equal, toItem: self.view,
                            attribute: .leading, multiplier: 1.0,
                            constant: 0)

        let rightContraints = NSLayoutConstraint(item: noBookingView, attribute:
            .trailing, relatedBy: .equal, toItem: self.view,
                             attribute: .trailing, multiplier: 1.0,
                             constant: 0)

        noBookingView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([topContraints,rightContraints,leftContraints,bottomContraints])
        noBookingView.bookNowButton.addTarget(self, action: #selector(onClickBookNowButton(_:)), for: .touchUpInside)
    }
    
}

extension MyBookingsViewController: YSLContainerViewControllerDelegate{
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {

    }
    
}

extension MyBookingsViewController: SlideNavigationControllerDelegate{
   
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return false
    }
}

