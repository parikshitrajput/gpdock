//
//  MobileOTPVerifyViewController.swift
//  GPDock
//
//  Created by Parikshit on 20/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase

//protocol MobileOTPVerifyViewControllerDelegate {
//    func mobileOTPVerifyViewController(viewController: MobileOTPVerifyViewController, requestBook:Bool)
//}
class MobileOTPVerifyViewController: UIViewController {
    
    @IBOutlet weak var otpNumberVerification : VPMOTPView!
    @IBOutlet weak var mobileNumberLabel : UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var submmitButton : UIButton!
    @IBOutlet weak var resendButton : UIButton!
    @IBOutlet weak var anotherPhoneNumberButton: UIButton!
    var mobileVerificationContact: String = ""
    var mobileVerificationCountryCode: String = ""
    var checkRequestBooking: Bool = false
    var discountPrice: Double?

    var enterOTP: String = ""
      var user : User!
    var cards = Array<Card>()
    var amountsArray : Array<Double>?//(category:String,amount:Double)>?
    var amount : Double!
    var marina: Marina!
    var paymentNote : String?
    var bookingDates: BookingDates!
    var selectedSize : BoatSize!
    var boat : ParkingSpace!
    var userOwnBoat : Boat?
    var payableTitle : String?
    var isPayingForMarina = false
    var count = 60
    var navigationTitleView : NavigationTitleView!
    var selectedOffer : OfferModal?

    override func viewDidLoad() {
        super.viewDidLoad()
       // print("Contact\(mobileVerificationContact)")
        //print("Countrycode\(mobileVerificationCountryCode)")
        resendButton.isSelected = false
        self.user = User(json: User.loadUserInfo()!)
        otpNumberVerification.otpFieldInputType  = .numeric
        otpNumberVerification.otpFieldsCount = 6
        otpNumberVerification.otpFieldSize = otpNumberVerification.frame.size.width / 7
        otpNumberVerification.otpFieldDisplayType = .square
        //otpNumberVerification.otpFieldSeparatorSpace = 10
        otpNumberVerification.otpFieldDefaultBorderColor = UIColor.black
        otpNumberVerification.otpFieldEnteredBorderColor = UIColor.black
        otpNumberVerification.otpFieldBorderWidth = 1
        otpNumberVerification.otpFieldDefaultBackgroundColor = UIColor.white
        otpNumberVerification.otpFieldEnteredBackgroundColor = UIColor.white
        otpNumberVerification.otpFieldSeparatorSpace = 2
        otpNumberVerification.delegate = self
        otpNumberVerification.initalizeUI()
        otpNumberVerification.becomeFirstResponder()
        self.checkContactAndCountryCode()
        self.setUpNavigationView()
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.submmitButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        
    }
    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = "Number Verification"
        self.navigationItem.titleView = self.navigationTitleView
    }
    
    @objc func update() {
        if(count >= 0) {
            timerLabel.text = String(count)
            count -= 1
        }else{
            resendButton.setTitleColor(UIColor.blue, for: .normal)
            resendButton.addTarget(self, action: #selector(onClickResendButton(_:)), for: .touchUpInside)
        }
    }
   func checkContactAndCountryCode(){
    if (user.countryCode != "") && (user.contact != "") && (mobileVerificationContact == "") && (mobileVerificationCountryCode == "") {
        self.verifyWithFirebase(contactNumber: user.contact, countryCode: user.countryCode)
        self.mobileNumberLabel.text = user.contact
    }else{
        self.mobileNumberLabel.text = mobileVerificationContact
    }
    
    //verifyWithFirebase(phonenumber: self.fullContact)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func verifyWithFirebase(contactNumber : String, countryCode: String){
        let fullContact = countryCode + contactNumber
       // print(" mobile Number\(fullContact)")
        PhoneAuthProvider.provider().verifyPhoneNumber(fullContact, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                showAlertWith(viewController: self, message: (error.localizedDescription), title: warningMessage.alertTitle.rawValue)
//                print("error: \(error.localizedDescription)")
                return
            }
            kUserDefaults.set(verificationID, forKey: "authVerificationID")
            kUserDefaults.synchronize()
            if !self.resendButton.isSelected {
            self.mobileVerificationContact = self.user.contact
            self.mobileVerificationCountryCode = self.user.countryCode
            }
            
        }
    }
    
    
    func receiveOTP() {
        print("the new otp:\(enterOTP)")
        CommonClass.showLoader(withStatus: "Verify..")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: kUserDefaults.string(forKey: "authVerificationID")!,
            verificationCode: enterOTP)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            CommonClass.hideLoader()
            if error != nil {
                // ...
                showAlertWith(viewController: self, message: (error?.localizedDescription)!, title: warningMessage.alertTitle.rawValue)
                //print("error:\(String(describing: error?.localizedDescription))")
                return
            }
            else{

                self.updateMobileStatus(self.user.ID, contact: self.mobileVerificationContact, countryCode:self.mobileVerificationCountryCode, contactVerified: "true")
                //self.navigationController?.popViewController(animated: true)
            print("Login Sucessfully")

            }
        }
    }
    
    func updateMobileStatus(_ userID: String,contact:String,countryCode:String,contactVerified:String) {
        BookingService.sharedInstance.verifiedUserContact(user.ID, contact: contact, country_code: countryCode, verifiedStatus: contactVerified){  (success, updatedUser, message) in
            if success {
                CommonClass.hideLoader()
                if self.checkRequestBooking {
                    if (self.user.countryCode != "") && (self.user.contact != "") && (self.mobileVerificationContact == "") && (self.mobileVerificationCountryCode == "") {
                        self.navigationController?.pop(true)
                        self.user.saveUserInfo(updatedUser!)
                    }else{
                        let desiredViewController = self.navigationController!.viewControllers.filter { $0 is RequestBookingViewController }.first!
                        if let dVC = desiredViewController as? RequestBookingViewController{
                            self.navigationController!.popToViewController(dVC, animated: true)
                        }
                        }
                    //let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    //self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                    
                }else{
                 self.navigateToPayment()
                }
//                for controller in self.navigationController!.viewControllers as Array {
//                    if controller.isKind(of: LoginViewController.self) {
//                        self.navigationController!.popToViewController(controller, animated: true)
//                        break
//                    }
//                }

            }else{
                showAlertWith(viewController: self, message: "Number is not verified", title: "Error")
                
            }
        }
    }
    
    func navigateToPayment() {
        let paymentVC = AppStoryboard.Home.viewController(PaymentOptionViewController.self)
        paymentVC.amount = self.amount
        paymentVC.isPayingForMarina = false
        paymentVC.marina = self.marina
        paymentVC.bookingDates = self.bookingDates
        paymentVC.boat = self.boat
        paymentVC.selectedSize = self.selectedSize
        paymentVC.userOwnBoat = self.userOwnBoat
        paymentVC.selectedOffer = self.selectedOffer
        paymentVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    @IBAction func onClickSubmmitButton(_ sender: UIButton){
        self.receiveOTP()
        
        }
    @IBAction func onClickResendButton(_ sender: UIButton) {
        resendButton.isSelected = true
        count = 60
        self.verifyWithFirebase(contactNumber: self.mobileVerificationContact, countryCode: self.mobileVerificationCountryCode)
        resendButton.setTitleColor(UIColor.black, for: .selected)
        
    }
    @IBAction func onClickAnotherPhoneButton(_ sender: UIButton) {
        if self.checkRequestBooking {
           self.requestBookingData()
        }else{
            paymentBookingData()
        }
    }
    
    func  requestBookingData() {
        let changeNumberVC = AppStoryboard.Booking.viewController(NumberVerificationViewController.self)
        changeNumberVC.checkRequestBooking = true
        self.navigationController?.pushViewController(changeNumberVC, animated: true)

    }
    func paymentBookingData() {
        let changeNumberVC = AppStoryboard.Booking.viewController(NumberVerificationViewController.self)
        changeNumberVC.checkRequestBooking = false
        changeNumberVC.amount = self.amount
        changeNumberVC.isPayingForMarina = false
        changeNumberVC.marina = self.marina
        changeNumberVC.bookingDates = self.bookingDates
        changeNumberVC.boat = self.boat
        changeNumberVC.selectedSize = self.selectedSize
        changeNumberVC.userOwnBoat = self.userOwnBoat
        changeNumberVC.selectedOffer = self.selectedOffer
        changeNumberVC.discountPrice = self.discountPrice
        self.navigationController?.pushViewController(changeNumberVC, animated: true)

        
    }
    
    @IBAction func onClickBackBarButton(_ sender: UIBarButtonItem){
        if self.checkRequestBooking {
            let desiredViewController = self.navigationController!.viewControllers.filter { $0 is RequestBookingViewController }.first!
            if let dVC = desiredViewController as? RequestBookingViewController{
                self.navigationController!.popToViewController(dVC, animated: true)
            }
        }else{
            let desiredViewController = self.navigationController!.viewControllers.filter { $0 is ReviewBookingDetailsViewController }.first!
            if let dVC = desiredViewController as? ReviewBookingDetailsViewController{
                self.navigationController!.popToViewController(dVC, animated: true)
            }
        }
        //self.navigationController?.pop(true)
        
    }
    

}
extension MobileOTPVerifyViewController: VPMOTPViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        print("Has entered all OTP? \(hasEntered)")
    }
    
    func enteredOTP(otpString: String) {
        self.enterOTP = otpString
        print("OTPString: \(otpString)")
    }
}
