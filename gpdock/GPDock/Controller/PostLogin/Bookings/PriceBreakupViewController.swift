//
//  PriceBreakupViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PriceBreakupViewController: UIViewController {
    @IBOutlet weak var aTableView: UITableView!
    var booking: Booking!
    var aMarinaPrices = MarinaPriceReview()
    var navigationTitleView : NavigationTitleView!
    var countForPriceDetails = 2


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationView()
        self.aTableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.getMarinaPrice()
    }

    func getMarinaPrice(){
        CommonClass.showLoader(withStatus: "Please wait..")
        self.booking.marina.getMarinaPriceByDate(fromDate: booking.fromDate, toDate: booking.toDate) { (resMarinaPrice) in
            CommonClass.hideLoader()
            if let _aMarinaPrice = resMarinaPrice{
                self.aMarinaPrices = _aMarinaPrice
                self.countForPriceDetails = self.rowsCountForPriceDetailsSection()
                self.aTableView.reloadData()
            }
        }
    }

    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = "Price Breakdowns"
        self.navigationItem.titleView = self.navigationTitleView
    }

    func totalBookingAmount() -> Double {
        var totalAmount = self.booking.amount
            let costForRegularDays = Double(aMarinaPrices.regularCost.totalDays)*aMarinaPrices.regularCost.price*Double(self.booking.boat.boatSize.length)
            let costForWeekEnd = Double(aMarinaPrices.weekEndCost.totalDays)*aMarinaPrices.weekEndCost.price*Double(self.booking.boat.boatSize.length)

            let costsForHolidays = aMarinaPrices.specialCosts.map({ (priceForDate) -> Double in
                return priceForDate.price*Double(self.booking.boat.boatSize.length)
            })
            let totalCostForHolidays = costsForHolidays.map({$0}).reduce(0, +)
            totalAmount = costForRegularDays + costForWeekEnd + totalCostForHolidays

        return totalAmount
    }

    func rowsCountForPriceDetailsSection() -> Int{
        var count = 2
        if aMarinaPrices.weekEndCost.totalDays != 0{
            count = 3
        }
        count = count + aMarinaPrices.specialCosts.count
        return count
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickCloseButton(_ sender: UIBarButtonItem){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension PriceBreakupViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
//        headerView.headerLabel.text = header[section].capitalized
//        headerView.headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
//        headerView.backgroundColor = UIColor(red: 240.0/255.0, green: 237.0/255.0, blue: 239.0/255.0, alpha: 1.0)
//        return headerView
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 55
//    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.rowsCountForPriceDetailsSection()
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewBookingDetailCell", for: indexPath) as! ReviewBookingDetailCell
        if indexPath.row == 0{
            cell.itemTitle.text = "Weekday Price"
            let str = "$\(aMarinaPrices.regularCost.price) * \(aMarinaPrices.regularCost.totalDays) * \(self.booking.boat.boatSize.length) = "
                let total = aMarinaPrices.regularCost.price*Double(aMarinaPrices.regularCost.totalDays)*Double(self.booking.boat.boatSize.length)
                cell.itemDetailText.text = str+"$ "+String(format: "%0.2f", total)
        }else if indexPath.row == (countForPriceDetails-1){
            cell.itemTitle.text = "Grand Total"
            cell.itemDetailText.text = "$ "+String(format: "%0.2f", self.totalBookingAmount())
        }else{
                if aMarinaPrices.weekEndCost.totalDays != 0{
                    if indexPath.row == 1{
                        cell.itemTitle.text = "Weekend Price"
                        let str = "$\(aMarinaPrices.weekEndCost.price) * \(aMarinaPrices.weekEndCost.totalDays) * \(self.booking.boat.boatSize.length) = "
                        let total = aMarinaPrices.weekEndCost.price*Double(aMarinaPrices.weekEndCost.totalDays)*Double(self.booking.boat.boatSize.length)
                        cell.itemDetailText.text = str+"$ "+String(format: "%0.2f", total)
                    }else{
                        cell.itemTitle.text = aMarinaPrices.specialCosts[indexPath.row-2].date
                        let total = aMarinaPrices.specialCosts[indexPath.row-2].price*Double(self.booking.boat.boatSize.length)
                        cell.itemDetailText.text = "$ "+String(format: "%0.2f * %d = $ %0.2f", aMarinaPrices.specialCosts[indexPath.row-2].price,self.booking.boat.boatSize.length,total)
                    }
                }else{
                    cell.itemTitle.text = aMarinaPrices.specialCosts[indexPath.row-2].date
                    let total = aMarinaPrices.specialCosts[indexPath.row-2].price*Double(self.booking.boat.boatSize.length)
                    cell.itemDetailText.text = "$ "+String(format: "%0.2f * %d = $ %0.2f", aMarinaPrices.specialCosts[indexPath.row-2].price,self.booking.boat.boatSize.length,total)
                }
        }

        return cell

    }
}
