//
//  NumberVerificationViewController.swift
//  GPDock
//
//  Created by Parikshit on 20/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase
import CountryPickerViewSwift

class NumberVerificationViewController: UIViewController {

    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!

    var checkRequestBooking: Bool = false
    var user : User!
    var cards = Array<Card>()
    var amountsArray : Array<Double>?//(category:String,amount:Double)>?
    var amount : Double!
    var marina: Marina!
    var paymentNote : String?
    var bookingDates: BookingDates!
    var selectedSize : BoatSize!
    var boat : ParkingSpace!
    var userOwnBoat : Boat?
    var payableTitle : String?
    var navigationTitleView : NavigationTitleView!
    var isPayingForMarina = false
    var discountPrice: Double?
    var selectedOffer : OfferModal?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo()!)
        self.countryCodeTextField.text = "+1"
        countryCodeTextField.delegate = self
        self.setUpNavigationView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.continueButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        
    }
    func setUpNavigationView() -> Void {
        navigationTitleView = NavigationTitleView.instanceFromNib()
        navigationTitleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        navigationTitleView.titleLabel.text = "Number Verification"
        self.navigationItem.titleView = self.navigationTitleView
    }

    func verifyWithFirebase(contactNumber : String, countryCode: String){
       let fullmobileContact = countryCode + contactNumber
        CommonClass.showLoader(withStatus: "Verify..")
    PhoneAuthProvider.provider().verifyPhoneNumber(fullmobileContact, uiDelegate: nil) { (verificationID, error) in
        CommonClass.hideLoader()
    if let error = error {
        //print("error: \(error.localizedDescription)")
        showAlertWith(viewController: self, message: (error.localizedDescription), title: warningMessage.alertTitle.rawValue)
    return
    }
     kUserDefaults.set(verificationID, forKey: "authVerificationID")
        kUserDefaults.synchronize()
     self.navigateToMobileOtp(contactNumber: contactNumber, countryCode: countryCode)

    // Sign in using the verificationID and the code sent to the user
    // ...
    }
    }
  func navigateToMobileOtp(contactNumber : String, countryCode: String) {
    if self.checkRequestBooking {
    let numVC = AppStoryboard.Booking.viewController(MobileOTPVerifyViewController.self)
        numVC.mobileVerificationContact = contactNumber
        numVC.mobileVerificationCountryCode = countryCode
        numVC.checkRequestBooking = true
        self.navigationController?.pushViewController(numVC, animated: true)

    }else{
    let numVC = AppStoryboard.Booking.viewController(MobileOTPVerifyViewController.self)
    numVC.mobileVerificationContact = contactNumber
    numVC.mobileVerificationCountryCode = countryCode
    numVC.amount = amount
    numVC.isPayingForMarina = false
    numVC.marina = self.marina
    numVC.bookingDates = self.bookingDates
    numVC.boat = self.boat
    numVC.selectedSize = self.selectedSize
    numVC.userOwnBoat = self.userOwnBoat
    numVC.discountPrice = self.discountPrice
    numVC.selectedOffer = self.selectedOffer
    numVC.discountPrice = self.discountPrice
    self.view.endEditing(true)
    self.navigationController?.pushViewController(numVC, animated: true)
    }
    }
    
     func showCountryPickerView() {
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            //self.countryNameLabel.text = "\(countryDic["en"] as! String)   \(countryDic["en"] as! String)"
            //self.countryImageView.image = countryDic["countryImage"] as? UIImage
            self.countryCodeTextField.text = "+\(countryDic["code"] as! NSNumber)"
        }
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackBarButton(_ sender: UIBarButtonItem){
        //self.navigationController?.pop(true)
        if self.checkRequestBooking {
            let desiredViewController = self.navigationController!.viewControllers.filter { $0 is RequestBookingViewController }.first!
            if let dVC = desiredViewController as? RequestBookingViewController{
                self.navigationController!.popToViewController(dVC, animated: true)
            }
        }else{
            let desiredViewController = self.navigationController!.viewControllers.filter { $0 is ReviewBookingDetailsViewController }.first!
            if let dVC = desiredViewController as? ReviewBookingDetailsViewController{
                self.navigationController!.popToViewController(dVC, animated: true)
            }
        }

        
    }
    @IBAction func onClickCountryCode(_ sender:UIButton){
        self.view.endEditing(true)
        self.showCountryPickerView()
    }

    
    @IBAction func onClickContinueButton(_ sender: UIBarButtonItem){
        guard let countryCode = self.countryCodeTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your country code", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let mobile = self.phoneNumberTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your mobile number", title: warningMessage.alertTitle.rawValue)
            return
        }


        //let phoneNumber = phoneNumberTextField.text!
        //let countryCode = countryCodeTextField.text!
        let validation = self.validateUserDetails(mobile, countryCode: countryCode)
        if !validation.result{
            showAlertWith(viewController: self, message: validation.message, title: warningMessage.alertTitle.rawValue)
            return
        }
        self.verifyWithFirebase(contactNumber: mobile, countryCode: countryCode)
//        if phoneNumber != "" && countryCode != "" {
//          self.verifyWithFirebase(contactNumber: phoneNumber, countryCode: countryCode)
//        }else{
//            showAlertWith(viewController: self, message: "Country code or Contact is not true", title: "Error", complitionBlock: nil
//            )
//        }
    }
    func validateUserDetails(_ mobile:String, countryCode: String) -> (result:Bool,message:String) {
        if countryCode.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (result:false,message:"Please enter your country code")
        }
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (result:false,message:"Please enter your mobile number")
        }

        
        if !CommonClass.validatePhoneNumber(mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)){
            return (result:false,message:"Please enter your mobile number in correct format (xxxxxxxxxx)")
        }
        
        return (result:true,message:"")
    }

}


extension NumberVerificationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextField {
          //showCountryPickerView()
            //countryCodeTextField.resignFirstResponder()
        }
    }
    
    
}
