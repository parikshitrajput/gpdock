//
//  TransactionDetailViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController {
    let headerTitles = ["Particulars","Description","Note"]
    let payingOptions = ["Groceries","Gas","Pump Out","Other"]
    let payingOptionIcons = ["grocerry","gass","pump","othersPay"]

    var transaction : ServiceTransaction!
    var categoryAmounts = Array<Double>()
    @IBOutlet weak var trTableView : UITableView!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Transaction Details".uppercased()
        self.navigationItem.titleView = self.titleView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.setupNavigationTitle()
        categoryAmounts = [self.transaction.payForGrocery,self.transaction.payForGasoline,self.transaction.payForPumpout,self.transaction.payForOthers]
        self.trTableView.dataSource = self
        self.trTableView.delegate = self
    }

    @IBAction func onclickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    func registerCells(){
        self.trTableView.register(UINib(nibName: "TransactionParticularsCell", bundle: nil), forCellReuseIdentifier: "TransactionParticularsCell")
        self.trTableView.register(UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderTableViewCell")
        self.trTableView.register(UINib(nibName: "MarinaPayingCategoryCell", bundle: nil), forCellReuseIdentifier: "MarinaPayingCategoryCell")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension TransactionDetailViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 1) ? 4 : 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = self.transactionParticularsCell(tableView, indexPath: indexPath)
            return cell
        }else if indexPath.section == 1{
            let cell = self.payingOptionCell(tableView, indexPath: indexPath)
            return cell
        }else{
            let cell = self.notesCell(tableView, indexPath: indexPath)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {return 112}
        else if indexPath.section == 1{return 44}
        else{return 119}
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
        headerView.headerLabel.text = "  "+self.headerTitles[section]
        headerView.headerLabel.font = fonts.Raleway.bold.font(.large)
        headerView.headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        headerView.backgroundColor = UIColor.groupTableViewBackground
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }

    func transactionParticularsCell(_ tableView: UITableView, indexPath:IndexPath)->TransactionParticularsCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionParticularsCell", for: indexPath) as! TransactionParticularsCell
        cell.amountLabel.text = "$ "+String(format:"%0.2f",transaction.amount)
        cell.trIDLabel.text = transaction.customerTransactionID
        cell.trDateLabel.text = CommonClass.formattedDateWithString(transaction.createdAt, format: "MM/dd/YYYY hh:mm a")
        return cell
    }

    func payingOptionCell(_ tableView: UITableView, indexPath:IndexPath)->MarinaPayingCategoryCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarinaPayingCategoryCell", for: indexPath) as! MarinaPayingCategoryCell
        cell.categoryIcon.image = UIImage(named:payingOptionIcons[indexPath.row])
        cell.categoryLabel.text = payingOptions[indexPath.row]
        cell.amountTextField.numberValue = NSDecimalNumber(value: self.categoryAmounts[indexPath.row])
        cell.amountTextField.isUserInteractionEnabled = false
        return cell
    }

    func notesCell(_ tableView: UITableView, indexPath:IndexPath)->TextViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewCell", for: indexPath) as! TextViewCell
        cell.inputTextView.placeholder = self.transaction.note as NSString
        cell.inputTextView.text = transaction.note
        cell.inputTextView.isUserInteractionEnabled = false
        return cell
    }

}
