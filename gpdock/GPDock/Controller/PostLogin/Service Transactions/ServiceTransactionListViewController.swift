//
//  ServiceTransactionListViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ServiceTransactionListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,LoginViewDelegate {
    var transactions = Array<ServiceTransaction>()

    var isFromMenu = true

    var titleView : NavigationTitleView!

    @IBOutlet weak var transactionsTableView : UITableView!
    var loginView : LoginView!
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ServiceTransactionListViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        return refreshControl
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if transactionsTableView != nil{
            transactionsTableView.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.transactionsTableView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionTableViewCell")
        self.transactionsTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.transactionsTableView.backgroundColor = UIColor.groupTableViewBackground
        self.transactionsTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceTransactionListViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceTransactionListViewController.userDidLoggedInHandler(_:)), name: .SESSION_EXPIRED_NOTIFICATION, object: nil)


        self.setupNavigationTitle()
        self.setupViews()
    }
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Service Transactions".uppercased()
        self.navigationItem.titleView = self.titleView
    }


    func loginViewDelegate(didTapOnLoginButton wantLogin: Bool) {
        GPDock.openLoginModule(in: self, shouldLogin: wantLogin)
    }

    func setupLoginView(_ message: String){
        self.loginView = self.addLoginView(message)
        self.loginView.delegate = self
    }

    @objc func userDidLoggedInHandler(_ notification: Notification) {

        self.setupViews()
    }



    func setupViews(){
        self.setupLeftBarButtons(isFromMenu: self.isFromMenu)
        if CommonClass.isLoggedIn{
            if self.loginView != nil{
                self.loginView.removeFromSuperview()
            }
            self.setupContainer()
        }else{
            self.setupLoginView("To view Service Transactions\r\nPlease Login or Sign up")
        }
    }


    func setupContainer(){
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON!)
        self.pageNumber = 1
        self.transactions.removeAll()
        self.transactionsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.transactionsTableView.dataSource = self
        self.transactionsTableView.delegate = self
        self.transactionsTableView.addSubview(self.refreshControl)
        self.loadTransactionsFor(self.user.ID,pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    func setupLeftBarButtons(isFromMenu:Bool) {
        let menuButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        menuButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        menuButton.setImage(isFromMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "back"), for: .normal)
        menuButton.addTarget(self, action: #selector(onclickBackButton(_:)), for: .touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        self.navigationItem.setLeftBarButtonItems([menuBarButton], animated: false)
    }

//    func setupRightBarButtons() {
//        self.navigationItem.setRightBarButtonItems(nil, animated: false)
//        if self.notifications.count == 0{return}
//        let deleteAllButton = UIButton(type: .system)
//        deleteAllButton.frame = CGRect(x:0, y:0, width:35, height:35)
//        deleteAllButton.tintColor = kApplicationRedColor
//        deleteAllButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
//        deleteAllButton.setImage(#imageLiteral(resourceName: "delete_icon"), for: .normal)
//        deleteAllButton.addTarget(self, action: #selector(onclickDeleteAllNotificationsButton(_:)), for: .touchUpInside)
//        let deleteBarButton = UIBarButtonItem(customView: deleteAllButton)
//        self.navigationItem.setRightBarButtonItems([deleteBarButton], animated: false)
//    }

    @IBAction func onclickDeleteAllNotificationsButton(_ sender : UIButton){
        self.askToClearNotification()
    }

    func askToClearNotification(){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Are you sure to clear all the notifications?", preferredStyle: .alert)
        let weakAlert = alert
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { [weakAlert](action) in
            self.clearAllNotifications()
            weakAlert.dismiss(animated: true, completion: nil)
        }

        let nopeAction = UIAlertAction(title: "Nope", style: .cancel){ [weakAlert](action) in
            weakAlert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesAction)
        alert.addAction(nopeAction)
        self.present(alert, animated: true, completion: nil)
    }

    func clearAllNotifications() {
        CommonClass.showLoader(withStatus: "Please wait..")
        self.user.clearAllNotifications { (done) in
            CommonClass.hideLoader()
            if done{
                self.pageNumber = 1
                self.transactions.removeAll()
                self.transactionsTableView.reloadData()
                CommonClass.showSuccess(withStatus: "Done")
            }
        }
    }



    @IBAction func onclickBackButton(_ sender : UIButton){
        if isFromMenu{
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }else{
            self.navigationController?.pop(true)
        }
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
    }


    func loadTransactionsFor(_ userID:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.isNewDataLoading = true
        if pageNumber == 1{
            self.transactions.removeAll()
            if transactionsTableView != nil{
                transactionsTableView.reloadData()
            }
        }

        TransactionService.sharedInstance.getTransactionsForUser(userID,page:pageNumber,perPage: recordPerPage) { (success,response,message) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            self.isNewDataLoading = false
            if let newNotificationArray = response{
                if newNotificationArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.transactions.append(contentsOf: newNotificationArray)
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if self.transactionsTableView != nil{
                self.transactionsTableView.reloadData()
            }
        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        CommonClass.sharedInstance.clearAllPendingRequests()
        pageNumber = 1
        self.transactions.removeAll()
        self.isNewDataLoading = true
        self.transactionsTableView.reloadData()
        TransactionService.sharedInstance.getTransactionsForUser(self.user.ID,page: self.pageNumber,perPage: self.recordsPerPage) { (success,response,message) in
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newNotificationsArray = response{
                self.transactions.append(contentsOf: newNotificationsArray)
            }
            if self.transactionsTableView != nil{
                self.transactionsTableView.reloadData()
            }
        }
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (transactions.count > 0) ? transactions.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if transactions.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No transaction found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
            let transaction = transactions[indexPath.row]
            cell.messageLabel.text = "Money Sent"
            cell.amountLabel.text = "$ "+String(format:"%0.2f",transaction.amount);
            cell.marinaTitle.text = transaction.marinaTitle
            cell.transactionDate.text = CommonClass.formattedDateWithString(transaction.createdAt, format: "MM/dd/YYYY hh:mm a")
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if transactions.count == 0{
            return
        }
        self.navigateToTransactionDetails(transaction: self.transactions[indexPath.row])
    }

    func navigateToTransactionDetails(transaction: ServiceTransaction) -> Void {
        let transactionDetailVC = AppStoryboard.Transactions.viewController(TransactionDetailViewController.self)
        transactionDetailVC.transaction = transaction
        self.navigationController?.pushViewController(transactionDetailVC, animated: true)
    }



    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == transactionsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        if self.transactions.count == 0{pageNumber=1}else{pageNumber+=1}
                        self.loadTransactionsFor(self.user.ID, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }

    }


}




