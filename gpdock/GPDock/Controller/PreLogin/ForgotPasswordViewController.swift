//
//  ForgotPasswordViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var sendButton : UIButton!
    var titleView : NavigationTitleView!

    var toolSendButton: UIButton!
    var toolBar: UIToolbar!
    func setupToolBarButton() {
        toolSendButton = UIButton(type: .custom)
        toolSendButton.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        toolSendButton.setTitleColor(.white, for: UIControlState())
        toolSendButton.setTitle("SEND", for: UIControlState())
        toolSendButton.titleLabel?.font = fonts.Raleway.bold.font(.xLarge)
        self.toolSendButton.addTarget(self, action: #selector(ForgotPasswordViewController.onClickOfSend(_:)), for: .touchUpInside)
        self.toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.isOpaque = true
        toolBar.tintColor = .white
        toolBar.barTintColor = .white
        toolBar.backgroundColor = .white
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        let doneButton = UIBarButtonItem(customView: self.toolSendButton)
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolSendButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        toolBar.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        self.emailTextField.inputAccessoryView = self.toolBar
    }

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Forgot Password"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.setupToolBarButton()
        self.emailTextField.delegate = self
    }
    override func viewDidLayoutSubviews() {
        self.sendButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickOfSend(_ sender:UIButton){
        self.processToOTP()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func processToOTP() {
        self.view.endEditing(true)
        let email = emailTextField.text!
        if email.isEmpty{
            showAlertWith(viewController: self, message: "Please enter your email", title: warningMessage.alertTitle.rawValue)
            return
        }
        if !CommonClass.validateEmail(email){
            showAlertWith(viewController: self, message: "Please enter a valid email", title: warningMessage.alertTitle.rawValue)
            return
        }
        LoginService.sharedInstance.forgotPasswordForEmail(email) { (success, message) in
            if success{
                self.goToOTPSceen(email)
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            self.processToOTP()
        }
        return false
    }

    func goToOTPSceen(_ email:String){
        let otpVC = AppStoryboard.Main.viewController(OTPVarificationViewController.self)
        otpVC.email = email
        self.navigationController?.pushViewController(otpVC, animated: true)
    }


    @IBAction func onClickOfBackButton(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }


}
