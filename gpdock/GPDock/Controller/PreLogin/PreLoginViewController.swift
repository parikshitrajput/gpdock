//
//  PreLoginViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//



import UIKit
//import EAIntroView


class PreLoginViewController: UIViewController {
    @IBOutlet weak var registerButton : UIButton!
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var introView : UIView!
    //var intro: EAIntroView!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  perform(#selector(showIntro), with: nil, afterDelay: 1)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickRegister(_ sender: UIButton){
        let registerVC = AppStoryboard.Main.viewController(RegisterViewController.self)
        self.navigationController?.pushViewController(registerVC, animated: true)
    }

    @IBAction func onClickLogin(_ sender: UIButton){
        let loginVC = AppStoryboard.Main.viewController(LoginViewController.self)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }

}
/*
extension PreLoginViewController:EAIntroDelegate{
    @objc func showIntro() {
        let sampleDescription1 = "Checkout availability of listed cars near the users location by knowing its active or inactive status"
        let sampleDescription2 = "A comprehensive Map and tracking options via Google map OR Map Box or any other SDK which helps to track the exact location of user"
        let sampleDescription3 = "If in case user is not able to take up the call, drivers can use the users’ alternative contact number to reach them immediately"
        let sampleDescription4 = "An absolute mechanism is crafted in your taxi booking app for estimating accurate taxi fare, distance, plate number and time taken in prior to the user’s ride"

        let page1 = EAIntroPage()
        page1.title = " ";
        page1.desc = sampleDescription1;

        let page2 = EAIntroPage()
        page2.title = " ";
        page2.desc = sampleDescription2;


        let page3 = EAIntroPage()
        page3.title = " ";
        page3.desc = sampleDescription3;

        let page4 = EAIntroPage()
        page4.title = " ";
        page4.desc = sampleDescription4;

        intro = EAIntroView(frame: self.introView.frame, andPages: [page1,page2,page3,page4])
        //intro?.pageControl.currentPageIndicatorTintColor = UIColor(patternImage: #imageLiteral(resourceName: "radio_icon_sel"))
        //intro?.pageControl.pageIndicatorTintColor = UIColor(patternImage: #imageLiteral(resourceName: "radio_icon"))

        intro?.swipeToExit = false
        intro?.skipButton.setTitle("", for: .normal)
        intro?.skipButton.removeTarget(nil, action: nil, for: .allEvents)
        intro?.delegate = self
        intro?.tapToNext = true
        intro?.show(in: self.view, animateDuration: 0.3)
    }

    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        
    }
}
*/
