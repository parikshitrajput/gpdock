//
//  ResetPasswordViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!

    var token: String!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Reset Password"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        guard let newPassword = newPasswordTextField.text else {
            showAlertWith(viewController: self, message: warningMessage.validPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if newPassword.count == 0{
            showAlertWith(viewController: self, message: warningMessage.enterPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if newPassword.count < 6{
            showAlertWith(viewController: self, message: warningMessage.validPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        guard let confirmPassword = confirmedPasswordTextField.text else {
            showAlertWith(viewController: self, message: warningMessage.confirmPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if confirmPassword.count == 0{
            showAlertWith(viewController: self, message: warningMessage.confirmPassword.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        if newPassword != confirmPassword{
            showAlertWith(viewController: self, message: warningMessage.passwordDidNotMatch.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        self.resetPassword(token: self.token, newPassword: newPassword)
    }

    func resetPassword(token:String,newPassword:String) {
        LoginService.sharedInstance.recoveryPassword(newPassword: newPassword, withToken: token) {(success, message) in
            if success{
                self.showSuccessAlert(message: message, title: warningMessage.alertTitle.rawValue)
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    func showSuccessAlert(message:String,title:String){

        self.confirmedPasswordTextField.text = ""
        self.newPasswordTextField.text = ""

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            self.navigationController?.popToRoot(true)
        }
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
//Please enter a valid password. Passwords should be 6 characters long.

    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if let cnfrmPassword = self.confirmedPasswordTextField.text{
            if let newpswrd = self.newPasswordTextField.text{
               // self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.lightGray : kApplicationRedColor
            }
        }
    }

}
