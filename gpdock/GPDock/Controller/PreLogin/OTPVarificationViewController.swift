//
//  OTPVarificationViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class OTPVarificationViewController: UIViewController {
    @IBOutlet weak var otpView: PinCodeTextField!
    @IBOutlet weak var donebutton: UIButton!
    var otpString = ""
    var email: String!
    var titleView : NavigationTitleView!



    
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Enter the code"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.otpView.keyboardType = .numberPad
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.otpView.becomeFirstResponder()
        }

    }

    override func viewDidLayoutSubviews() {
        self.donebutton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickDoneButton(_ sender: UIButton){
        if let varificationToken = otpView.text{
            if varificationToken.count == 6{
                self.varifyOTP(varificationToken, email: self.email)
            }else{
                showAlertWith(viewController: self, message: "Please enter 6 digit code", title: warningMessage.alertTitle.rawValue)
            }
        }else{
            showAlertWith(viewController: self, message: "Please enter 6 digit code", title: warningMessage.alertTitle.rawValue)
        }
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    func varifyOTP(_ otp: String,email: String){
        LoginService.sharedInstance.varifyOTPWithEmail(self.email, OTP: otp) { (validation, message) in
            if let otpValidation = validation{
                if otpValidation.isVarified{
                    let resetPasswordVC = AppStoryboard.Main.viewController(ResetPasswordViewController.self)
                    resetPasswordVC.token = otpValidation.token
                    self.navigationController?.pushViewController(resetPasswordVC, animated: true)
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension OTPVarificationViewController: VPMOTPViewDelegate {
//    func hasEnteredAllOTP(hasEntered: Bool) {
//        self.donebutton.isEnabled = hasEntered
//    }
//
//    func enteredOTP(otpString: String) {
//        self.otpString = otpString
//    }
//}

