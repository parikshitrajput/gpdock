//
//  LoginViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//


/*
 All the login logic is here
 */

import UIKit
import Google
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!

    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var fbButton : UIButton!
    @IBOutlet weak var googleButton : UIButton!
    @IBOutlet weak var createAccountButton : UIButton!
    @IBOutlet weak var forgotPasswordButton : UIButton!
    @IBOutlet weak var privacyPoliciesButton : UIButton!
    @IBOutlet weak var termsAndConditionsButton : UIButton!
     //var appVersion = ""
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Login"
        self.navigationItem.titleView = self.titleView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        emailTextField.text = DEBUG ? "nakulsharma.1296@gmail.com" : ""
        passwordTextField.text = DEBUG ? "123456" : ""

        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().clientID = kGoogleClientId
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")

        // Do any additional setup after loading the view.

        let login = FBSDKLoginManager()
        login.logOut()
        GIDSignIn.sharedInstance().signOut()
    }

    
    
    override func viewWillLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.googleButton, borderColor: UIColor.gray, borderWidth: 1, cornerRadius:1)
        CommonClass.makeViewCircularWithCornerRadius(self.fbButton, borderColor: UIColor.gray, borderWidth: 1, cornerRadius:1)
        self.loginButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickOfLoginButton(_ sender:UIButton){
        self.view.endEditing(true)
        guard let user = self.emailTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {
            showAlertWith(viewController: self, message: "Please enter your email", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let pass = self.passwordTextField.text else{
            showAlertWith(viewController: self, message: "Please enter your password", title: warningMessage.alertTitle.rawValue)
            return
        }

        let validationResult = self.validateParams(user, password: pass)
        if !validationResult.success{
            showAlertWith(viewController: self, message: validationResult.message, title: warningMessage.alertTitle.rawValue)
            return
        }
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        self.loginWith(user, password: pass)
    }

    @IBAction func onClickOfBackButton(_ sender:UIBarButtonItem){
        self.view.endEditing(true)
        if self.isModal(){
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.pop(true)
        }
    }

    @IBAction func onClickOfCreateAccount(_ sender:UIButton){
        self.view.endEditing(true)
        var viewControllers = self.navigationController!.viewControllers
        for i in 0..<viewControllers.count{
            if viewControllers[i] is LoginViewController{
                let registerVC = AppStoryboard.Main.viewController(RegisterViewController.self)
                viewControllers.remove(at: i)
                viewControllers.insert(registerVC, at: i)
                self.navigationController?.setViewControllers(viewControllers, animated: true)
                break
            }
        }
    }
    
    @IBAction func onClickOfFacebookButton(_ sender:UIButton){
        self.view.endEditing(true)

        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)

            return
        }
        self.loginWithFacebook()
    }

    @IBAction func onClickOfGoogleButton(_ sender:UIButton){
        self.view.endEditing(true)

        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        GIDSignIn.sharedInstance().signIn()

    }


    @IBAction func onClickOfForgotPassword(_ sender:UIButton){
        let forgotPasswordVC = AppStoryboard.Main.viewController(ForgotPasswordViewController.self)
        self.view.endEditing(true)
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }
    @IBAction func onClickOfTermsAndConditions(_ sender:UIButton){
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = false
        termsVc.isFromMenu = false
        self.view.endEditing(true)

        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickOfPrivacyPolicies(_ sender:UIButton){
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = true
        termsVc.isFromMenu = false
        self.view.endEditing(true)
        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }


    func loginWith(_ userName:String,password:String){
        CommonClass.showLoader(withStatus: "Authenticating..")
        LoginService.sharedInstance.loginWith(userName, password: password) { (success,responseUser,message)  in

            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            if success{
                if let user = responseUser, user.ID != "" {
                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                    NotificationCenter.default.post(name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil, userInfo: ["user":user])
                    CommonClass.sharedInstance.reRegisterForFirebase()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }

            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func validateParams(_ userName:String,password:String) -> (success:Bool,message:String) {
        if userName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your email")
        }

        if !CommonClass.validateEmail(userName){
            return (success:false,message:"Please enter a valid email")
        }

        if (password.count == 0){
            return (success:false,message:"Please enter your password")
        }

        if password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter a valid password")
        }

        if !CommonClass.validatePassword(password){
            return (success:false,message:"Please enter a valid password")
        }

        return (success:true,message:"")
    }

    func loginWithSocialMedia(email:String,socialID:String,name:String,imageUrl:String,providerType:SocialLoginType){
        CommonClass.showLoader(withStatus: "Authenticating..")
        LoginService.sharedInstance.loginWithSocialAuth(email, socialID: socialID, name: name, providerType: providerType,imageUrl: imageUrl) { (success,responseUser,message)  in

            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            if success{
                if let user = responseUser,user.ID != "" {
                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                    NotificationCenter.default.post(name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil, userInfo: ["user":user])
                    CommonClass.sharedInstance.reRegisterForFirebase()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }

            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    
    func socialLogin(_ email:String,name:String,socialID:String,imageUrl:String,socialLoginType:SocialLoginType){
        //guard let appToken = kUserDefaults.value(forKey: kAppToken) as? String else{return}
        self.loginWithSocialMedia(email: email, socialID: socialID, name: name,imageUrl: imageUrl, providerType: socialLoginType)
    }

}

extension LoginViewController: GIDSignInDelegate,GIDSignInUIDelegate{
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            var profileImageUrl = ""
            guard let userId = user.userID as String? else{self.showErrorAlertWithMessage("Didn't get your userId,Please review your profile!");return}
            guard let name = user.profile.name as String? else{self.showErrorAlertWithMessage("Didn't get your name,Please review your profile!");return}
            guard let email = user.profile.email as String? else{self.showErrorAlertWithMessage("Didn't get your email,Please review your profile!");return}
            if let profilePicUrl = user.profile.imageURL(withDimension: 400).absoluteString as String?{
                profileImageUrl = profilePicUrl
            }
            self.socialLogin(email, name: name, socialID: userId, imageUrl: profileImageUrl, socialLoginType:.google)

        } else {
            showAlertWith(viewController: self, message: error.localizedDescription, title: warningMessage.alertTitle.rawValue)
        }

    }

    public func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }


    func loginWithFacebook(){
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }

        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.native
        login.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {(result, error) in

            if error != nil {
                showAlertWith(viewController: self, message: error?.localizedDescription ?? "Facebook sign in error", title: warningMessage.alertTitle.rawValue)
            }else if (result?.isCancelled )!{
                showAlertWith(viewController: self, message: error?.localizedDescription ?? "Login process cancelled by user", title: warningMessage.alertTitle.rawValue)
            }
            else {
                CommonClass.showLoader(withStatus: "Fetching")
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).start(completionHandler: { (connection, response, error) in
                    CommonClass.hideLoader()

                    if error != nil{
                        showAlertWith(viewController: self, message: error?.localizedDescription ?? "Facebook sign in error", title: warningMessage.alertTitle.rawValue)
                    }else{
                        if let result = response as AnyObject?{
                            var profileImageUrl = "" ;
                            guard let userId = result.value(forKeyPath: "id") as! String? else{self.showErrorAlertWithMessage("Sorry! we didn't get your userId from Facebook, please review your profile!");return}

                            guard let firstName = result.value(forKeyPath:"first_name") as? String else{self.showErrorAlertWithMessage("Sorry! we didn't get your name, please review your profile!");return}
                            guard let lastName = result.value(forKeyPath:"last_name") as? String else{self.showErrorAlertWithMessage("Didn't get your name, please review your profile!");return}
                            guard let email = result.value(forKeyPath:"email") as? String else{self.showErrorAlertWithMessage("Sorry! we didn't get your email from Facebook, please review your profile!");return}

                            if let profilePicUrl = result.value(forKeyPath:"picture.data.url") as? String{
                                profileImageUrl = profilePicUrl
                            }

                            self.socialLogin(email, name: "\(firstName) \(lastName)", socialID: userId, imageUrl: profileImageUrl, socialLoginType:.facebook)
                        }
                    }
                })
            }
            
        })
    }

    func showErrorAlertWithMessage(_ message: String){
        GPDock.showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
    }
}

