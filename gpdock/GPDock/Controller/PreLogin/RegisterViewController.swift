//
//  RegisterViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Google.SignIn
import FBSDKCoreKit
import FBSDKLoginKit
import CountryPickerViewSwift


class RegisterViewController: UIViewController {
    @IBOutlet weak var firstNameTextField : UITextField!
    @IBOutlet weak var lastNameTextField : UITextField!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var mobileTextField : UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!

    @IBOutlet weak var fbButton : UIButton!
    @IBOutlet weak var googleButton : UIButton!
    @IBOutlet weak var registerButton : UIButton!


    var firstName = ""; var lastName = "";var email="";var password="";var mobile="";var countryCode = ""

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "Sign Up"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.setUpTextDelegate()
        self.countryCodeTextField.text = "+1"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().clientID = kGoogleClientId
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        // Do any additional setup after loading the view.

        let login = FBSDKLoginManager()
        login.logOut()
        GIDSignIn.sharedInstance().signOut()
    }
    func setUpTextDelegate() {
        firstNameTextField.delegate = self
        firstNameTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)

        lastNameTextField.delegate = self
        lastNameTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)

        mobileTextField.delegate = self
        mobileTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)
        
        countryCodeTextField.delegate = self
        countryCodeTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)

        passwordTextField.delegate = self
        passwordTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)

        emailTextField.delegate = self
        emailTextField.addTarget(self, action: #selector(textDidChanged(_:)), for: .editingChanged)

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewWillLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.googleButton, borderColor: UIColor.gray, borderWidth: 1, cornerRadius:1)
        CommonClass.makeViewCircularWithCornerRadius(self.fbButton, borderColor: UIColor.gray, borderWidth: 1, cornerRadius:1)
        self.registerButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickOfBackButton(_ sender:UIBarButtonItem){
        self.view.endEditing(true)
        if self.isModal(){
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.pop(true)
        }
    }


    @IBAction func onClickOfSignUp(_ sender:UIButton){
        self.view.endEditing(true)
        let validationResult = self.validateParams(firstName,lastName: lastName, email: email, password: password, mobile: mobile, countryCode: countryCode)
        if !validationResult.success{
            showAlertWith(viewController: self, message: validationResult.message, title: warningMessage.alertTitle.rawValue)
            self.showErrorAlertWithMessage(validationResult.message)
            return
        }
        if !CommonClass.isConnectedToNetwork{
            self.showErrorAlertWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.registerUserWith(firstName.trimmingCharacters(in: .whitespaces),lastName: lastName, email: email, password: password, mobile: mobile, countryCode: countryCode)

    }

    func registerUserWith(_ firstName:String,lastName:String,email:String,password:String,mobile:String,countryCode:String) -> Void {
        CommonClass.showLoader(withStatus: "Registering..")
        LoginService.sharedInstance.registerUserWith(firstName,lastName: lastName, email: email, password: password, mobile: mobile, countryCode: countryCode, profileImage: nil) { (success,responseUser,message)  in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            if success{
                if let user = responseUser,user.ID != "" {
                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                    NotificationCenter.default.post(name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil, userInfo: ["user":user])
                    CommonClass.sharedInstance.reRegisterForFirebase()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    @IBAction func onTermsAndConditions(_ sender:UIButton){
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = false
        termsVc.isFromMenu = false
        self.view.endEditing(true)
        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func onClickCountryCode(_ sender:UIButton){
        self.view.endEditing(true)
        self.showCountryPickerView()
    }

    @IBAction func onClickPrivacyPolicies(_ sender:UIButton){
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = true
        termsVc.isFromMenu = false
        self.view.endEditing(true)
        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false

        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickOfSignIn(_ sender:UIButton){
            self.view.endEditing(true)
            var viewControllers = self.navigationController!.viewControllers
            for i in 0..<viewControllers.count{
                if viewControllers[i] is RegisterViewController{
                    let signInVC = AppStoryboard.Main.viewController(LoginViewController.self)
                    viewControllers.remove(at: i)
                    viewControllers.insert(signInVC, at: i)
                    self.navigationController?.setViewControllers(viewControllers, animated: true)
                    break
                }
            }
    }

    
    @IBAction func onClickOfFacebookButton(_ sender:UIButton){
        self.view.endEditing(true)

        if !CommonClass.isConnectedToNetwork{
            self.showErrorAlertWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.loginWithFacebook()
    }

    @IBAction func onClickOfGoogleButton(_ sender:UIButton){
        self.view.endEditing(true)

        if !CommonClass.isConnectedToNetwork{
            self.showErrorAlertWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    func showCountryPickerView() {
        self.view.endEditing(true)
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countryCodeTextField.text = "+\(countryDic["code"] as! NSNumber)"
            self.countryCode = self.countryCodeTextField.text!
        }
    }


    func validateParams(_ firstName:String,lastName:String,email:String,password:String,mobile:String,countryCode:String) -> (success:Bool,message:String) {
        if firstName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your first name")
        }
        if lastName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your last name")
        }

        if email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your email")
        }
        if !CommonClass.validateEmail(email){
            return (success:false,message:"Please enter a valid email")
        }
        if password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your password")
        }
        if password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count < 6{
            return (success:false,message:warningMessage.validPassword.rawValue)
        }

        if !CommonClass.validatePassword(password){
            return (success:false,message:"Please enter a valid password")
        }
        if countryCode.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your country code")
        }
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your mobile")
        }
        if !CommonClass.validatePhoneNumber(mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)){
            return (success:false,message:"Please enter your mobile in correct format (xxxxxxxxxx)")
        }


        return (success:true,message:"")
    }


}

extension RegisterViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == firstNameTextField{
            firstName = textField.text!
        }else if textField == lastNameTextField{
            lastName = textField.text!
        }else if textField == emailTextField{
            email = textField.text!
        }else if textField == passwordTextField{
            password = textField.text!
        }else if textField == mobileTextField{
            mobile = textField.text!
        }else if textField == countryCodeTextField{
            countryCode = textField.text!
            //mobileTextField.resignFirstResponder()
//            showCountryPickerView()

        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == firstNameTextField{
            firstName = textField.text!
        }else if textField == lastNameTextField{
            lastName = textField.text!
        }else if textField == emailTextField{
            email = textField.text!
        }else if textField == passwordTextField{
            password = textField.text!
        }else if textField == mobileTextField{
            mobile = textField.text!
        }//else if textField == countryCodeTextField{
            //showCountryPickerView()
            countryCode = textField.text!
        //}
    }

    @objc func textDidChanged(_ textField: UITextField) -> Void {
        if textField == firstNameTextField{
            firstName = textField.text!
        }else if textField == lastNameTextField{
            lastName = textField.text!
        }else if textField == emailTextField{
            email = textField.text!
        }else if textField == passwordTextField{
            password = textField.text!
        }else if textField == mobileTextField{
            mobile = textField.text!
        }else if textField == countryCodeTextField{
            countryCode = textField.text!
           // mobileTextField.resignFirstResponder()
            //showCountryPickerView()
            //countryCodeTextField.resignFirstResponder()

        }
    }
    


}





extension RegisterViewController: GIDSignInDelegate,GIDSignInUIDelegate{
    func loginWithSocialMedia(_ email:String,socialID:String,name:String,imageUrl:String,providerType:SocialLoginType){
        if !CommonClass.isLoaderOnScreen{
            CommonClass.showLoader(withStatus: "Authenticating..")
        }else{
            CommonClass.updateLoader(withStatus: "Authenticating..")
        }
        LoginService.sharedInstance.loginWithSocialAuth(email, socialID: socialID, name: name, providerType: providerType,imageUrl: imageUrl) { (success,responseUser,message)  in

            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            if success{
                if let user = responseUser,user.ID != "" {
                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                    NotificationCenter.default.post(name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil, userInfo: ["user":user])
                    CommonClass.sharedInstance.reRegisterForFirebase()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }else{
                    showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
                }

            }else{
                showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func socialLogin(_ email:String,name:String,socialID:String,imageUrl:String,socialLoginType:SocialLoginType){
        //guard let appToken = kUserDefaults.value(forKey: kAppToken) as? String else{return}
        self.loginWithSocialMedia(email, socialID: socialID, name: name,imageUrl: imageUrl, providerType: socialLoginType)
    }

    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            var profileImageUrl = ""
            guard let userId = user.userID as String? else{self.showErrorAlertWithMessage("Sorry! we didn't get your userId from Google, please review your profile!");return}
            guard let name = user.profile.name as String? else{self.showErrorAlertWithMessage("Sorry! we didn't get your name from Google, please review your profile!");return}
            guard let email = user.profile.email as String? else{self.showErrorAlertWithMessage("Sorry! we didn't get your email from Google, please review your profile!");return}
            if let profilePicUrl = user.profile.imageURL(withDimension: 400).absoluteString as String?{
                profileImageUrl = profilePicUrl
            }
            self.socialLogin(email, name: name, socialID: userId, imageUrl: profileImageUrl, socialLoginType:.google)
        } else {
            showAlertWith(viewController: self, message: error.localizedDescription, title: warningMessage.alertTitle.rawValue)
        }

    }

    public func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {

    }


    func loginWithFacebook(){
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }

        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.native
        login.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {(result, error) in
            if error != nil {
                showAlertWith(viewController: self, message: error?.localizedDescription ?? "Facebook sign in error", title: warningMessage.alertTitle.rawValue)
            }else if (result?.isCancelled )!{
                showAlertWith(viewController: self, message: error?.localizedDescription ?? "Login process cancelled by user", title: warningMessage.alertTitle.rawValue)
            }
            else {
                CommonClass.showLoader(withStatus: "Fetching")
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).start(completionHandler: { (connection, response, error) in
                    CommonClass.hideLoader()

                    if error != nil{
                        showAlertWith(viewController: self, message: error?.localizedDescription ?? "Facebook sign in error", title: warningMessage.alertTitle.rawValue)
                    }else{
                        if let result = response as AnyObject?{
                            var profileImageUrl = "" ;
                            guard let userId = result.value(forKeyPath: "id") as! String? else{self.showErrorAlertWithMessage("Sorry! we didn't get your userId from Facebook, please review your profile!");return}

                            guard let firstName = result.value(forKeyPath:"first_name") as? String else{self.showErrorAlertWithMessage("Sorry! we didn't get your name, please review your profile!");return}
                            guard let lastName = result.value(forKeyPath:"last_name") as? String else{self.showErrorAlertWithMessage("Didn't get your name, please review your profile!");return}
                            guard let email = result.value(forKeyPath:"email") as? String else{self.showErrorAlertWithMessage("Sorry! we didn't get your email from Facebook, please review your profile!");return}

                            if let profilePicUrl = result.value(forKeyPath:"picture.data.url") as? String{
                                profileImageUrl = profilePicUrl
                            }

                            self.socialLogin(email, name: "\(firstName) \(lastName)", socialID: userId, imageUrl: profileImageUrl, socialLoginType:.facebook)
                        }
                    }
                })
            }

        })
    }


    func showErrorAlertWithMessage(_ message: String){
        GPDock.showAlertWith(viewController: self, message: message, title: warningMessage.alertTitle.rawValue)
    }

}





