//
//  EditProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
protocol EditProfileViewControllerDelegate {
    func user(didEditProfile viewController: EditProfileViewController,withUpdatedUser user: User)
}

class EditProfileViewController: UIViewController {
    var user : User!
    var delegate : EditProfileViewControllerDelegate?
    var shouldPostData : Bool = false
    var imagePickerController : UIImagePickerController!

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBackgroundView: UIImageView!

    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!

    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!

    @IBOutlet weak var doneButton: UIButton!

    var userImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCodeTextField.delegate = self
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.addGestureRecognizer()
        self.refreshUserDetails(user: self.user)
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        self.doneButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        self.fullNameTextField.addBottomThinBorderWithColor(.lightGray)
        self.lastNameTextField.addBottomThinBorderWithColor(.lightGray)
        self.phoneNumberTextField.addBottomThinBorderWithColor(.lightGray)
        self.countryCodeTextField.addBottomThinBorderWithColor(.lightGray)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickDone(_ sender: UIButton){
 
        guard let name = self.fullNameTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your first name", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let lastname = self.lastNameTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your last name", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let countryCode = self.countryCodeTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your country code", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let mobile = self.phoneNumberTextField.text else {
            showAlertWith(viewController: self, message: "Please enter your mobile number", title: warningMessage.alertTitle.rawValue)
            return
        }


        let validation = self.validateUserDetails(name, andLastName: lastname, mobile: mobile, countryCode: countryCode)
        if !validation.result{
            showAlertWith(viewController: self, message: validation.message, title: warningMessage.alertTitle.rawValue)
            return
        }

        self.updateProfile(self.user.ID, name: name+" "+lastname, email: self.user.email, mobile: mobile, countryCode: countryCode, profileImage: self.userImage)
        
    }
    func validateUserDetails(_ name: String, andLastName lastName : String,mobile:String, countryCode: String) -> (result:Bool,message:String) {
        if name.trimmingCharacters(in: .whitespaces).count < 1{
            return (result:false,message:"Please enter your first name")
        }
        if lastName.trimmingCharacters(in: .whitespaces).count < 1{
            return (result:false,message:"Please enter your last name")
        }
        if countryCode.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (result:false,message:"Please enter your country code")
        }
        
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (result:false,message:"Please enter your mobile number")
        }

        if !CommonClass.validatePhoneNumber(mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)){
            return (result:false,message:"Please enter your mobile number in correct format (+ccxxxxxxxxxx)")
        }

        return (result:true,message:"")
    }


    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(profileTapGestureRecognizer)
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.showAlertToChooseAttachmentOption()
    }

    func refreshUserDetails(user: User) {
        profileImageView.setIndicatorStyle(.white)
        profileImageView.setShowActivityIndicator(true)
        profileImageView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName))
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        profileImageView.addshadow(top: true, left: true, bottom: true, right: true)
        profileBackgroundView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:(self.userImage ?? CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName)))
        fullNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        phoneNumberTextField.text = user.contact
        if user.countryCode == ""{
            countryCodeTextField.text = "+1"
        }else{
        countryCodeTextField.text = user.countryCode
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    @IBAction func onClickCountryCode(_ sender:UIButton){
        self.view.endEditing(true)
        self.showCountryPickerView()
    }
    func showCountryPickerView() {
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            //self.countryNameLabel.text = "\(countryDic["en"] as! String)   \(countryDic["en"] as! String)"
            //self.countryImageView.image = countryDic["countryImage"] as? UIImage
            self.countryCodeTextField.text = "+\(countryDic["code"] as! NSNumber)"
        }
    }
    
    func updateProfile(_ userID: String,name:String,email:String,mobile:String,countryCode:String,profileImage:UIImage?){
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(viewController: self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Updating..")
        LoginService.sharedInstance.updateUserWith(userID, name: name, email: email, mobile: mobile, countryCode: countryCode, profileImage: profileImage) { (success, updatedUser, message) in
            CommonClass.hideLoader()
            if success{
                if let freshUser = updatedUser{
                    NotificationCenter.default.post(name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil, userInfo: ["user":freshUser])
                    self.delegate?.user(didEditProfile: self, withUpdatedUser: freshUser)
                }
            }
        }
    }

}
extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.userImage = tempImage
            self.profileImageView.image = self.userImage
            self.profileBackgroundView.image = self.userImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.userImage = tempImage
            self.profileImageView.image = self.userImage
            self.profileBackgroundView.image = self.userImage
        }
        picker.dismiss(animated: true) {}
    }
}
extension EditProfileViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextField {
            //showCountryPickerView()
           // countryCodeTextField.resignFirstResponder()
        }
    }
}

//MARK: - RSKImageCropViewControllerDelegate
//extension EditProfileViewController:RSKImageCropViewControllerDelegate{
//    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
//        self.navigationController?.pop(true)
//    }
//
//
//    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
//        self.userImage = croppedImage
//        self.profileImageView.image = croppedImage
//        self.profileBackgroundView.image = croppedImage
//        self.navigationController?.pop(true)
//    }
//
//}

