//
//  LoginService.swift
//  Bloom Trade
//
//  Created by TecOrb on 05/09/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let kUserJSON = "UserJSON"

class LoginService {
    static let sharedInstance = LoginService()
    fileprivate init() {}

    func updateDeviceTokeOnServer(){
        if let userJson = User.loadUserInfo(){
            var params = Dictionary<String,String>()
            let user = User(json:userJson)
            let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
            params.updateValue(user.ID, forKey: "user_id")
            params.updateValue(currentVersion, forKey: "current_version")
            if user.ID.trimmingCharacters(in: .whitespaces).count == 0{return}
            params.updateValue("ios", forKey: "device_type")
            params.updateValue("customer", forKey: "role")

            if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
                params.updateValue(deviceID, forKey: "device_id")
            }else{
                params.updateValue("--", forKey: "device_id")
            }
            var shouldUpdate :Bool = true
            for (_, value) in params{
                if value == "" || value == "--"{
                    shouldUpdate = false
                    break
                }
            }
            if !shouldUpdate{
                return
            }

            let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
            print_debug("hitting \(UPDATE_DEVICE_TOKEN_URL) with param \(params) and headers :\(head)")
            Alamofire.request(UPDATE_DEVICE_TOKEN_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("update device token json is:\n\(json)")
                        if let responseCode = json["code"].int as Int?
                        {
                            if responseCode == 200{}else{
                                //self.retryUpdateDeviceTokeOnServer()
                            }
                        }else{
                            //self.retryUpdateDeviceTokeOnServer()
                        }
                    }else{
                        //self.retryUpdateDeviceTokeOnServer()
                    }
                case .failure(let error):
                    print_debug(error.localizedDescription)
                   // self.retryUpdateDeviceTokeOnServer()
                }
            }

        }

    }

   private func retryUpdateDeviceTokeOnServer() {
        self.updateDeviceTokeOnServer()
    }

    func logOut(_ userID: String, completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        let params = ["user_id":userID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(LOGOUT_URL) with param \(params) and headers :\(head)")
            CommonClass.showLoader(withStatus: "Logging Out..")
            Alamofire.request(LOGOUT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
                if CommonClass.isLoaderOnScreen{
                    CommonClass.hideLoader()
                }
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("log out json is:\n\(json)")
                        let userParser = UserParser(json: json)
//                        if userParser.code == 345{
//                            CommonClass.hideLoader()
//                            CommonClass.sharedInstance.hadleAccessTokenError(false)
//                        }else{
                            completionBlock((userParser.code == 200 || userParser.code == 404 || userParser.code == 345),userParser.user,userParser.message)
//                        }
                    }else{
                        completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                case .failure(let error):
                    completionBlock(false,nil,error.localizedDescription)
                }
            }
    }

    func loginWith(_ userName:String,password:String,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
         var params = ["email":userName,"password":password]
        params.updateValue("ios", forKey: "device_type")
        params.updateValue("customer", forKey: "role")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: false)
        print_debug("hitting \(LOGIN_URL) with param \(params) and headers :\(head)")
        CommonClass.showLoader(withStatus: "Authenticating..")
        Alamofire.request(LOGIN_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.code == 200{
                        userParser.user.saveUserInfo(userParser.user)
                        kUserDefaults.set(true, forKey: kIsLoggedIN)
                    }
                    completionBlock((userParser.code == 200),userParser.user,userParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }





    func forgotPasswordForEmail(_ email: String,completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){

        let param = ["email":email,"role":"customer"]
        CommonClass.showLoader(withStatus: "Please wait..")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: false)

        Alamofire.request(FORGOT_PASSWORD_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("forgotpassword json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(false)
                        }else if responseCode == 200{
                            completionBlock(true,json["message"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }


    func varifyOTPWithEmail(_ email: String,OTP:String,completionBlock:@escaping (_ otpValidation: OTPValidationParser?,_ message:String)-> Void){

        let param = ["email":email,"role":"customer","otp":OTP]
        CommonClass.showLoader(withStatus: "Varifying..")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: false)

        Alamofire.request(OTP_VARIFICATION_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("varify OTP json is:\n\(json)")
                    let otpValidation = OTPValidationParser(json:json)
                    completionBlock(otpValidation,otpValidation.message)
                }else{
                    completionBlock(nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(nil,error.localizedDescription)
            }
        }

    }

    func recoveryPassword(newPassword password : String, withToken token: String,completionBlock:@escaping (_ success: Bool,_ message:String)-> Void){

        let param = ["new_password":password,"role":"customer","token":token]
        CommonClass.showLoader(withStatus: "Resetting..")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(PASSWORD_RECOVERY_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Reset Password json is:\n\(json)")
                    let otpValidation = OTPValidationParser(json:json)
                    if otpValidation.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                    }else{
                        completionBlock((otpValidation.code == 200),otpValidation.message)
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }


    func updatePassword(_ userID:String, oldPassword password : String, withNewPassword newPassword: String,completionBlock:@escaping (_ success: Bool,_ message:String)-> Void){

        var param = ["new_password":newPassword,"role":"customer","old_password":password]
        param.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        CommonClass.showLoader(withStatus: "Resetting..")
        Alamofire.request(UPDATE_PASSWORD_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Reset Password json is:\n\(json)")
                    let otpValidation = UserParser(json:json)
                    if otpValidation.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((otpValidation.code == 200),otpValidation.message)
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }


    func registerUserWith(_ firstName:String,lastName:String,email:String,password: String,mobile:String,countryCode: String,profileImage: UIImage?,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        /*
         "fname": "Jai",
         "lname": "Rajput",
         "email": "loveyhtu21@gmail.com",
         "password": "12345",
         "contact": "2144333",
         "address": "Sector 63",
         "city": "Noida",
         "state": "UP",
         "country": "India",
         "zipcode": "23001",
         "role": "customer",
         "image": ""
         */

        var params = Dictionary<String,String >()
        params.updateValue(firstName, forKey: "fname")
        params.updateValue(lastName, forKey: "lname")
        params.updateValue(email, forKey: "email")
        params.updateValue(password, forKey: "password")
        params.updateValue("customer", forKey: "role")
        params.updateValue(mobile, forKey: "contact")
        params.updateValue(countryCode, forKey: "country_code")
        params.updateValue("ios", forKey: "device_type")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: false)
        print_debug("hitting \(SIGN_UP_URL) with param \(params) and headers :\(head)")

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = profileImage{
                    if let data = UIImageJPEGRepresentation(pimage, 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: SIGN_UP_URL,method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        CommonClass.hideLoader()
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("sign up json is:\n\(json)")
                                let userParser = UserParser(json: json)
                                if userParser.code == 200{
                                    userParser.user.saveUserInfo(userParser.user)
                                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                                    kUserDefaults.synchronize()
                                }
                                completionBlock((userParser.code == 200),userParser.user,userParser.message)
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
            }
        )

    }


    func updateUserWith(_ userID:String,name:String,email:String,mobile:String,countryCode:String,profileImage: UIImage?,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        var params = Dictionary<String,String >()
        params.updateValue(userID, forKey: "user_id")
        let fullName = name.components(separatedBy: " ")
        if let fname = fullName.first{
            params.updateValue(fname, forKey: "fname")
        }

        if fullName.count > 1{
            var lName = ""
            for i in 1..<fullName.count{
                if lName == ""{
                    lName = fullName[i]
                }else{
                    lName = lName + " " + fullName[i]
                }
            }
            params.updateValue(lName, forKey: "lname")

        }
        params.updateValue(email, forKey: "email")
        params.updateValue("customer", forKey: "role")
        params.updateValue(mobile, forKey: "contact")
         params.updateValue(countryCode, forKey: "country_code")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(EDIT_PROFILE_URL) with param \(params) and headers :\(head)")
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = profileImage{
                    if let data = UIImageJPEGRepresentation(pimage, 0.9) as Data?{
                        multipartFormData.append(data, withName: "image_data", fileName: "image_data", mimeType: "image/jpg")
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: EDIT_PROFILE_URL,method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        CommonClass.hideLoader()
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("update profile json is:\n\(json)")
                                let userParser = UserParser(json: json)
                                if userParser.code == 345{
                                    CommonClass.hideLoader()
                                    CommonClass.sharedInstance.hadleAccessTokenError(true)
                                    return
                                }

                                if userParser.code == 200{
                                    userParser.user.saveUserInfo(userParser.user)
                                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                                }
                                completionBlock((userParser.code == 200),userParser.user,userParser.message)
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )

    }


    func loginWithSocialAuth(_ email:String,socialID:String,name:String,providerType:SocialLoginType,imageUrl:String,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        var params = Dictionary<String,String >()
        params.updateValue(email, forKey: "email")
        params.updateValue(providerType.rawValue, forKey: "social_media")
        params.updateValue(socialID, forKey: "social_id")
        params.updateValue(imageUrl, forKey: "image_url")
        let fullName = name.components(separatedBy: " ")
        if let fname = fullName.first{
            params.updateValue(fname, forKey: "fname")
        }

        if fullName.count > 1{
            var lName = ""
            for i in 1..<fullName.count{
                if lName == ""{
                    lName = fullName[i]
                }else{
                    lName = lName + " " + fullName[i]
                }
            }
            params.updateValue(lName, forKey: "lname")

        }


        params.updateValue("customer", forKey: "role")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }
        params.updateValue("ios", forKey: "device_type")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: false)

        print_debug("hitting \(SOCIAL_AUTH_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SOCIAL_AUTH_URL,method: .post ,parameters: params,headers:head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)

                    if userParser.code == 200{
                        userParser.user.saveUserInfo(userParser.user)
                        kUserDefaults.set(true, forKey: kIsLoggedIN)
                        kUserDefaults.synchronize()
                        completionBlock(true,userParser.user,userParser.message)
                    }else{
                        completionBlock((userParser.code == 200),nil,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func sendEmailToSupport(_ userID:String,message:String,image: UIImage?,completionBlock:@escaping (_ success:Bool,_ support:Support? ,_ message:String)-> Void){
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        let params = ["user_id":userID,"message":message]

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = image{
                    if let data = UIImageJPEGRepresentation(pimage, 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: WRITE_TO_US_URL,method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("write to us json is:\n\(json)")
                                let supportParser = SupportParser(json: json)
                                if supportParser.code == 345{
                                    CommonClass.hideLoader()
                                    CommonClass.sharedInstance.hadleAccessTokenError(true)
                                    return
                                }

                                completionBlock((supportParser.code == 200),supportParser.support,supportParser.message)
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )

    }






    func badgecounts(_ userID:String,completionBlock:@escaping (_ badgecount:Int, _ shouldShowRatingPopUp:Bool) -> Void){
        var param = ["user_id":userID]
        param.updateValue("ios", forKey: "device_type")
        param.updateValue(UIApplication.appVersion(), forKey: "version")
        param.updateValue(UIApplication.appBuild(), forKey: "build")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(BADGE_COUNT_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("badgescount json is:\n\(json)")
                    let badgesCount = BadgeCountParser(json:json)
                    if badgesCount.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(badgesCount.badgesCount,badgesCount.shouldShowRatingPopup)
                }else{
                    completionBlock(0,false)
                }
            case .failure:
                completionBlock(0,false)
            }
        }
    }


    func setAppStoreRating(_ userID:String,completionBlock:@escaping (_ badgecount:Int, _ didRated:Bool) -> Void){
        var param = ["user_id":userID]
        param.updateValue("ios", forKey: "device_type")
        param.updateValue(UIApplication.appVersion(), forKey: "version")
        param.updateValue(UIApplication.appBuild(), forKey: "build")
        print_debug("hitting \(RATE_APP_ON_APPSTORE) with param \(param)")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(RATE_APP_ON_APPSTORE, method: .post, parameters: param,  headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("rated on appstore json is:\n\(json)")
                    let badgesCountParser = BadgeCountParser(json:json)
                    if badgesCountParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(badgesCountParser.badgesCount, badgesCountParser.didRatedOnAppStore)
                }else{
                    completionBlock(0,false)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(0,false)
            }
        }
    }
    
    func checkAppVersion(completionBlock:@escaping (_ success:Bool, _ message:String, _ currentVersion: String, _ updateLevel: String)-> Void){
        
        CommonClass.showLoader(withStatus: "Please wait..")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting\(APP_VERSION)")
        Alamofire.request(APP_VERSION, method: .post , headers: head).responseJSON { response in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("appVersion json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(false)
                            return
                        }
                        if responseCode == 200{
                            completionBlock(true,json["message"].string ?? "", json["ios_customer_current_version"].string ?? "", json["ios_customer_update_level"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!", "", "")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!", "", "")
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!", "", "")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription, "", "")
            }
        }
        
    }



}





