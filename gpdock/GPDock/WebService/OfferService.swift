//
//  OfferService.swift
//  GPDock
//
//  Created by Parikshit on 09/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class OfferService {
    static let sharedInstance = OfferService()
    fileprivate init() {}
    func getOffersList(userID : String,page:Int,perPage:Int,completionBlock:@escaping (_ success: Bool,Array<OfferModal>?,_ totalPages:Int, _ totalRecord: Int) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")
         params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(OFFERS_LIST_URL) with param \(params) and headers :\(head)")

        Alamofire.request(OFFERS_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("offers json is:\n\(json)")
                    let offerParser = OfferParser(json: json)
                    if offerParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                    }else{
                        completionBlock(offerParser.responseCode==200, offerParser.offers, offerParser.totalPages, offerParser.totalRecord)
                    }

                }else{
                    completionBlock(false, nil, 0, 0)
                }
            case .failure:
                completionBlock(false, nil, 0, 0)
            }
        }
    }

}
