//
//  TransactionService.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TransactionService {
    static let sharedInstance = TransactionService()
    fileprivate init() {}


    func getTransactionsForUser(_ userID:String,page:Int,perPage:Int,completionBlock:@escaping (_ success:Bool,Array<ServiceTransaction>?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("customer", forKey: "role")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SERVICE_TRANSACTIONS_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SERVICE_TRANSACTIONS_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("transactions json is:\n\(json)")
                    let stParser = ServiceTransactionParser(json: json)
                    if stParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(stParser.code==200, stParser.transactions, stParser.message)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func getDetailsForTransaction(_ transactionID:String, userID:String,completionBlock:@escaping (_ success:Bool,_ transaction : ServiceTransaction?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(transactionID, forKey: "payment_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(TRANSACTION_DETAILS) with param \(params) and headers :\(head)")
        Alamofire.request(TRANSACTION_DETAILS,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("transactions json is:\n\(json)")
                    let stParser = ServiceTransactionParser(json: json)
                    if stParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(stParser.code==200, stParser.transaction, stParser.message)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    

}
