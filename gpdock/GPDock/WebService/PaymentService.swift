//
//  PaymentService.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe
/*
{
    "code": 200,
    "message": "success! pay at marina",
    "pay": {
        "id": 65,
        "amount": 5500,
        "transaction_id": "ch_1BJyYSHCw2A9l24gl3e808AQ",
        "created_at": "2017-11-03T06:21:25.000Z",
        "pay_for_grocery": 20,
        "pay_for_pumpout": 10,
        "pay_for_gasoline": 0,
        "pay_for_others": 25,
        "pay_note": "ddd",
        "paid_amount_in_doller": 55
    }
}
*/
class PaymentParser: NSObject {

    var responseCode = 0
    var responseMessage = ""
    var payment = Payment()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.responseCode = _code
        }

        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }

        if let _payment = json["pay"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.payment = Payment(params: _payment)
        }

    }
}


class Payment:NSObject{
    /*
    "id": 65,
    "amount": 5500,
    "transaction_id": "ch_1BJyYSHCw2A9l24gl3e808AQ",
    "created_at": "2017-11-03T06:21:25.000Z",
    "pay_for_grocery": 20,
    "pay_for_pumpout": 10,
    "pay_for_gasoline": 0,
    "pay_for_others": 25,
    "pay_note": "ddd",
    "paid_amount_in_doller": 55
 */
    let kID = "id"
    let kAmount = "service_amount"
    let kTransactionID = "transaction_id"
    let kCustomerTransactionID = "customer_transaction_id"
    let kCreatedAt = "created_at"
    let kPayForGrocery = "pay_for_grocery"
    let kPayForPumpout = "pay_for_pumpout"
    let kPayForGasoline = "pay_for_gasoline"
    let kPayForOthers = "pay_for_others"
    let kPayNote = "pay_note"
    let kAccountNumber = "account_number"


    var ID = ""
    var transactionID = ""
    var CustomerTransactionID = ""
    var createdAt = ""
    var payNote = ""
    var accountNumber = ""
    var amount : Double = 0.0
    var payForGrocery : Double = 0.0
    var payForPumpout : Double = 0.0
    var payForGasoline : Double = 0.0
    var payForOthers : Double = 0.0

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _trID = params[kTransactionID] as? String{
            self.transactionID = _trID
        }
        if let _ctrID = params[kCustomerTransactionID] as? String{
            self.CustomerTransactionID = _ctrID
        }

        if let _createdAt = params[kCreatedAt] as? String{
            self.createdAt = _createdAt
        }

        if let _payNote = params[kPayNote] as? String{
            self.payNote = _payNote
        }

        if let _amount = params[kAmount] as? Double{
            self.amount = _amount
        }else if let _amount = params[kAmount] as? String{
            self.amount = Double(_amount) ?? 0.0
        }
        if let _accountNumber = params[kAccountNumber] as? Double{
            self.accountNumber = "\(_accountNumber)"
        }else if let _accountNumber = params[kAccountNumber] as? String{
            self.accountNumber = _accountNumber
        }

        if let _amountPaid = params[kAmount] as? Double{
            self.amount = _amountPaid
        }else if let _amountPaid = params[kAmount] as? String{
            self.amount = Double(_amountPaid) ?? 0.0
        }

        if let _payForGrocery = params[kPayForGrocery] as? Double{
            self.payForGrocery = _payForGrocery
        }else if let _payForGrocery = params[kPayForGrocery] as? String{
            self.amount = Double(_payForGrocery) ?? 0.0
        }

        if let _payForPumpout = params[kPayForPumpout] as? Double{
            self.payForPumpout = _payForPumpout
        }else if let _payForPumpout = params[kPayForPumpout] as? String{
            self.payForPumpout = Double(_payForPumpout) ?? 0.0
        }

        if let _payForGasoline = params[kPayForGasoline] as? Double{
            self.payForGasoline = _payForGasoline
        }else if let _payForGasoline = params[kPayForGasoline] as? String{
            self.payForGasoline = Double(_payForGasoline) ?? 0.0
        }

        if let _payForOthers = params[kPayForOthers] as? Double{
            self.payForOthers = _payForOthers
        }else if let _payForOthers = params[kPayForOthers] as? String{
            self.payForOthers = Double(_payForOthers) ?? 0.0
        }
        super.init()
    }
}


class CardParser: NSObject {

    var responseCode = 0
    var responseMessage = ""
    var cards = Array<Card>()
    var card = Card()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.responseCode = _code
        }

        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }

        if let _cards = json["cards"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _cardDict in _cards{
                let card = Card(params: _cardDict)
                self.cards.append(card)
            }
        }

        if let _card = json["card"].dictionaryObject as Dictionary<String,AnyObject>?{
             self.card = Card(params: _card)
        }

    }
}

class Card:NSObject{
/*
    "id": 3,
    "card_token": "card_1AlxIOHCw2A9l24g1yf1va48",
    "exp_month": "1",
    "exp_year": "2020",
    "last4": "4242",
    "brand": "Visa",
    "funding_type": "credit",
    "is_default": true
    */
    let kID = "id"
    let kCardToken = "card_token"
    let kExpMonth = "exp_month"
    let kExpYear = "exp_year"
    let kLast4 = "last4"
    let kBrand = "brand"
    let kFundingType = "funding_type"
    let kIsDefault = "is_default"


    var ID = ""
    var cardToken : String = ""
    var expMonth: UInt = 0
    var expYear : UInt = 0
    var last4 : String = ""
    var brand : String = ""
    var fundingType :String = ""
    var isDefault : Bool = false

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _token = params[kCardToken] as? Int{
            self.cardToken = "\(_token)"
        }else if let _token = params[kCardToken] as? String{
            self.cardToken = _token
        }

        if let _expMonth = params[kExpMonth] as? UInt{
            self.expMonth = _expMonth
        }else if let _expMonth = params[kExpMonth] as? String{
            self.expMonth = UInt(_expMonth) ?? 0
        }

        if let _expYear = params[kExpYear] as? UInt{
            self.expYear = _expYear
        }else if let _expYear = params[kExpYear] as? String{
            self.expYear = UInt(_expYear) ?? 0
        }

        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _brand = params[kBrand] as? String{
            self.brand = _brand
        }
        if let _fundingType = params[kFundingType] as? String{
            self.fundingType = _fundingType
        }
        if let _isDefault = params[kIsDefault] as? Bool{
            self.isDefault = _isDefault
        }
        super.init()
    }
}




class PaymentService {
    static let sharedInstance = PaymentService()
    fileprivate init() {}


    func getCardsForUser(_ userID:String,completionBlock:@escaping (Array<Card>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CARDS_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(CARDS_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if cardParser.responseCode == 200{
                        completionBlock(cardParser.cards)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    func payForMarinaWith(_ userID: String,marinaID:String,amounts:[Double],sourceToken:String,cardID:String,message:String,coupanCode:String,completionBlock:@escaping (_ success:Bool,_ payment:Payment?,_ message:String)->Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(cardID, forKey: "card_id")
        params.updateValue(sourceToken, forKey: "source")
        params.updateValue(message, forKey: "pay_note")
        params.updateValue("true", forKey: "is_saved")
        params.updateValue(coupanCode, forKey: "coupan_code")

        params.updateValue("\(amounts[0]*100)", forKey: "pay_for_grocery")
        params.updateValue("\(amounts[1]*100)", forKey: "pay_for_gasoline")
        params.updateValue("\(amounts[2]*100)", forKey: "pay_for_pumpout")
        params.updateValue("\(amounts[3]*100)", forKey: "pay_for_others")


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
       // print_debug("hitting \(PAY_AT_MARINA_URL) with \(params) and headers :\(head)")

        Alamofire.request(PAY_AT_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("payment json is:\n\(json)")

                    let paymentParser = PaymentParser(json: json)
                    if paymentParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if paymentParser.responseCode == 200{
                        completionBlock(true,paymentParser.payment,paymentParser.responseMessage)
                    }else{
                        completionBlock(false,nil,paymentParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    //MARK: To request a booking

    func requestBookingFor(_ userID: String, marinaID: String, bookingDates:BookingDates,boatID:String,completionBlock:@escaping (_ success:Bool,_ booking:Booking?,_ message:String)->Void){

        let fromDate = CommonClass.sharedInstance.formattedDateWith(bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(bookingDates.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()

        params.updateValue(userID, forKey: "user_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        params.updateValue(boatID, forKey: "boat_id")
        params.updateValue(marinaID, forKey: "marina_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REQUEST_BOOKING_URL) with \(params) and headers :\(head)")
        Alamofire.request(REQUEST_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(true,bookingParser.booking,bookingParser.responseMessage)
                    }else{
                        completionBlock(false,nil,bookingParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }





    func payAndCreateBookingFor(_ userID: String,bookingDates:BookingDates,parkingSpaceID: String,pricePerFeet:Double,boatID:String,amount:Double,sourceToken:String,cardID:String,willSave :Bool,coupanCode:String,completionBlock:@escaping (_ success:Bool,_ booking:Booking?,_ message:String)->Void){

        let fromDate = CommonClass.sharedInstance.formattedDateWith(bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(bookingDates.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        params.updateValue(parkingSpaceID, forKey: "parking_space_id")
        params.updateValue("\(pricePerFeet)", forKey: "price_per_foot")
        params.updateValue(boatID, forKey: "boat_id")
        let cents = String(format: "%.0lf", amount*100)
        params.updateValue(cents, forKey: "amount")
        params.updateValue(sourceToken, forKey: "source")
        params.updateValue(willSave ? "true" : "false", forKey: "is_saved")
        params.updateValue(cardID, forKey: "card_id")
        params.updateValue(coupanCode, forKey: "coupan_code")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CREATE_BOOKING_URL) with \(params) and headers :\(head)")

        CommonClass.updateLoader(withStatus: "Creating..")
        Alamofire.request(CREATE_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    CommonClass.updateLoader(withStatus: "Creating..")

                    print_debug("booking json is:\n\(json)")
                    
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(true,bookingParser.booking,bookingParser.responseMessage)
                    }else{
                        completionBlock(false,nil,bookingParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func removeCard(_ cardID:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){

        let url = REMOVE_CARD_URL+cardID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REMOVE_CARD_URL) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)

                    print_debug("remove card json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(cardParser.responseCode == 200,cardParser.responseMessage)
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func addCardForUser(_ userID:String, cardToken:String,completionBlock:@escaping (_ success:Bool,_ card:Card?,_ message:String) -> Void){
        var params = Dictionary<String,Any>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(cardToken, forKey: "source")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_CARD_URL) and params: \(params) headers :\(head)")
        Alamofire.request(ADD_CARD_URL,method: .post,parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("add card json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((cardParser.responseCode == 200),cardParser.card,cardParser.responseMessage)
                }else{
                    completionBlock(false,nil, response.result.error?.localizedDescription ?? "Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    

    func getBookingDetails(_ bookingID:String,completionBlock:@escaping (_ booking:Booking?) -> Void){

        let url = BOOKING_DETAILS_URL+bookingID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(BOOKING_DETAILS_URL) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.booking)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }



    func getBookingRefundDetails(_ bookingID:String,completionBlock:@escaping (_ refund:Refund?) -> Void){
        let params = ["booking_id":bookingID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REFUND_DETAILS_URL) and headers :\(head)")
        Alamofire.request(REFUND_DETAILS_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refund details json is:\n\(json)")
                    let refundParser = RefundPaser(json: json)
                    if refundParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if refundParser.responseCode == 200{
                        completionBlock(refundParser.refund)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }



    func checkAndGetBookingRefundableDetails(_ bookingID:String,completionBlock:@escaping (_ refundableDetails:RefundableDetails?) -> Void){
        let params = ["booking_id":bookingID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CHECK_REFUNDABLE_BOOKING_DETAILS_URL) and headers :\(head)")
        Alamofire.request(CHECK_REFUNDABLE_BOOKING_DETAILS_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refundable details json is:\n\(json)")
                    let refundableDetailsParser = RefundableDetailsPaser(json: json)
                    if refundableDetailsParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if refundableDetailsParser.responseCode == 200{
                        completionBlock(refundableDetailsParser.refundableDetails)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }



}
