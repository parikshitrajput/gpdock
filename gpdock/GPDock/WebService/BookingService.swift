//
//  BookingService.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BookingService {
    static let sharedInstance = BookingService()
    fileprivate init() {}


    func getBookingsForUser(_ userID:String,status:String,page:Int,perPage:Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(status, forKey: "status")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(LIST_OF_BOOKINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LIST_OF_BOOKINGS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    func isBookingsFoundForUser(_ userID:String,completionBlock:@escaping (_ apiResult:Bool,_ result:Bool,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(DID_USER_BOOKED_EARLIER) with param \(params) and headers :\(head)")
        Alamofire.request(DID_USER_BOOKED_EARLIER,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(bookingParser.responseCode == 200,bookingParser.isBookingFound,bookingParser.responseMessage)
                }else{
                    completionBlock(false,false,response.error?.localizedDescription ?? "Unknow error")
                }
            case .failure(let error):
                completionBlock(false,false,error.localizedDescription)
            }
        }
    }

//    "marina_id": "10",
//    "user_id": "5",
//    "booking_id": 1378,
//    "title": "Average visit",
//    "description": "I have been visited there but not too much as expected.",
//    "rating": "3"

    func addReviewForBookingByUser(_ userID:String,marinaID:String,bookingID:String,rating:Double,title:String,reviewDesc:String,completionBlock:@escaping (_ result:Bool,_ review:Review?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(bookingID, forKey: "booking_id")
        params.updateValue(title, forKey: "title")
        params.updateValue(reviewDesc, forKey: "description")
        params.updateValue("\(rating)", forKey: "rating")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_REVIEW_URL) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_REVIEW_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            CommonClass.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("add review json is:\n\(json)")
                    let reviewParser = ReviewParser(json: json)
                    if reviewParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(reviewParser.code == 200,reviewParser.review,reviewParser.message)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Unknow error")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    func isFavoritesFoundForUser(_ userID:String,completionBlock:@escaping (_ apiResult:Bool,_ result:Bool,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(DID_USER_FAVORATE_EARLIER) with param \(params) and headers :\(head)")
        Alamofire.request(DID_USER_FAVORATE_EARLIER,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("faverated json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(marinaParser.responseCode == 200,marinaParser.isFavoritesMarinasFound,marinaParser.responseMessage)
                }else{
                    completionBlock(false,false,response.error?.localizedDescription ?? "Unknow error")
                }
            case .failure(let error):
                completionBlock(false,false,error.localizedDescription)
            }
        }
    }


   



    func cancelBookings(with bookingID:String,userID:String,completionBlock:@escaping (_ success: Bool, _ boat:Booking?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        params.updateValue(userID, forKey: "user_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CANCEL_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(CANCEL_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("booking cancelled json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock((bookingParser.responseCode == 200), bookingParser.booking,bookingParser.responseMessage)
                }else{
                    completionBlock(false, nil,response.error?.localizedDescription ?? "Something went wrong\r\nPlease refresh by pull down")
                }
            case .failure(let error):
                completionBlock(false, nil,error.localizedDescription)
            }
        }
    }



    func emailInvoiceOfBooking(with bookingID:String,userID:String,completionBlock:@escaping (_ success: Bool,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        params.updateValue(userID, forKey: "user_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MAIL_INVOICE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MAIL_INVOICE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(bookingParser.responseCode == 200,bookingParser.responseMessage)
                }else{
                    completionBlock(false,"Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }

    
    func verifiedUserContact(_ userID:String,contact:String,country_code:String,verifiedStatus: String,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        let params = ["user_id":userID,"contact":contact,"country_code":country_code ,"is_verified":verifiedStatus ]
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(USER_CONTACT_VERIFIED) with param \(params) and headers :\(head)")
        CommonClass.showLoader(withStatus: "Update..")
        Alamofire.request(USER_CONTACT_VERIFIED,method: .post ,parameters: params, headers:head).responseJSON { response in
          CommonClass.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if userParser.code == 200{
                        userParser.user.saveUserInfo(userParser.user)
                        kUserDefaults.set(true, forKey: kIsLoggedIN)
                        completionBlock((userParser.code == 200),userParser.user,userParser.message)
                    }
                   
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    func generateInvoice(_ userID: String,marinaID: String, boatID: String,fromDate:String,toDate:String,price:String,completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){
        
        let param = ["user_id":userID,"marina_id":marinaID,"boat_id":boatID,"date_from":fromDate,"date_to":toDate,"price":price]
        print_debug("hitting \(GENERATE_INVOICE_URL) with param \(param)")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(GENERATE_INVOICE_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("generate invoice json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(false)
                            return
                        }
                        if responseCode == 200{
                            completionBlock(true,json["message"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }
        
    }



}
