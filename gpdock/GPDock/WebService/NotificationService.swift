//
//  NotificationService.swift
//  GPDock
//
//  Created by TecOrb on 16/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class NotificationService {
    static let sharedInstance = NotificationService()
    fileprivate init() {}

    func clearAllNotifications(_ userID:String,completionBlock:@escaping (_ success:Bool) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("customer", forKey: "role")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(CLEAR_ALL_NOTIFICATIONS_LIST_URL, method: .post, parameters: params,  headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Clear notifications json is:\n\(json)")
                    let notificationParser = NotificationParser(json: json)
                    if notificationParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(notificationParser.responseCode == 200)
                }else{
                    completionBlock(false)
                }
            case .failure:
                completionBlock(false)
            }
        }
    }

    func getNotificationsForUser(_ userID:String,page:Int,perPage:Int,completionBlock:@escaping (_ success:Bool,Array<NotificationModel>?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("customer", forKey: "role")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(NOTIFICATIONS_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(NOTIFICATIONS_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("notification json is:\n\(json)")
                    let notificationParser = NotificationParser(json: json)
                    if notificationParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock(notificationParser.responseCode==200, notificationParser.notifications, notificationParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func setNotificationAsReadByUser(_ userID:String,_ notificationID : String,completionBlock:@escaping (_ didRead:Bool) -> Void){
        var param = ["user_id":userID]
        param.updateValue(notificationID, forKey: "notification_id")
        print_debug("hitting \(NOTIFICATION_TOGGLE_STATUS_URL) with param \(param)")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        Alamofire.request(NOTIFICATION_TOGGLE_STATUS_URL, method: .post, parameters: param,  headers: head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("read by user json is:\n\(json)")
                    let notificationParser = NotificationParser(json: json)
                    if notificationParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(notificationParser.notificationDidRead)
                }else{
                    completionBlock(false)
                }
            case .failure:
                completionBlock(false)
            }
        }
    }
}
