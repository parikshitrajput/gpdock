//
//  MarinaService.swift
//  GPDock
//
//  Created by TecOrb on 21/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class MarinaService {
    static let sharedInstance = MarinaService()

    func toggleFavorites(_ userID:String,marinaID:String,completionBlock:@escaping (_ status:Bool,_ marinaID:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting \(FAVORITES_TOGGLE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(FAVORITES_TOGGLE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("favorate json is:\n\(json)")
                    let favParser = FavoriteParser(json: json)
                    if favParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }

                    if favParser.code == 200{
                        completionBlock(favParser.status,favParser.marinaID)
                    }else{
                        completionBlock(false,"")
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false,"")
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(false,"")
            }
        }
    }



    func searchBySearchText(searchText:String,pageNumber:Int,perPage:Int,completionBlock:@escaping (_ success:Bool ,_ suggestions:Array<Suggestion>?, _ message:String) -> Void) {
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        var params = ["page":"\(pageNumber)","per_page":"\(perPage)"]
        params.updateValue(searchText, forKey: "suggest_text")

        //print_debug("hitting \(SEARCH_BY_TEXT_URL)with param \(params) and headers :\(head)")

        Alamofire.request(SEARCH_BY_TEXT_URL,method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                     print_debug("suggestions json is:\n\(json)")
                    let suggestionParser = SuggestionParser(json: json)
                    if suggestionParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock((suggestionParser.code == 200),suggestionParser.suggestions, suggestionParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }







    func getCitiesFromServer(keyword:String?,userID: String?,coordinates:CLLocationCoordinate2D?,pageNumber: Int,perPage: Int,completionBlock:@escaping (_ success:Bool ,_ cities:Array<City>?,_ totalPage: Int,_ message:String) -> Void){
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        var params = ["page":"\(pageNumber)","per_page":"\(perPage)","user_id":userID ?? "-"]
        if let key = keyword{
            params.updateValue(key, forKey: "keyword")
        }
        
        var isOrderByGeolocation = false
        if let location = coordinates{
            isOrderByGeolocation = true
            params.updateValue("\(location.latitude)", forKey: "latitude")
            params.updateValue("\(location.longitude)", forKey: "longitude")
        }

        print_debug("hitting \(isOrderByGeolocation ? NEARBY_CITIES_URL : CITIES_URL)with param \(params) and headers :\(head)")
        Alamofire.request(isOrderByGeolocation ? NEARBY_CITIES_URL : CITIES_URL,method: isOrderByGeolocation ? .post : .get, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cities json is:\n\(json)")
                    let cityParser = CityParser(json: json)
                    if cityParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock((cityParser.responseCode == 200),cityParser.cities,cityParser.totalPages,cityParser.responseMessage)
                }else{
                    completionBlock(false,nil,0,response.result.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,nil,0,error.localizedDescription)
            }
        }
    }

    func getParkingAvailabilityCountForUser(_ userID:String, marinaID:String,bookingDate:BookingDates,completionBlock:@escaping (Array<Boat>?) -> Void){

        let fromDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_USER_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_USER_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("user boats json is:\n\(json)")

                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if boatParser.responseCode == 200{
                        completionBlock(boatParser.boats)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    func getUserBoatList(_ userID:String,page:Int,perPage:Int,completionBlock:@escaping (Array<Boat>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(USER_BOAT_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(USER_BOAT_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("user boats json is:\n\(json)")

                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if boatParser.responseCode == 200{
                        completionBlock(boatParser.boats)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    func getParkingAvailabilityCountForMarina(_ marinaID:String,boatSize:BoatSize,bookingDate:BookingDates,completionBlock:@escaping (Int?) -> Void){

        let fromDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue("count", forKey: "search_for")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue("\(boatSize.length)", forKey: "length")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Availability json is:\n\(json)")
                    if let code = json["code"].int, code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if let count = json["result"].int as Int?{
                        completionBlock(count)
                    }else if let count = json["result"].string as String?{
                        completionBlock(Int(count) ?? 0)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

    func getFloorPlanForMarina(_ marinaID:String,boatSize:BoatSize,bookingDate:BookingDates,completionBlock:@escaping (FloorPlan?) -> Void){
        let fromDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.sharedInstance.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue("\(boatSize.length)", forKey: "length")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("floor plan json is:\n\(json)")
                    let floorPlan = FloorPlan(json: json)
                    if floorPlan.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if floorPlan.responseCode == 200{
                        completionBlock(floorPlan)
                    }else{
                        showAlertWith(viewController:((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: floorPlan.responseMessage, title: warningMessage.alertTitle.rawValue)
                        completionBlock(nil)
                    }
                }else{
                    showAlertWith(viewController:((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Oops! Something went wrong", title: warningMessage.alertTitle.rawValue)
                    completionBlock(nil)
                }
            case .failure(let error):
                showAlertWith(viewController:((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: error.localizedDescription, title: warningMessage.alertTitle.rawValue)
                completionBlock(nil)
            }
        }
    }


    func getMarinaList(_ cityID:String?,userID:String?, latitude:Double?,longitude:Double?,page:Int,perPage:Int, completionBlock:@escaping (_ success:Bool ,_ marinas:Array<Marina>?,_ city: City?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")
        if let city = cityID{params.updateValue(city, forKey: "city_id")}
        if let uID = userID{params.updateValue(uID, forKey: "user_id")}

        if let lat = latitude{params.updateValue("\(lat)", forKey: "latitude")}
        if let longi = longitude{params.updateValue("\(longi)", forKey: "longitude")}

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_LIST_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marinas json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if marinaParser.responseCode == 200{
                        completionBlock(true,marinaParser.marinas,marinaParser.city,marinaParser.responseMessage)
                    }else{
                        completionBlock(false,nil,nil,marinaParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,nil,error.localizedDescription)
            }
        }
    }


    func getFilteredMarinaList(_ sortingKey:String?,sortingOrder:String?,cityID:String?,userID:String?,filters: (shouldShowNearby: Bool, userRating: Float, minPriceRange: Double, maxPriceMax: Double, marinaName:String, marinaType:MarinaTypeFilter, service: Set<Service>, amenities: Set<Aminity>)?,page:Int,perPage:Int, minPriceRange: Double? = 0.0, maxPriceMax: Double?, completionBlock:@escaping (AnyObject?) -> Void){

        var params = ["page":"\(page)","per_page":"\(perPage)"]
        if let sortingK = sortingKey{params.updateValue(sortingK.lowercased(), forKey: "sorting_key")}
        if let orderBy = sortingOrder{params.updateValue(orderBy.uppercased(), forKey: "sorting_order")}
        if let city = cityID{params.updateValue(city, forKey: "city_id")}

        if let uID = userID, uID.count != 0{
            params.updateValue(uID, forKey: "user_id")
        }


        if let filter = filters{

            params.updateValue(String(format:"%0.0f",filter.userRating), forKey: "rating")
            let minPrice = String(format:"%0.0f",filter.minPriceRange.rounded(.down))
            let maxPrice  = String(format:"%0.0f",filter.maxPriceMax.rounded(.up))
            params.updateValue("\(minPrice)-\(maxPrice)", forKey: "price_range")

            if filter.marinaName.count > 0{
                params.updateValue(filter.marinaName, forKey: "title_keyword")
            }else{
                if filter.service.count > 0{
                    params.updateValue("\(CommonClass.getSectedSevices(services: Array(filter.service)))", forKey: "services")
                }

                if filter.amenities.count > 0{
                    params.updateValue("\(CommonClass.getSectedAmenities(amenities: Array(filter.amenities)))", forKey: "amenities")
                }

                let marinaType = (filter.marinaType.filterTypeName.lowercased().count == 0) ? "both" : filter.marinaType.filterTypeName.lowercased()
                params.updateValue(marinaType, forKey: "marina_type")
        }

        }else{
            params.updateValue("0", forKey: "rating")
            let minPrice = String(format:"%0.0f",minPriceRange?.rounded(.down) ?? 0)
            let maxPrice  = String(format:"%0.0f",((maxPriceMax ?? 1000)+0.5).nextUp)
            params.updateValue("\(minPrice)-\(maxPrice)", forKey: "price_range")
        }

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_FILTERS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_FILTERS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marinas json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }

                    if marinaParser.responseCode == 200{
                        completionBlock(marinaParser.marinas as AnyObject)
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    

    func getFavoritesMarinaList(_ userId:String,page:Int,perPage:Int, completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["page":"\(page)","per_page":"\(perPage)","user_id":userId]

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(FAVORITES_MARINA_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(FAVORITES_MARINA_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marinas json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if marinaParser.responseCode == 200{
                        completionBlock(marinaParser.marinas as AnyObject)
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }


    func getMarinaDetails(_ marinaID:String,userID:String?,completionBlock:@escaping (_ success:Bool, _ marina: Marina?, _ message: String) -> Void){
        let params = ["id":marinaID,"user_id":userID ?? "-"]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_DETAILS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_DETAILS_URL,method: .get ,parameters: params, headers:head).responseJSON { response in

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock((marinaParser.responseCode == 200),marinaParser.marina,marinaParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }

            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }



    func addBoatWith(_ userID: String,name:String,boatSize: BoatSize,boatImage:UIImage?,completionBlock:@escaping (_ success:Bool,_ boat:Boat?,_ message:String) -> Void) {
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(boatSize.length)", forKey: "long")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(name, forKey: "name")

        if let image = boatImage{
            if let base64String = UIImageJPEGRepresentation(image, 0.8)?.base64EncodedString() {
                params.updateValue(base64String, forKey: "image")
            }
        }

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_BOAT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_BOAT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("add boat json: \(json)")
                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock((boatParser.responseCode == 200),boatParser.boat,boatParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }




    func updateBoatWith(_ userID: String,boatID:String, name:String,boatSize: BoatSize,boatImage:UIImage?,completionBlock:@escaping (_ success:Bool,_ boat:Boat?,_ message:String) -> Void) {
        var params = Dictionary<String,String>()
        params.updateValue(boatID, forKey: "boat_id")
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(boatSize.length)", forKey: "long")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(name, forKey: "name")

        if let image = boatImage{
            if let base64String = UIImageJPEGRepresentation(image, 0.8)?.base64EncodedString() {
                params.updateValue(base64String, forKey: "image")
            }
        }


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(UPDATE_BOAT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(UPDATE_BOAT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("updated boat json: \(json)")
                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock((boatParser.responseCode == 200),boatParser.boat,boatParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }

    }


    func deleteBoatWith(_ userID: String, boatID: String,completionBlock:@escaping (_ success:Bool, _ message: String) -> Void) {
        let delteBoatUrl = DELETE_BOAT_URL+userID+"/boats/"+boatID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(delteBoatUrl) and headers :\(head)")
        Alamofire.request(delteBoatUrl,method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("delete boat json: \(json)")
                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }

                    completionBlock((boatParser.responseCode == 200),boatParser.responseMessage)
                }else{
                    completionBlock(false,response.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }
        
    }





    func lockParkingSpaceFor(_ userID: String, parkingID:String,completionBlock:@escaping (Bool) -> Void){
        var params = ["parking_space_id":parkingID]
        params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting \(LOCK_MARINA_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LOCK_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("locking parking json: \(json)")

                    if let code = json["code"].int as Int?{
                        if code == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }else if code == 200{
                            if let isLocked = json["current_state"].bool as Bool?{
                                completionBlock(isLocked)
                            }else{
                                showErrorWithMessage("Couldn't locked your choice!")
                                completionBlock(false)
                            }
                        }else{
                            showErrorWithMessage("Couldn't locked your choice!")
                            completionBlock(false)
                        }
                    }else{
                        showErrorWithMessage("Couldn't locked your choice!")
                        completionBlock(false)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false)
                }
            case .failure(let error):
                showErrorWithMessage("Oops! \(error.localizedDescription)")
                completionBlock(false)
            }
        }
    }




    func getMarinaReviewsAndRating(_ marinaID:String,ratingWanted wantRating:Bool,page:Int,perPage: Int,completionBlock:@escaping (AnyObject?) -> Void){
        var params = ["marina_id":marinaID]
        params.updateValue(wantRating ? "true" : "false", forKey: "need_ratings")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "perPage")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_REVIEWS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_REVIEWS_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("reviews json: \(json)")
                    let reviewParser = ReviewParser(json: json)
                        if reviewParser.code == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(false)
                            return
                        }

                        if reviewParser.code == 200{
                            completionBlock(reviewParser)
                        }else{
                            showErrorWithMessage(reviewParser.message)
                    }

                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }


    func getMarinaReviewsForRatings(_ marinaID:String, reviewsForRating rating:Int,page:Int,perPage: Int,completionBlock:@escaping (Array<Review>?) -> Void){
        var params = ["marina_id":marinaID]
        params.updateValue("\(rating)", forKey: "rating")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "perPage")
        params.updateValue("false", forKey: "need_ratings")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_REVIEWS_FOR_RATINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_REVIEWS_FOR_RATINGS_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("reviews json: \(json)")
                    let reviewParser = ReviewParser(json: json)
                    if reviewParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if reviewParser.code == 200{
                        completionBlock(reviewParser.reviews)
                    }else{
                        showErrorWithMessage(reviewParser.message)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }


    func getAllServicesFromServer(completionBlock:@escaping (Array<Service>?) -> Void){

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SERVICES_URL) headers :\(head)")
        Alamofire.request(SERVICES_URL,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("services json is:\n\(json)")
                    let serviceParser = ServiceParser(json: json)
                    if serviceParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }

                    if serviceParser.responseCode == 200{
                        completionBlock(serviceParser.services)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    func getAllAmenitiesFromServer(completionBlock:@escaping (Array<Aminity>?) -> Void){
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(AMENITIES_URL) headers :\(head)")
        Alamofire.request(AMENITIES_URL,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("amenities json is:\n\(json)")
                    let amenityParser = AmenitiesParser(json: json)
                    if amenityParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if amenityParser.responseCode == 200{
                        completionBlock(amenityParser.amenities)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure:
                completionBlock(nil)
            }
        }
    }


    func reviewBookingPrice(_ marinaID:String,fromDate:String,toDate:String,completionBlock:@escaping (_ success:Bool,_ marinaPriceReview: MarinaPriceReview?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "from_date")
        params.updateValue(toDate, forKey: "to_date")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REVIEW_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(REVIEW_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("review price json is:\n\(json)")

                    let marinaPriceReview = MarinaPriceReview(json: json)
                    if marinaPriceReview.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock(marinaPriceReview.code == 200,marinaPriceReview)
                }else{
                    completionBlock(false,nil)
                }
            case .failure:
                completionBlock(false,nil)
            }
        }
    }

    func getFAQsFromServer(completionBlock:@escaping (_ success:Bool, _ faqModels:Array<FAQModel>?, _ message:String) -> Void) {
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(FAQ_URL) and headers :\(head)")
        Alamofire.request(FAQ_URL,method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("faqs json: \(json)")
                    let faqParser = FAQParser(json: json)
                    if faqParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    completionBlock((faqParser.code == 200),faqParser.faqs,faqParser.message)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }

    }

    
}
