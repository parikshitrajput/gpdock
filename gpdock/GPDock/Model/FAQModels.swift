//
//  FAQModels.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class Question: NSObject {
    let kQuestion = "question"
    let kID = "id"
    let kAnswer = "answer"

    var question = ""
    var ID = ""
    var answer = ""

    override init() {
        super.init()
    }
    init(dictionary: Dictionary<String,AnyObject>) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _question = dictionary[kQuestion] as? String{
            self.question = _question
        }
        if let _answer = dictionary[kAnswer] as? String{
            self.answer = _answer
        }

        super.init()
    }
}


class FAQModel: NSObject {
    let kCategory = "category"
    let kQuestions = "questions"

    var questions = Array<Question>()
    var category = ""

    override init() {
        super.init()
    }
    init(dictionary: Dictionary<String,AnyObject>) {
        if let _categoryname = dictionary[kCategory] as? String{
            self.category = _categoryname
        }
        if let _questionArray = dictionary[kQuestions] as? Array<Dictionary<String,AnyObject>>{
            for _questionDict in _questionArray{
                let question = Question(dictionary: _questionDict)
                self.questions.append(question)
            }
        }
        
        super.init()
    }
}


class FAQParser: NSObject {
    let kFQAs = "faqs"
    var code:Int = 0
    var message:String = ""
    var faqs = Array<FAQModel>()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json["code"].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }

        if let _faqs = json[kFQAs].arrayObject as? [[String:AnyObject]]{
            self.faqs.removeAll()
            for _category in _faqs{
                let faqModel = FAQModel(dictionary: _category)
                self.faqs.append(faqModel)
            }
        }
        
        super.init()
    }


}

