//
//  MarinaMarker.swift
//  GPDock
//
//  Created by TecOrb on 28/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import GoogleMaps

class MarinaMarker: GMSMarker {
    var marina: Marina!
    var index:Int!
    init(marina: Marina,index: Int) {
        self.marina = marina
        self.index = index
        let coordinate = CLLocationCoordinate2D(latitude: marina.latitude, longitude: marina.longitude)
        super.init()
        self.position = coordinate
        self.icon = imageForMarinaMarker()
    }

    func  imageForMarinaMarker() -> UIImage {
        let myview = MarkerView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        myview.markerImage.image = marina.isPrivate ? #imageLiteral(resourceName: "private_location_unsel") : #imageLiteral(resourceName: "marker_dark_blue")
        myview.priceLabel.textColor = marina.isPrivate ? kApplicationRedColor : kNavigationColor
        myview.priceLabel.isGradientApplied = !marina.isPrivate
        myview.priceLabel.text = self.marina.isSigned ? "$"+String(format: "%.2f", marina.pricePerFeet) : ""
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }

    func  imageForSelectedMarinaMarker() -> UIImage {
        let myview = MarkerView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        myview.markerImage.image = #imageLiteral(resourceName: "marker_green")
        myview.priceLabel.isGradientApplied = false
        myview.priceLabel.textColor = UIColor(red: 26.0/255.0, green: 188.0/255.0, blue: 96.0/255.0, alpha: 1.0)
        myview.priceLabel.text = self.marina.isSigned ? "$"+String(format: "%.2f", marina.pricePerFeet) : ""
        myview.setNeedsLayout()
        myview.layoutIfNeeded()
        return imageWithView(view: myview,scale: UIScreen.main.scale)
    }

    func imageWithView(view : UIView,scale:CGFloat) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let myimg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return myimg
    }


}
