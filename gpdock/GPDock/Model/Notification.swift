//
//  Notification.swift
//  GPDock
//
//  Created by TecOrb on 16/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

let kSupportReplyByAdmin = "support_reply_by_admin"
class NotificationModel: NSObject {
    let kID =  "id"
    let kReceiver = "receiver"
    let kStatus = "status"
    let kBooking = "booking"
    let kTimeOffset = "received_at"
    let kMessage = "message"
    let kType = "notification_type"
    let kPendingInvoice = "pending_invoice"

    var ID =  ""
    var receiver = User()
    var status: Bool = false
    var booking = Booking()
    var timeOffset = ""
    var message = ""
    var notificationType = ""
    var pendingInvoice = PendingInvoice()

    override init() {
        super.init()
    }

    init(dictionary:[String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }

        if let _receiver = dictionary[kReceiver] as? Dictionary<String,AnyObject>{
            self.receiver = User(dictionary: _receiver)
        }

        if let _booking = dictionary[kBooking] as? Dictionary<String,AnyObject>{
            self.booking = Booking(dictionary: _booking)
        }
        if let _pendingInvoice = dictionary[kPendingInvoice] as? Dictionary<String,AnyObject>{
            self.pendingInvoice = PendingInvoice(dictionary: _pendingInvoice)
        }

        if let _message = dictionary[kMessage] as? String{
            self.message = _message
        }
        if let _timeOffset = dictionary[kTimeOffset] as? String{
            self.timeOffset = _timeOffset
        }
        if let _type = dictionary[kType] as? String{
            self.notificationType = _type
        }
        super.init()
    }

    init(json:JSON){

        if let _ID = json[kID].int
        {
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string{
            self.ID = _ID
        }

        if let _status = json[kStatus].bool{
            self.status = _status
        }

        if let _receiver = json[kReceiver].dictionaryObject as Dictionary<String,AnyObject>?{
            self.receiver = User(dictionary: _receiver)
        }

        if let _booking = json[kBooking].dictionaryObject as Dictionary<String,AnyObject>?{
            self.booking = Booking(dictionary: _booking)
        }
        if let _booking = json[kBooking].dictionaryObject as Dictionary<String,AnyObject>?{
            self.booking = Booking(dictionary: _booking)
        }
        if let _pendingInvoice = json[kPendingInvoice].dictionaryObject as Dictionary<String,AnyObject>?{
            self.pendingInvoice = PendingInvoice(dictionary: _pendingInvoice)
        }


        if let _message = json[kMessage].string{
            self.message = _message
        }

        if let _timeOffset = json[kTimeOffset].string{
            self.timeOffset = _timeOffset
        }

        if let _type = json[kType].string{
            self.notificationType = _type
        }
        super.init()
    }

    func didReadbyUser(userID:String,completionBlock: @escaping (_ didRead:Bool) -> Void){
        NotificationService.sharedInstance.setNotificationAsReadByUser(userID, self.ID) { (didRead) in
            if didRead{
                self.status = true
                completionBlock(didRead)
            }
        }
    }

}


class PendingInvoice: NSObject {
    let kUser =  "user"
    let kMarina = "marina"
    
    var user =  User()
    var marina = Marina()
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        
        if let _marina = dictionary[kMarina] as? Dictionary<String,AnyObject>{
            self.marina = Marina(dictionary: _marina)
        }
        if let _user = dictionary[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _user)
        }
         super.init()
    }
    init(json:JSON){
        if let _marina = json[kMarina].dictionaryObject as Dictionary<String,AnyObject>?{
            self.marina = Marina(dictionary: _marina)
        }
        if let _user = json[kUser].dictionaryObject as Dictionary<String,AnyObject>?{
            self.user = User(dictionary: _user)
        }

      super.init()
    }

}

class NotificationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kNotifications = "notifications"
    let kNotification = "notification"

    var responseCode:Int = 0
    var responseMessage = ""
    var notifications = [NotificationModel]()
    var notification = NotificationModel()
    var notificationDidRead = false
    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let notificationArray = json[kNotifications].arrayObject as? [[String:AnyObject]]{
            for notificationDict in notificationArray{
                let notification = NotificationModel(dictionary: notificationDict)
                self.notifications.append(notification)
            }
        }


        if let _notificationDidRead = json[kNotifications].bool as Bool?{
            self.notificationDidRead = _notificationDidRead
        }


        if let notificationDict = json[kNotification].dictionaryObject as [String:AnyObject]?{
            self.notification = NotificationModel(dictionary: notificationDict)
        }

        super.init()
    }
}
