//
//  Offer.swift
//  GPDock
//
//  Created by Parikshit on 09/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

//"coupn_code" : "GPDOCK30",
//"max_usages_count" : 500,
//"user_used_count" : 0,
//"per_user_count" : 50,
//"description" : "Discount on Every Marina on booking",
//"offer_percentage" : 30,
//"title" : "30% Discount @ Marina Booking",
//"id" : 6,
//"expires_at" : "2018-06-15T00:00:00.000Z",
//"max_used" : 0,
//"image" : null


class OfferModal: NSObject {
    let kID = "id"
    let kofferPrice = "offer_price"
    let kOfferPercentage = "offer_percentage"
    let kCoupanCode = "coupn_code"
    let kImage = "image"
    let kTitle = "title"
    let kDescription = "description"
    let kExpireAt = "expires_at"
    let kCategoryImage = "category_image"
    let kMaxUsagesCount = "max_usages_count"
    let kUserUsedCount = "user_used_count"
    let kPerUserCount = "per_user_count"
    let KMaxUsed = "max_used"
    let kmainPrice = "main_price"
    
    var ID = ""
    var offerPrice:Double = 0.0
    var offerPercentage:Double = 0.0
    var coupanCode = ""
    var image = ""
    var title = ""
    var descrption = ""
    var expireAt = ""
    var categoryImage = ""
    var isSelect = false
    var maxUsagesCount:Int = 0
    var userUsedCount :Int = 0
    var perUserCount:Int = 0
    var maxUsed :Int = 0
    var mainPrice:Double = 0.0

    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int {
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String {
            self.ID = _ID
        }
        if let _minPrice = dictionary[kofferPrice] as? Double {
            self.offerPrice = _minPrice/100.0
        }else if let _minPrice = dictionary[kofferPrice] as? String {
            let amnt  = Double(_minPrice) ?? 0.0
            self.offerPrice = amnt/100.0


        }
        if let _offerPercentage = dictionary[kOfferPercentage] as? Double {
            self.offerPercentage = _offerPercentage
        }else if let _offerPercentage = dictionary[kOfferPercentage] as? String {
            self.offerPercentage = Double(_offerPercentage) ?? 0.0
        }
        if let _coupanCode = dictionary[kCoupanCode] as? String {
            self.coupanCode = _coupanCode
        }
        if let _image  = dictionary[kImage] as? String {
            self.image = _image
        }
        if let _title = dictionary[kTitle] as? String {
            self.title = _title
        }
        if let _description = dictionary[kDescription] as? String {
            self.descrption = _description
        }
        if let _expireAt = dictionary[kExpireAt] as? String {
            self.expireAt = CommonClass.dateWithString(_expireAt)
        }
        if let _categoryImage = dictionary[kCategoryImage] as? String {
            self.categoryImage = _categoryImage
        }
        
        if let _maxUsagesCount = dictionary[kMaxUsagesCount] as? Int {
            self.maxUsagesCount = _maxUsagesCount
        }else if let _maxUsagesCount = dictionary[kMaxUsagesCount] as? String {
            self.maxUsagesCount = Int(_maxUsagesCount) ?? 0
        }
        if let _perUserCount = dictionary[kPerUserCount] as? Int {
            self.perUserCount = _perUserCount
        }else if let _perUserCount = dictionary[kPerUserCount] as? String {
            self.perUserCount = Int(_perUserCount) ?? 0
        }
        if let _userUsedCount = dictionary[kUserUsedCount] as? Int {
            self.userUsedCount = _userUsedCount
        }else if let _userUsedCount = dictionary[kUserUsedCount] as? String {
            self.userUsedCount = Int(_userUsedCount) ?? 0
        }
        
        if let _maxUsed = dictionary[KMaxUsed] as? Int {
            self.maxUsed = _maxUsed
        }else if let _maxUsed = dictionary[KMaxUsed] as? String {
            self.maxUsed = Int(_maxUsed) ?? 0
        }
        if let _mainPrice = dictionary[kmainPrice] as? Double {
            self.mainPrice = _mainPrice/100.0
        }else if let _mainPrice = dictionary[kmainPrice] as? String {
            let amnt  = Double(_mainPrice) ?? 0.0
            self.mainPrice = amnt/100.0
            
        }

      super.init()
    }
    
    init(json:JSON) {
        if let _ID = json[kID].int {
            self.ID = "\(_ID)"
        } else if let _ID = json[kID].string {
            self.ID = _ID
        }
        if let _minPrice = json[kofferPrice].double as Double?{
            self.offerPrice = _minPrice/100.0
        } else if let _minPrice = json[kofferPrice].string {
         let amnt  = Double(_minPrice) ?? 0.0
            self.offerPrice = amnt/100.0
        }
        if let _offerPercentage = json[kOfferPercentage].double as Double? {
            self.offerPercentage = _offerPercentage
        } else if let _offerPercentage = json[kOfferPercentage].string {
            self.offerPercentage = Double(_offerPercentage) ?? 0.0
        }

        if let _coupanCode = json[kCoupanCode].string {
            self.coupanCode = _coupanCode
        }
        if let _image = json[kImage].string {
            self.image = _image
        }
        if let _title = json[kTitle].string {
            self.title = _title
        }
        if let _description = json[kDescription].string {
            self.descrption = _description
        }
        if let _expireAt = json[kExpireAt].string {
            self.expireAt = CommonClass.dateWithString(_expireAt)
        }
        if let _categoryImage = json[kCategoryImage].string {
            self.categoryImage = _categoryImage
        }
        
        if let _maxUsagesCount = json[kMaxUsagesCount].int as Int? {
            self.maxUsagesCount = _maxUsagesCount
        } else if let _maxUsagesCount = json[kMaxUsagesCount].string {
            self.maxUsagesCount = Int(_maxUsagesCount) ?? 0
            
        }
        if let _perUserCount = json[kPerUserCount].int as Int? {
            self.perUserCount = _perUserCount
        } else if let _perUserCount = json[kPerUserCount].string {
            self.perUserCount = Int(_perUserCount) ?? 0
            
        }
        if let _perUserCount = json[kPerUserCount].int as Int? {
            self.perUserCount = _perUserCount
        } else if let _perUserCount = json[kPerUserCount].string {
            self.perUserCount = Int(_perUserCount) ?? 0
            
        }
        if let _userUsedCount = json[kUserUsedCount].int as Int? {
            self.userUsedCount = _userUsedCount
        } else if let _userUsedCount = json[kUserUsedCount].string {
            self.userUsedCount = Int(_userUsedCount) ?? 0
            
        }
        if let _maxUsed = json[KMaxUsed].int as Int? {
            self.maxUsed = _maxUsed
        } else if let _maxUsed = json[KMaxUsed].string {
            self.maxUsed = Int(_maxUsed) ?? 0
            
        }
        if let _mainPrice = json[kmainPrice].double as Double?{
            self.mainPrice = _mainPrice/100.0
        } else if let _mainPrice = json[kmainPrice].string {
          let amnt  = Double(_mainPrice) ?? 0.0
            self.mainPrice = amnt/100.0
        }

       
       super.init()
    }
    
}
class OfferParser: NSObject {
    let kResponseCode = "code"
    let kTotalPages = "total_pages"
    let kTotalRecord = "total_records"
    let kOffers = "offers"
    let kOffer = "offers"
    
    var responseCode:Int = 0
    var totalPages:Int = 0
    var totalRecord:Int = 0
    var offers = [OfferModal]()
    var offer = OfferModal()
    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _totalPages = json[kTotalPages].int as Int?{
            self.totalPages = _totalPages
        }
        if let _totalrecord = json[kTotalRecord].int as Int?{
            self.totalRecord = _totalrecord
        }
        
        if let offerArray = json[kOffers].arrayObject as? [[String:AnyObject]]{
            for offerDict in offerArray{
                let offer = OfferModal(dictionary: offerDict)
                self.offers.append(offer)
            }
        }
        
        
        if let offerDict = json[kOffer].dictionaryObject as [String:AnyObject]?{
            self.offer = OfferModal(dictionary: offerDict)
        }
        
        super.init()
    }
}


















