//
//  Review.swift
//  GPDock
//
//  Created by TecOrb on 08/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


class Review: NSObject {
    let kID = "id"//: 12,
    let kTitle = "title"//: "A very nice one in fact!",
    let kDescription = "description"//: "We rented a fishing pontoon for the day. A very nice one in fact! This was the highlight of our 10 day trip.",
    let kRating = "rating"//: "5",
    let kCreatedAt = "created_at"//: "2017-06-08T14:00:43.000Z",
    let kUpdatedAt = "updated_at"//: "2017-06-08T14:00:43.000Z",
    let kUser = "user"


    var ID: String = ""
    var title : String = ""
    var reviewDescription:String = ""
    var rating:Double = 0.0
    var createdAt: String = ""//: "2017-06-08T14:00:43.000Z",
    var updatedAt:String = ""//: "2017-06-08T14:00:43.000Z",
    var user = User()

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _Id = dictionary[kID] as? NSInteger{
            self.ID = "\(_Id)"
        }else if let _Id = dictionary[kID] as? String{
            self.ID = _Id
        }

        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _reviewDesc = dictionary[kDescription] as? String{
            self.reviewDescription = _reviewDesc
        }
        if let _rating = dictionary[kRating] as? Double{
            self.rating = _rating
        }else if let _rating = dictionary[kRating] as? String{
            self.rating = Double(_rating) ?? 0.0
        }

        if let _createdAt = dictionary[kCreatedAt] as? String{
            self.createdAt = CommonClass.formattedDateWithString(_createdAt, format: "MM/dd/YY")
        }
        if let _updatedAt = dictionary[kUpdatedAt] as? String{
            self.updatedAt = _updatedAt
        }
        if let _userDict = dictionary[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _userDict)
        }
        super.init()
    }

}

class Rating:NSObject{
    let kFiveStars = "fivestars"//: 4,
    let kFourStars = "fourstars"//: 4,
    let kThreeStars = "threestars"//: 0,
    let kTwoStars = "twostars"//: 0,
    let kOneStars = "onestars"//: 0

    var fiveStars: Double = 0
    var fourStars: Double = 0
    var threeStars: Double = 0
    var twoStars: Double = 0
    var oneStars: Double = 0

    override init() {
        super.init()
    }

    init(dict: Dictionary<String,AnyObject>) {
        if let fiveS = dict[kFiveStars] as? Double{
            self.fiveStars = fiveS
        }
        if let fourS = dict[kFourStars] as? Double{
            self.fourStars = fourS
        }
        if let threeS = dict[kThreeStars] as? Double{
            self.threeStars = threeS
        }
        if let twoS = dict[kTwoStars] as? Double{
            self.twoStars = twoS
        }
        if let oneS = dict[kOneStars] as? Double{
            self.oneStars = oneS
        }
        super.init()
    }
}

class ReviewParser: NSObject {
    let kCode = "code"//: 200,
    let kMessage = "message"//: "success! Marina Reviews",
    let kRating = "ratings"//: {
    let kReviews = "reviews"
    let kReview = "review"
    let kTotalReviews = "total_reviews"
    var code: Int = 0
    var message: String = ""
    var ratings = Rating()
    var reviews = [Review]()
    var review = Review()
    var totalReviews: Int = 0

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json[kCode].int as Int?{self.code = _code}
        if let _totalReviews = json[kTotalReviews].int as Int?{self.totalReviews = _totalReviews}
        if let _message = json[kMessage].string as String?{self.message = _message}
        if let _ratings = json[kRating].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ratings = Rating(dict: _ratings)
        }

        if let _reviewArray = json[kReviews].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for rv in _reviewArray{
                let rvw = Review(dictionary: rv)
                self.reviews.append(rvw)
            }
        }

        if let _review = json[kReview].dictionaryObject as Dictionary<String,AnyObject>?{
            self.review = Review(dictionary: _review)
        }

        super.init()
    }
}

class FavoriteParser: NSObject {
    let kCode = "code"//: 200,
    let kMessage = "message"//: "success! Marina Reviews",
    let kMarinaID = "marina_id"
    let kStatus = "status"
    var code: Int = 0
    var message: String = ""
    var marinaID = ""
    var status = false

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json[kCode].int as Int?{self.code = _code}
        if let _status = json[kStatus].bool as Bool?{self.status = _status}
        if let _message = json[kMessage].string as String?{self.message = _message}
        if let _Id = json[kMarinaID].int as Int?{
            self.marinaID = "\(_Id)"
        }else if let _Id = json[kMarinaID].string as String?{
            self.marinaID = _Id
        }
        super.init()
    }
}



class SupportParser: NSObject {
    let kCode = "code"//: 200,
    let kMessage = "message"//: "success! Marina Reviews",
    let kSupport = "support"

    var code: Int = 0
    var message: String = ""
    var support = Support()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json[kCode].int as Int?{self.code = _code}
        if let _message = json[kMessage].string as String?{self.message = _message}

        if let _supportDic = json[kSupport].dictionaryObject as Dictionary<String,AnyObject>?{
            self.support = Support(dictionary:_supportDic)
        }

        super.init()
    }
}

class Support: NSObject {
    let kID = "id"//: 200,
    let kReferenceNo = "reference_no"//: "success! Marina Reviews",
    let kStatus = "status"
    let kUser = "user"
    //let kCreatedAt = "created_at"

    var ID: String = ""
    var referenceNumber: String = ""
    var status:Bool = false
    var user = User()

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {

        if let _Id = dictionary[kID] as? NSInteger{
            self.ID = "\(_Id)"
        }else if let _Id = dictionary[kID] as? String{
            self.ID = _Id
        }

        if let _refNo = dictionary[kReferenceNo] as? String{
            self.referenceNumber = _refNo
        }

        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }

        if let _userDict = dictionary[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _userDict)
        }

        super.init()
    }

}


