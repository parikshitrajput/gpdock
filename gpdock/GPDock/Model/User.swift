//
//  User.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    //key
    let kID = "id"
    let kFirstName = "fname"
    let kLastName = "lname"
    let kEmail = "email"
    let kContact = "contact"
    let kProfileImage = "image"
    let kUserRole = "role"
    let kUserName = "user_name"
    let kDescription = "description"
    let kIsPhoneNumberVerified = "isPhoneNumberVerified"
    let kAddress = "address"
    let kCity = "city"
    let kState = "state"
    let kCountry = "country"
    let kPinCode = "pincode"
    let kIsContactVerified = "is_contact_verified"
    let kCountryCode = "country_code"
    let kAPIKey = "api_key"


    //properties
    var ID : String = ""
    var userName: String = ""
    var firstName : String = ""
    var lastName : String = ""
    var email : String = ""
    var contact : String = ""
    var role : String = ""
    var profileImage : String = ""
    var isPhoneNumberVarified: Bool = false
    var address = ""
    var city = ""
    var state = ""
    var country = ""
    var pinCode = ""
    var countryCode = ""
    var isContactVerified: Bool = false
    var userToken = ""

    override init() {
        super.init()
    }

    init(json : JSON){
        if let userId = json[kID].int as Int?{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string as String?{
            self.ID = userId
        }

        if let fName = json[kFirstName].string as String?{
            self.firstName = fName
        }
        if let _apiKey = json[kAPIKey].string as String?{
            self.userToken = _apiKey
        }

        if let _userName = json[kUserName].string as String?{
            self.userName = _userName
        }
        if let lName = json[kLastName].string as String?{
            self.lastName = lName
        }
        
        if let userEmail = json[kEmail].string as String?{
            self.email = userEmail
        }

        if let userContact = json[kContact].string as String?{
            self.contact = userContact
        }

        if let _userRole = json[kUserRole].string as String?{
            self.role = _userRole
        }

        if let userProfileImage = json[kProfileImage].string as String?{
            self.profileImage = userProfileImage
        }


        if let _address = json[kAddress].string as String?{
            self.address = _address
        }
        if let _city = json[kCity].string as String?{
            self.city = _city
        }

        if let _state = json[kState].string as String?{
            self.state = _state
        }
        if let _country = json[kCountry].string as String?{
            self.country = _country
        }

        if let _pinCode = json[kPinCode].int as Int?{
            self.pinCode = "\(_pinCode)"
        }else if let _pinCode = json[kPinCode].string as String?{
            self.pinCode = _pinCode
        }

        if let _isPhoneNumberVerified = json[kIsPhoneNumberVerified].bool as Bool?{
            self.isPhoneNumberVarified = _isPhoneNumberVerified
        }
        if let _countryCode = json[kCountryCode].string as String?{
            self.countryCode = _countryCode
        }
        if let _isContactVerified = json[kIsContactVerified].bool as Bool?{
            self.isContactVerified = _isContactVerified
        }


        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let userId = dictionary[kID] as? NSInteger{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let fName = dictionary[kFirstName] as? String{
            self.firstName = fName
        }
        if let _apiKey = dictionary[kAPIKey] as? String{
            self.userToken = _apiKey
        }
        if let _userName = dictionary[kUserName] as? String{
            self.userName = _userName
        }
        if let lName = dictionary[kLastName] as? String{
            self.lastName = lName
        }
        if let userEmail = dictionary[kEmail] as? String{
            self.email = userEmail
        }

        if let userContact = dictionary[kContact] as? String{
            self.contact = userContact
        }

        if let _userRole = dictionary[kUserRole] as? String{
            self.role = _userRole
        }

        if let userProfileImage = dictionary[kProfileImage] as? String{
            self.profileImage = userProfileImage
        }

        if let _address = dictionary[kAddress] as? String{
            self.address = _address
        }
        if let _city = dictionary[kCity] as? String{
            self.city = _city
        }

        if let _state = dictionary[kState] as? String{
            self.state = _state
        }
        if let _country = dictionary[kCountry] as? String{
            self.country = _country
        }

        if let _pinCode = dictionary[kPinCode] as? Int{
            self.pinCode = "\(_pinCode)"
        }else if let _pinCode = dictionary[kPinCode] as? String{
            self.pinCode = _pinCode
        }

        if let _isPhoneNumberVerified = dictionary[kIsPhoneNumberVerified] as? Bool{
            self.isPhoneNumberVarified = _isPhoneNumberVerified
        }
        if let _isContactVerified = dictionary[kIsContactVerified] as? Bool{
            self.isContactVerified = _isContactVerified
        }
        if let _countryCode = dictionary[kCountryCode] as? String{
            self.countryCode = _countryCode
        }
        super.init()
    }



    func saveUserInfo(_ user:User) {
        let documentPath = NSHomeDirectory() + "/Documents/"
        var userInfo = [String:AnyObject]()
        userInfo.updateValue(user.ID as AnyObject, forKey:kID)
        userInfo.updateValue(user.firstName as AnyObject, forKey:kFirstName)
        userInfo.updateValue(user.lastName as AnyObject, forKey:kLastName)
        userInfo.updateValue(user.email as AnyObject, forKey:kEmail)
        userInfo.updateValue(user.contact as AnyObject, forKey:kContact)
        userInfo.updateValue(user.profileImage as AnyObject, forKey:kProfileImage)
        userInfo.updateValue(user.userName as AnyObject, forKey: kUserName)
        userInfo.updateValue(user.role as AnyObject, forKey: kUserRole)
        userInfo.updateValue(user.userToken as AnyObject, forKey: kAPIKey)

        userInfo.updateValue(user.address as AnyObject, forKey: kAddress)
        userInfo.updateValue(user.city as AnyObject, forKey: kCity)
        userInfo.updateValue(user.country as AnyObject, forKey: kCountry)
        userInfo.updateValue(user.pinCode as AnyObject, forKey: kPinCode)
        userInfo.updateValue(user.state as AnyObject, forKey: kState)
        userInfo.updateValue(user.countryCode as AnyObject, forKey: kCountryCode)
        userInfo.updateValue(user.isContactVerified as AnyObject, forKey: kIsContactVerified)
        userInfo.updateValue(user.userToken as AnyObject, forKey: kAPIKey)

        //let map = ["employee":userInfo]
        do {
            let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
            let path = documentPath + "user"
            try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            print_debug(path)
        }catch{
            print_debug("error in saving userinfo")
        }
        UserDefaults.standard.synchronize()
    }



    class func loadUserInfo()->JSON? {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "user"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = try JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting userinfo")
        }
        return json
    }

    class func logOut(_ completionBlock:@escaping (_ success:Bool,_ user:User?,_ message:String) -> Void){
        let user = User(json:User.loadUserInfo()!)

        LoginService.sharedInstance.logOut(user.ID) { (success, user, message) in
            completionBlock(success, user, message)
        }
    }

    
    func ratedOnAppStore() {
        Settings.sharedInstance.shouldShowRatingPopUp = false
        let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
        Settings.sharedInstance.previousRatedVersion = currentVersion
        LoginService.sharedInstance.setAppStoreRating(self.ID) { (badges, didRated) in
            //do nothing
        }
    }
    
    func clearAllNotifications(completionBlock:@escaping (_ done:Bool) -> Void){
        NotificationService.sharedInstance.clearAllNotifications(self.ID, completionBlock: completionBlock)
    }
    


    func isUserHaveBooking(completionBlock:@escaping(_ apiResult:Bool,_ result:Bool,_ message: String)->Void){
        BookingService.sharedInstance.isBookingsFoundForUser(self.ID) { (apiResult,result,message) in
            completionBlock(apiResult,result,message)
        }
    }
    
    func isUserHaveFavorites(completionBlock:@escaping(_ apiResult:Bool,_ result:Bool,_ message: String)->Void){
        BookingService.sharedInstance.isFavoritesFoundForUser(self.ID) { (apiResult,result,message) in
            completionBlock(apiResult,result,message)
        }
    }
    
    func payAtMarina(marinaID:String,cardID:String,sourceToken:String,amounts:[Double],paymentNote:String,coupanCode: String,completionBlock:@escaping (_ success:Bool,_ payment:Payment?,_ message:String) -> Void){
        CommonClass.showLoader(withStatus: "Connecting..")
        PaymentService.sharedInstance.payForMarinaWith(self.ID, marinaID: marinaID, amounts: amounts, sourceToken: sourceToken, cardID: cardID, message: paymentNote, coupanCode: coupanCode) { (success, payment, message) in
            CommonClass.hideLoader()
            completionBlock(success, payment, message)
        }
    }
}

class UserParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kUsers = "users"
    let kUser = "user"

    var code = 0
    var message = ""
    var users = [User]()
    var user = User()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let userDict = json[kUser].dictionaryObject as [String:AnyObject]?{
            self.user = User(dictionary: userDict)
        }

        if let usrs  = json[kUsers].arrayObject as? [[String: AnyObject]]
        {
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                users.append(usr)
            }
        }
    }
    
}

class OTPValidationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"

    var code = 0
    var message = ""
    var isVarified = false
    var token = ""
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let resultDic = json["result"].dictionaryObject as [String:AnyObject]?{
            if let _varified = resultDic["is_verified"] as? Bool{
                self.isVarified = _varified
            }
            if let _token = resultDic["reset_password_token"] as? String{
                self.token = _token
            }

        }


    }

}

/*
{
    "code": 200,
    "message": "success! User badges count",
    "badge_counts": {
        "notifications": 4,
        "show_rate_popup": false
    }
}

 "ratings": {
 "rated_at_app_store": true,
 "rated_at_play_store": false
 }
*/

class BadgeCountParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"

    var code = 0
    var message = ""
    var shouldShowRatingPopup = false
    var didRatedOnAppStore = false
    var badgesCount: Int = 0
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let resultDic = json["badge_counts"].dictionaryObject as [String:AnyObject]?{
            if let _shouldRatingPopUp = resultDic["show_rate_popup"] as? Bool{
                self.shouldShowRatingPopup = _shouldRatingPopUp
                self.didRatedOnAppStore = !_shouldRatingPopUp
            }

            if let _notifications = resultDic["notifications"] as? Int{
                self.badgesCount = _notifications
            }
        }

        if let resultOfRatedOnAppStore = json["ratings"].dictionaryObject as [String:AnyObject]?{
            if let _didRatedOnAppStore = resultOfRatedOnAppStore["rated_at_app_store"] as? Bool{
                self.didRatedOnAppStore = _didRatedOnAppStore
                self.shouldShowRatingPopup = !_didRatedOnAppStore
            }
        }


    }

}



