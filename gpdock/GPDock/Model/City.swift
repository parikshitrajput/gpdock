//
//  City.swift
//  GPDock
//
//  Created by TecOrb on 20/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class City: NSObject {

    let kID  = "id"
    let kName  = "name"
    let kImage  = "image"
    let kState = "state"

    let kLatitude = "latitude"
    let kDistance = "distance"
    let kThumbImage = "thumb_image"
    let kTotalMarinas = "total_marinas"
    let kMarinaMinPrice = "marina_min_price"
    let kLongitude = "longitude"
    let kMarinaMaxPrice = "marina_max_price"
    let kCountry = "country"


    var ID  = ""
    var name  = ""
    var image  = ""
    var state  = ""

    var distance = ""
    var thumbImage = ""
    var country = ""
    var totalMarinas = 0
    var latitude = 0.0
    var longitude = 0.0
    var marinaMinPrice = 0.0
    var marinaMaxPrice = 10.0


    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }

        if let _name = json[kName].string as String?{
            self.name = _name
        }

        if let _image = json[kImage].string as String?{
            self.image = _image
        }
        if let _state = json[kState].string as String?{
            self.state = _state
        }


        if let _distance = json[kDistance].string as String?{
            self.distance = _distance
        }
        if let _thumbImage = json[kThumbImage].string as String?{
            self.thumbImage = _thumbImage
        }
        if let _country = json[kCountry].string as String?{
            self.country = _country
        }

        if let _totalMarinas = json[kTotalMarinas].int as Int?{
            self.totalMarinas = _totalMarinas
        }else if let _totalMarinas = json[kTotalMarinas].string as String?{
            self.totalMarinas = Int(_totalMarinas) ?? 0
        }
        if let _Lat = json[kLatitude].double as Double?{
            self.latitude = _Lat
        }else if let _Lat = json[kLatitude].string as String?{
            self.latitude = Double(_Lat) ?? 0.0
        }

        if let _Longi = json[kLongitude].double as Double?{
            self.longitude = _Longi
        }else if let _Longi = json[kLongitude].string as String?{
            self.longitude = Double(_Longi) ?? 0.0
        }

        if let _marinaMinPrice = json[kMarinaMinPrice].double as Double?{
            self.marinaMinPrice = _marinaMinPrice
        }else if let _marinaMinPrice = json[kMarinaMinPrice].string as String?{
            self.marinaMinPrice = Double(_marinaMinPrice) ?? 0.0
        }

        if let _marinaMaxPrice = json[kMarinaMaxPrice].double as Double?{
            self.marinaMaxPrice = _marinaMaxPrice
        }else if let _marinaMaxPrice = json[kMarinaMaxPrice].string as String?{
            self.marinaMaxPrice = Double(_marinaMaxPrice) ?? 10.0
        }

        super.init()
    }
    init(dictionary: [String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _name = dictionary[kName] as? String{
            self.name = _name
        }

        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }

        if let _state = dictionary[kState] as? String{
            self.state = _state
        }


        if let _distance = dictionary[kDistance] as? String{
            self.distance = _distance
        }
        if let _thumbImage = dictionary[kThumbImage] as? String{
            self.thumbImage = _thumbImage
        }
        if let _country = dictionary[kCountry] as? String{
            self.country = _country
        }

        if let _totalMarinas = dictionary[kTotalMarinas] as? Int{
            self.totalMarinas = _totalMarinas
        }else if let _totalMarinas = dictionary[kTotalMarinas] as? String{
            self.totalMarinas = Int(_totalMarinas) ?? 0
        }
        if let _Lat = dictionary[kLatitude] as? Double{
            self.latitude = _Lat
        }else if let _Lat = dictionary[kLatitude] as? String{
            self.latitude = Double(_Lat) ?? 0.0
        }

        if let _Longi = dictionary[kLongitude] as? Double{
            self.longitude = _Longi
        }else if let _Longi = dictionary[kLongitude] as? String{
            self.longitude = Double(_Longi) ?? 0.0
        }

        if let _marinaMinPrice = dictionary[kMarinaMinPrice] as? Double{
            self.marinaMinPrice = _marinaMinPrice
        }else if let _marinaMinPrice = dictionary[kMarinaMinPrice] as? String{
            self.marinaMinPrice = Double(_marinaMinPrice) ?? 0.0
        }

        if let _marinaMaxPrice = dictionary[kMarinaMaxPrice] as? Double{
            self.marinaMaxPrice = _marinaMaxPrice
        }else if let _marinaMaxPrice = dictionary[kMarinaMaxPrice] as? String{
            self.marinaMaxPrice = Double(_marinaMaxPrice) ?? 0.0
        }
        super.init()
    }
}

class CityParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kCities = "cities"

    let kTotalPages = "total_pages" // 18,
    let kCurrentPage = "currentPage"//: "1",
    //"per_page": "15"
    var responseCode:Int = 0
    var totalPages:Int = 0
    var currentPage:Int = 1

    var responseMessage = ""
    var cities = [City]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _totalPages = json[kTotalPages].int as Int?{
            self.totalPages = _totalPages
        }else if let _totalPages = json[kTotalPages].string as String?{
            self.totalPages = Int(_totalPages) ?? 0
        }
        if let _currentPage = json[kCurrentPage].int as Int?{
            self.currentPage = _currentPage
        }else if let _currentPage = json[kCurrentPage].string as String?{
            self.currentPage = Int(_currentPage) ?? 0
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let carArray = json[kCities].arrayObject as? [[String:AnyObject]]{
            for carDict in carArray{
                let city = City(dictionary: carDict)
                self.cities.append(city)
            }
        }
        super.init()
    }
}






//Marina Images model

class MarinaImage: NSObject {

    let kID  = "id"
    let kMarinaID  = "marina_id"
    let kUrl  = "url"

    var ID  = ""
    var marinaID  = ""
    var url  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _mID = json[kMarinaID].int as Int?{
            self.marinaID = "\(_mID)"
        }else if let _mID = json[kMarinaID].string as String?{
            self.marinaID = _mID
        }
        if let _url = json[kUrl].string as String?{
            self.url = _url
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        if let _mID = dictionary[kMarinaID] as? Int{
            self.marinaID = "\(_mID)"
        }else if let _mID = dictionary[kMarinaID] as? String{
            self.marinaID = _mID
        }
        if let _url = dictionary[kUrl] as? String{
            self.url = _url
        }
        super.init()
    }
    
    
    
}


//Marina Services model

class ServiceParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kServices = "services"

    var responseCode:Int = 0
    var responseMessage = ""
    var services = [Service]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let serviceArray = json[kServices].arrayObject as? [[String:AnyObject]]{
            for serviceDict in serviceArray{
                let service = Service(dictionary: serviceDict)
                self.services.append(service)
            }
        }
        super.init()
    }
}
open class Service: NSObject {

    let kID  = "id"
    let kTitle  = "title"
    let kIcon = "url"

    var ID  = ""
    var title  = ""
    var icon  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _title = json[kTitle].string as String?{
            self.title = _title
        }
        if let _icon = json[kIcon].string as String?{
            self.icon = _icon
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _icon = dictionary[kIcon] as? String{
            self.icon = _icon
        }
        super.init()
    }


    
}



//Marina Aminities model


open class Aminity: NSObject {

    let kID  = "id"
    let kTitle  = "title"
    let kIcon = "url"

    var ID  = ""
    var title  = ""
    var icon  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _title = json[kTitle].string as String?{
            self.title = _title
        }
        if let _icon = json[kIcon].string as String?{
            self.icon = _icon
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _icon = dictionary[kIcon] as? String{
            self.icon = _icon
        }
        super.init()
    }
}

public func ==(lhs: Aminity, rhs: Aminity) -> Bool {
    return lhs.ID == rhs.ID
}

public func ==(lhs: Service, rhs: Service) -> Bool {
    return lhs.ID == rhs.ID
}
class AmenitiesParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kServices = "amenities"

    var responseCode:Int = 0
    var responseMessage = ""
    var amenities = [Aminity]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let serviceArray = json[kServices].arrayObject as? [[String:AnyObject]]{
            for serviceDict in serviceArray{
                let amenity = Aminity(dictionary: serviceDict)
                self.amenities.append(amenity)
            }
        }
        super.init()
    }
}



