//
//  SearchModel.swift
//  GPDock
//
//  Created by TecOrb on 27/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


enum SuggestionType {
    case marina,city
}

class Suggestion: NSObject {
/*
 "id": 171,
 "lowercasename": "31st street harbor",
 "name": "31st Street Harbor",
 "category": "Marina",
 "image":
 */

    let kID  = "id"
    let kName  = "name"
    let kLowerCaseName  = "lowercasename"
    let kImage  = "image"
    let kCategory = "category"

    var ID  = ""
    var name  = ""
    var lowerCaseName  = ""
    var image  = ""
    var suggestionType:SuggestionType  = SuggestionType.marina
    var category: String = "Marina"

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }

        if let _name = json[kName].string as String?{
            self.name = _name.capitalized
        }

        if let _image = json[kImage].string as String?{
            self.image = _image
        }

        if let _category = json[kCategory].string as String?{
            self.category = _category
        }

        if let _category = json[kCategory].string as String?{
            if _category.lowercased().elementsEqual("marina"){
                self.suggestionType = .marina
            }else{
                self.suggestionType = .city
            }
        }
        super.init()
    }

    init(dictionary: [String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _name = dictionary[kName] as? String{
            self.name = _name.capitalized
        }

        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }

        if let _category = dictionary[kCategory] as? String{
            self.category = _category
        }

        if let _category = dictionary[kCategory] as? String{
            if _category.lowercased().elementsEqual("marina"){
                self.suggestionType = .marina
            }else{
                self.suggestionType = .city
            }
        }
        super.init()
    }
}




class SuggestionParser: NSObject {
    let kCode = "code"
    let kMessage = "message"
    let kSuggestions = "suggestions"

    var code:Int = 0
    var message = ""
    var suggestions = [Suggestion]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kCode].int as Int?{
            self.code = _rCode
        }

        if let _rMessage = json[kMessage].string as String?{
            self.message = _rMessage
        }


        if let suggestionsArray = json[kSuggestions].arrayObject as? [[String:AnyObject]]{
            self.suggestions.removeAll()
            for suggestionDict in suggestionsArray{
                let suggestion = Suggestion(dictionary: suggestionDict)
                self.suggestions.append(suggestion)
            }
        }

        super.init()
    }
}
