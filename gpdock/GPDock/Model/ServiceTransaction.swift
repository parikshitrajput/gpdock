//
//  ServiceTransaction.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class ServiceTransaction: NSObject {

    let kID = "id"
    let kAmount = "service_amount"
    let kCreatedAt = "created_at"
    let kStripeTransactionID = "stripe_transaction_id"
    let kDescription = "description"
    let kPayForGrocery = "pay_for_grocery"
    let kPayForPumpout = "pay_for_pumpout"
    let kPayForGasoline = "pay_for_gasoline"
    let kPayForOthers = "pay_for_others"
    let kNote = "note"
    let kCustomerTransactionID = "customer_transaction_id"
    let kUserName = "user_name"
    let kMarinaTitle = "marina_title"

    var ID = ""
    var amount:Double = 0.0
    var createdAt = ""
    var stripeTransactionID = ""
    var transactionDescription = ""

    var payForGrocery:Double = 0.0
    var payForPumpout:Double = 0.0
    var payForGasoline:Double = 0.0
    var payForOthers:Double = 0.0

    var note = ""
    var customerTransactionID = ""
    var userName = ""
    var marinaTitle = ""

    override init() {
        super.init()
    }
    init(dictionary:Dictionary<String,AnyObject>) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _amount = dictionary[kAmount] as? Double{
            self.amount = _amount/100
        }else if let _amount = dictionary[kAmount] as? String{
            self.amount = (Double(_amount) ?? 0.0)/100.0
        }

        if let _createdAt = dictionary[kCreatedAt] as? String{
            self.createdAt = _createdAt
        }

        if let _stripeTransactionID = dictionary[kStripeTransactionID] as? String{
            self.stripeTransactionID = _stripeTransactionID
        }
        if let _desc = dictionary[kDescription] as? String{
            self.transactionDescription = _desc
        }


        if let _payForGrocery = dictionary[kPayForGrocery] as? Double{
            self.payForGrocery = _payForGrocery/100
        }else if let _payForGrocery = dictionary[kPayForGrocery] as? String{
            self.payForGrocery = (Double(_payForGrocery) ?? 0.0)/100
        }

        if let _payForPumpout = dictionary[kPayForPumpout] as? Double{
            self.payForPumpout = _payForPumpout/100
        }else if let _payForPumpout = dictionary[kPayForPumpout] as? String{
            self.payForPumpout = (Double(_payForPumpout) ?? 0.0)/100
        }

        if let _payForGasoline = dictionary[kPayForGasoline] as? Double{
            self.payForGasoline = _payForGasoline/100
        }else if let _payForGasoline = dictionary[kPayForGasoline] as? String{
            self.payForGasoline = (Double(_payForGasoline) ?? 0.0)/100
        }


        if let _payForOthers = dictionary[kPayForOthers] as? Double{
            self.payForOthers = _payForOthers/100
        }else if let _payForOthers = dictionary[kPayForPumpout] as? String{
            self.payForOthers = (Double(_payForOthers) ?? 0.0)/100
        }


        if let _note = dictionary[kNote] as? String{
            self.note = _note
        }

        if let _customerTransactionID = dictionary[kCustomerTransactionID] as? String{
            self.customerTransactionID = _customerTransactionID
        }

        if let _userName = dictionary[kUserName] as? String{
            self.userName = _userName
        }

        if let _marinaTitle = dictionary[kMarinaTitle] as? String{
            self.marinaTitle = _marinaTitle
        }
        
        super.init()
    }
    

}


class ServiceTransactionParser: NSObject {
    var code = 0
    var message = ""
    var transactions = Array<ServiceTransaction>()
    var transaction = ServiceTransaction()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _code = json["code"].int{
            self.code = _code
        }
        if let _message = json["message"].string{
            self.message = _message
        }

        if let _tsDict = json["payment"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.transaction = ServiceTransaction(dictionary: _tsDict)
        }

        if let _tsArray = json["payments"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for trsn in _tsArray{
                let tr = ServiceTransaction(dictionary: trsn)
                self.transactions.append(tr)
            }
        }

    }
}
