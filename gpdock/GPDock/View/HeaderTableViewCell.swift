//
//  HeaderTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 26/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var headerLabel : NKCustomLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
