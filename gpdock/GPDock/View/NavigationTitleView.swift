//
//  NavigationTitleView.swift
//  GPDock
//
//  Created by TecOrb on 29/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    @IBOutlet weak var titleLabel :NKCustomLabel!
    let gradient = CAGradientLayer()


    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }


    class func instanceFromNib() -> NavigationTitleView {
        return UINib(nibName: "NavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigationTitleView
    }

    override func layoutSubviews() {
//        // gradient colors in order which they will visually appear
//        gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
//
//        // Gradient from left to right
//        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//        // set the gradient layer to the same size as the view
//        gradient.frame = self.frame
//        // add the gradient layer to the views layer for rendering
//        self.layer.insertSublayer(gradient, at: 0)//(gradient)
//        self.mask = titleLabel
    }

}
