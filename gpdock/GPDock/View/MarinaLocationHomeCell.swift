//
//  MarinaLocationHomeCell.swift
//  GPDock
//
//  Created by TecOrb on 12/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaLocationHomeCell: UITableViewCell {
    @IBOutlet weak var locationNameLabel: NKCustomLabel!
    @IBOutlet weak var categoryLabel: NKCustomLabel!
    @IBOutlet weak var locationPhoto: UIImageView!
    @IBOutlet weak var containnerView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews(){
        if containnerView != nil{
            self.containnerView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 0.5)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //CommonClass.makeViewCircularWithRespectToHeight(locationNameLabel, borderColor: UIColor.clear, borderWidth: 0)
        // Configure the view for the selected state
    }

}



class NoDataCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
