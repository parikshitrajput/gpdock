//
//  NoFavoritesView.swift
//  GPDock
//
//  Created by TecOrb on 26/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NoFavoritesView: UIView {
    @IBOutlet weak var exploreMarinasButton : UIButton!

    let startColor = UIColor(red: 255.0/255.0, green: 103.0/255.0, blue: 103.0/255.0, alpha: 1.0)
    let endColor = UIColor(red: 255.0/255.0, green: 13.0/255.0, blue: 13.0/255.0, alpha: 1.0)
    class func instanceFromNib() -> NoFavoritesView {
        return UINib(nibName: "NoFavoritesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NoFavoritesView
    }

    override func layoutSubviews() {
        exploreMarinasButton.applyGradient(withColours: [startColor,endColor], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.exploreMarinasButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)
    }

}
