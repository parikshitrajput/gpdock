//
//  SwipeButtonViews.swift
//  CarDetailingApp
//
//  Created by TecOrb on 04/02/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class SwipeButtonView: UIView {
    @IBOutlet weak var buttonIcon : UIImageView!
    @IBOutlet weak var buttonNameLabel : UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


class SwipeButton:  MGSwipeButton{
    @IBOutlet weak var buttonIcon : UIImageView!
    @IBOutlet weak var buttonNameLabel : UILabel!
}



