//
//  MarinaCell.swift
//  GPDock
//
//  Created by TecOrb on 12/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaCell: UITableViewCell {
    @IBOutlet weak var marinaNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var bookingButton: UIButton!
    @IBOutlet weak var payForServiceButton: UIButton!

    @IBOutlet weak var priceLabel: UILabel!
    //@IBOutlet weak var totalVotesLabel: UILabel!

   // @IBOutlet var serviceImages: [UIImageView]!
    @IBOutlet weak var isPrivateImageView: UIImageView!

    @IBOutlet weak var marinaPhoto: UIImageView!
    var isLiked:Bool = false
    @IBOutlet weak var likeButton: SparkButton!
    @IBOutlet weak var containnerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithRespectToHeight(distanceLabel, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithCornerRadius(bookingButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: bookingButton.frame.size.height/10)
        CommonClass.makeViewCircularWithCornerRadius(self.marinaPhoto, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)

        self.bookingButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithRespectToHeight(ratingButton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircularWithRespectToHeight(priceLabel, borderColor: UIColor.clear, borderWidth: 0)
        if containnerView != nil{
            self.containnerView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 0.5)
        }
    }

//    @IBAction func likeButtonTapped(_ sender: AnyObject) {
//        isLiked = !isLiked
//        if isLiked == true {
//            likeButton.setImage(UIImage(named: "liked"), for: UIControlState())
//            likeButton.likeBounce(0.6)
//            likeButton.animate()
//        }
//        else{
//            likeButton.setImage(UIImage(named: "like"), for: UIControlState())
//            likeButton.unLikeBounce(0.4)
//        }
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
//        for imageview in serviceImages{
//            CommonClass.makeViewCircularWithCornerRadius(imageview, borderColor: UIColor.clear, borderWidth: 0.7, cornerRadius: 2)
//        }
    }

    /*
    func configureServices(services:[Service]){
        var remaining = 0
        for i in 0..<services.count{
            if i < serviceImages.count-1{
                self.serviceImages[i].setIndicatorStyle(.gray)
                self.serviceImages[i].setShowActivityIndicator(true)
                self.serviceImages[i].sd_setImage(with: URL(string:services[i].icon))
                CommonClass.makeViewCircularWithCornerRadius(self.serviceImages[i], borderColor: UIColor.darkGray, borderWidth: 1, cornerRadius: 2)
            }else{
                remaining += 1
            }
        }
    }
 */
}
