//
//  OffersHeaderView.swift
//  GPDock
//
//  Created by Parikshit on 11/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class OffersHeaderView: UIView {
    @IBOutlet weak var offerCollectionView: UICollectionView!
     @IBOutlet weak var pageControl: UIPageControl!
    
    
    
    class func instanceFromNib() -> OffersHeaderView {
        let nib = UINib(nibName: "OffersHeaderView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? OffersHeaderView
        view?.offerCollectionView.register(UINib(nibName: "OffersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OffersCollectionViewCell")
        view?.offerCollectionView.register(UINib(nibName: "NoDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoDataCollectionViewCell")
        //view?.offerCollectionView.dataSource = view
        //view?.offerCollectionView.delegate = view
        //view?.collectionView.reloadData()
        
        return view!
        //return UINib(nibName: "AllTaxiView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AllTaxiView
    }
   

}
