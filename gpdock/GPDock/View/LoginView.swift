//
//  LoginView.swift
//  GPDock
//
//  Created by TecOrb on 31/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

protocol LoginViewDelegate{
    func loginViewDelegate(didTapOnLoginButton wantLogin:Bool)
}

class LoginView: UIView {
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var signUpButton : UIButton!
    let startColor = UIColor(red: 255.0/255.0, green: 103.0/255.0, blue: 103.0/255.0, alpha: 1.0)
    let endColor = UIColor(red: 255.0/255.0, green: 13.0/255.0, blue: 13.0/255.0, alpha: 1.0)

    var delegate : LoginViewDelegate?
    
    class func instanceFromNib() -> LoginView {
        return UINib(nibName: "LoginView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoginView
    }

    override func layoutSubviews() {
        loginButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.loginButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)
        signUpButton.applyGradient(withColours: [startColor,endColor], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.signUpButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)
    }
    
    @IBAction func onClickLoginButton(_ sender:UIButton){
        self.delegate?.loginViewDelegate(didTapOnLoginButton: true)
    }

    @IBAction func onClickSignUpButton(_ sender:UIButton){
        self.delegate?.loginViewDelegate(didTapOnLoginButton: false)
    }

}




