//
//  TextFieldCell.swift
//  TaxiApp
//
//  Created by TecOrb on 02/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView

class TextFieldCell: UITableViewCell {
    @IBOutlet weak var inputTextField : UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.inputTextField, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.inputTextField.frame.size.height/2)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class TextViewCell: UITableViewCell {
    @IBOutlet weak var inputTextView : RSKPlaceholderTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


class RegisterButtonCell: UITableViewCell {
    @IBOutlet weak var registerButton : UIButton!
    @IBOutlet weak var supportButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.registerButton, borderColor: UIColor.white, borderWidth: 1, cornerRadius: self.registerButton.frame.size.height/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
class ReferralCodeCell: UITableViewCell {
    @IBOutlet weak var gotReferralCodeButton : UIButton!
    @IBOutlet weak var helpButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        // CommonClass.makeViewCircularWithCornerRadius(self.registerButton, borderColor: UIColor.white, borderWidth: 1, cornerRadius: self.registerButton.frame.size.height/2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

