//
//  FilterMarinaNameCell.swift
//  GPDock
//
//  Created by TecOrb on 12/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class FilterMarinaNameCell: UITableViewCell {
    @IBOutlet weak var marinaNameTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonClass.sharedInstance.setLeftIconForTextField(self.marinaNameTextField, leftIcon: #imageLiteral(resourceName: "search"))
        self.marinaNameTextField.returnKeyType = .done
        if self.marinaNameTextField.canBecomeFirstResponder{
            self.marinaNameTextField.becomeFirstResponder()
        }
    }

    override func layoutSubviews() {
        CommonClass.sharedInstance.setLeftIconForTextField(self.marinaNameTextField, leftIcon: #imageLiteral(resourceName: "search"))

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
