//
//  FilterListCell.swift
//  GPDock
//
//  Created by TecOrb on 05/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class FilterListCell: UITableViewCell {
    @IBOutlet weak var tickIcon : UIImageView!
    @IBOutlet weak var filterDataLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
