//
//  PrivateMarinaCell.swift
//  GPDock
//
//  Created by TecOrb on 03/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PrivateMarinaCell: UITableViewCell {
    @IBOutlet weak var locationTypeLabel : UILabel!
    @IBOutlet weak var accessTimeLabel : UILabel!
    @IBOutlet weak var insuranceRequiredLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
