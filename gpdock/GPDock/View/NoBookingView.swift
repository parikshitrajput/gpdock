//
//  NoBookingView.swift
//  GPDock
//
//  Created by TecOrb on 18/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NoBookingView: UIView {
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var bookNowButton : UIButton!

    class func instanceFromNib() -> NoBookingView {
        return UINib(nibName: "NoBookingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NoBookingView
    }

    override func layoutSubviews() {
        bookNowButton.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.bookNowButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)
    }

}
