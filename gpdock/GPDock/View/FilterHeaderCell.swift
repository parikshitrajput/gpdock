//
//  FilterHeaderCell.swift
//  GPDock
//
//  Created by TecOrb on 05/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class FilterHeaderCell: UITableViewCell {
    @IBOutlet weak var headerIcon : UIImageView!
    @IBOutlet weak var headerTitleLabel: NKCustomLabel!
    @IBOutlet weak var selectionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
