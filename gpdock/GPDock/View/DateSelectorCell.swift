//
//  DateSelectorCell.swift
//  GPDock
//
//  Created by TecOrb on 10/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class DateSelectorCell: UITableViewCell {
    @IBOutlet weak var fromLabel : UILabel!
    @IBOutlet weak var toLabel : UILabel!
    @IBOutlet weak var button : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
