//
//  MarinaPayingCategoryCell.swift
//  GPDock
//
//  Created by TecOrb on 09/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaPayingCategoryCell: UITableViewCell {
    @IBOutlet weak var categoryIcon : UIImageView!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var amountTextField : BKCurrencyTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.amountTextField.numberFormatter.currencySymbol = "$"
        self.amountTextField.numberFormatter.currencyCode = "USD"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
