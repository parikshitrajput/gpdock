//
//  OffersCollectionViewCell.swift
//  GPDock
//
//  Created by Parikshit on 10/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import MarqueeLabel

class OffersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var promCodeDescription: UILabel!

    //@IBOutlet weak var promCodeDescription: MarqueeLabel!
    @IBOutlet weak var promoSelectButton: UIButton!
    @IBOutlet weak var promoCodeTitle : UILabel!
    @IBOutlet weak var promoImage: UIImageView!
    @IBOutlet weak var offerPercentageLabel : UILabel!
    @IBOutlet weak var offerExpireLabel : UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var usedOfferCount : UILabel!
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var timeImage: UIImageView!
    @IBOutlet weak var selectOfferImage: UIImageView!
    @IBOutlet weak var onFirstBookingLabel: UILabel!
    @IBOutlet weak var validUptoLabel: UILabel!
    @IBOutlet weak var usedCodeLabel: UILabel!
    //@IBOutlet weak var pageNoLabel: UILabel!
    @IBOutlet weak var pagecountLabel: UILabel!





    //@IBOutlet weak var backGroundButton: UIButton!


    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }
//    override func layoutSubviews() {
//
//
//        self.promoSelectButton.setImage(#imageLiteral(resourceName: "offer_unsel"), for: .normal)
//        self.promoSelectButton.setImage(#imageLiteral(resourceName: "offer_sel"), for: .selected)
//    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    class func instanceFromNib() -> OffersCollectionViewCell {
//        return UINib(nibName: "OffersCollectionViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OffersCollectionViewCell
//    }
}
