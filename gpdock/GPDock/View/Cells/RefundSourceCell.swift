//
//  RefundSourceCell.swift
//  GPDock
//
//  Created by TecOrb on 15/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class RefundSourceCell: UITableViewCell {
    @IBOutlet weak var sourceTypeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: NKCustomLabel!
  //  @IBOutlet weak var timeInRefundLabel: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
