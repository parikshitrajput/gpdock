//
//  CurrentRefundStatusCell.swift
//  GPDock
//
//  Created by TecOrb on 15/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CurrentRefundStatusCell: UITableViewCell {
    @IBOutlet weak var cancellationInitiatedButton: UIButton!
    @IBOutlet weak var cancellationSuccessButton: UIButton!

    @IBOutlet weak var refundInitiatedButton: UIButton!
    @IBOutlet weak var refundProcessedButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    func configureButtons() {
        cancellationInitiatedButton.setImage(#imageLiteral(resourceName: "refundPending"), for: .normal)
        cancellationSuccessButton.setImage(#imageLiteral(resourceName: "refundPending"), for: .normal)
        refundInitiatedButton.setImage(#imageLiteral(resourceName: "refundPending"), for: .normal)
        refundProcessedButton.setImage(#imageLiteral(resourceName: "refundPending"), for: .normal)

        cancellationInitiatedButton.setImage(#imageLiteral(resourceName: "refundDone"), for: .selected)
        cancellationSuccessButton.setImage(#imageLiteral(resourceName: "refundDone"), for: .selected)
        refundInitiatedButton.setImage(#imageLiteral(resourceName: "refundDone"), for: .selected)
        refundProcessedButton.setImage(#imageLiteral(resourceName: "refundDone"), for: .selected)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
