//
//  AminityTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 06/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class AminityTableViewCell: UITableViewCell {
    @IBOutlet weak var aminityIcon: UIImageView!
    @IBOutlet weak var aminityTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
