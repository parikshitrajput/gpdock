//
//  CustomerRatingTableViewCell.swift
//  EatWeDo
//
//  Created by TecOrb on 30/03/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
//import GTProgessBar

protocol CustomerRatingTableViewCellDelegate {
    func customerRatingTableViewCell(_ cell: CustomerRatingTableViewCell,didTapOnRating rating: Int)
}
class CustomerRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var overAllRatingView : NKFloatRatingView!
    @IBOutlet weak var overAllRatingCountLabel : UILabel!

//    @IBOutlet weak var foodRatingCount : UILabel!
//    @IBOutlet weak var serviceRatingCount : UILabel!
//    @IBOutlet weak var ambienceRatingCount : UILabel!
//    @IBOutlet weak var noiseRatingCount : UILabel!

    @IBOutlet weak var oneProgressBar : LinearProgressView!
    @IBOutlet weak var twoProgressBar : LinearProgressView!
    @IBOutlet weak var threeProgressBar : LinearProgressView!
    @IBOutlet weak var fourProgressBar : LinearProgressView!
    @IBOutlet weak var fiveProgressBar : LinearProgressView!
    var delegate : CustomerRatingTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func onClickRatingButton(_ sender:UIButton){
        if let rating = sender.tag as Int?{
            delegate?.customerRatingTableViewCell(self, didTapOnRating: rating)
        }
    }
    override func layoutSubviews() {
        oneProgressBar.setNeedsDisplay()
        twoProgressBar.setNeedsDisplay()
        threeProgressBar.setNeedsDisplay()
        fourProgressBar.setNeedsDisplay()
        fiveProgressBar.setNeedsDisplay()
    }

}
