//
//  TransactionParticularsCell.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TransactionParticularsCell: UITableViewCell {
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var trIDLabel : UILabel!
    @IBOutlet weak var trDateLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
