//
//  BookingIDRepresentingCell.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BookingIDRepresentingCell: UITableViewCell {
    @IBOutlet weak var bookingIDLabel: UILabel!
    @IBOutlet weak var statusLabel:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {

        self.statusLabel.applyGradient(withColours: [gradientViewColor.start,gradientViewColor.end], gradientOrientation: .horizontal, locations: [0.0,1.0])
        CommonClass.makeViewCircularWithCornerRadius(self.statusLabel, borderColor: .clear, borderWidth: 0, cornerRadius: 3)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
