//
//  DiscountOfferTableViewCell.swift
//  GPDock
//
//  Created by Parikshit on 16/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class DiscountOfferTableViewCell: UITableViewCell {
    @IBOutlet weak var discountOfferLabel: UILabel!
    @IBOutlet weak var discountOfferAmount: UILabel!
    @IBOutlet weak var discountImage: UIImageView!
    @IBOutlet weak var mainPriceAmount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
