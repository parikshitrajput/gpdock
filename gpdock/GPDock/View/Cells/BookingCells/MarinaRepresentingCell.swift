//
//  MarinaRepresentingCell.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaRepresentingCell: UITableViewCell {
    @IBOutlet weak var marinaTitleLabel : UILabel!
    @IBOutlet weak var marinaAddressLabel : UILabel!
    @IBOutlet weak var directionButton : UIButton!
    @IBOutlet weak var marinaNameLabelButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
