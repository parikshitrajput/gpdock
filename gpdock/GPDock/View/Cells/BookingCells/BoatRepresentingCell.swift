//
//  BoatRepresentingCell.swift
//  GPDock
//
//  Created by TecOrb on 13/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BoatRepresentingCell: UITableViewCell {
    @IBOutlet weak var boatNameLabel : UILabel!
    @IBOutlet weak var boatImageView : UIImageView!
    @IBOutlet weak var lengthLabel : UILabel!
    @IBOutlet weak var widthLabel : UILabel!
    @IBOutlet weak var depthLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
