//
//  InfoTableViewCell.swift
//  DemoBuisness
//
//  Created by Saif Chaudhary on 1/23/17.
//  Copyright © 2017 Saif Chaudhary. All rights reserved.
//

import UIKit
import GoogleMaps
import UITags
class MarinaInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var checkInLabel: UILabel!
    @IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var pricePerFeetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


class MarinaMapDetailsCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var directionsButton: UIButton!
    //@IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapViewImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        self.mapViewImageView.addshadow(top: true, left: true, bottom: true, right: true)
        //self.mapView.addshadow(top: true, left: true, bottom: true, right: true)
    }

}




class AboutMarinaTableViewCell: UITableViewCell {
    @IBOutlet weak var aboutMarinaLabel: UILabel!
   // @IBOutlet weak var cellIcon: UIImageView!

       override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class MarinaPolicyTableViewCell: UITableViewCell {
    @IBOutlet weak var policyLabel: UILabel!
    @IBOutlet weak var cellIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class TagsTableViewCell: UITableViewCell {
    @IBOutlet weak var tagsView: UITags!
    var tags = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
