//
//  BoatSizeCVCell.swift
//  GPDock
//
//  Created by TecOrb on 12/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BoatSizeCVCell: UICollectionViewCell {
    @IBOutlet weak var sizeLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

class BoatSizeRangeCell: UICollectionViewCell {
    @IBOutlet weak var boatRangeIcon : UIImageView!
    @IBOutlet weak var leftCountLabel : UILabel!
    @IBOutlet weak var removeBoatButton : UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
