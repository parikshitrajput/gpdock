//
//  ServiceCollectionViewCell.swift
//  GPDock
//
//  Created by TecOrb on 06/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var serviceImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}




class ServiceTableViewCell: UITableViewCell {
    @IBOutlet weak var serviceCV: UICollectionView!
    var services = Array<Service>()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setupCollectionView() {
        self.serviceCV.dataSource = self
        self.serviceCV.delegate = self
    }


}


extension ServiceTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
        let service = self.services[indexPath.item]
        cell.serviceImageView.setIndicatorStyle(.gray)
        cell.serviceImageView.setShowActivityIndicator(true)
        cell.serviceImageView.sd_setImage(with: URL(string:service.icon))
        CommonClass.makeViewCircularWithCornerRadius(cell.serviceImageView, borderColor: UIColor.darkGray, borderWidth: 1, cornerRadius: 2)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 36, height: 36)
    }
}

