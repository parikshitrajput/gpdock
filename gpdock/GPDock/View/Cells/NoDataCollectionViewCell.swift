//
//  NoDataCollectionViewCell.swift
//  GPDock
//
//  Created by Parikshit on 11/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class NoDataCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var noDataLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
