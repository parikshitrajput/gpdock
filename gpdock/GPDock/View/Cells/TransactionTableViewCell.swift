//
//  TransactionTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var marinaImageView : UIImageView!

    @IBOutlet weak var marinaTitle : UILabel!
    @IBOutlet weak var transactionDate : UILabel!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
