//
//  RefundAmountAndStausOnDateCell.swift
//  GPDock
//
//  Created by TecOrb on 15/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class RefundAmountAndStausOnDateCell: UITableViewCell {
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
