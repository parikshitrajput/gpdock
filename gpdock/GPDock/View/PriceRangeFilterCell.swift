//
//  PriceRangeFilterCell.swift
//  GPDock
//
//  Created by TecOrb on 12/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PriceRangeFilterCell: UITableViewCell {
    @IBOutlet weak var priceSliderView : NHRangeSliderView!
   


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

        if #available(iOS 11.0, *) {
            self.userInteractionEnabledWhileDragging = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
