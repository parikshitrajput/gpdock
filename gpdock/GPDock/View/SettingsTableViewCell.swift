//
//  SettingsTableViewCell.swift
//  TaxiApp
//
//  Created by TecOrb on 31/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var placeHolderLabel : UILabel!
    @IBOutlet weak var textField : UITextField!
    @IBOutlet weak var icon : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        placeHolderLabel.text = ""
        textField.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
