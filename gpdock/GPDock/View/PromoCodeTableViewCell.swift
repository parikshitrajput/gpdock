//
//  PromoCodeTableViewCell.swift
//  GPDock
//
//  Created by Parikshit on 09/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PromoCodeTableViewCell: UITableViewCell {
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var promCodeDescription: UILabel!
    @IBOutlet weak var promoSelectButton: UIButton!
    @IBOutlet weak var promoCodeTitle : UILabel!
    @IBOutlet weak var promoImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
