//
//  AddressPickerView.swift
//  TaxiApp
//
//  Created by TecOrb on 05/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class AddressPickUpView: UIView {
    @IBOutlet weak var pickupContainner : UIView!
    @IBOutlet weak var greenDot : UIView!

    @IBOutlet weak var pickUpBgImage : UIImageView!
    @IBOutlet weak var pickFromLabel : UILabel!
    @IBOutlet weak var pinLocationLabel : UILabel!
    @IBOutlet weak var favButton : UIButton!
    @IBOutlet weak var addressPickUpButton : UIButton!


    @IBOutlet weak var dropOffContainner : UIView!
    @IBOutlet weak var addressDropOffButton : UIButton!
    @IBOutlet weak var dropOffPinLocationLabel : UILabel!
    @IBOutlet weak var redDot : UIView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


    func animateToFront() -> Void {

    }
    func animateToBack() -> Void {
        
    }

}

class AddressDropOffView: UIView {
    @IBOutlet weak var dropOffContainner : UIView!
    @IBOutlet weak var greenDot : UIView!

    @IBOutlet weak var dropOffBgImage : UIImageView!
    @IBOutlet weak var dropToLabel : UILabel!
    @IBOutlet weak var pinLocationLabel : UILabel!
    @IBOutlet weak var favButton : UIButton!
    @IBOutlet weak var addressDropOffButton : UIButton!


    @IBOutlet weak var pickupContainner : UIView!
    @IBOutlet weak var addressPickUpButton : UIButton!
    @IBOutlet weak var pickUpPinLocationLabel : UILabel!
    @IBOutlet weak var redDot : UIView!

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    func animateToFront() -> Void {

    }
    func animateToBack() -> Void {

    }
    
}


class GettingAddressView: UIView {
    @IBOutlet weak var dotView : UIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var getAddressButton : UIButton!

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
