//
//  LocationFilterCell.swift
//  GPDock
//
//  Created by TecOrb on 14/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
enum LocationFilterType {
    case nationWide,city,state
}
class LocationFilterCell: UITableViewCell {
    let selectedColor = UIColor(red: 64.0/255.0, green: 210.0/255.0, blue: 97.0/255.0, alpha: 1.0)
    @IBOutlet weak var nationWideButton : UIButton!
    @IBOutlet weak var cityButton : UIButton!
    @IBOutlet weak var stateButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func toggleButtons(_ button: UIButton){
        button.isSelected = true
        button.backgroundColor = selectedColor
        if button == self.nationWideButton{
            self.cityButton.isSelected = false
            self.stateButton.isSelected = false
            self.cityButton.backgroundColor = UIColor.white
            self.stateButton.backgroundColor = UIColor.white
        }
        else if button == self.cityButton{
            self.nationWideButton.isSelected = false
            self.stateButton.isSelected = false
            self.nationWideButton.backgroundColor = UIColor.white
            self.stateButton.backgroundColor = UIColor.white
        }
        else{
            self.cityButton.isSelected = false
            self.nationWideButton.isSelected = false
            self.cityButton.backgroundColor = UIColor.white
            self.nationWideButton.backgroundColor = UIColor.white
        }
    }
    func setupButtons(){
        nationWideButton.setTitle("Nation Wide", for: .normal)
        nationWideButton.setTitle("Nation Wide", for: .selected)
        nationWideButton.setTitleColor(UIColor.black, for: .normal)
        nationWideButton.setTitleColor(UIColor.white, for: .selected)

        cityButton.setTitle("City", for: .normal)
        cityButton.setTitle("City", for: .selected)
        cityButton.setTitleColor(UIColor.black, for: .normal)
        cityButton.setTitleColor(UIColor.white, for: .selected)

        stateButton.setTitle("State", for: .normal)
        stateButton.setTitle("State", for: .selected)
        stateButton.setTitleColor(UIColor.black, for: .normal)
        stateButton.setTitleColor(UIColor.white, for: .selected)

    }
}
