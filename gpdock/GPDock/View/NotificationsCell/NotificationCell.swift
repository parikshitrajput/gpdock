//
//  BookingNotificationCell.swift
//  GPDock
//
//  Created by TecOrb on 13/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var timeOffSetLabel: NKCustomLabel!
    @IBOutlet weak var containnerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        if containnerView != nil{
            self.containnerView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 0.5)
        }
    }
}



class ReviewCancellationPolicyCell: UITableViewCell {
    @IBOutlet weak var policyTextLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}




