//
//  MarinaTypeFilterCell.swift
//  GPDock
//
//  Created by TecOrb on 31/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaTypeFilterCell: UITableViewCell {
    @IBOutlet weak var privateButton : UIButton!
    @IBOutlet weak var publicButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        self.publicButton.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        self.privateButton.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        self.publicButton.setImage(#imageLiteral(resourceName: "check_sel"), for: .selected)
        self.privateButton.setImage(#imageLiteral(resourceName: "check_sel"), for: .selected)
        self.publicButton.setTitle(" Public", for: .normal)
        self.publicButton.setTitle(" Public", for: .selected)
        self.privateButton.setTitle(" Private", for: .normal)
        self.privateButton.setTitle(" Private", for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
