//
//  GTProgressBar.swift
//  Pods
//
//  Created by Grzegorz Tatarzyn on 19/09/2016.

import UIKit

@IBDesignable
open class GTProgressBar: UIView {
    fileprivate let minimumProgressBarWidth: CGFloat = 20
    fileprivate let minimumProgressBarFillHeight: CGFloat = 1
    fileprivate let backgroundView = UIView()
    fileprivate let fillView = UIView()
    fileprivate let progressLabel = UILabel()
    fileprivate var _progress: CGFloat = 1
    
    @objc open var font: UIFont = UIFont.systemFont(ofSize: 12) {
        didSet {
            progressLabel.font = font
            self.setNeedsLayout()
        }
    }
    
    @objc open var progressLabelInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var progressLabelInsetLeft: CGFloat = 0.0 {
        didSet {
            self.progressLabelInsets.left = max(0.0, progressLabelInsetLeft)
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var progressLabelInsetRight: CGFloat = 0.0 {
        didSet {
            self.progressLabelInsets.right = max(0.0, progressLabelInsetRight)
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var progressLabelInsetTop: CGFloat = 0.0 {
        didSet {
            self.progressLabelInsets.top = max(0.0, progressLabelInsetTop)
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var progressLabelInsetBottom: CGFloat = 0.0 {
        didSet {
            self.progressLabelInsets.bottom = max(0.0, progressLabelInsetBottom)
            self.setNeedsLayout()
        }
    }
    
    open var barMaxHeight: CGFloat? {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barMaxHeightInt: Int = 0 {
        didSet {
            self.barMaxHeight = barMaxHeightInt == 0 ? nil : CGFloat(barMaxHeightInt)
        }
    }
    
    open var barMaxWidth: CGFloat? {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barMaxWidthInt: Int = 0 {
        didSet {
            self.barMaxWidth = barMaxWidthInt == 0 ? nil : CGFloat(barMaxWidthInt)
        }
    }
    
    @IBInspectable
    open var barBorderColor: UIColor = UIColor.black {
        didSet {
            backgroundView.layer.borderColor = barBorderColor.cgColor
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barBackgroundColor: UIColor = UIColor.white {
        didSet {
            backgroundView.backgroundColor = barBackgroundColor
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barFillColor: UIColor = UIColor.black {
        didSet {
            fillView.backgroundColor = barFillColor
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barBorderWidth: CGFloat = 2 {
        didSet {
            backgroundView.layer.borderWidth = barBorderWidth
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var barFillInset: CGFloat = 2 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var progress: CGFloat {
        get {
            return self._progress
        }
        
        set {
            self._progress = min(max(newValue,0), 1)
            
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var labelTextColor: UIColor = UIColor.black {
        didSet {
            progressLabel.textColor = labelTextColor
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var displayLabel: Bool = true {
        didSet {
            self.progressLabel.isHidden = !self.displayLabel
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.masksToBounds = cornerRadius != 0.0
            self.layer.cornerRadius = cornerRadius
            self.setNeedsLayout()
        }
    }
    
    open var labelPosition: GTProgressBarLabelPosition = GTProgressBarLabelPosition.left {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var labelPositionInt: Int = 0 {
        didSet {
            let enumPosition = GTProgressBarLabelPosition(rawValue: labelPositionInt)
            
            if let position = enumPosition {
                self.labelPosition = position
            }
        }
    }
    
    open var orientation: GTProgressBarOrientation = GTProgressBarOrientation.horizontal {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    open var orientationInt: Int = 0 {
        didSet {
            let enumOrientation = GTProgressBarOrientation(rawValue: orientationInt)
            
            if let orientation = enumOrientation {
                self.orientation = orientation
            }
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        prepareSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareSubviews()
    }
    
    fileprivate func prepareSubviews() {
        progressLabel.textAlignment = NSTextAlignment.center
        progressLabel.font = font
        progressLabel.textColor = labelTextColor
        addSubview(progressLabel)

        backgroundView.backgroundColor = barBackgroundColor
        backgroundView.layer.borderWidth = barBorderWidth
        backgroundView.layer.borderColor = barBorderColor.cgColor
        addSubview(backgroundView)
        
        fillView.backgroundColor = barFillColor
        backgroundView.addSubview(fillView)
    }
    
    open override func layoutSubviews() {
        updateProgressLabelText()
        updateViewsLocation()
    }
    
    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        return createFrameCalculator().sizeThatFits(size)
    }
    
    fileprivate func updateViewsLocation() {
        let frameCalculator: FrameCalculator = createFrameCalculator()
        
        progressLabel.frame = frameCalculator.labelFrame()
        frameCalculator.centerLabel(progressLabel)

        backgroundView.frame = frameCalculator.backgroundViewFrame()
        backgroundView.layer.cornerRadius = cornerRadiusFor(backgroundView)
        frameCalculator.centerBar(backgroundView)

        fillView.frame = createFrameCalculator().fillViewFrameFor(_progress)
        fillView.layer.cornerRadius = cornerRadiusFor(fillView)
    }
    
    fileprivate func createFrameCalculator() -> FrameCalculator {
        switch labelPosition {
        case .right:
            return LabelRightFrameCalculator(progressBar: self)
        case .top:
            return LabelTopFrameCalculator(progressBar: self)
        case .bottom:
            return LabelBottomFrameCalculator(progressBar: self)
        default:
            return LabelLeftFrameCalculator(progressBar: self)
        }
    }
    
    fileprivate func updateProgressLabelText() {
        progressLabel.text = "\(Int(_progress * 100))%"
    }
    
    @objc open func animateTo(_ progress: CGFloat) {
        let newProgress = min(max(progress,0), 1)
        let fillViewFrame = createFrameCalculator().fillViewFrameFor(newProgress)
        let frameChange: () -> () = {
            self.fillView.frame = fillViewFrame
            self._progress = newProgress
            self.updateProgressLabelText()
        }
        
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator(duration: 0.8, curve: .easeInOut, animations: frameChange)
                .startAnimation()
        } else {
            UIView.animate(withDuration: 0.8,
                delay: 0,
                options: [UIViewAnimationOptions.curveEaseInOut],
                animations: frameChange,
                completion: nil);
        }
    }
    
    fileprivate func cornerRadiusFor(_ view: UIView) -> CGFloat {
        if cornerRadius != 0.0 {
            return cornerRadius
        }
        
        switch orientation {
        case .horizontal:
            return view.frame.height / 2 * 0.7
        case .vertical:
            return view.frame.width / 2 * 0.7
        }
    }
}
